package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.CountriesDataResponse
import com.piesagiving.com.R

class CountriesAdapter(context: Context, list: ArrayList<CountriesDataResponse>) :
    RecyclerView.Adapter<CountriesAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<CountriesDataResponse>? = null
    private var row_index = -1

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_coutries, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItemAt(position: Int):CountriesDataResponse{
        return mList!![position]
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_header_name.text = mList!![position].name
        holder.tv_header_name.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
        }
        if (row_index == position) {
            holder.ll_header.background =
                ContextCompat.getDrawable(mContext!!, R.color.bottom_nav_item)
        } else {
            holder.ll_header.background = ContextCompat.getDrawable(mContext!!, R.color.white)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_header_name = view.findViewById(R.id.tv_header_name) as TextView
        var ll_header = view.findViewById(R.id.ll_header) as LinearLayout
    }

}
    
