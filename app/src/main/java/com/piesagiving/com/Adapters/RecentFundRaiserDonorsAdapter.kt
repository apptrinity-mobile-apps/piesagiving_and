package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.ChurchProjectArrayResponse
import com.piesagiving.com.ApiInterface.DonationsDataResponse
import com.piesagiving.com.ApiInterface.FundRaisersArrayResponse
import com.piesagiving.com.R
import com.piesagiving.com.Utils.StringManipulations
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class RecentFundRaiserDonorsAdapter(
    context: Context,
    val array: ArrayList<DonationsDataResponse>
) :
    RecyclerView.Adapter<RecentFundRaiserDonorsAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_item_fundraiser_recent_donor, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_fundraiser_name.text = StringManipulations().capitalizeFirstLetterOfEachWord(array[position].firstName!! + " " + array[position].lastName!!)
        holder.tv_fundraiser_amount.text = "$${Formatter().format("%,.2f", array[position].donationAmount!!)}"
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_fundraiser_name = view.findViewById<TextView>(R.id.tv_fundraiser_name)
        val tv_fundraiser_amount = view.findViewById<TextView>(R.id.tv_fundraiser_amount)
    }

}
    
