package com.piesagiving.com.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.UserNotificationsData
import com.piesagiving.com.ApiInterface.NotificationsData
import com.piesagiving.com.R

class AllNotificationsAdapter(
    context: Context,
    list: ArrayList<UserNotificationsData>
) :
    RecyclerView.Adapter<AllNotificationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<UserNotificationsData>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.section_custom_row_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.section_label.text = mList!![position].date
        val dataList = ArrayList<NotificationsData>()
        dataList.addAll(mList!![position].donation!!)
        dataList.addAll(mList!![position].follow_request!!)
        dataList.addAll(mList!![position].organisation!!)

        //recycler view for inner items
        holder.item_recycler_view.layoutManager = /*NestedRecyclerLinearLayoutManager(mContext)*/
            LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        holder.item_recycler_view.setHasFixedSize(true)
        holder.item_recycler_view.isNestedScrollingEnabled = false
        val adapter = AllNotificationsSubAdapter(mContext!!, dataList)
        holder.item_recycler_view.adapter = adapter
       // adapter.notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var section_label = view.findViewById(R.id.section_label) as TextView
        var item_recycler_view = view.findViewById(R.id.item_recycler_view) as RecyclerView
    }

}
    
