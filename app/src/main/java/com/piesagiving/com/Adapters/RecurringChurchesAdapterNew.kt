package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.RecurringDonationDetailedViewActivity
import com.piesagiving.com.ApiInterface.RecurringDonationsListResponse
import com.piesagiving.com.R

@SuppressLint("SetTextI18n")
class RecurringChurchesAdapterNew(
    context: Context,
    list: ArrayList<RecurringDonationsListResponse>,
    val screen_type: String
) :
    RecyclerView.Adapter<RecurringChurchesAdapterNew.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<RecurringDonationsListResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recur_donation_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_recurring_amount.text = String.format("%.2f", mList!![position].amount!!.toDouble())
        holder.tv_recurring_freq.text =
            mList!![position].reccurringType + " " + mContext!!.getString(R.string.recurring)
        holder.tv_org_name.text =
            mList!![position].name
        holder.tv_bottom_title_location.text =
            " (" + mList!![position].orgStreetAddress + "," + mList!![position].orgStateId + ")"
        holder.cv_delete.setOnClickListener {
            Toast.makeText(mContext!!,"test",Toast.LENGTH_SHORT).show()
        }
        holder.rl_main.setOnClickListener {
            val intent = Intent(mContext!!, RecurringDonationDetailedViewActivity::class.java)
            if (mList!![position].reccurringType == null) {
                intent.putExtra("reccurringType", "")
            } else {
                intent.putExtra("reccurringType", mList!![position].reccurringType)
            }
            if (mList!![position].cardHolderName == null) {
                intent.putExtra("cardHolderName", "")
            } else {
                intent.putExtra("cardHolderName", mList!![position].cardHolderName)
            }
            if (mList!![position].name == null) {
                intent.putExtra("orgName", "")
            } else {
                intent.putExtra("orgName", mList!![position].name)
            }
            if (mList!![position].orgStreetAddress == null) {
                intent.putExtra("orgStreetAddress", "")
            } else {
                intent.putExtra("orgStreetAddress", mList!![position].orgStreetAddress)
            }
            if (mList!![position].orgStateId == null) {
                intent.putExtra("orgStateId", "")
            } else {
                intent.putExtra("orgStateId", mList!![position].orgStateId)
            }
            if (mList!![position].cardLast4Digits == null) {
                intent.putExtra("cardLast4Digits", "")
            } else {
                intent.putExtra("cardLast4Digits", mList!![position].cardLast4Digits.toString())
            }
            if (mList!![position].amount == null) {
                intent.putExtra("amount", "")
            } else {
                intent.putExtra("amount", mList!![position].amount!!.toString())
            }
            if (mList!![position].id == null) {
                intent.putExtra("donationId", "")
            } else {
                intent.putExtra("donationId", mList!![position].id)
            }
            if (mList!![position].startDate == null) {
                intent.putExtra("start_date", "")
            } else {
                intent.putExtra("start_date", mList!![position].startDate)
            }
            if (mList!![position].endDate == null) {
                intent.putExtra("end_date", "")
            } else {
                intent.putExtra("end_date", mList!![position].endDate)
            }
            intent.putExtra("screen_type", screen_type)
            mContext!!.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_recurring_amount = view.findViewById(R.id.tv_recurring_amount) as TextView
        var tv_recurring_freq = view.findViewById(R.id.tv_recurring_freq) as TextView
        var tv_org_name = view.findViewById(R.id.tv_org_name) as TextView
        var tv_bottom_title_location = view.findViewById(R.id.tv_bottom_title_location) as TextView
        var rl_main = view.findViewById(R.id.rl_main) as RelativeLayout
        var cv_delete = view.findViewById(R.id.cv_delete) as CardView
    }

    private fun stringFormatter(value: String): String {
        var formattedString = ""
        var numberDecimal = java.lang.String.valueOf(value)
        numberDecimal = numberDecimal.substring(numberDecimal.indexOf("."))
        if (numberDecimal == ".0" || numberDecimal == ".00") {
            formattedString = value.toDouble().toInt().toString()
        } else {
            formattedString = value
        }
        return formattedString
    }

}
    
