package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.ChurchProjectArrayResponse
import com.piesagiving.com.ApiInterface.FundRaisersArrayResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class FundRaiserAdapter(
    context: Context,
    val array: ArrayList<FundRaisersArrayResponse>
) :
    RecyclerView.Adapter<FundRaiserAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.recycle_item_church_home, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_bottom_title_id.text = array[position].name.toString()
        Picasso.with(mContext).load(array[position].image.toString()).error(R.drawable.ic_profile).into(holder.img_charity_id)
        holder.tv_bottom_title_location.visibility = View.GONE
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img_charity_id = view.findViewById<ImageView>(R.id.img_charity_id)
        val tv_bottom_title_id = view.findViewById<TextView>(R.id.tv_bottom_title_id)
        val tv_bottom_title_location = view.findViewById<TextView>(R.id.tv_bottom_title_location)
    }

}
    
