package com.piesagiving.com.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.TicketsDetailViewActivity
import com.piesagiving.com.ApiInterface.TicketsListResponse
import com.piesagiving.com.R

class TicketsListAdapter(context: Context, list: ArrayList<TicketsListResponse>) :
    RecyclerView.Adapter<TicketsListAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<TicketsListResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_ticket, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_item_ticket_no.text = mList!![position].ticketNo.toString()
        holder.tv_item_ticket_name.text = mList!![position].orgName.toString()
        holder.tv_item_ticket_subject.text = mList!![position].subject.toString()
        holder.tv_item_ticket_date.text = mList!![position].date.toString()
        if (mList!![position].date.toString().equals("church",ignoreCase = true)) {
            holder.tv_item_ticket_type.text = mContext!!.getString(R.string.church_name)
        } else if (mList!![position].date.toString().equals("charity",ignoreCase = true)) {
            holder.tv_item_ticket_type.text = mContext!!.getString(R.string.charity_name)
        }
        holder.cv_item_card.setOnClickListener {
            val intent = Intent(mContext!!, TicketsDetailViewActivity::class.java)
            intent.putExtra("ticketNo",  mList!![position].ticketNo.toString())
            intent.putExtra("orgId", mList!![position].orgId.toString())
            intent.putExtra("orgName", mList!![position].orgName.toString())
            intent.putExtra("subject", mList!![position].subject.toString())
            intent.putExtra("date", mList!![position].date.toString())
            intent.putExtra("orgType", mList!![position].orgType.toString())
            intent.putExtra("ticketStatus", mList!![position].ticketStatus.toString())
            intent.putExtra("ticketId", mList!![position].ticketId.toString())
            mContext!!.startActivity(intent)
        }

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_ticket_no = view.findViewById(R.id.tv_item_ticket_no) as TextView
        var tv_item_ticket_name = view.findViewById(R.id.tv_item_ticket_name) as TextView
        var tv_item_ticket_subject = view.findViewById(R.id.tv_item_ticket_subject) as TextView
        var tv_item_ticket_date = view.findViewById(R.id.tv_item_ticket_date) as TextView
        var tv_item_ticket_type = view.findViewById(R.id.tv_item_ticket_type) as TextView
        var cv_item_card = view.findViewById(R.id.cv_item_card) as CardView
    }

}
    
