package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.ApiInterface.OrganisationFollowersArrayResponse
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

@SuppressLint("SetTextI18n")
class OrganisationFollowAdapter(
    context: Context,
    val ourRecommendationsArray: ArrayList<OrganisationFollowersArrayResponse>
) :
    RecyclerView.Adapter<OrganisationFollowAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var sessionManager: SessionManager? = null
    private var userId = ""
    private var userDetails: HashMap<String, String>? = null

    init {
        this.mContext = context
        this.sessionManager = SessionManager(mContext!!)
        userDetails = sessionManager!!.getUserDetails()
        userId = userDetails!![SessionManager.ID_KEY]!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_org_followers, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return ourRecommendationsArray.size
    }

    //manibabu
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_org_follower_name.text =
            ourRecommendationsArray[position].firstName.toString() + " " + ourRecommendationsArray[position].lastName.toString()
        if (ourRecommendationsArray[position].profilePic.toString()
                .equals(null) || ourRecommendationsArray[position].profilePic.equals("null") || ourRecommendationsArray[position].profilePic.equals(
                ""
            )
        ) {
            Picasso.with(mContext).load(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.iv_org_follower)
        } else {

            Picasso.with(mContext).load(ourRecommendationsArray[position].profilePic.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_org_follower)
        }


        Log.e("profile_pics", ourRecommendationsArray[position].profilePic.toString())
        holder.ll_header.setOnClickListener {
            if (ourRecommendationsArray[position].id.toString() != userId) {
                val intent = Intent(mContext!!, YourCircleProfileActivity::class.java)
                intent.putExtra("privacy", ourRecommendationsArray[position].privacy.toString())
                intent.putExtra("firstName", ourRecommendationsArray[position].firstName.toString())
                intent.putExtra("lastName", ourRecommendationsArray[position].lastName.toString())
                intent.putExtra(
                    "profilePic",
                    ourRecommendationsArray[position].profilePic.toString()
                )
                intent.putExtra("id", ourRecommendationsArray[position].id.toString())
                intent.putExtra("userName", ourRecommendationsArray[position].userName.toString())
                intent.putExtra("notificationId", "")
                intent.putExtra("from_screen", "search")
                mContext!!.startActivity(intent)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv_org_follower = view.findViewById<ImageView>(R.id.iv_org_follower)
        val tv_org_follower_name = view.findViewById<TextView>(R.id.tv_org_follower_name)
        val ll_header = view.findViewById<LinearLayout>(R.id.ll_header)
    }

}
    
