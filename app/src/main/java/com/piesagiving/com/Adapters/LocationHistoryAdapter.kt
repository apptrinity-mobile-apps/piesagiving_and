package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.HistoryResultsResponse
import com.piesagiving.com.R
import kotlin.collections.ArrayList

class LocationHistoryAdapter(
    context: Context,
    val locationHistoryArray: ArrayList<HistoryResultsResponse>
) :
    RecyclerView.Adapter<LocationHistoryAdapter.ViewHolder>() {

    private var mContext: Context? = null
    var count=1
    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.location_history_item, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return locationHistoryArray.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_type_id.text = locationHistoryArray[position].type.toString()

        if(locationHistoryArray[position].location.toString().equals("")){
            holder.tv_location_id.text = "UnKnown"
        }else{
            holder.tv_location_id.text = locationHistoryArray[position].location.toString()
        }

        /*val inputSDF = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val outputSDF = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        val sDate = inputSDF.parse(locationHistoryArray[position].date.toString())*/
        holder.tv_date_id.text = /*outputSDF.format(sDate!!)*/locationHistoryArray[position].date.toString()
        holder.tv_time_id.text = locationHistoryArray[position].time.toString()

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tv_type_id = view.findViewById<TextView>(R.id.tv_type_id)
        val tv_location_id = view.findViewById<TextView>(R.id.tv_location_id)
        val tv_date_id = view.findViewById<TextView>(R.id.tv_date_id)
        val tv_time_id = view.findViewById<TextView>(R.id.tv_time_id)


    }

}
    
