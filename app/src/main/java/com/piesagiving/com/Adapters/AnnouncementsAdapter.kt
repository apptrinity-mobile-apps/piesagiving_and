package com.piesagiving.com.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.AnnouncementsResponse
import com.piesagiving.com.R

class AnnouncementsAdapter(context: Context, list: ArrayList<AnnouncementsResponse>) :
    RecyclerView.Adapter<AnnouncementsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<AnnouncementsResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_announcement, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.tv_item_date.text = mList!![position].date.toString()
        holder.tv_item_description.text = mList!![position].message.toString()
        if (position % 2 == 0) {
            holder.cv_item_card.setCardBackgroundColor(
                ContextCompat.getColor(mContext!!, R.color.bottom_nav_item)
            )
        } else {
            holder.cv_item_card.setCardBackgroundColor(
                ContextCompat.getColor(mContext!!, R.color.search_background)
            )
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_date = view.findViewById(R.id.tv_item_date) as TextView
        var tv_item_description = view.findViewById(R.id.tv_item_description) as TextView
        var cv_item_card = view.findViewById(R.id.cv_item_card) as CardView
    }

}
    
