package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.SpecialCauseUpdatesArrayResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class ChurchUpdatesAdapter(
    context: Context,
    val projectUpdatesArrayList: ArrayList<SpecialCauseUpdatesArrayResponse>
) :
    RecyclerView.Adapter<ChurchUpdatesAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_updates, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return projectUpdatesArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        if (projectUpdatesArrayList[position].updatePicture.toString() == "") {
            Picasso.with(mContext)
                .load(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        } else {
            Picasso.with(mContext)
                .load(projectUpdatesArrayList[position].updatePicture.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        }
        holder.tv_date.text = projectUpdatesArrayList[position].updatedOn
        holder.tv_description.text = projectUpdatesArrayList[position].description

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iv_icon: ImageView = view.findViewById(R.id.iv_icon)
        var tv_date: TextView = view.findViewById(R.id.tv_date)
        var tv_description: TextView = view.findViewById(R.id.tv_description)

    }

}
    
