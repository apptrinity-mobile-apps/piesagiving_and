package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.ChurchProjectArrayResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class ProfileAdapter(
    context: Context,
    val ourRecommendationsArray: ArrayList<ChurchProjectArrayResponse>
) :
    RecyclerView.Adapter<ProfileAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_home, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return ourRecommendationsArray.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_bottom_title_id.text = ourRecommendationsArray[position].name.toString()
        Picasso.with(mContext).load(ourRecommendationsArray[position].projectPicture.toString())
            .error(R.drawable.ic_profile)
            .into(holder.img_charity_id)

        Log.e("profile_pics", ourRecommendationsArray[position].projectPicture.toString())


    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img_charity_id = view.findViewById<ImageView>(R.id.img_charity_id)
        val tv_bottom_title_id = view.findViewById<TextView>(R.id.tv_bottom_title_id)


    }

}
    
