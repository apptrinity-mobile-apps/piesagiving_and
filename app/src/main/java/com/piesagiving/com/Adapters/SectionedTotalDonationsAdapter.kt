package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.TotalDonationsDataResponse
import com.piesagiving.com.ApiInterface.DonationReportsOrgList
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class SectionedTotalDonationsAdapter(
    context: Context,
    list: ArrayList<TotalDonationsDataResponse>,
    frequency: String
) :
    RecyclerView.Adapter<SectionedTotalDonationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<TotalDonationsDataResponse>? = null
    private var frequency: String? = null

    init {
        this.mContext = context
        this.mList = list
        this.frequency = frequency
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.section_custom_row_layout, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val history = mList!![position]
        if (frequency == "week") {
            holder.section_label.text = "Week of ${history.interval}"
        } else {
            holder.section_label.text = history.interval
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            holder.section_label.setTextColor(mContext!!.getColor(R.color.header))
        } else {
            holder.section_label.setTextColor(mContext!!.resources.getColor(R.color.header))
        }

        //recycler view for items
        val linearLayoutManager =
            LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        holder.item_recycler_view.layoutManager = linearLayoutManager
        holder.item_recycler_view.setHasFixedSize(true)
        holder.item_recycler_view.isNestedScrollingEnabled = false
        val adapter = InnerAdapter(mContext!!, history.orgList!!)
        holder.item_recycler_view.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var section_label = view.findViewById(R.id.section_label) as TextView
        var item_recycler_view = view.findViewById(R.id.item_recycler_view) as RecyclerView
    }

    // inner recyclerview adapter
    inner class InnerAdapter(context: Context, list: ArrayList<DonationReportsOrgList>) :
        RecyclerView.Adapter<InnerAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<DonationReportsOrgList>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_item_total_donations, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_name.text = mList!![position].organizationName
            holder.tv_address.text = mList!![position].cityName
            Picasso.with(mContext).load(mList!![position].profilePic.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
            holder.tv_amount.text = "$${Formatter().format("%,.2f",mList!![position].donationSum!!)}"
            holder.tv_date.text = mList!![position].date
            if(mList!![position].status.equals("pending")){
                holder.tv_processed.text = "Processing"
            } else if(mList!![position].status.equals("Completed")){
                holder.tv_processed.text = "Donated"
            } else {
                holder.tv_processed.text = mList!![position].status
            }

            holder.tv_cause.text = mList!![position].proCauseName
            holder.tv_payment_type.text = mList!![position].paymentType
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_name = view.findViewById(R.id.nameView) as TextView
            var tv_address = view.findViewById(R.id.tv_address) as TextView
            var tv_date = view.findViewById(R.id.tv_date) as TextView
            var tv_amount = view.findViewById(R.id.tv_amount) as TextView
            var tv_processed = view.findViewById(R.id.tv_processed) as TextView
            var tv_cause = view.findViewById(R.id.tv_cause) as TextView
            var tv_payment_type = view.findViewById(R.id.tv_payment_type) as TextView
            var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        }

    }
}
