package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.ProjectDetailedViewActivity
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.ApiInterface.NotificationsData
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class AllNotificationsSubAdapter(
    context: Context,
    list: ArrayList<NotificationsData>
) :
    RecyclerView.Adapter<AllNotificationsSubAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<NotificationsData>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_all_notifications, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(
        holder: ViewHolder,
        @SuppressLint("RecyclerView") position: Int
    ) {
        holder.tv_name.text = mList!![position].description.toString()
        if (mList!![position].notificationType.toString() == "donation") {
            holder.cv_image.visibility = View.GONE
        } else {
            holder.cv_image.visibility = View.VISIBLE
        }
        if (mList!![position].notificationType.toString() == "follow_request" || mList!![position].notificationType.toString() == "friend_request") {
            if (mList!![position].profilePic.toString() == "") {
                Picasso.with(mContext).load(R.drawable.ic_church_profile_edited)
                    .error(R.drawable.ic_church_profile_edited)
                    .into(holder.iv_icon)
            } else {
                Picasso.with(mContext).load(mList!![position].profilePic.toString())
                    .error(R.drawable.ic_church_profile_edited)
                    .into(holder.iv_icon)
            }
        }

        holder.itemView.setOnClickListener {
            if (mList!![position].notificationType.toString() == "follow_request" || mList!![position].notificationType.toString() == "friend_request") {
                val intent = Intent(mContext!!, YourCircleProfileActivity::class.java)
                intent.putExtra("privacy", "")
                intent.putExtra("firstName", mList!![position].friendFirstName.toString())
                intent.putExtra("lastName", mList!![position].friendLastName.toString())
                intent.putExtra("profilePic", mList!![position].profilePic.toString())
                intent.putExtra("id", mList!![position].friendUserId.toString())
                intent.putExtra("userName", mList!![position].friendUserName.toString())
                intent.putExtra("notificationId", "")
                intent.putExtra("from_screen", "request")
                mContext!!.startActivity(intent)
            } else if (mList!![position].notificationType.toString() == "organisation") {
                if (mList!![position].projectType.toString() == "project") {
                    val intent = Intent(mContext!!, ProjectDetailedViewActivity::class.java)
                    intent.putExtra("from_screen", "project")
                    intent.putExtra("organiz_id", mList!![position].orgId.toString())
                    intent.putExtra("project_id", mList!![position].projectId.toString())
                    intent.putExtra("project_name", mList!![position].projectName.toString())
                    intent.putExtra("display_name",  "")

                    mContext!!.startActivity(intent)
                } else if (mList!![position].projectType.toString() == "special_cause") {
                    val intent = Intent(mContext!!, ProjectDetailedViewActivity::class.java)
                    intent.putExtra("from_screen", "special_cause")
                    intent.putExtra("organiz_id", mList!![position].orgId.toString())
                    intent.putExtra("project_id", mList!![position].causeId.toString())
                    intent.putExtra("project_name", mList!![position].causeName.toString())
                    intent.putExtra("display_name",  "")

                    mContext!!.startActivity(intent)
                }
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.tv_name) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        var ll_header = view.findViewById(R.id.ll_header) as LinearLayout
        var cv_image = view.findViewById(R.id.cv_image) as CardView
    }

}