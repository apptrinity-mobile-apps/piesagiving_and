package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.R

class YourCircleAdapter(context: Context) :
    RecyclerView.Adapter<YourCircleAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_your_circle, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return 5
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.cv_yourcircle_item.setOnClickListener {
            val intent = Intent(mContext, YourCircleProfileActivity::class.java)
            mContext!!.startActivity(intent)


        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var cv_yourcircle_item: CardView = view.findViewById(R.id.cv_yourcircle_item)
    }

}
    
