package com.piesagiving.com.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.SavedCardsArrayDataResponse
import com.piesagiving.com.R

 class SavedCardListAdapter(
    context: Context,
    val account_list: ArrayList<SavedCardsArrayDataResponse>,
    val user_id: String,val delete_status:String
) :
    RecyclerView.Adapter<SavedCardListAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.list_item_savedcards, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return account_list.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_card_name: TextView = view.findViewById(R.id.tv_card_name)
        var iv_card_type: ImageView = view.findViewById(R.id.iv_card_type)
        var tv_card_number: TextView = view.findViewById(R.id.tv_card_number)
        var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_card_name.text = account_list[position].name
        //holder.iv_card_type.text="*******"+account_list[position].last4
        holder.tv_card_number.text = "*****-" + account_list[position].last4

        if(delete_status.equals("yes")){
            holder.img_delete_id.visibility=View.VISIBLE
        }

        holder.img_delete_id.setOnClickListener {

        }
    }




}
