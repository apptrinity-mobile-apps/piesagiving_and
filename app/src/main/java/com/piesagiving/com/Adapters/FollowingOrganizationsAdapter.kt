package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.ChurchProfileActivity
import com.piesagiving.com.ApiInterface.SearchOrganizationDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class FollowingOrganizationsAdapter(
    context: Context,
    list: ArrayList<SearchOrganizationDataResponse>,
    userId: String
) :
    RecyclerView.Adapter<FollowingOrganizationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<SearchOrganizationDataResponse>? = null
    private var userId = ""

    init {
        this.mContext = context
        this.mList = list
        this.userId = userId
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_favourites, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_name.text = mList!![position].name
        holder.tv_address.text = mList!![position].state
        Picasso.with(mContext).load(mList!![position].profilePic.toString())
            .error(R.drawable.ic_profile)
            .into(holder.iv_icon)
        holder.cv_header.setOnClickListener {
            val intent = Intent(mContext!!, ChurchProfileActivity::class.java)
            intent.putExtra("from_screen", "")
            intent.putExtra("display_name", mList!![position].name)
            intent.putExtra("reco_organizationId", mList!![position].id)
            mContext!!.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.tv_name) as TextView
        var tv_address = view.findViewById(R.id.tv_address) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        var cv_header = view.findViewById(R.id.cv_header) as CardView
    }

}
    
