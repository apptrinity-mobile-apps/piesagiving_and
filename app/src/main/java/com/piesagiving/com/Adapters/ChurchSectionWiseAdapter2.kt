package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.ChurchProfileActivity
import com.piesagiving.com.Activities.ProjectDetailedViewActivity
import com.piesagiving.com.ApiInterface.CharityListDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class ChurchSectionWiseAdapter2(context: Context, tab_name: String, list: ArrayList<CharityListDataResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val SECTION_VIEW = 0
    val CONTENT_VIEW = 1
    var mList: ArrayList<CharityListDataResponse>? = null
    var mContext: Context? = null
    var mTabName: String? = null

    init {
        this.mContext = context
        this.mList = list
        this.mTabName = tab_name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == SECTION_VIEW) {
            return SectionHeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_contacts_header, parent, false)
            )
        }
        return ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_fav, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mList!![position].isSection) {
            SECTION_VIEW
        } else {
            CONTENT_VIEW
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (SECTION_VIEW == getItemViewType(position)) {
            val sectionHeaderViewHolder = holder as SectionHeaderViewHolder
            val nameCapitalized: String = mList!![position].displayHeader.toString().toUpperCase(Locale.getDefault())
            //val nameCapitalized = s1 + user_name.substring(1)
            sectionHeaderViewHolder.headerTitleTextview.text = nameCapitalized
            return
        }
        val itemViewHolder = holder as ItemViewHolder
        itemViewHolder.nameTextview.text = mList!![position].displayName.toString()
        itemViewHolder.tv_mycirclecountry.text = mList!![position].city.toString() + ", " + mList!![position].state.toString()
        Picasso.with(mContext).load(mList!![position].profilePic.toString())
            .error(R.drawable.ic_profile)
            .into(itemViewHolder.iv_mycircle)
        itemViewHolder.cv_main.setOnClickListener {
            if (mTabName == "charity" || mTabName == "church") {
                val intent = Intent(mContext!!, ChurchProfileActivity::class.java)
                intent.putExtra("from_screen", "profile")
                intent.putExtra("reco_organizationId", mList!![position].organizationId)
                intent.putExtra("display_name", mList!![position].displayName)
                mContext!!.startActivity(intent)
            } else if (mTabName == "special_cause") {
                val intent = Intent(mContext!!, ProjectDetailedViewActivity::class.java)
                intent.putExtra("from_screen", "special_cause")
                intent.putExtra("organiz_id", mList!![position].organizationId)
                intent.putExtra("project_id", mList!![position].causeId)
                intent.putExtra("project_name", mList!![position].displayName)
                intent.putExtra("display_name", mList!![position].displayName)
                mContext!!.startActivity(intent)
            }
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTextview: TextView = itemView.findViewById(R.id.tv_name)
        var tv_mycirclecountry: TextView = itemView.findViewById(R.id.tv_mycirclecountry)
        var iv_mycircle: ImageView = itemView.findViewById(R.id.iv_mycircle)
        var cv_main: CardView = itemView.findViewById(R.id.cv_main)
    }

    class SectionHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var headerTitleTextview: TextView = itemView.findViewById(R.id.tv_header_title)
    }
}