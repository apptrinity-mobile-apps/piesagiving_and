package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.FaqArrayListResponse
import com.piesagiving.com.R

class FaqAdapter(
    context: Context,
    val faqArray: ArrayList<FaqArrayListResponse>
) :
    RecyclerView.Adapter<FaqAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.faq_list_item, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return faqArray.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_faq_qtn_id.text = faqArray[position].faqTitle!!.trim()
        holder.tv_faq_ans_id.text = Html.fromHtml(faqArray[position].faqDescription!!.trim())

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ll_question_id = view.findViewById<LinearLayout>(R.id.ll_question_id)
        val tv_faq_qtn_id = view.findViewById<TextView>(R.id.tv_faq_qtn_id)
        val tv_faq_ans_id = view.findViewById<TextView>(R.id.tv_faq_ans_id)

    }

}
    
