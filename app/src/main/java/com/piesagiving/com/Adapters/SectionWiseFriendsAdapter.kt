package com.piesagiving.com.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.ApiInterface.CharityListDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList


class SectionWiseFriendsAdapter(context: Context, list: ArrayList<CharityListDataResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val SECTION_VIEW = 0
    val CONTENT_VIEW = 1
    var mList: ArrayList<CharityListDataResponse>? = null
    var mContext: Context? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == SECTION_VIEW) {
            return SectionHeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_contacts_header, parent, false)
            )
        }
        return ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_recent, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mList!![position].isSection) {
            SECTION_VIEW
        } else {
            CONTENT_VIEW
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (SECTION_VIEW == getItemViewType(position)) {
            val sectionHeaderViewHolder = holder as SectionHeaderViewHolder

            val nameCapitalized: String = mList!![position].displayHeader.toString().toUpperCase(
                Locale.getDefault()
            )
            sectionHeaderViewHolder.headerTitleTextview.text = nameCapitalized
            return
        }
        val itemViewHolder = holder as ItemViewHolder
        itemViewHolder.tv_name.text =
            mList!![position].firstName.toString() + " " + mList!![position].lastName.toString()
        if (mList!![position].profilePic.toString() == "") {
            Picasso.with(mContext).load(R.drawable.ic_church_profile_edited)
                .error(R.drawable.ic_church_profile_edited)
                .into(itemViewHolder.iv_icon)
        } else {
            Picasso.with(mContext).load(mList!![position].profilePic.toString())
                .error(R.drawable.ic_church_profile_edited)
                .into(itemViewHolder.iv_icon)
        }
        itemViewHolder.cv_header.background = ContextCompat.getDrawable(mContext!!, R.color.white)
        itemViewHolder.cv_header.setOnClickListener {
            val intent = Intent(mContext!!, YourCircleProfileActivity::class.java)
            intent.putExtra("privacy", mList!![position].privacy.toString())
            intent.putExtra("firstName", mList!![position].firstName.toString())
            intent.putExtra("lastName", mList!![position].lastName.toString())
            intent.putExtra("profilePic", mList!![position].profilePic.toString())
            intent.putExtra("id", mList!![position].otherUserId.toString())
            intent.putExtra("userName", mList!![position].userName.toString())
            intent.putExtra("notificationId", "")
            intent.putExtra("from_screen", "search")
            mContext!!.startActivity(intent)
        }

    }

    class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.tv_name) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        var cv_header = view.findViewById(R.id.cv_header) as LinearLayout

    }

    class SectionHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var headerTitleTextview: TextView

        init {
            headerTitleTextview = itemView.findViewById(R.id.tv_header_title)
        }
    }
}