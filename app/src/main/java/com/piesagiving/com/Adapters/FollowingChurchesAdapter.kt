package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.RecommendationArray
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class FollowingChurchesAdapter(
    context: Context,
    val ourRecommendationsArray: ArrayList<RecommendationArray>
) :
    RecyclerView.Adapter<FollowingChurchesAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_home, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return ourRecommendationsArray.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        holder.tv_bottom_title_id.text = ourRecommendationsArray[position].name.toString()

        if(!ourRecommendationsArray[position].type.equals(null) || !ourRecommendationsArray[position].type.equals("null")){
            if(ourRecommendationsArray[position].type.equals("charity")){
                holder.tv_bottom_title_location.visibility = View.GONE
                holder.tv_bottom_title_location.text = ourRecommendationsArray[position].location.toString()
            }else if(ourRecommendationsArray[position].type.equals("church")){
                holder.tv_bottom_title_location.visibility = View.VISIBLE
                holder.tv_bottom_title_location.text =  ourRecommendationsArray[position].location.toString()
            }
        }else{
           // holder.tv_bottom_title_location.visibility = View.GONE
        }

        Picasso.with(mContext).load(ourRecommendationsArray[position].profilePic.toString())
            .error(R.drawable.noimagefound3)
            .into(holder.img_charity_id)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val img_charity_id = view.findViewById<ImageView>(R.id.img_charity_id)
        val tv_bottom_title_id = view.findViewById<TextView>(R.id.tv_bottom_title_id)
        val tv_bottom_title_location = view.findViewById<TextView>(R.id.tv_bottom_title_location)


    }

}
    
