package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.CommentsResponse
import com.piesagiving.com.ApiInterface.SpecialCauseDonationsArrayResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class DonationsAdapter(
    context: Context,
    val specialCauseDonationsArrayList: ArrayList<SpecialCauseDonationsArrayResponse>
) :
    RecyclerView.Adapter<DonationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var commentsList: ArrayList<CommentsResponse>? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_donation_list, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return specialCauseDonationsArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        commentsList = ArrayList()
        holder.tv_name.text = specialCauseDonationsArrayList[position].donatedBy
        if (specialCauseDonationsArrayList[position].profilePic.toString() == "") {
            Picasso.with(mContext)
                .load(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        } else {
            Picasso.with(mContext)
                .load(specialCauseDonationsArrayList[position].profilePic.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        }
        holder.rv_comments.layoutManager = holder.layoutManager
        holder.rv_comments.hasFixedSize()
        /*if (specialCauseDonationsArrayList[position].comments!!.size > 0) {
            for (i in 0 until specialCauseDonationsArrayList[position].comments!!.size) {
                if (specialCauseDonationsArrayList[position].comments!![i].comment != "") {
                    commentsList!!.add(specialCauseDonationsArrayList[position].comments!![i])
                }
            }
        }
        Log.d("comments", "${commentsList!!.size}")

        if (commentsList!!.size > 0) {
            val commentsAdapter =
                CommentsAdapter(mContext!!, commentsList = commentsList!!)
            holder.rv_comments.adapter = commentsAdapter
            commentsAdapter.notifyDataSetChanged()
            holder.rv_comments.visibility = View.VISIBLE
        } else {
            holder.rv_comments.visibility = View.GONE
        }*/
        holder.rv_comments.visibility = View.GONE
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var iv_icon: ImageView = view.findViewById(R.id.iv_icon)
        var tv_name: TextView = view.findViewById(R.id.tv_name)
        var rv_comments: RecyclerView = view.findViewById(R.id.rv_comments)
        val layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
    }

    inner class CommentsAdapter(context: Context, commentsList: ArrayList<CommentsResponse>) :
        RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {
        private var mList: ArrayList<CommentsResponse>? = null
        private var context: Context? = null

        init {
            this.context = context
            this.mList = commentsList
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_comments, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return if (mList!!.size >= 2) {
                2
            } else {
                1
            }
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_date.text = mList!![position].createdOn
            holder.tv_comment.text = mList!![position].comment!!.trim()
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_date: TextView = view.findViewById(R.id.tv_date)
            var tv_comment: TextView = view.findViewById(R.id.tv_comment)
        }
    }
}
    
