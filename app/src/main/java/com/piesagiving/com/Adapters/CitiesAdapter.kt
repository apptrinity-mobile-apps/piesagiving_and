package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.CitiesDataResponse
import com.piesagiving.com.R
import java.util.*
import kotlin.collections.ArrayList

class CitiesAdapter(context: Context, list: ArrayList<CitiesDataResponse>) :
    RecyclerView.Adapter<CitiesAdapter.ViewHolder>(), Filterable {

    private var mContext: Context? = null
    private var mList: ArrayList<CitiesDataResponse>? = null
    private var mFilteredList: ArrayList<CitiesDataResponse> = ArrayList()
    private var row_index = -1
    private var mFilter: NewFilter? = null

    init {
        this.mContext = context
        this.mList = list
        this.mFilteredList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_coutries, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mFilteredList.size
    }

    fun getItemAt(position: Int):CitiesDataResponse{
        return mFilteredList[position]
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_header_name.text = mFilteredList[position].name
        holder.tv_header_name.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
        }
        if (row_index == position) {
            holder.ll_header.background =
                ContextCompat.getDrawable(mContext!!, R.color.bottom_nav_item)
        } else {
            holder.ll_header.background = ContextCompat.getDrawable(mContext!!, R.color.white)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_header_name = view.findViewById(R.id.tv_header_name) as TextView
        var ll_header = view.findViewById(R.id.ll_header) as LinearLayout
    }

    fun removeSelectionAt(position: Int){
        row_index = position
    }

    override fun getFilter(): Filter {
        if (mFilter == null) {
            mFilter = NewFilter()
        }
        return mFilter!!
    }

    inner class NewFilter : Filter() {
        override fun performFiltering(charSequence: CharSequence): FilterResults {

            val results = FilterResults()
            mFilteredList = if (charSequence.isEmpty()) {
                mList!!
            } else {
                val mFilterList: ArrayList<CitiesDataResponse> = ArrayList()
                for (item in mList!!) {
                    if (item.name!!.toLowerCase(Locale.getDefault()).trim().contains(charSequence)){
                        mFilterList.add(item)
                    }
                }
                mFilterList
            }
            results.values = mFilteredList
            return results
        }

        override fun publishResults(
            charSequence: CharSequence,
            filterResults: FilterResults
        ) {
            try {
                mFilteredList = filterResults.values!! as ArrayList<CitiesDataResponse>
                notifyDataSetChanged()
            }catch (e:Exception){
                e.printStackTrace()
            }
        }

    }

}
    
