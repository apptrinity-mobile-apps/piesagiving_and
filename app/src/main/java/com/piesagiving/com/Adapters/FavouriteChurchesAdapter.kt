package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.R

class FavouriteChurchesAdapter(context: Context) :
    RecyclerView.Adapter<FavouriteChurchesAdapter.ViewHolder>() {

    private var mContext: Context? = null

    init {
        this.mContext = context
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_fav, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    }

}
    
