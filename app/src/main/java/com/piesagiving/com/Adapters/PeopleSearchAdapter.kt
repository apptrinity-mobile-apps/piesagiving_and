package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.ApiInterface.UsersDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class PeopleSearchAdapter(
    context: Context,
    list: ArrayList<UsersDataResponse>,
    userId: String,
    from_screen:String
) :
    RecyclerView.Adapter<PeopleSearchAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<UsersDataResponse>? = null
    private var userId = ""
    private var from_screen = ""

    init {
        this.mContext = context
        this.mList = list
        this.userId = userId
        this.from_screen = from_screen
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_church_recent, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    fun getItemAt(position: Int):UsersDataResponse{
        return mList!![position]
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_name.text =
            mList!![position].firstName.toString() + " " + mList!![position].lastName.toString()
        if (mList!![position].profilePic.toString() == "") {
            Picasso.with(mContext).load(R.drawable.ic_church_profile_edited)
                .error(R.drawable.ic_church_profile_edited)
                .into(holder.iv_icon)
        } else {
            Picasso.with(mContext).load(mList!![position].profilePic.toString())
                .error(R.drawable.ic_church_profile_edited)
                .into(holder.iv_icon)
        }
        holder.cv_header.setOnClickListener {
            val intent = Intent(mContext!!, YourCircleProfileActivity::class.java)
            intent.putExtra("privacy", mList!![position].privacy.toString())
            intent.putExtra("firstName", mList!![position].firstName.toString())
            intent.putExtra("lastName", mList!![position].lastName.toString())
            intent.putExtra("profilePic", mList!![position].profilePic.toString())
            intent.putExtra("id", mList!![position].id.toString())
            intent.putExtra("userName", mList!![position].userName.toString())
            intent.putExtra("notificationId", "")
            intent.putExtra("from_screen", from_screen)
            mContext!!.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.tv_name) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        var cv_header = view.findViewById(R.id.cv_header) as LinearLayout
    }

}
    
