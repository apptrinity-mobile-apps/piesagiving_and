package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.DonationDetailedResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class DetailedDonationsAdapter(context: Context, list: ArrayList<DonationDetailedResponse>) :
    RecyclerView.Adapter<DetailedDonationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<DonationDetailedResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_item_total_donations, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_name.text = mList!![position].orgName
        holder.tv_address.text = mList!![position].cityName
        Picasso.with(mContext).load(mList!![position].profilePic.toString())
            .error(R.drawable.ic_profile)
            .into(holder.iv_icon)
        holder.tv_amount.text = "$${Formatter().format("%,.2f", mList!![position].amount!!)}"
        holder.tv_date.text = mList!![position].date
        holder.tv_processed.text = mList!![position].status
        holder.tv_cause.text = mList!![position].projectName
        holder.tv_payment_type.text = mList!![position].paymentType
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.nameView) as TextView
        var tv_address = view.findViewById(R.id.tv_address) as TextView
        var tv_date = view.findViewById(R.id.tv_date) as TextView
        var tv_amount = view.findViewById(R.id.tv_amount) as TextView
        var tv_processed = view.findViewById(R.id.tv_processed) as TextView
        var tv_cause = view.findViewById(R.id.tv_cause) as TextView
        var tv_payment_type = view.findViewById(R.id.tv_payment_type) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
    }

}
