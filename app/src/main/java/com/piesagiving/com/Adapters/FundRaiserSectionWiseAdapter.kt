package com.piesagiving.com.Adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.FundRaisersActivity
import com.piesagiving.com.ApiInterface.FundRaiserFavouriteListDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList


class FundRaiserSectionWiseAdapter(context: Context, list: ArrayList<FundRaiserFavouriteListDataResponse>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val SECTION_VIEW = 0
    val CONTENT_VIEW = 1
    var mList: ArrayList<FundRaiserFavouriteListDataResponse>? = null
    var mContext: Context? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == SECTION_VIEW) {
            return SectionHeaderViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_contacts_header, parent, false)
            )
        }
        return ItemViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_fundraiser_fav, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (mList!![position].isSection) {
            SECTION_VIEW
        } else {
            CONTENT_VIEW
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (SECTION_VIEW == getItemViewType(position)) {
            val sectionHeaderViewHolder = holder as SectionHeaderViewHolder
            val nameCapitalized: String = mList!![position].displayHeader!!.toString().uppercase(Locale.getDefault())
            //val nameCapitalized = s1 + user_name.substring(1)
            sectionHeaderViewHolder.headerTitleTextview.text = nameCapitalized
            return
        }
        val itemViewHolder = holder as ItemViewHolder
        itemViewHolder.nameTextview.text = mList!![position].name.toString()
        Picasso.with(mContext).load(R.drawable.ic_profile)
            .error(R.drawable.ic_profile)
            .into(itemViewHolder.iv_mycircle)
        itemViewHolder.cv_main.setOnClickListener {
            val intent = Intent(mContext, FundRaisersActivity::class.java)
            intent.putExtra("fundraiser_id", mList!![position].id)
            mContext!!.startActivity(intent)
        }
    }

    class ItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTextview: TextView = itemView.findViewById(R.id.tv_name)
        var iv_mycircle: ImageView = itemView.findViewById(R.id.iv_mycircle)
        var cv_main: CardView = itemView.findViewById(R.id.cv_main)
    }

    class SectionHeaderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var headerTitleTextview: TextView = itemView.findViewById(R.id.tv_header_title)

    }
}