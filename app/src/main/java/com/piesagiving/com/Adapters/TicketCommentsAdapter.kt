package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.ApiInterface.TicketMessagesResponse
import com.piesagiving.com.R

@SuppressLint("SetTextI18n")
class TicketCommentsAdapter(
    context: Context,
    list: ArrayList<TicketMessagesResponse>,
    orgName: String
) :
    RecyclerView.Adapter<TicketCommentsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<TicketMessagesResponse>? = null
    private var orgName: String? = null

    init {
        this.mContext = context
        this.mList = list
        this.orgName = orgName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycle_item_ticket_comment, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position % 2 == 0) {
            holder.cv_item_card.setCardBackgroundColor(
                ContextCompat.getColor(mContext!!, R.color.bottom_nav_item)
            )
        } else {
            holder.cv_item_card.setCardBackgroundColor(
                ContextCompat.getColor(mContext!!, R.color.white)
            )
        }
        when {
            mList!![position].type.toString().equals("user", ignoreCase = true) -> {
                holder.tv_item_comment_by.text = "Me:"
            }
            mList!![position].type.toString().equals("superAdmin", ignoreCase = true) -> {
                holder.tv_item_comment_by.text = "Admin:"
            }
            mList!![position].type.toString().equals("orgAdmin", ignoreCase = true) -> {
                holder.tv_item_comment_by.text = "$orgName:"
            }
            else -> {
                holder.tv_item_comment_by.text = mList!![position].type.toString() + ":"
            }
        }
        holder.tv_item_comment_date.text = mList!![position].date.toString()
        holder.tv_item_comment.text = mList!![position].message.toString()

    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_item_comment_by = view.findViewById(R.id.tv_item_comment_by) as TextView
        var tv_item_comment_date = view.findViewById(R.id.tv_item_comment_date) as TextView
        var tv_item_comment = view.findViewById(R.id.tv_item_comment) as TextView
        var cv_item_card = view.findViewById(R.id.cv_item_card) as CardView
    }

}
    
