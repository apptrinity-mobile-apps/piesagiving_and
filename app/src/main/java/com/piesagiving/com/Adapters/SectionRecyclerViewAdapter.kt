package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.ReportsDetailedActivity
import com.piesagiving.com.ApiInterface.DonationReportsHistoryResponse
import com.piesagiving.com.ApiInterface.DonationReportsOrgList
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class SectionRecyclerViewAdapter(
    context: Context,
    list: ArrayList<DonationReportsHistoryResponse>
) :
    RecyclerView.Adapter<SectionRecyclerViewAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<DonationReportsHistoryResponse>? = null

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.section_custom_row_layout, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val donationReports = mList!![position]
        holder.section_label.text = donationReports.interval

        //recycler view for items
        val linearLayoutManager =
            LinearLayoutManager(mContext!!, LinearLayoutManager.VERTICAL, false)
        holder.item_recycler_view.layoutManager = linearLayoutManager
        holder.item_recycler_view.setHasFixedSize(true)
        holder.item_recycler_view.isNestedScrollingEnabled = false
        val adapter =
            InnerAdapter(mContext!!, donationReports.orgList!!, donationReports.interval!!)
        holder.item_recycler_view.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var section_label = view.findViewById(R.id.section_label) as TextView
        var item_recycler_view = view.findViewById(R.id.item_recycler_view) as RecyclerView
    }

    // inner recyclerview adapter
    inner class InnerAdapter(
        context: Context,
        list: ArrayList<DonationReportsOrgList>,
        interval: String
    ) :
        RecyclerView.Adapter<InnerAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<DonationReportsOrgList>? = null
        private var mInterval:String?= null

        init {
            this.mContext = context
            this.mList = list
            this.mInterval = interval
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_user_item, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_name.text = mList!![position].organizationName
            holder.tv_address.text =
                mList!![position].streetAddress + ", " + mList!![position].cityName
            holder.tv_donated_amount.text = "$${Formatter().format("%,.2f", mList!![position].totalSum)}"
            Picasso.with(mContext).load(mList!![position].profilePic.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
            holder.cv_header.setOnClickListener {
                val i = Intent(mContext!!, ReportsDetailedActivity::class.java)
                i.putExtra("interval", mInterval)
                i.putExtra("orgId", mList!![position].id.toString())
                i.putExtra("orgName", mList!![position].organizationName.toString())
                mContext!!.startActivity(i)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_name = view.findViewById(R.id.nameView) as TextView
            var tv_address = view.findViewById(R.id.tv_address) as TextView
            var tv_donated_amount = view.findViewById(R.id.tv_donated_amount) as TextView
            var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
            var cv_header = view.findViewById(R.id.cv_header) as CardView
        }

    }
}
