package com.piesagiving.com.Adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Activities.ChurchProfileActivity
import com.piesagiving.com.Activities.ProjectDetailedViewActivity
import com.piesagiving.com.ApiInterface.OrganizationDataResponse
import com.piesagiving.com.R
import com.squareup.picasso.Picasso

class OrganizationsAdapter(
    context: Context,
    list: ArrayList<OrganizationDataResponse>,
    userId: String,
    specialCause: Boolean,
    filtersAvailable: Boolean
) :
    RecyclerView.Adapter<OrganizationsAdapter.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<OrganizationDataResponse>? = null
    private var userId = ""
    private var specialCause = false
    private var filtersAvailable = false

    init {
        this.mContext = context
        this.mList = list
        this.userId = userId
        this.specialCause = specialCause
        this.filtersAvailable = filtersAvailable
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        return if (!filtersAvailable) {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_favourites, parent, false)
            ViewHolder(view)
        } else {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_favourites_with_filter, parent, false)
            ViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return mList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        holder.tv_name.text = mList!![position].organizationName
        if (!specialCause) {
            holder.tv_address.text = mList!![position].cityName
        } else {
            holder.tv_address.text = mList!![position].streetAddress
        }
        if (mList!![position].profilePic.toString() == "") {
            Picasso.with(mContext).load(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        } else {
            Picasso.with(mContext).load(mList!![position].profilePic.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_icon)
        }
        // to remove "" in response
        holder.tv_org_sub_category.text =
            TextUtils.join(", ", mList!![position].categoryTypes!!).replace("\"", "")
        holder.cv_header.setOnClickListener {
            if (!specialCause) {
                val intent = Intent(mContext!!, ChurchProfileActivity::class.java)
                intent.putExtra("from_screen", "")
                intent.putExtra("display_name", mList!![position].organizationName)
                intent.putExtra("reco_organizationId", mList!![position].id)
                mContext!!.startActivity(intent)
            } else {
                val intent = Intent(mContext!!, ProjectDetailedViewActivity::class.java)
                intent.putExtra("from_screen", "special_cause")
                intent.putExtra("organiz_id", mList!![position].organizationId)
                intent.putExtra("project_id", mList!![position].id)
                intent.putExtra("project_name", mList!![position].organizationName)
                intent.putExtra("display_name",   mList!![position].organizationName)
                mContext!!.startActivity(intent)
            }
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tv_name = view.findViewById(R.id.tv_name) as TextView
        var tv_address = view.findViewById(R.id.tv_address) as TextView
        var tv_org_sub_category = view.findViewById(R.id.tv_org_sub_category) as TextView
        var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
        var cv_header = view.findViewById(R.id.cv_header) as CardView
    }

}
    
