package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.TicketCommentsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class TicketsDetailViewActivity : AppCompatActivity() {

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>
    private lateinit var cd: ConnectionDetector
    private var isNetworkAvailable = false
    private var userId = ""
    private var ticketNo = ""
    private var ticketStatus = ""
    private var ticketId = ""
    private var orgId = ""
    private var orgName = ""
    private var orgType = ""
    private var subject = ""
    private var date = ""

    private lateinit var tv_header: TextView
    private lateinit var iv_back: ImageView
    private lateinit var tv_ticket_no: TextView
    private lateinit var tv_ticket_name: TextView
    private lateinit var tv_ticket_subject: TextView
    private lateinit var tv_ticket_date: TextView
    private lateinit var et_new_comment: EditText
    private lateinit var tv_submit: TextView
    private lateinit var rv_ticket_comments: RecyclerView
    private lateinit var loading_dialog: Dialog
    private lateinit var tv_org_type: TextView
    private lateinit var ll_send_message: LinearLayout
    private lateinit var ll_re_open_ticket: LinearLayout
    private lateinit var tv_re_open: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_tickets_detail_view)

        ticketNo = intent.getStringExtra("ticketNo")!!
        orgId = intent.getStringExtra("orgId")!!
        orgName = intent.getStringExtra("orgName")!!
        subject = intent.getStringExtra("subject")!!
        date = intent.getStringExtra("date")!!
        orgType = intent.getStringExtra("orgType")!!
        ticketStatus = intent.getStringExtra("ticketStatus")!!
        ticketId = intent.getStringExtra("ticketId")!!

        initialize()
        messagesApi()

        tv_header.text = getString(R.string.ticket_of) + " " + orgName
        tv_ticket_no.text = ticketNo
        tv_ticket_name.text = orgName
        tv_ticket_subject.text = subject
        tv_ticket_date.text = date
        if (orgType.equals("church", ignoreCase = true)) {
            tv_org_type.text = getString(R.string.church_name)
        } else if (orgType.equals("charity", ignoreCase = true)) {
            tv_org_type.text = getString(R.string.charity_name)
        }
        when {
            /*status - new, open, closed, reopen, reclose
            * new, open - commenting available
            * closed - commenting not available, re-open available
            * reopen - commenting available, re-open not available
            * reclose - commenting not available, re-open not available */

            ticketStatus.equals("open", true) -> {
                ll_send_message.visibility = View.VISIBLE
                ll_re_open_ticket.visibility = View.GONE
            }
            ticketStatus.equals("new", true) -> {
                ll_send_message.visibility = View.VISIBLE
                ll_re_open_ticket.visibility = View.GONE
            }
            ticketStatus.equals("closed", true) -> {
                ll_send_message.visibility = View.GONE
                ll_re_open_ticket.visibility = View.VISIBLE
            }
            ticketStatus.equals("reopen", true) -> {
                ll_send_message.visibility = View.VISIBLE
                ll_re_open_ticket.visibility = View.GONE
            }
            ticketStatus.equals("reclose", true) -> {
                ll_send_message.visibility = View.GONE
                ll_re_open_ticket.visibility = View.GONE
            }
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }

        tv_submit.setOnClickListener {
            if (isNetworkAvailable) {
                val dataObj = JSONObject()
                dataObj.put("userId", userId)
                dataObj.put("orgId", orgId)
                dataObj.put("ticketNo", ticketNo)
                dataObj.put("message", et_new_comment.text.toString().trim())
                val jsonParser = JsonParser()
                val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                sendMessageInTicket(finalJsonObject)
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        tv_re_open.setOnClickListener {
            if (isNetworkAvailable) {
                val dataObj = JSONObject()
                dataObj.put("userId", userId)
                dataObj.put("ticketId", ticketId)
                val jsonParser = JsonParser()
                val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                reOpenTicket(finalJsonObject)
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun initialize() {
        sessionManager = SessionManager(this)
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!
        cd = ConnectionDetector(this)
        isNetworkAvailable = cd.isConnectingToInternet
        loadingDialog()
        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_ticket_no = findViewById(R.id.tv_ticket_no)
        tv_ticket_name = findViewById(R.id.tv_ticket_name)
        tv_ticket_subject = findViewById(R.id.tv_ticket_subject)
        tv_ticket_date = findViewById(R.id.tv_ticket_date)
        et_new_comment = findViewById(R.id.et_new_comment)
        tv_submit = findViewById(R.id.tv_submit)
        rv_ticket_comments = findViewById(R.id.rv_ticket_comments)
        tv_org_type = findViewById(R.id.tv_org_type)
        ll_send_message = findViewById(R.id.ll_send_message)
        ll_re_open_ticket = findViewById(R.id.ll_re_open_ticket)
        tv_re_open = findViewById(R.id.tv_re_open)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_ticket_comments.layoutManager = layoutManager
    }

    private fun messagesApi() {
        if (isNetworkAvailable) {
            val dataObj = JSONObject()
            dataObj.put("userId", userId)
            dataObj.put("orgId", orgId)
            dataObj.put("ticketNo", ticketNo)
            val jsonParser = JsonParser()
            val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
            getMessagesInTicket(finalJsonObject)
        } else {
            Toast.makeText(
                this,
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun sendMessageInTicket(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.sendMessageInTicketApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TicketsDetailViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        messagesApi()
                        et_new_comment.setText("")
                        Toast.makeText(
                            this@TicketsDetailViewActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        // do nothing
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getMessagesInTicket(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getMessagesInTicketApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TicketsDetailViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    rv_ticket_comments.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val list = resp.messages
                        // list!!.reverse()
                        val adapter =
                            TicketCommentsAdapter(this@TicketsDetailViewActivity, list!!, orgName)
                        rv_ticket_comments.adapter = adapter
                        adapter.notifyDataSetChanged()
                        if (list.size > 0) {
                            rv_ticket_comments.visibility = View.VISIBLE
                        } else {
                            rv_ticket_comments.visibility = View.GONE
                        }
                    } else {
                        rv_ticket_comments.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun reOpenTicket(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.reOpenTicketApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        ll_send_message.visibility = View.VISIBLE
                        ll_re_open_ticket.visibility = View.GONE
                    } else {
                        ll_send_message.visibility = View.GONE
                        ll_re_open_ticket.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}