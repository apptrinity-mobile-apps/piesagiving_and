package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.LayoutInflaterCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.SectionedTotalDonationsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.TotalDonationsResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class TotalDonationsActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    var type = ""
    var frequency = ""
    var header = ""
    var userId = ""

    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    private lateinit var recyclerView: RecyclerView
    lateinit var ll_frequency: LinearLayout
    lateinit var sp_frequency: Spinner
    lateinit var frequency_types: ArrayList<String>
    lateinit var frequency_types_ids: ArrayList<String>
    lateinit var loading_dialog: Dialog
    lateinit var tv_empty: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_total_donations)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        type = intent.getStringExtra("type")!!
        header = intent.getStringExtra("header")!!

        initialize()

        tv_header.text = header
        if (type == "") {
            ll_frequency.visibility = View.GONE
            if (isNetworkAvailable) {
//                 frequency = "week"
                frequency = "month"
                val dataObj = JSONObject()
                dataObj.put("userId", userId)
                dataObj.put("historyType", frequency)
                dataObj.put("donationTo", type)
                val jParser = JsonParser()
                val final_object = jParser.parse(dataObj.toString()) as JsonObject
                Log.d("object-- ",final_object.toString())
                getTotalDonationsHistoryApi(final_object)
            } else {
                Toast.makeText(
                    this@TotalDonationsActivity,
                    getString(R.string.no_network_available), Toast.LENGTH_SHORT
                ).show()
            }
        } else {
            ll_frequency.visibility = View.VISIBLE
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
        val adapter = ArrayAdapter(
            this, R.layout.support_simple_spinner_dropdown_item, frequency_types
        )
        sp_frequency.adapter = adapter
        sp_frequency.setSelection(2) // for monthly
        sp_frequency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                frequency = frequency_types_ids[position]
                Log.d("frequency_stg", frequency)
                if (isNetworkAvailable) {
                    val dataObj = JSONObject()
                    dataObj.put("userId", userId)
                    dataObj.put("historyType", frequency)
                    dataObj.put("donationTo", type)
                    val jParser = JsonParser()
                    val final_object = jParser.parse(dataObj.toString()) as JsonObject
                    getTotalDonationsHistoryApi(final_object)
                } else {
                    Toast.makeText(
                        this@TotalDonationsActivity,
                        getString(R.string.no_network_available), Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!

        loadingDialog()

        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        ll_frequency = findViewById(R.id.ll_frequency)
        sp_frequency = findViewById(R.id.sp_frequency)
        tv_empty = findViewById(R.id.tv_empty)
        recyclerView = findViewById(R.id.recycler_view)

        frequency_types = ArrayList()
        frequency_types_ids = ArrayList()

        frequency_types.add("Weekly")
        frequency_types.add("Monthly")
        frequency_types.add("Annual")

        frequency_types_ids.add("week")
        frequency_types_ids.add("month")
        frequency_types_ids.add("year")

        val layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@TotalDonationsActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@TotalDonationsActivity)
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getTotalDonationsHistoryApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getTotalDonationsHistoryApi(final_object)
        call.enqueue(object : Callback<TotalDonationsResponse> {
            override fun onFailure(call: Call<TotalDonationsResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()

                    tv_empty.visibility = View.VISIBLE
                    tv_empty.text = getString(R.string.no_donations_found)
                    recyclerView.visibility = View.GONE
                    Toast.makeText(
                        this@TotalDonationsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<TotalDonationsResponse>,
                response: Response<TotalDonationsResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val history = resp.history!!
                            if (history.size > 0) {
                                tv_empty.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE
                            } else {
                                tv_empty.text = getString(R.string.no_donations_found)
                                tv_empty.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            }
                            val adapter =
                                SectionedTotalDonationsAdapter(
                                    this@TotalDonationsActivity,
                                    history,
                                    frequency
                                )
                            recyclerView.adapter = adapter
                        }
                        else -> {
                            tv_empty.text = getString(R.string.no_donations_found)
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
