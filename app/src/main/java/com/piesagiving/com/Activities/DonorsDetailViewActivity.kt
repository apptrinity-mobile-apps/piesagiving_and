package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.ChurchUpdatesAdapter
import com.piesagiving.com.Adapters.DonationsAdapter
import com.piesagiving.com.Adapters.DonationsAdapterWithComments
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class DonorsDetailViewActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private var from_screen = ""
    private var organiz_id = ""
    private var orgName = ""
    private var project_id = ""
    private var project_name = ""
    private var specialCauseId = ""
    var isFollowing = false

    lateinit var img_project_profile_id: ImageView
    lateinit var tv_hosted_by: TextView
    lateinit var tv_amount_received: TextView
    lateinit var tv_total_amount: TextView
    lateinit var tv_last_day: TextView
    lateinit var tv_projects: TextView
    lateinit var header: TextView
    lateinit var tv_updates_empty: TextView
    lateinit var tv_donations_empty: TextView
    lateinit var tv_church_bio: TextView
    lateinit var tv_donated_by: TextView
    lateinit var tv_follow: TextView
    lateinit var tv_donate: TextView
    lateinit var ll_hosted: LinearLayout
    lateinit var ll_last_day: LinearLayout
    lateinit var seekBar: SeekBar
    lateinit var rv_updates: RecyclerView
    lateinit var rv_donations: RecyclerView
    lateinit var iv_back: ImageView
    lateinit var lm_updates: LinearLayoutManager
    lateinit var lm_donations: LinearLayoutManager
    lateinit var donationsAdapter: DonationsAdapter
    lateinit var specialcauseUpdatesAdapter: ChurchUpdatesAdapter
    lateinit var loading_dialog: Dialog


    lateinit var projectDonationsArrayList: ArrayList<projectDonationsArrayResponse>
    lateinit var projectUpdatesArrayList: ArrayList<projectUpdatesArrayResponse>
    lateinit var specialcauseUpdatesArrayList: ArrayList<SpecialCauseUpdatesArrayResponse>
    lateinit var specialcauseDonationsArrayList: ArrayList<SpecialCauseDonationsArrayResponse>
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_donors_detailed_view)

        from_screen = intent.getStringExtra("from_screen")!!
        organiz_id = intent.getStringExtra("organiz_id")!!
        project_id = intent.getStringExtra("project_id")!!
        project_name = intent.getStringExtra("project_name")!!

        if (intent.getStringExtra("project_id")!! != null) {
            specialCauseId = intent.getStringExtra("project_id")!!
        }
        Log.e(
            "project_details_input",
            from_screen + "-----" + organiz_id + "----" + project_id + "----" + specialCauseId
        )
        initialize()
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!

        header.text = project_name
        if (from_screen == "project") {

            val obj = JSONObject()
            obj.put("userId", userId)
            obj.put("projectId", project_id)
            val jParser = JsonParser()
            val jObject = jParser.parse(obj.toString()) as JsonObject
            Log.e("project_details_input", jObject.toString())
            ProfileDetailsApi(jObject)

        } else if (from_screen == "special_cause") {

            val objspl = JSONObject()
            objspl.put("userId", userId)
            objspl.put("specialCauseId", specialCauseId)
            val jParserspl = JsonParser()
            val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
            Log.e("project_details_input", jObjectspl.toString())
            ProfileDetailsSpecialCauseApi(jObjectspl)

            /*donationsAdapter = DonationsAdapter(this@DonorsDetailViewActivity, specialcauseDonationsArrayList)
            specialcauseUpdatesAdapter = ChurchUpdatesAdapter(this@DonorsDetailViewActivity, specialcauseUpdatesArrayList)*/
        }
        if (from_screen == "project") {
            tv_projects.text = "Project Donations"
        } else if (from_screen == "special_cause") {
            tv_projects.text = "Special Cause Donations"
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet

        tv_projects = findViewById(R.id.tv_projects)
        header = findViewById(R.id.header)
        rv_updates = findViewById(R.id.rv_updates)
        tv_updates_empty = findViewById(R.id.tv_updates_empty)
        rv_donations = findViewById(R.id.rv_donations)
        tv_donations_empty = findViewById(R.id.tv_donations_empty)
        iv_back = findViewById(R.id.iv_back)

        lm_updates =
            LinearLayoutManager(this@DonorsDetailViewActivity, RecyclerView.VERTICAL, false)
        lm_donations =
            LinearLayoutManager(this@DonorsDetailViewActivity, RecyclerView.VERTICAL, false)

        rv_updates.layoutManager = lm_updates
        rv_donations.layoutManager = lm_donations

        rv_updates.isNestedScrollingEnabled = false
        rv_donations.isNestedScrollingEnabled = false
        //donationsAdapter.notifyDataSetChanged()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun ProfileDetailsApi(json_obj: JsonObject) {
        loading_dialog.show()

        projectDonationsArrayList = ArrayList()
        specialcauseDonationsArrayList = ArrayList()
        specialcauseDonationsArrayList.clear()
        projectDonationsArrayList.clear()
        projectUpdatesArrayList = ArrayList()
        projectUpdatesArrayList.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectDetailsApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                loading_dialog.dismiss()
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val project_details = jsondata.project!![0]
                        if (!jsondata.projectDonations!!.size.equals(null)) {
                            for (i in 0 until jsondata.projectDonations!!.size) {
                                specialcauseDonationsArrayList.add(
                                    response.body()!!.projectDonations!!.get(
                                        i
                                    )
                                )
                            }
                        }
                        val adapter = DonationsAdapter(
                            this@DonorsDetailViewActivity,
                            specialcauseDonationsArrayList
                        )
                        rv_donations.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun ProfileDetailsSpecialCauseApi(json_obj: JsonObject) {
        loading_dialog.show()
        specialcauseDonationsArrayList = ArrayList()
        specialcauseUpdatesArrayList = ArrayList()
        specialcauseDonationsArrayList.clear()
        specialcauseUpdatesArrayList.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectDetailsSpecialCauseApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                loading_dialog.dismiss()
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val specialcausedetails_details = jsondata.specialCause!![0]
                        val name = specialcausedetails_details.name
                        val description = specialcausedetails_details.description
                        val endDate = specialcausedetails_details.endDate
                        val goalAmount = specialcausedetails_details.goalAmount
                        val orgId = specialcausedetails_details.orgId
                        orgName = specialcausedetails_details.orgName.toString()
                        val startDate = specialcausedetails_details.startDate
                        val status = specialcausedetails_details.status

                        //seekBar.progress = goalAmount!!.toInt()
                        /* if (endDate != null) {
                             val formatter =
                                 SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.US)
                             val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                             val date = formatter.parse(endDate)!!
                           val date_stg = sdf.format(date)
                             Log.d("date", "date $date $date_stg")
                             tv_last_day.setText("Last Day to Donate: "+date_stg)
                         }
                         */
                        if (!jsondata.specialCauseDonations!!.size.equals(null)) {
                            for (i in 0 until jsondata.specialCauseDonations.size) {
                                specialcauseDonationsArrayList.add(
                                    response.body()!!.specialCauseDonations!![i]
                                )
                            }
                        }
                        if (!jsondata.specialCauseUpdates!!.size.equals(null)) {
                            for (i in 0 until jsondata.specialCauseUpdates.size) {
                                specialcauseUpdatesArrayList.add(
                                    response.body()!!.specialCauseUpdates!![i]
                                )
                            }
                        }
                        val adapter = DonationsAdapterWithComments(
                            this@DonorsDetailViewActivity,
                            specialcauseDonationsArrayList
                        )
                        rv_donations.adapter = adapter
                        adapter.notifyDataSetChanged()
                        val adapter_updates = ChurchUpdatesAdapter(
                            this@DonorsDetailViewActivity,
                            specialcauseUpdatesArrayList
                        )
                        rv_updates.adapter = adapter_updates
                        adapter.notifyDataSetChanged()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
