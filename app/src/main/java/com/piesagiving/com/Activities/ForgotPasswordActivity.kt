package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var et_email: EditText
    lateinit var tv_submit: TextView
    lateinit var iv_back: ImageView
    lateinit var loading_dialog: Dialog

    lateinit var cd: ConnectionDetector
    var isNetWorkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    var userId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_forgot_password)
        initialize()

        iv_back.setOnClickListener {
            onBackPressed()
        }
        tv_submit.setOnClickListener {
            if (isNetWorkAvailable) {
                if (et_email.text.toString().isEmpty()) {
                    Toast.makeText(
                        this@ForgotPasswordActivity,
                        "Email should not be empty",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (!isValidEmail(et_email.text.toString())) {
                    Toast.makeText(
                        this@ForgotPasswordActivity,
                        "Invalid E-mail address",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    val jObject = JSONObject()
                    jObject.put("email", et_email.text.toString())
                    //jObject.put("userId", userId)
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(jObject.toString()) as JsonObject
                    sendOtpApi(finalObject = finalObj)
                }
            } else {
                Toast.makeText(
                    this@ForgotPasswordActivity,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!
        cd = ConnectionDetector(this)
        isNetWorkAvailable = cd.isConnectingToInternet
        et_email = findViewById(R.id.et_email)
        tv_submit = findViewById(R.id.tv_submit)
        iv_back = findViewById(R.id.iv_back)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ForgotPasswordActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ForgotPasswordActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun sendOtpApi(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.forgotPasswordApi(finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Log.d("sendOtp", "sendOtpApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ForgotPasswordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val intent =
                            Intent(this@ForgotPasswordActivity, ResetPasswordActivity::class.java)
                        intent.putExtra("email", et_email.text.toString())
                        startActivity(intent)
                    } else {
                        Toast.makeText(this@ForgotPasswordActivity, resp.result, Toast.LENGTH_SHORT).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
