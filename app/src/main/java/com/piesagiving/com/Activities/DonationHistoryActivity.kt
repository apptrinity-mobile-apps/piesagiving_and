package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.DonationHistoryPojoResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class DonationHistoryActivity : AppCompatActivity() {

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    lateinit var ll_reports_id: LinearLayout
    lateinit var cv_totaldonations: CardView
    lateinit var tv_totaldonations_price: TextView
    lateinit var tv_totaldonations: TextView

    lateinit var loading_dialog: Dialog
    lateinit var cv_totalchurchdonations: CardView
    lateinit var tv_totalchurchdonations_price: TextView
    lateinit var tv_totalchurchdonations: TextView

    lateinit var cv_totalcharitydonations: CardView
    lateinit var tv_totalcharitydonations_price: TextView
    lateinit var tv_totalcharitydonations: TextView

    lateinit var cv_totalspecialcausedonations: CardView
    lateinit var tv_totalspecialcausedonations_price: TextView
    lateinit var tv_totalspecialcausedonations: TextView

    lateinit var iv_back: ImageView
    lateinit var sp_room_no_id: Spinner
    var yearType = ""
    private var userId = ""

    lateinit var year_types_array: ArrayList<String>
    lateinit var year_types_adapter: ArrayAdapter<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_donation_history)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        year_types_array = ArrayList()
        // default year type set to 'All time'
        yearType = "alltime"
        TotalDonationHistory(yearType, true)
        iv_back.setOnClickListener {
            onBackPressed()
        }

        sp_room_no_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                yearType = year_types_array[position]
                if (yearType == "All Time") {
                    yearType = "alltime"
                }
                TotalDonationHistory(yearType, false)
                if (yearType == "alltime") {
                    yearType = "All Time"
                }
                tv_totaldonations.text = "$yearType Donations"
                tv_totalchurchdonations.text = "$yearType ${getString(R.string.places_of_worship)} Donations"
                tv_totalcharitydonations.text = "$yearType Charity Donations"
//                tv_totalspecialcausedonations.text = "$yearType Special Cause Donations"
                tv_totalspecialcausedonations.text = "$yearType Fund Raisers Donations"
                Log.e("type_name_stg", yearType)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }

    }

    fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        isInternetConnected = cd.isConnectingToInternet

        iv_back = findViewById(R.id.iv_back)
        sp_room_no_id = findViewById(R.id.sp_room_no_id)
        cv_totaldonations = findViewById(R.id.cv_totaldonations)
        tv_totaldonations_price = findViewById(R.id.tv_totaldonations_price)
        tv_totaldonations = findViewById(R.id.tv_totaldonations)

        cv_totalchurchdonations = findViewById(R.id.cv_totalchurchdonations)
        tv_totalchurchdonations_price = findViewById(R.id.tv_totalchurchdonations_price)
        tv_totalchurchdonations = findViewById(R.id.tv_totalchurchdonations)

        cv_totalcharitydonations = findViewById(R.id.cv_totalcharitydonations)
        tv_totalcharitydonations_price = findViewById(R.id.tv_totalcharitydonations_price)
        tv_totalcharitydonations = findViewById(R.id.tv_totalcharitydonations)

        cv_totalspecialcausedonations = findViewById(R.id.cv_totalspecialcausedonations)
        tv_totalspecialcausedonations_price = findViewById(R.id.tv_totalspecialcausedonations_price)
        tv_totalspecialcausedonations = findViewById(R.id.tv_totalspecialcausedonations)

        ll_reports_id = findViewById(R.id.ll_reports_id)

        ll_reports_id.setOnClickListener {
            val intent = Intent(this, ReportsActivity::class.java)
            startActivity(intent)
        }
        cv_totaldonations.setOnClickListener {
            val intent = Intent(this, TotalDonationsActivity::class.java)
            intent.putExtra("header", getString(R.string.total_donations))
            intent.putExtra("type", "")
            startActivity(intent)
        }
        cv_totalchurchdonations.setOnClickListener {
            val intent = Intent(this, TotalDonationsActivity::class.java)
            intent.putExtra("header", getString(R.string.total_places_donations))
            intent.putExtra("type", "church")
            startActivity(intent)
        }
        cv_totalcharitydonations.setOnClickListener {
            val intent = Intent(this, TotalDonationsActivity::class.java)
            intent.putExtra("header", getString(R.string.total_charity_donations))
            intent.putExtra("type", "charity")
            startActivity(intent)
        }
        cv_totalspecialcausedonations.setOnClickListener {
            val intent = Intent(this, TotalDonationsActivity::class.java)
//            intent.putExtra("header", getString(R.string.total_special_causes_donations))
//            intent.putExtra("type", "special_cause")
            intent.putExtra("header", getString(R.string.total_fund_raisers_donations))
            intent.putExtra("type", "fundraiser")
            startActivity(intent)
        }
    }

    private fun TotalDonationHistory(yearstg: String, load_spinner: Boolean) {
        val obj = JSONObject()
        obj.put("userId", userId)
        obj.put("year", yearstg)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("dash_bard_input", jObject.toString())
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.totalDonationsApi(jObject)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationHistoryPojoResponse> {
            override fun onFailure(call: Call<DonationHistoryPojoResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<DonationHistoryPojoResponse>,
                response: Response<DonationHistoryPojoResponse>?
            ) {

                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        val totalCharitySum = response.body()!!.totalCharitySum
                        val totalChurchSum = response.body()!!.totalChurchSum
                        val totalDonationSum = response.body()!!.totalDonationSum
                        val totalSpecialCauseSum = response.body()!!.totalFundraiserSum
                        val totalFundraiserSum = response.body()!!.totalFundraiserSum
                        if (load_spinner) {
                            year_types_array = ArrayList()
                            year_types_array = response.body()!!.unqYears!!
                            year_types_adapter = ArrayAdapter(
                                this@DonationHistoryActivity,
                                R.layout.support_simple_spinner_dropdown_item,
                                year_types_array
                            )
                            sp_room_no_id.adapter = year_types_adapter
                        }
                        Log.e(
                            "DonationHistiryValues",
                            "Charity - " + totalCharitySum + "Church - " + totalChurchSum + "Total - " + totalDonationSum + "Speciacl - " + totalSpecialCauseSum
                        )
                        val totalcharityDecimal = Formatter().format("%,.2f", totalCharitySum)
                        val totalChurchDecimal = Formatter().format("%,.2f", totalChurchSum)
                        val totalDonationDecimal = Formatter().format("%,.2f", totalDonationSum)
                        val totalSpecialCauseDecimal = Formatter().format("%,.2f", totalSpecialCauseSum)
                        val totalFundraiserSumDecimal = Formatter().format("%,.2f", totalFundraiserSum)
                        tv_totaldonations_price.text = totalDonationDecimal.toString()
                        tv_totalchurchdonations_price.text = totalChurchDecimal.toString()
                        tv_totalcharitydonations_price.text = totalcharityDecimal.toString()
//                        tv_totalspecialcausedonations_price.text = totalSpecialCauseDecimal.toString()
                        tv_totalspecialcausedonations_price.text = totalFundraiserSumDecimal.toString()
                    } else {
                        Toast.makeText(
                            this@DonationHistoryActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}
