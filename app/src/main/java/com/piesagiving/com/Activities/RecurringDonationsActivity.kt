package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.piesagiving.com.Fragments.CharitiesFragment
import com.piesagiving.com.Fragments.ChurchesFragment
import com.piesagiving.com.Helpers.NonSwipeableViewPager
import com.piesagiving.com.R


@Suppress("DEPRECATION")
class RecurringDonationsActivity : AppCompatActivity() {

    lateinit var viewpager: ViewPager
    lateinit var tabLayout: TabLayout

    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView

    lateinit var iv_back: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_recurring_donations)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
    }

    fun initialize() {
        iv_back = findViewById(R.id.iv_back)
        viewpager = findViewById<NonSwipeableViewPager>(R.id.viewpager)
        viewpager.offscreenPageLimit = 1

        setupViewPager(viewpager)

        tabLayout = findViewById(R.id.tabLayout)
        tabLayout.setupWithViewPager(viewpager)
        setupTabIcons()

        tabLayout.addOnTabSelectedListener(object :
            TabLayout.BaseOnTabSelectedListener<TabLayout.Tab> {
            override fun onTabSelected(tab: TabLayout.Tab) {
                Log.e("data_tabs", tab.position.toString() + "" + tab.text)

                /*Fragment f = mAdapter.getFragment(mPager.getCurrentItem());
               f.loadData();*/

                if (tab.position == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(
                            resources.getColor(
                                R.color.login_button_background,
                                theme
                            )
                        )
                        tabTwo.setTextColor(resources.getColor(R.color.black, theme))
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.login_button_background))
                        tabTwo.setTextColor(resources.getColor(R.color.black))
                    }
                    tabOne.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_church_selected_new,
                        0,
                        0
                    )
                    tabTwo.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_charity_unselected_new,
                        0,
                        0
                    )

                } else if (tab.position == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(resources.getColor(R.color.black, theme))
                        tabTwo.setTextColor(
                            resources.getColor(
                                R.color.login_button_background,
                                theme
                            )
                        )
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.black))
                        tabTwo.setTextColor(resources.getColor(R.color.login_button_background))
                    }
                    tabOne.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_church_unselected_new,
                        0,
                        0
                    )
                    tabTwo.setCompoundDrawablesWithIntrinsicBounds(
                        0,
                        R.drawable.ic_charity_selected_new,
                        0,
                        0
                    )
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

        })

        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupTabIcons() {

        /*tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.ic_church_selected_new));
        for (i in 0 until tabLayout.getTabCount()) {
            val tab: TabLayout.Tab = tabLayout.getTabAt(i)!!
            tab?.setCustomView(R.layout.custom_tab)
        }*/
        tabOne = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabOne.text = getString(R.string.places_of_worship)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tabOne.setTextColor(resources.getColor(R.color.login_button_background, theme))
        } else {
            tabOne.setTextColor(resources.getColor(R.color.login_button_background))
        }
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_church_selected_new, 0, 0)
        tabLayout.getTabAt(0)!!.customView = tabOne
        tabOne.compoundDrawablePadding = 10

        tabTwo = layoutInflater.inflate(R.layout.textonly_layout, null) as TextView
        tabTwo.text = getString(R.string.charities)
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_charity_unselected_new, 0, 0)
        tabLayout.getTabAt(1)!!.customView = tabTwo
        tabTwo.compoundDrawablePadding = 10

    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFrag(ChurchesFragment(), getString(R.string.places_of_worship))
        adapter.addFrag(CharitiesFragment(), getString(R.string.charity))
        viewPager.adapter = adapter

    }

    internal inner class ViewPagerAdapter(manager: FragmentManager) :
        FragmentPagerAdapter(manager) {
        private val mFragmentList = java.util.ArrayList<Fragment>()
        private val mFragmentTitleList = java.util.ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFrag(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence {
            return mFragmentTitleList[position]
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@RecurringDonationsActivity, DashBoardActivity::class.java)
        intent.putExtra("to_fragment", "wallet")
        intent.putExtra("cat_id", "")
        intent.putExtra("show_lock", "no")
        startActivity(intent)
        finish()
    }

}
