package com.piesagiving.com.Activities

import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.GetUserPrivacyResponse
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PrivacyActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var radio_privacy: RadioGroup
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var radioglobal = ""

    private var radioButton: RadioButton? = null
    private var radioButtonprivacy: RadioButton? = null
    private var radioButtonpublic: RadioButton? = null
    private var radioButtonOnlyMe: RadioButton? = null
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var userPrivacy = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_privacy)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        if (userPrivacy == "null") {
            Log.d("userPrivacy", userPrivacy)
            radioglobal = "private"
            val objspll = JSONObject()
            objspll.put("userId", userId)
            val jParserspll = JsonParser()
            val jObjectspll = jParserspll.parse(objspll.toString()) as JsonObject
            Log.e("UserPrivacyApi", jObjectspll.toString())
            getUserPrivacyApi(jObjectspll)
        } else {
            Log.d("userPrivacy", userPrivacy)
            radioglobal = userPrivacy
            when (radioglobal) {
                "public" -> {
                    radioButtonpublic!!.isChecked = true
                    radioButtonprivacy!!.isChecked = false
                    radioButtonOnlyMe!!.isChecked = false
                }
                "private" -> {
                    radioButtonpublic!!.isChecked = false
                    radioButtonprivacy!!.isChecked = true
                    radioButtonOnlyMe!!.isChecked = false
                }
                "onlyme" -> {
                    radioButtonpublic!!.isChecked = false
                    radioButtonprivacy!!.isChecked = false
                    radioButtonOnlyMe!!.isChecked = true
                }
            }
        }

        radio_privacy.setOnCheckedChangeListener { radioGroup, i ->
            val radioclicked = radioGroup.checkedRadioButtonId
            radioButton = findViewById(radioclicked)

            when (radioclicked) {
                R.id.radioButtonpublic -> {
                    radioglobal = "public"
                }
                R.id.radioButtonprivacy -> {
                    radioglobal = "private"
                }
                R.id.rb_only_me -> {
                    radioglobal = "onlyme"
                }
            }

            val objspl = JSONObject()
            objspl.put("userId", userId)
            objspl.put("privacy", radioglobal)
            val jParserspl = JsonParser()
            val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
            Log.e("UserPrivacyApi", jObjectspl.toString())
            UserPrivacyApi(jObjectspl)
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        userPrivacy = user_details[SessionManager.PRIVACY_KEY]!!
        iv_back = findViewById(R.id.iv_back)

        radio_privacy = findViewById(R.id.radio_privacy)
        radioButtonpublic = findViewById(R.id.radioButtonpublic)
        radioButtonprivacy = findViewById(R.id.radioButtonprivacy)
        radioButtonOnlyMe = findViewById(R.id.rb_only_me)

        val font = Typeface.createFromAsset(assets, "fonts/Poppins-Medium.ttf")
        radioButtonprivacy!!.typeface = font
        radioButtonpublic!!.typeface = font
        radioButtonOnlyMe!!.typeface = font

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun getUserPrivacyApi(json_obj: JsonObject) {
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getUserPrivacy(json_obj)
        call.enqueue(object : Callback<GetUserPrivacyResponse> {
            override fun onFailure(call: Call<GetUserPrivacyResponse>, t: Throwable) {
                Log.e("response_privacy", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GetUserPrivacyResponse>,
                response: Response<GetUserPrivacyResponse>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result = jsondata.result!!.privacy!!
                        Log.d("userPrivacy", result)
                        if (result == "private") {
                            radioglobal = "private"
                        } else if (result == "public") {
                            radioglobal = "public"
                        } else if (result == "onlyme") {
                            radioglobal = "onlyme"
                        }
                        sessionManager.updatePrivacy(radioglobal)
                        if (radioglobal == "private") {
                            radioButtonprivacy!!.isChecked = true
                            radioButtonpublic!!.isChecked = false
                            radioButtonOnlyMe!!.isChecked = false

                        } else if (radioglobal == "public") {
                            radioButtonprivacy!!.isChecked = false
                            radioButtonpublic!!.isChecked = true
                            radioButtonOnlyMe!!.isChecked = false
                        } else if (radioglobal == "onlyme") {
                            radioButtonprivacy!!.isChecked = false
                            radioButtonpublic!!.isChecked = false
                            radioButtonOnlyMe!!.isChecked = true
                        }

                        //Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_SHORT).show()
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result.toString(), Toast.LENGTH_SHORT)
                            .show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun UserPrivacyApi(json_obj: JsonObject) {
        //  loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.UserPrivacyApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("response_privacy", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        if (radioglobal == "private") {
                            radioButtonprivacy!!.isChecked = true
                            radioButtonpublic!!.isChecked = false
                            radioButtonOnlyMe!!.isChecked = false
                        } else if (radioglobal == "public") {
                            radioButtonprivacy!!.isChecked = false
                            radioButtonpublic!!.isChecked = true
                            radioButtonOnlyMe!!.isChecked = false
                        } else if (radioglobal == "onlyme") {
                            radioButtonprivacy!!.isChecked = false
                            radioButtonpublic!!.isChecked = false
                            radioButtonOnlyMe!!.isChecked = true
                        }
                        sessionManager.updatePrivacy(radioglobal)
//                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
