package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.PeopleSearchAdapter
import com.piesagiving.com.Adapters.SearchByNameAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPeopleActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    var isSearchCharEmpty = true
    var from_screen = ""

    lateinit var loading_dialog: Dialog
    lateinit var iv_back: ImageView
    lateinit var et_search_people: EditText
    lateinit var rv_people_search: RecyclerView
    lateinit var tv_empty_people: TextView
    lateinit var tv_header: TextView
    lateinit var ll_search_view: LinearLayout
    lateinit var ll_people: LinearLayout
    lateinit var ll_organizations: LinearLayout
    lateinit var tv_empty_organizations: TextView
    lateinit var rv_organizations_search: RecyclerView
    lateinit var peopleList: ArrayList<UsersDataResponse>
    lateinit var orgList: ArrayList<SearchOrganizationDataResponse>
    lateinit var peopleSearchAdapter: PeopleSearchAdapter
    lateinit var searchAdapter: SearchByNameAdapter
    lateinit var tabLayout: TabLayout
    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView
    lateinit var iv_cancel: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_search_people)

        initialize()
        from_screen = intent.getStringExtra("from_screen")!!

        if (from_screen == "search") {
            tv_header.text = getString(R.string.search)
            ll_search_view.visibility = View.VISIBLE
            ll_organizations.visibility = View.VISIBLE
            tabLayout.visibility = View.VISIBLE
            ll_people.visibility = View.GONE
            tv_empty_people.text = getString(R.string.no_results_found)
        } else if (from_screen == "request") {
            tv_header.text = getString(R.string.friend_request_notifications)
            ll_search_view.visibility = View.GONE
            ll_organizations.visibility = View.GONE
            tabLayout.visibility = View.GONE
            ll_people.visibility = View.VISIBLE
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                val jsonParser = JsonParser()
                val finalObj = jsonParser.parse(obj.toString()) as JsonObject
                getFriendRequestsApi(finalObj)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }

        et_search_people.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s!!.isEmpty()) {
                    tv_empty_people.visibility = View.GONE
                    rv_people_search.visibility = View.GONE
                    tv_empty_organizations.visibility = View.GONE
                    rv_organizations_search.visibility = View.GONE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // do nothing
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty() || count > 0) {
                    val obj = JSONObject()
                    obj.put("userId", userId)
                    obj.put("orgName", s)
                    val jsonParser = JsonParser()
                    val finalObj = jsonParser.parse(obj.toString()) as JsonObject
                    searchOrganizationApi(finalObj)
                    isSearchCharEmpty = false
                } else {
                    isSearchCharEmpty = true
                    tv_empty_people.visibility = View.GONE
                    rv_people_search.visibility = View.GONE
                    tv_empty_organizations.visibility = View.GONE
                    rv_organizations_search.visibility = View.GONE
                }
            }
        })

        iv_cancel.setOnClickListener {
            if (et_search_people.text.isNotEmpty()) {
                et_search_people.setText("")
            } else {
                onBackPressed()
            }
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab!!.position == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(
                            resources.getColor(R.color.login_button_background, theme)
                        )
                        tabTwo.setTextColor(resources.getColor(R.color.black, theme))
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.login_button_background))
                        tabTwo.setTextColor(resources.getColor(R.color.black))
                    }
                    ll_organizations.visibility = View.VISIBLE
                    ll_people.visibility = View.GONE
                } else if (tab.position == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(resources.getColor(R.color.black, theme))
                        tabTwo.setTextColor(
                            resources.getColor(R.color.login_button_background, theme)
                        )
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.black))
                        tabTwo.setTextColor(resources.getColor(R.color.login_button_background))
                    }
                    ll_organizations.visibility = View.GONE
                    ll_people.visibility = View.VISIBLE
                }
            }
        })

        rv_organizations_search.addOnItemTouchListener(
            RecyclerItemClickListener(this@SearchPeopleActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val data = searchAdapter.getItemAt(position)
                        val orgId = data.id
                        val obj = JSONObject()
                        obj.put("userId", userId)
                        obj.put("orgId", orgId)
                        val jParser = JsonParser()
                        val finalObject = jParser.parse(obj.toString()) as JsonObject
                        saveRecentSearches(finalObject)
                    }
                })
        )

        rv_people_search.addOnItemTouchListener(
            RecyclerItemClickListener(this@SearchPeopleActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {}
                })
        )

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        isNetworkAvailable = cd.isConnectingToInternet

        iv_back = findViewById(R.id.iv_back)
        et_search_people = findViewById(R.id.et_search_people)
        rv_people_search = findViewById(R.id.rv_people_search)
        tv_empty_people = findViewById(R.id.tv_empty_people)
        tv_header = findViewById(R.id.tv_header)
        ll_search_view = findViewById(R.id.ll_search_view)
        ll_organizations = findViewById(R.id.ll_organizations)
        ll_people = findViewById(R.id.ll_people)
        rv_organizations_search = findViewById(R.id.rv_organizations_search)
        tv_empty_organizations = findViewById(R.id.tv_empty_organizations)
        iv_cancel = findViewById(R.id.iv_cancel)

        tabLayout = findViewById(R.id.tabLayout)
        setupTabIcons()
        val layoutManager =
            LinearLayoutManager(this@SearchPeopleActivity, RecyclerView.VERTICAL, false)
        rv_people_search.layoutManager = layoutManager
        val layoutManager1 =
            LinearLayoutManager(this@SearchPeopleActivity, RecyclerView.VERTICAL, false)
        rv_organizations_search.layoutManager = layoutManager1

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setupTabIcons() {
        val view = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        val view1 = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        tabOne = view.findViewById(R.id.tv_tab)
        tabTwo = view1.findViewById(R.id.tv_tab)
        tabOne.text = getString(R.string.organizations)
        tabTwo.text = getString(R.string.people)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tabOne.setTextColor(resources.getColor(R.color.login_button_background, theme))
        } else {
            tabOne.setTextColor(resources.getColor(R.color.login_button_background))
        }
        tabLayout.addTab(tabLayout.newTab().setCustomView(view), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1))

    }

    private fun searchOrganizationApi(final_object: JsonObject) {
        peopleList = ArrayList()
        orgList = ArrayList()
        val api = ApiInterface.create()
        val call = api.searchOrganizationApi(final_object)
        call.enqueue(object : Callback<SearchOrganizationResponse> {
            override fun onFailure(call: Call<SearchOrganizationResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@SearchPeopleActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<SearchOrganizationResponse>,
                response: Response<SearchOrganizationResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        orgList = resp.orgList!!
                        peopleList = resp.userList!!
                        searchAdapter =
                            SearchByNameAdapter(this@SearchPeopleActivity, orgList, userId)
                        rv_organizations_search.adapter = searchAdapter
                        peopleSearchAdapter =
                            PeopleSearchAdapter(
                                this@SearchPeopleActivity,
                                peopleList,
                                userId,
                                from_screen
                            )
                        rv_people_search.adapter = peopleSearchAdapter
                        peopleSearchAdapter.notifyDataSetChanged()
                        if (isSearchCharEmpty) {
                            tv_empty_people.visibility = View.GONE
                            rv_people_search.visibility = View.GONE
                            tv_empty_organizations.visibility = View.GONE
                            rv_organizations_search.visibility = View.GONE
                        } else {
                            if (peopleList.size > 0) {
                                rv_people_search.visibility = View.VISIBLE
                                tv_empty_people.visibility = View.GONE
                            } else {
                                rv_people_search.visibility = View.GONE
                                tv_empty_people.visibility = View.VISIBLE
                            }
                            if (orgList.size > 0) {
                                rv_organizations_search.visibility = View.VISIBLE
                                tv_empty_organizations.visibility = View.GONE
                            } else {
                                rv_organizations_search.visibility = View.GONE
                                tv_empty_organizations.visibility = View.VISIBLE
                            }
                        }
                    } else {
                        rv_people_search.visibility = View.GONE
                        tv_empty_people.visibility = View.VISIBLE
                        rv_organizations_search.visibility = View.GONE
                        tv_empty_organizations.visibility = View.VISIBLE
//                        Toast.makeText(this@SearchPeopleActivity, resp.result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getFriendRequestsApi(final_object: JsonObject) {
        peopleList = ArrayList()
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getFriendRequestsListApi(final_object)
        call.enqueue(object : Callback<AllUsersResponse> {
            override fun onFailure(call: Call<AllUsersResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@SearchPeopleActivity, getString(R.string.try_again), Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllUsersResponse>,
                response: Response<AllUsersResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.requested_users
                        peopleList = data!!
                        peopleSearchAdapter =
                            PeopleSearchAdapter(
                                this@SearchPeopleActivity, peopleList, userId, from_screen
                            )
                        rv_people_search.adapter = peopleSearchAdapter
                        peopleSearchAdapter.notifyDataSetChanged()
                        if (peopleList.size > 0) {
                            rv_people_search.visibility = View.VISIBLE
                            tv_empty_people.visibility = View.GONE
                        } else {
                            rv_people_search.visibility = View.GONE
                            tv_empty_people.visibility = View.VISIBLE
                        }
                    } else {
                        rv_people_search.visibility = View.GONE
                        tv_empty_people.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun saveRecentSearches(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.saveRecentSearchesApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@SearchPeopleActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
//                        Toast.makeText(this@SearchPeopleActivity, resp.result, Toast.LENGTH_SHORT).show()
                    } else {
                        // Toast.makeText(activity!!, resp.result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        if (from_screen == "request") {
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                val jsonParser = JsonParser()
                val finalObj = jsonParser.parse(obj.toString()) as JsonObject
                getFriendRequestsApi(finalObj)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}
