package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.*
import android.widget.*
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.FundRaiserDonorsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.FundraisersResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class FundraiserDonationsListActivity : AppCompatActivity() {
    private var type = ""
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var fundraiserId = ""
    private var fundraiserName = ""
    private var orgId = ""
    lateinit var loading_dialog: Dialog

    //   biometric
    lateinit var userNotifications: HashMap<String, String>
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var userEmail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView

    private lateinit var iv_back: ImageView
    private lateinit var tv_header: TextView
    private lateinit var tv_donate: TextView
    private lateinit var tv_total_donations: TextView
    private lateinit var tv_donors_empty: TextView
    private lateinit var rv_donors_list: RecyclerView
    private lateinit var lm_fundraiser_donors: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_fundraiser_donations_list)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        type = intent.getStringExtra("type")!!
        fundraiserId = intent.getStringExtra("fundraiser_id")!!
        getData()
        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }
        if (type == "all") {
            tv_header.text = "All ${getString(R.string.donations)}"
        } else if (type == "top") {
            tv_header.text = "Top ${getString(R.string.donations)}"
        }
        tv_donate.setOnClickListener {
            Haptics().vibrate(this)
            showBiometricPrompt()
        }
    }

    fun initialize() {
        loadingDialog()
        passCodeDialog()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userNotifications = sessionManager.getUserNotifications
        userId = user_details[SessionManager.ID_KEY]!!
        userEmail = user_details[SessionManager.EMAIL_KEY]!!
        biometrics = user_details[SessionManager.FACEID_KEY]!!
        passCode = user_details[SessionManager.PASSCODE_KEY]!!
        biometricManager = BiometricManager.from(this@FundraiserDonationsListActivity)
        executor = ContextCompat.getMainExecutor(this@FundraiserDonationsListActivity)
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }

        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_donate = findViewById(R.id.tv_donate)
        tv_total_donations = findViewById(R.id.tv_total_donations)
        tv_donors_empty = findViewById(R.id.tv_donors_empty)
        rv_donors_list = findViewById(R.id.rv_donors_list)

        lm_fundraiser_donors = LinearLayoutManager(this@FundraiserDonationsListActivity, RecyclerView.VERTICAL, false)
        rv_donors_list.layoutManager = lm_fundraiser_donors
        rv_donors_list.setHasFixedSize(true)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getData() {
        val obj = JSONObject()
        obj.put("userId", userId)
        obj.put("fundraiserId", fundraiserId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        getFundraiserDetails(jObject)
    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    val intent = Intent(this@FundraiserDonationsListActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    val intent = Intent(this@FundraiserDonationsListActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            val intent = Intent(this@FundraiserDonationsListActivity, FundraiserDonateActivity::class.java)
            intent.putExtra("fundraiser_id", fundraiserId)
            intent.putExtra("fundraiser_name", fundraiserName)
            intent.putExtra("org_id", orgId)
            startActivity(intent)
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)
            .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@FundraiserDonationsListActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    val intent = Intent(this@FundraiserDonationsListActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view = LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
                val intent = Intent(this@FundraiserDonationsListActivity, FundraiserDonateActivity::class.java)
                intent.putExtra("fundraiser_id", fundraiserId)
                intent.putExtra("fundraiser_name", fundraiserName)
                intent.putExtra("org_id", orgId)
                startActivity(intent)
                ll_check_email_passcode.visibility = View.GONE
            } else {
                Toast.makeText(this@FundraiserDonationsListActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj = JSONObject()
            obj.put("passcode", passCode)
            obj.put("email", userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    // forgot passcode api call
    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(this@FundraiserDonationsListActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text =
                                getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    // fundraisers api call
    private fun getFundraiserDetails(obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getFundraiserDetailsApi(obj)
        call.enqueue(object : Callback<FundraisersResponse> {
            override fun onFailure(call: Call<FundraisersResponse>, t: Throwable) {
                loading_dialog.dismiss()
                Toast.makeText(
                    this@FundraiserDonationsListActivity,
                    getString(R.string.try_again),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(call: Call<FundraisersResponse>, response: Response<FundraisersResponse>) {
                loading_dialog.dismiss()
                try {
                    val data = response.body()!!
                    if (data.responseStatus == 1) {
                        val fundraiserData = data.fundraiserData
                        orgId = fundraiserData!!.orgId!!
                        fundraiserName = fundraiserData.name!!
                        tv_total_donations.text = "${getString(R.string.donations)} (${fundraiserData.donationsCount})"
                       if (type == "all") {
                           if (fundraiserData.allDonationsList!!.isNotEmpty()) {
                               tv_donors_empty.visibility = View.GONE
                               rv_donors_list.visibility = View.VISIBLE
                               val adapter = FundRaiserDonorsAdapter(this@FundraiserDonationsListActivity, fundraiserData.allDonationsList!!)
                               rv_donors_list.adapter = adapter
                           } else {
                               tv_donors_empty.visibility = View.VISIBLE
                               rv_donors_list.visibility = View.GONE
                           }
                       } else if (type == "top") {
                           if (fundraiserData.topDonationsList!!.isNotEmpty()) {
                               tv_donors_empty.visibility = View.GONE
                               rv_donors_list.visibility = View.VISIBLE
                               val adapter = FundRaiserDonorsAdapter(this@FundraiserDonationsListActivity, fundraiserData.topDonationsList!!)
                               rv_donors_list.adapter = adapter
                           } else {
                               tv_donors_empty.visibility = View.VISIBLE
                               rv_donors_list.visibility = View.GONE
                           }
                       }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}