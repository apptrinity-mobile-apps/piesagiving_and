package com.piesagiving.com.Activities

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R

class AboutUsActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var tv_versionnumber: TextView
    lateinit var tv_termsofuse: TextView
    lateinit var tv_datapolicy: TextView
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var versionName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_aboutus)
        initialize()

        tv_datapolicy.setOnClickListener {
            val intent = Intent(this, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        tv_termsofuse.setOnClickListener {
            val intent = Intent(this, TermsAndConditions::class.java)
            startActivity(intent)
        }
        tv_versionnumber.setOnClickListener {
            try {
                val pInfo = packageManager.getPackageInfo(packageName, 0)
                versionName = pInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            Toast.makeText(this, "Current version: v$versionName", Toast.LENGTH_SHORT).show()
        }

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        tv_versionnumber = findViewById(R.id.tv_versionnumber)
        tv_termsofuse = findViewById(R.id.tv_termsofuse)
        tv_datapolicy = findViewById(R.id.tv_datapolicy)
        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

}
