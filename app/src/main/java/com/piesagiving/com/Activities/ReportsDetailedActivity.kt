package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.DetailedDonationsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.Exception

@SuppressLint("SetTextI18n")
@Suppress("DEPRECATION")
class ReportsDetailedActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    lateinit var iv_back: ImageView
    lateinit var loading_dialog: Dialog
    lateinit var tv_empty: TextView
    lateinit var tv_header: TextView

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    var userId = ""
    var orgName = ""
    var orgId = ""
    var interval = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_reports_detailed)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        try {
            interval = intent.getStringExtra("interval")!!
            orgId = intent.getStringExtra("orgId")!!
            orgName = intent.getStringExtra("orgName")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initialize()
        tv_header.text = "$orgName ${getString(R.string.reports)}"
        val jObj = JSONObject()
        jObj.put("userId", userId)
        jObj.put("orgId", orgId)
        jObj.put("interval", interval)
        val jParser = JsonParser()
        val final_object = jParser.parse(jObj.toString()) as JsonObject
        Log.e("final_object", final_object.toString())
        getDetailedReportsApi(final_object)
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        recyclerView = findViewById(R.id.recycler_view)
        tv_empty = findViewById(R.id.tv_empty)
        val layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.hasFixedSize()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getDetailedReportsApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getDonationDetailsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()

                    tv_empty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    Toast.makeText(
                        this@ReportsDetailedActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val donationsList = resp.donationsList
                            if (donationsList!!.size > 0) {
                                tv_empty.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE
                            } else {
                                tv_empty.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            }
                            val adapter =
                                DetailedDonationsAdapter(
                                    this@ReportsDetailedActivity,
                                    donationsList
                                )
                            recyclerView.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }
                        else -> {
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}