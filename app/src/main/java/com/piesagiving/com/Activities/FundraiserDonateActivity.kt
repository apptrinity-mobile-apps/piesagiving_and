package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.drawable.Animatable
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.Executor
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class FundraiserDonateActivity : AppCompatActivity() {
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var fundraiserName = ""
    private var fundraiserId = ""
    private var isAnonymousDonation = false
    private var orgId = ""
    lateinit var loading_dialog: Dialog
    lateinit var card_dialog: Dialog
    lateinit var bank_dialog: Dialog
    var payType = 0
    var checkbox_savecard_checked: Boolean = false
    var piesagivingPercentage = 0.00
    var piesaBankExtracharges = 0.00
    var piesaBankPercentage = 0.00
    var piesaCardExtracharges = 0.00
    var piesaCardPercentage = 0.00
    var stripeBankExtracharges = 0.00
    var stripeBankPercentage = 0.00
    var stripeCardExtracharges = 0.00
    var stripeCardPercentage = 0.00
    var global_stripe_percent = 0.00
    var global_stripe_total_fee = 0.00
    var global_ex_amount = 0
    var global_piesa_percent = 0.00
    var global_piesa_total_fee = 0.00
    var global_piesa_card_ex_amount = 0
    var global_piesa_bank_ex_amount = 0
    var edit_amount: Double? = 0.00
    var tip_amount: Double = 0.00

    //   biometric
    lateinit var userNotifications: HashMap<String, String>
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var userEmail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView

    lateinit var donation_types_array: ArrayList<String>
    lateinit var donation_types_array_ids: ArrayList<Int>
    private lateinit var iv_back: ImageView
    lateinit var sp_donation_type_id: Spinner
    lateinit var et_amount_id: EditText
    lateinit var et_tip_amount_id: EditText
    lateinit var tv_donate: TextView
    lateinit var array_savedcards_list: ArrayList<SavedCardsArrayDataResponse>
    lateinit var array_account_list: ArrayList<BankAccountsArrayList>
    var cards_default_source_id = ""
    var bank_default_source_id = ""
    var card_global_id = ""
    lateinit var btn_saved_card_pay: Button
    lateinit var stripe_card_id: CardInputWidget
    lateinit var et_card_holder_id: EditText
    lateinit var btn_saved_bank_pay: Button
    lateinit var get_card: Card
    lateinit var id_empty_data_id: TextView
    lateinit var rv_account_details_id: RecyclerView
    lateinit var rv_savedcardslist: RecyclerView
    lateinit var img_bank_close_id: ImageView
    lateinit var btn_pay_id: Button
    lateinit var img_close_id: ImageView
    lateinit var checkbox_savecard: CheckBox
    lateinit var tv_total_amount_id_payment: TextView
    lateinit var tv_donate_amount_id_payment: TextView
    lateinit var tv_total_amount_id: TextView
    lateinit var tv_tax_amount_id_payment: TextView
    lateinit var tv_donate_amount_id: TextView
    lateinit var tv_tax_amount_id: TextView
    lateinit var tv_organisation_name_payment: TextView
    lateinit var ll_tip_amount: LinearLayout
    lateinit var img_fee_id: ImageView
    lateinit var img_tip_fee_id: ImageView
    lateinit var tv_tip_amount_id: TextView
    lateinit var tv_tip_amount_id_payment: TextView
    lateinit var cb_anonymous: CheckBox

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_fundraiser_donate)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        donation_types_array = ArrayList()
        donation_types_array_ids = ArrayList()
        donation_types_array.add("Select Bank Account")
        donation_types_array.add("Select Credit Card")
        donation_types_array_ids.add(0)
        donation_types_array_ids.add(1)
        initialize()
        fundraiserId = intent.getStringExtra("fundraiser_id")!!
        fundraiserName = intent.getStringExtra("fundraiser_name")!!
        orgId = intent.getStringExtra("org_id")!!
        donationFeeApi()
        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }
        img_fee_id.setOnClickListener {
            processFeeHelpDialog()
        }
        img_tip_fee_id.setOnClickListener {
            tipAmountHelpDialog()
        }
        sp_donation_type_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
                payType = donation_types_array_ids[position]
                calculateAmounts(edit_amount!!, tip_amount)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
        tv_donate.setOnClickListener {
            Haptics().vibrate(this)
            if (et_amount_id.text.toString().trim() == "" || et_amount_id.text.toString().toInt() < 1) {
                toast("You must enter minimum donation amount of $1.00")
            } else {
                showBiometricPrompt()
            }
        }
        et_tip_amount_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (!s.isNullOrEmpty()) {
                        et_tip_amount_id.setSelection(et_tip_amount_id.length())
                        tip_amount = (et_tip_amount_id.text.toString()).toDouble()
                    } else {
                        tip_amount = 0.00
                    }
                    calculateAmounts(edit_amount!!, tip_amount)
                } catch (e: Exception) {
                    e.printStackTrace()
                    tip_amount = 0.00
                    calculateAmounts(edit_amount!!, tip_amount)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        et_amount_id.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (!s.isNullOrEmpty()) {
                        et_amount_id.setSelection(et_amount_id.length())
                        edit_amount = (et_amount_id.text.toString()).toDouble()
                        /*if (edit_amount!!.toInt() <= 1) {
                            Toast.makeText(this@FundraiserDonateActivity, "Amount should be greater than $1.00", Toast.LENGTH_LONG).show()
                        }*/
                        calculateAmounts(edit_amount!!, tip_amount)
                    } else {
                        edit_amount = 0.00
                        calculateAmounts(edit_amount!!, tip_amount)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    edit_amount = 0.00
                    calculateAmounts(edit_amount!!, tip_amount)
                }
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        cb_anonymous.setOnCheckedChangeListener { _, isChecked ->
            isAnonymousDonation = isChecked
        }
    }

    fun initialize() {
        loadingDialog()
        passCodeDialog()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userNotifications = sessionManager.getUserNotifications
        userId = user_details[SessionManager.ID_KEY]!!
        userEmail = user_details[SessionManager.EMAIL_KEY]!!
        biometrics = user_details[SessionManager.FACEID_KEY]!!
        passCode = user_details[SessionManager.PASSCODE_KEY]!!
        biometricManager = BiometricManager.from(this@FundraiserDonateActivity)
        executor = ContextCompat.getMainExecutor(this@FundraiserDonateActivity)
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }

        cardsDialog()
        bankAccountDialog()
        iv_back = findViewById(R.id.iv_back)
        sp_donation_type_id = findViewById(R.id.sp_donation_type_id)
        et_tip_amount_id = findViewById(R.id.et_tip_amount_id)
        et_amount_id = findViewById(R.id.et_amount_id)
        tv_donate = findViewById(R.id.tv_donate)
        tv_tax_amount_id = findViewById(R.id.tv_tax_amount_id)
        tv_donate_amount_id = findViewById(R.id.tv_donate_amount_id)
        tv_total_amount_id = findViewById(R.id.tv_total_amount_id)
        img_fee_id = findViewById(R.id.img_fee_id)
        img_tip_fee_id = findViewById(R.id.img_tip_fee_id)
        tv_tip_amount_id = findViewById(R.id.tv_tip_amount_id)
        cb_anonymous = findViewById(R.id.cb_anonymous)

        val adapter = ArrayAdapter(this@FundraiserDonateActivity, R.layout.support_simple_spinner_dropdown_item, donation_types_array)
        sp_donation_type_id.adapter = adapter
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun cardsDialog() {
        card_dialog = BottomSheetDialog(this)
        card_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        card_dialog.setCancelable(false)
        card_dialog.window!!.setLayout(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT)
        val view = View.inflate(this, R.layout.stripe_payment_dialog, null)
        card_dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.isDraggable = false
        stripe_card_id = card_dialog.findViewById(R.id.stripe_card_id)
        et_card_holder_id = card_dialog.findViewById(R.id.et_card_holder_id)
        checkbox_savecard = card_dialog.findViewById(R.id.checkbox_savecard)
        tv_organisation_name_payment = card_dialog.findViewById(R.id.tv_organisation_name_payment)
        ll_tip_amount = card_dialog.findViewById(R.id.ll_tip_amount)
        tv_tax_amount_id_payment = card_dialog.findViewById(R.id.tv_tax_amount_id_payment)
        tv_donate_amount_id_payment = card_dialog.findViewById(R.id.tv_donate_amount_id_payment)
        tv_total_amount_id_payment = card_dialog.findViewById(R.id.tv_total_amount_id_payment)
        tv_tip_amount_id_payment = card_dialog.findViewById(R.id.tv_tip_amount_id_payment)
        rv_savedcardslist = card_dialog.findViewById(R.id.rv_savedcardslist)
        btn_pay_id = card_dialog.findViewById(R.id.btn_pay_id)
        btn_saved_card_pay = card_dialog.findViewById(R.id.btn_saved_card_pay)
        img_close_id = card_dialog.findViewById(R.id.img_close_id)
        ll_tip_amount.visibility = View.VISIBLE
        tv_organisation_name_payment.visibility = View.GONE
        getSavedCards()
        checkbox_savecard.setOnCheckedChangeListener { _, isChecked ->
            checkbox_savecard_checked = isChecked
        }
        val layoutManager =
            LinearLayoutManager(this@FundraiserDonateActivity, RecyclerView.VERTICAL, false)
        rv_savedcardslist.layoutManager = layoutManager
        rv_savedcardslist.setHasFixedSize(true)
        rv_savedcardslist.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        card_global_id = array_savedcards_list[position].id!!.toString()
                        btn_saved_card_pay.visibility = View.VISIBLE
                    }
                })
        )
        img_close_id.setOnClickListener {
            card_dialog.dismiss()
        }
        btn_saved_card_pay.setOnClickListener {
            val obj = JSONObject()
            obj.put("fundraiserId", fundraiserId)
            obj.put("orgId", orgId)
            obj.put("userId", userId)
            obj.put("cardId", card_global_id)
            obj.put("donationAmount", edit_amount!!)
            obj.put("tipAmount", tip_amount)
            obj.put("totalAmount", edit_amount!! + tip_amount)
            obj.put("paymentType", 1)
            obj.put("isAnonymous", isAnonymousDonation)
            val jParser = JsonParser()
            val finalObject = jParser.parse(obj.toString()) as JsonObject
            Log.e("donation_input", finalObject.toString())
            paymentApi(finalObject)
        }
        btn_pay_id.setOnClickListener {
            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                toast("Invalid Card Data")
            } else if (et_card_holder_id.text.toString().trim() == "") {
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                loading_dialog.show()
                stripeTokenApi()
            }
        }
        TooltipCompat.setTooltipText(btn_pay_id, "Payment Button")
    }

    private fun bankAccountDialog() {
        bank_dialog = BottomSheetDialog(this)
        bank_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_dialog.setCancelable(false)
        bank_dialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        bank_dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        val view = View.inflate(this, R.layout.bank_payment_dialog, null)
        bank_dialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.isDraggable = false
        passCodeDialog.dismissWithAnimation = true
        img_bank_close_id = bank_dialog.findViewById(R.id.img_bank_close_id)
        id_empty_data_id = bank_dialog.findViewById(R.id.id_empty_data_id)
        btn_saved_bank_pay = bank_dialog.findViewById(R.id.btn_saved_bank_pay)
        rv_account_details_id = bank_dialog.findViewById(R.id.rv_account_details_id)
        img_bank_close_id.setOnClickListener {
            bank_dialog.dismiss()
        }
        getSavedBankAccounts()
        btn_saved_bank_pay.setOnClickListener {
            val obj = JSONObject()
            obj.put("fundraiserId", fundraiserId)
            obj.put("orgId", orgId)
            obj.put("userId", userId)
            obj.put("bankAccountId", card_global_id)
            obj.put("donationAmount", edit_amount!!)
            obj.put("tipAmount", tip_amount)
            obj.put("totalAmount", edit_amount!! + tip_amount)
            obj.put("paymentType", 2)
            obj.put("isAnonymous", isAnonymousDonation)
            val jParser = JsonParser()
            val finalObject = jParser.parse(obj.toString()) as JsonObject
            Log.e("donation_input", finalObject.toString())
            paymentApi(finalObject)
        }
    }

    private fun processFeeHelpDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawableResource(R.color.transparent)
        dialog.setContentView(R.layout.donation_help_screen_dialog)
        val img_help_close_id = dialog.findViewById<ImageView>(R.id.img_help_close_id)
        val tv_help_id = dialog.findViewById<TextView>(R.id.tv_help_id)
        tv_help_id.text = getString(R.string.transaction_fees_card_and_bank)
        img_help_close_id.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun tipAmountHelpDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog.window!!.setBackgroundDrawableResource(R.color.transparent)
        dialog.setContentView(R.layout.donation_help_screen_dialog)
        val img_help_close_id = dialog.findViewById<ImageView>(R.id.img_help_close_id)
        val tv_help_id = dialog.findViewById<TextView>(R.id.tv_help_id)
        tv_help_id.text = getString(R.string.tip_amount_help)
        img_help_close_id.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun calculateAmounts(donation_amount: Double, tip_amount: Double) {
        val totalAmount = donation_amount + tip_amount
        val strpextracharge: Double
        val piesaextracharge: Double
        if (totalAmount == 0.00) {
            strpextracharge = 0.00
            piesaextracharge = 0.00
        } else {
            strpextracharge = (stripeBankExtracharges)
            piesaextracharge = (piesaBankExtracharges)
        }
        if (payType == 0) {
            val decimal =
                (totalAmount * (stripeBankPercentage / 100)) + (strpextracharge) + (totalAmount * (piesaBankPercentage / 100)) + piesaextracharge
            val tax = "%.2f".format(decimal)
            tv_tax_amount_id.text = "$${Formatter().format("%,.2f", decimal)}"
            tv_tax_amount_id_payment.text = "$${Formatter().format("%,.2f", decimal)}"
            tv_donate_amount_id.text = "$${Formatter().format("%,.2f", totalAmount - tax.toDouble())}"
            tv_donate_amount_id_payment.text = "$${Formatter().format("%,.2f", totalAmount - tax.toDouble())}"
            tv_total_amount_id.text = Formatter().format("%,.2f", totalAmount).toString()
            tv_total_amount_id_payment.text = Formatter().format("%,.2f", totalAmount).toString()
            tv_tip_amount_id.text = "$${Formatter().format("%,.2f", tip_amount)}"
            tv_tip_amount_id_payment.text = "$${Formatter().format("%,.2f", tip_amount)}"
            global_stripe_percent = stripeBankPercentage
            global_stripe_total_fee = "%.2f".format((totalAmount * stripeBankPercentage / 100)).toDouble()
            global_ex_amount = (stripeBankExtracharges * 100).toInt()
            global_piesa_percent = piesaBankPercentage
            global_piesa_total_fee = "%.2f".format((totalAmount * piesaBankPercentage / 100)).toDouble()
            global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()
        } else {
            val decimal =
                (totalAmount * (stripeCardPercentage / 100)) + (strpextracharge) + (totalAmount * (piesagivingPercentage / 100)) + piesaextracharge
            val tax = "%.2f".format(decimal)
            tv_tax_amount_id.text = "$${Formatter().format("%,.2f", decimal)}"
            tv_tax_amount_id_payment.text = "$${Formatter().format("%,.2f", decimal)}"
            tv_donate_amount_id.text = "$${Formatter().format("%,.2f", totalAmount - tax.toDouble())}"
            tv_donate_amount_id_payment.text = "$${Formatter().format("%,.2f", totalAmount - tax.toDouble())}"
            tv_total_amount_id.text = Formatter().format("%,.2f", totalAmount).toString()
            tv_total_amount_id_payment.text = Formatter().format("%,.2f", totalAmount).toString()
            tv_tip_amount_id.text = "$${Formatter().format("%,.2f", tip_amount)}"
            tv_tip_amount_id_payment.text = "$${Formatter().format("%,.2f", tip_amount)}"
            global_stripe_percent = stripeCardPercentage
            global_stripe_total_fee = "%.2f".format((totalAmount * stripeCardPercentage / 100)).toDouble()
            global_ex_amount = (stripeCardExtracharges * 100).toInt()
            global_piesa_percent = piesaCardPercentage
            global_piesa_total_fee = "%.2f".format((totalAmount * piesaCardPercentage / 100)).toDouble()
            global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
        }
    }

    private fun getSavedCards() {
        val obj = JSONObject()
        obj.put("userId", userId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        savedCardsListApi(jObject)
    }

    private fun getSavedBankAccounts() {
        val obj = JSONObject()
        obj.put("userId", userId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        accountsListApi(jObject)
    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    if (payType == 0) {
                        bank_dialog.show()
                    } else {
                        stripe_card_id.clear()
                        checkbox_savecard_checked = false
                        et_card_holder_id.setText("")
                        card_dialog.show()
                    }
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    if (payType == 0) {
                        bank_dialog.show()
                    } else {
                        stripe_card_id.clear()
                        checkbox_savecard_checked = false
                        et_card_holder_id.setText("")
                        card_dialog.show()
                    }
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            if (payType == 0) {
                bank_dialog.show()
            } else {
                stripe_card_id.clear()
                checkbox_savecard_checked = false
                et_card_holder_id.setText("")
                card_dialog.show()
            }
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)
            .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@FundraiserDonateActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    if (payType == 0) {
                        bank_dialog.show()
                    } else {
                        stripe_card_id.clear()
                        checkbox_savecard_checked = false
                        et_card_holder_id.setText("")
                        card_dialog.show()
                    }
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())

                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view = LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
                if (payType == 0) {
                    bank_dialog.show()
                } else {
                    stripe_card_id.clear()
                    checkbox_savecard_checked = false
                    et_card_holder_id.setText("")
                    card_dialog.show()
                }
                ll_check_email_passcode.visibility = View.GONE
            } else {
                Toast.makeText(this@FundraiserDonateActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj = JSONObject()
            obj.put("passcode", passCode)
            obj.put("email", userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    // forgot passcode api call
    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(this@FundraiserDonateActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text = getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun savedCardsListApi(jsonObj: JsonObject) {
        loading_dialog.show()
        array_savedcards_list = ArrayList()
        array_savedcards_list.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.savedCardsList(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<SavedCardListResponse> {
            override fun onFailure(call: Call<SavedCardListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(call: Call<SavedCardListResponse>, response: Response<SavedCardListResponse>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val resp = response.body()
                    Log.e("STRIPERESPOMNSEDONATION", resp.toString())
                    if (response.body()!!.responseStatus == "1") {
                        for (i in 0 until response.body()!!.savedCardsList!!.data!!.size) {
                            array_savedcards_list.add(response.body()!!.savedCardsList!!.data!![i])
                        }
                        cards_default_source_id =
                            response.body()!!.customerDetails!!.default_source!!
                        Log.e("cards_default_source_id", cards_default_source_id)
                        rv_savedcardslist.visibility = View.VISIBLE
                        val adapter = SavedCardListAdapter(this@FundraiserDonateActivity, array_savedcards_list, "no", cards_default_source_id)
                        rv_savedcardslist.adapter = adapter
                    } else {
                        toast(response.body()!!.result!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun accountsListApi(jsonObj: JsonObject) {
        loading_dialog.show()
        array_account_list = ArrayList()
        array_account_list.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(call: Call<BankAccountListPojo>, response: Response<BankAccountListPojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response.body().toString())
                    if (response.body()!!.responseStatus == 1) {
                        if (response.body()!!.bankAccountList!!.data!!.size.equals(0)) {
                            id_empty_data_id.visibility = View.VISIBLE
                        }
                        for (i in 0 until response.body()!!.bankAccountList!!.data!!.size) {
                            array_account_list.add(response.body()!!.bankAccountList!!.data!![i])
                        }
                        bank_default_source_id = response.body()!!.customerDetails!!.default_source!!
                        rv_account_details_id.visibility = View.VISIBLE
                        val adapter = BankListAdapter(this@FundraiserDonateActivity, array_account_list, bank_default_source_id)
                        rv_account_details_id.setHasFixedSize(true)
                        rv_account_details_id.adapter = adapter
                    } else {
                        id_empty_data_id.visibility = View.VISIBLE
                        toast(response.body()!!.result!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun donationFeeApi() {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetExtraFeeApi()
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationFeePojo> {
            override fun onFailure(call: Call<DonationFeePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(call: Call<DonationFeePojo>, response: Response<DonationFeePojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response.body()!!.responseStatus!! == 1) {
                        piesaBankPercentage = response.body()!!.stripeFeeDetails!!.piesaBankPercentage!!
                        piesaBankExtracharges = response.body()!!.stripeFeeDetails!!.piesaBankExtracharges!!
                        piesaCardPercentage = response.body()!!.stripeFeeDetails!!.piesaCardPercentage!!
                        piesaCardExtracharges = response.body()!!.stripeFeeDetails!!.piesaCardExtracharges!!
                        piesagivingPercentage = response.body()!!.stripeFeeDetails!!.piesagivingPercentage!!
                        stripeBankExtracharges = response.body()!!.stripeFeeDetails!!.stripeBankExtracharges!!
                        stripeBankPercentage = response.body()!!.stripeFeeDetails!!.stripeBankPercentage!!
                        stripeCardExtracharges = response.body()!!.stripeFeeDetails!!.stripeCardExtracharges!!
                        stripeCardPercentage = response.body()!!.stripeFeeDetails!!.stripeCardPercentage!!
                        /*val defaultAmount = 0
                        if (payType == 0) {
                            val strpextracharge = (stripeBankExtracharges)
                            val piesaextracharge = (piesaBankExtracharges)
                            val decimal =
                                (defaultAmount * (stripeBankPercentage / 100)) + (strpextracharge) + (defaultAmount * (piesaBankPercentage / 100)) + piesaextracharge
                            val tax = "%.2f".format(decimal)
                            tv_tax_amount_id.text = "$$tax"
                            tv_tax_amount_id_payment.text = "$$tax"
                            tv_donate_amount_id.text = "$" + (defaultAmount.toDouble() - tax.toDouble())
                            tv_donate_amount_id_payment.text = "$" + (defaultAmount.toDouble() - tax.toDouble())
                            tv_total_amount_id.text = "%.2f".format(defaultAmount.toDouble())
                            tv_total_amount_id_payment.text = "%.2f".format(defaultAmount.toDouble())
                            global_stripe_percent = stripeBankPercentage
                            global_stripe_total_fee = "%.2f".format((defaultAmount * stripeBankPercentage / 100)).toDouble()
                            global_ex_amount = (stripeBankExtracharges * 100).toInt()
                            global_piesa_percent = piesaBankPercentage
                            global_piesa_total_fee = "%.2f".format((defaultAmount * piesaBankPercentage / 100)).toDouble()
                            global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()
                        } else {
                            val strpextracharge = (stripeCardExtracharges)
                            val piesaextracharge = (piesaCardExtracharges)
                            val decimal =
                                (defaultAmount * (stripeCardPercentage / 100)) + (strpextracharge) + (defaultAmount * (piesagivingPercentage / 100)) + piesaextracharge
                            val tax = "%.2f".format(decimal)
                            tv_tax_amount_id.text = "$$tax"
                            tv_tax_amount_id_payment.text = "$$tax"
                            tv_donate_amount_id.text = "$" + (defaultAmount.toDouble() - tax.toDouble())
                            tv_donate_amount_id_payment.text = "$" + (defaultAmount.toDouble() - tax.toDouble())
                            tv_total_amount_id.text = "%.2f".format(defaultAmount.toDouble())
                            tv_total_amount_id_payment.text = String.format("%.2f", (defaultAmount.toDouble()))
                            global_stripe_percent = stripeCardPercentage
                            global_stripe_total_fee = "%.2f".format((defaultAmount * stripeCardPercentage / 100)).toDouble()
                            global_ex_amount = (stripeCardExtracharges * 100).toInt()
                            global_piesa_percent = piesaCardPercentage
                            global_piesa_total_fee = "%.2f".format((defaultAmount * piesaCardPercentage / 100)).toDouble()
                            global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                        }*/
                    } else {
                        Log.e("failed_0", response.body()!!.result!!.toString())
                        Toast.makeText(this@FundraiserDonateActivity, response.body()!!.result!!.toString(), Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun stripeTokenApi() {
        val validation = get_card.validateCard()
        if (validation) {
            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
                number = get_card.number!!,
                exp_month = get_card.expMonth!!,
                exp_year = get_card.expYear!!,
                cvc = get_card.cvc!!,
                name = et_card_holder_id.text.toString()
            )
            Log.e("input_card", get_card.number!! + "--" + get_card.expMonth!! + "--" + get_card.expYear!! + "--" + get_card.cvc!!)
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)
                }

                override fun onResponse(call: Call<CardTokenPojo>, response: Response<CardTokenPojo>) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {
                        val obj = JSONObject()
                        obj.put("fundraiserId", fundraiserId)
                        obj.put("orgId", orgId)
                        obj.put("userId", userId)
                        obj.put("cardToken", response.body()!!.id!!.toString())
                        obj.put("donationAmount", edit_amount!!)
                        obj.put("tipAmount", tip_amount)
                        obj.put("totalAmount", edit_amount!! + tip_amount)
                        obj.put("cardSave", checkbox_savecard_checked)
                        obj.put("paymentType", 1)
                        obj.put("isAnonymous", isAnonymousDonation)
                        val jParser = JsonParser()
                        val finalObject = jParser.parse(obj.toString()) as JsonObject
                        Log.e("donation_input", finalObject.toString())
                        paymentApi(finalObject)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            /*End Card Token API*/
        } else if (!get_card.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun paymentApi(jsonObj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.donateFundraiserApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(call: Call<RegisterUserResponse>, response: Response<RegisterUserResponse>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("STRIPERESPOMNSEDONATION", response.body().toString())
                    if (response.body()!!.responseStatus == "1") {
                        val intent = Intent(this@FundraiserDonateActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "fundraiser")
                        intent.putExtra("from_type", "project")
                        intent.putExtra("special_cause_id", "")
                        intent.putExtra("display_name", fundraiserName)
                        intent.putExtra("project_name", "")
                        intent.putExtra("paid_amount", "%.2f".format(edit_amount!!.toDouble() + tip_amount))
                        startActivity(intent)
                        finish()
                        Toast.makeText(this@FundraiserDonateActivity, "Payment Success!", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this@FundraiserDonateActivity, "Payment Failed!", Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class BankListAdapter(context: Context, val account_list: ArrayList<BankAccountsArrayList>, var bank_default_source_id: String) :
        RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_wallet_accounts, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {
            holder.tv_bank_name_id.text = account_list[position].bank_name
            holder.tv_ac_no_id.text = "*******" + account_list[position].last4
            holder.tv_ac_holder_name_id.text = account_list[position].account_holder_name
            holder.img_delete_id.visibility = View.GONE
            if (account_list[position].status.equals("new")) {
                holder.cv_setupwallet.visibility = View.GONE
            } else {
                (holder.img_verify_id.drawable as Animatable).start()
                holder.tv_verify_id.visibility = View.GONE
                holder.img_verify_id.visibility = View.GONE
                holder.cv_setupwallet.visibility = View.VISIBLE
                holder.ll_verify_amount_id.visibility = View.GONE
            }
            holder.cv_setupwallet.setOnClickListener {
                card_global_id = account_list[position].id!!
                btn_saved_bank_pay.visibility = View.VISIBLE
                selectedPosition = position
                holder.img_verify_id.visibility = View.GONE
                notifyDataSetChanged()
                bank_default_source_id = ""
            }
            if (selectedPosition == position) {
                (holder.img_verify_id.drawable as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_verify_id.drawable as Animatable).start()
                holder.img_verify_id.visibility = View.INVISIBLE
            }
            if (bank_default_source_id == account_list[position].id) {
                card_global_id = account_list[position].id!!.toString()
                (holder.img_verify_id.drawable as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var cv_setupwallet: CardView = view.findViewById(R.id.cv_setupwallet)
            var img_verify_id: ImageView = view.findViewById(R.id.img_verify_id)
            var tv_bank_name_id: TextView = view.findViewById(R.id.tv_bank_name_id)
            var tv_ac_no_id: TextView = view.findViewById(R.id.tv_ac_no_id)
            var tv_ac_holder_name_id: TextView = view.findViewById(R.id.tv_ac_holder_name_id)
            var tv_verify_id: TextView = view.findViewById(R.id.tv_verify_id)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var ll_verify_amount_id: LinearLayout = view.findViewById(R.id.ll_verify_amount_id)
        }
    }

    inner class SavedCardListAdapter(
        context: Context,
        val account_list: ArrayList<SavedCardsArrayDataResponse>, val delete_status: String, var default_source_id: String
    ) : RecyclerView.Adapter<SavedCardListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_savedcards, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_card_name: TextView = view.findViewById(R.id.tv_card_name)
            var iv_card_type: ImageView = view.findViewById(R.id.iv_card_type)
            var tv_card_number: TextView = view.findViewById(R.id.tv_card_number)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var img_check_id: ImageView = view.findViewById(R.id.img_check_id)
            var card_select_id: CardView = view.findViewById(R.id.card_select_id)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_card_name.text = account_list[position].name
            holder.tv_card_number.text = "*****-" + account_list[position].last4
            if (delete_status == "yes") {
                holder.img_delete_id.visibility = View.VISIBLE
            }
            holder.card_select_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                default_source_id = ""
            }
            if (selectedPosition == position) {
                (holder.img_check_id.drawable as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_check_id.drawable as Animatable).start()
                holder.img_check_id.visibility = View.INVISIBLE
            }
            Log.e("default_source_id", default_source_id + "--" + account_list[position].id)
            if (default_source_id == account_list[position].id) {
                card_global_id = account_list[position].id!!.toString()
                (holder.img_check_id.drawable as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }
        }
    }

    override fun onDestroy() {
        if (bank_dialog.isShowing){
            bank_dialog.dismiss()
        }
        if (card_dialog.isShowing) {
            card_dialog.dismiss()
        }
        super.onDestroy()
    }
}