package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.view.WindowInsets
import android.view.WindowManager
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R

@Suppress("DEPRECATION")
@SuppressLint("SetJavaScriptEnabled", "SetTextI18n")
class PrivacyPolicyActivity : AppCompatActivity() {

    private var paylink: String? = ""
    internal lateinit var id: String
    var mywebview: WebView? = null
    internal lateinit var sessionManager: SessionManager
    lateinit var uri: Uri
    lateinit var iv_back: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_privacy_policy)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        mywebview = findViewById(R.id.webview_privacypolicy)
        myLoading()
        iv_back = findViewById(R.id.iv_back)
        iv_back.setOnClickListener {
            onBackPressed()
        }
        my_loader.show()
        sessionManager = SessionManager(this)
        paylink = "${ApiInterface.BASE}/home/mobile_privacy_policy"
        uri = if (paylink!!.contains("http") || paylink!!.contains("https")) {
            Uri.parse(paylink)
        } else {
            Uri.parse("https://$paylink")
        }
        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                my_loader.dismiss()
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
//                my_loader.show()
            }
        }
        mywebview!!.settings.javaScriptEnabled = true
        mywebview!!.isHorizontalScrollBarEnabled = false
        mywebview!!.loadUrl(uri.toString())
    }

    internal lateinit var my_loader: Dialog

    private fun myLoading() {
        my_loader = Dialog(this)
        my_loader.requestWindowFeature(Window.FEATURE_NO_TITLE)
        my_loader.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        my_loader.setCancelable(false)
        my_loader.setContentView(R.layout.loading_dialog)
    }

}
