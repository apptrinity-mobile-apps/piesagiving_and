package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.drawable.Animatable
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SeekBarFloat
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor
import kotlin.math.roundToInt

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class DonateFundsActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_screen = ""
    var organization_id = ""
    var payType = 0
    lateinit var type_id_stg: String
    var type_name_stg: String? = null
    lateinit var type_project: String
    lateinit var display_name: String

    lateinit var iv_back: ImageView
    lateinit var tv_donate: TextView
    lateinit var tv_recurring: TextView
    lateinit var tv_organisation_name: TextView
    lateinit var tv_orgname_donate: TextView
    lateinit var sp_type_id: Spinner
    lateinit var sp_donation_type_id: Spinner
    lateinit var amount_seekbar_id: SeekBar
    lateinit var et_amount_id: EditText
    lateinit var tv_donate_amount_id: TextView
    lateinit var tv_tax_amount_id: TextView
    lateinit var tv_total_amount_id: TextView

    lateinit var only_names: ArrayList<String>
    lateinit var only_types: ArrayList<String>
    lateinit var only_ids: ArrayList<String>
    lateinit var special_cause_names: ArrayList<String>
    lateinit var projectDonationsArray: ArrayList<DropDownProjectDetailsArrayResponse>
    lateinit var specialCauseDetailsArray: ArrayList<DropDownSpecialCauseDetailsArrayResponse>
    lateinit var loading_dialog: Dialog
    lateinit var full_dialog: Dialog
    lateinit var bank_full_dialog: Dialog

    lateinit var donation_types_array: ArrayList<String>
    lateinit var donation_types_array_ids: ArrayList<Int>

    lateinit var stripe_card_id: CardInputWidget
    lateinit var et_card_holder_id: EditText
    lateinit var checkbox_savecard: CheckBox
    var checkbox_savecard_checked: Boolean = false
    lateinit var tv_organisation_name_payment: TextView
    lateinit var tv_tax_amount_id_payment: TextView
    lateinit var tv_donate_amount_id_payment: TextView
    lateinit var rv_savedcardslist: RecyclerView
    lateinit var tv_total_amount_id_payment: TextView
    lateinit var btn_pay_id: Button
    lateinit var btn_saved_card_pay: Button
    lateinit var img_close_id: ImageView
    lateinit var ll_donations_id: LinearLayout
    lateinit var img_fee_id: ImageView
    lateinit var get_card: Card

    lateinit var img_bank_close_id: ImageView
    lateinit var id_empty_data_id: TextView
    lateinit var btn_saved_bank_pay: Button
    lateinit var rv_account_details_id: RecyclerView
    lateinit var array_account_list: ArrayList<BankAccountsArrayList>
    lateinit var array_savedcards_list: ArrayList<SavedCardsArrayDataResponse>
    lateinit var customSeekBar: SeekBarFloat
    lateinit var tv_min_value_id: TextView

    var edit_amount: Double? = null

    var piesagivingPercentage = 0.00
    var piesaBankExtracharges = 0.00
    var piesaBankPercentage = 0.00
    var piesaCardExtracharges = 0.00
    var piesaCardPercentage = 0.00
    var stripeBankExtracharges = 0.00
    var stripeBankPercentage = 0.00
    var stripeCardExtracharges = 0.00
    var stripeCardPercentage = 0.00


    var global_stripe_percent = 0.00
    var global_stripe_total_fee = 0.00
    var global_ex_amount = 0
    var global_piesa_percent = 0.00
    var global_piesa_total_fee = 0.00
    var global_piesa_card_ex_amount = 0
    var global_piesa_bank_ex_amount = 0
    var card_global_id = ""

    var cards_default_source_id = ""
    var bank_default_source_id = ""

    //   biometric
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var userEmail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var userDetails: HashMap<String, String>
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView
    private var project_name = ""

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_donate)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        donation_types_array = ArrayList()
        donation_types_array.add("Select Bank Account")
        donation_types_array.add("Select Credit Card")

        donation_types_array_ids = ArrayList()
        donation_types_array_ids.add(0)
        donation_types_array_ids.add(1)
        try {
            organization_id = intent.getStringExtra("organiz_id")!!
            from_screen = intent.getStringExtra("from_screen")!!
            display_name = intent.getStringExtra("display_name")!!
            project_name = intent.getStringExtra("project_name")!!
        } catch (e: Exception) {
            e.printStackTrace()
        }
        initialize()

        Log.e("organization_id", organization_id)
        Log.e("from_screen_donate", from_screen)
        tv_organisation_name.text = "To $display_name"
        tv_orgname_donate.text = "To $display_name"
        et_amount_id.setText("5.00")
        Log.e("organization_id_donate", organization_id)
        ProjectDonationTypeApi(organization_id)
        val obj = JSONObject()
        obj.put(
            "userId", sessionManager.getUserDetails()[SessionManager.ID_KEY].toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("project_donation_input", jObject.toString())
        SavedCardsListApi(jObject)
        tv_donate.setOnClickListener {
            if (et_amount_id.text.toString().trim().equals("") || et_amount_id.text.toString()
                    .toDouble() <= 4.0
            ) {
                toast("You must enter donation amount min $5")
            } else if (tv_total_amount_id.text.toString().trim()
                    .equals("") || tv_total_amount_id.text.toString().toDouble() <= 4.0
            ) {
                toast("You must enter donation amount min $5")
            } else {
                showBiometricPrompt()
            }
            Log.e("DROPDWONID", type_id_stg.toString() + "-----" + only_types)
        }

        tv_recurring.setOnClickListener {
            val amount = (et_amount_id.text.toString().trim())
            val intent = Intent(this@DonateFundsActivity, CreateRecurringActivity::class.java)
            intent.putExtra("organization_id", organization_id)
            intent.putExtra("display_name", display_name)
            intent.putExtra("from_screen", from_screen)
            intent.putExtra("type_project", type_project)
            intent.putExtra("donation_amount", (tv_total_amount_id.text.toString()))
            intent.putExtra("payment_type", payType.toString())
            intent.putExtra("type_id", type_id_stg.toString())
            intent.putExtra("type_name", type_name_stg.toString())
            startActivity(intent)
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }




        full_dialog = Dialog(this, R.style.DialogTheme)
        full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // full_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        full_dialog.setCancelable(false);

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        full_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);

        full_dialog.setContentView(R.layout.stripe_payment_dialog)
        stripe_card_id = full_dialog.findViewById(R.id.stripe_card_id)
        et_card_holder_id = full_dialog.findViewById(R.id.et_card_holder_id)
        checkbox_savecard = full_dialog.findViewById(R.id.checkbox_savecard)
        tv_organisation_name_payment = full_dialog.findViewById(R.id.tv_organisation_name_payment)
        tv_tax_amount_id_payment = full_dialog.findViewById(R.id.tv_tax_amount_id_payment)
        tv_donate_amount_id_payment = full_dialog.findViewById(R.id.tv_donate_amount_id_payment)
        tv_total_amount_id_payment = full_dialog.findViewById(R.id.tv_total_amount_id_payment)
        rv_savedcardslist = full_dialog.findViewById(R.id.rv_savedcardslist)
        btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        btn_saved_card_pay = full_dialog.findViewById(R.id.btn_saved_card_pay)
        img_close_id = full_dialog.findViewById<ImageView>(R.id.img_close_id)

        checkbox_savecard.setOnCheckedChangeListener { buttonView, isChecked ->
            checkbox_savecard_checked = isChecked

            Log.e("cardchecked", checkbox_savecard_checked.toString())
        }

        val layoutManager =
            LinearLayoutManager(this@DonateFundsActivity, RecyclerView.VERTICAL, false)
        rv_savedcardslist.layoutManager = layoutManager
        rv_savedcardslist.setHasFixedSize(true)

        rv_savedcardslist.addOnItemTouchListener(
            RecyclerItemClickListener(this!!,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        // loading_dialog.show()
                        card_global_id = array_savedcards_list[position].id!!.toString()
                        btn_saved_card_pay.visibility = View.VISIBLE
                    }
                })
        )

        img_close_id.setOnClickListener {
            full_dialog.dismiss()
        }

        btn_saved_card_pay.setOnClickListener {
            if (type_project.equals("project")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                        .toString()
                )
                obj.put("projectId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 1)
                obj.put("cardId", card_global_id)
                obj.put("cardToken", "")
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("proj_savedcards_input", jObject.toString())
                StripePaymentApi(jObject)
            }

            if (type_project.equals("special_cause")) {
                Log.e("mmmm", type_project)

                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                        .toString()
                )
                obj.put("causeId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 1)
                obj.put("cardId", card_global_id)
                obj.put("cardToken", "")
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("spec_cause_savedcards", jObject.toString())
                SpecialDontateNowApi(jObject)
            }

            if (type_project.equals("generalfund")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                        .toString()
                )
                obj.put("projectId", "0")
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 1)
                obj.put("cardId", card_global_id)
                obj.put("cardToken", "")
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("cardSave", false)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("generalfund_input", jObject.toString())
                StripePaymentApi(jObject)
            }
        }

        btn_pay_id.setOnClickListener {

            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                toast("Invalid Card Data")
            } else if (et_card_holder_id.text.toString().trim().equals("")) {
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                //get_card.name!!=et_card_holder_id.text.toString()
                loading_dialog.show()
                buy()
            }
        }

        TooltipCompat.setTooltipText(btn_pay_id, "Payment Button")

    }

    private fun initialize() {
        loadingDialog()
        passCodeDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet

        iv_back = findViewById(R.id.iv_back)
        tv_donate = findViewById(R.id.tv_donate)
        tv_recurring = findViewById(R.id.tv_recurring)
        tv_organisation_name = findViewById(R.id.tv_organisation_name)
        tv_orgname_donate = findViewById(R.id.tv_orgname_donate)
        sp_type_id = findViewById(R.id.sp_type_id)
        sp_donation_type_id = findViewById(R.id.sp_donation_type_id)
        amount_seekbar_id = findViewById(R.id.amount_seekbar_id)
        et_amount_id = findViewById(R.id.et_amount_id)
        tv_donate_amount_id = findViewById(R.id.tv_donate_amount_id)
        tv_tax_amount_id = findViewById(R.id.tv_tax_amount_id)
        tv_total_amount_id = findViewById(R.id.tv_total_amount_id)
        ll_donations_id = findViewById(R.id.ll_donations_id)
        img_fee_id = findViewById(R.id.img_fee_id)
        img_fee_id.setOnClickListener {
            //SnakbarAlert()
            helpDialog()
        }
        if (from_screen.equals("special_cause")) {
            tv_recurring.visibility = View.GONE
        }

        biometricManager = BiometricManager.from(this@DonateFundsActivity)
        executor = ContextCompat.getMainExecutor(this@DonateFundsActivity)
        userDetails = sessionManager.getUserDetails()
        biometrics = userDetails[SessionManager.FACEID_KEY]!!
        userEmail = userDetails[SessionManager.EMAIL_KEY]!!
        passCode = userDetails[SessionManager.PASSCODE_KEY]!!
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }

        Bank_full_dilag()

        val adapter = ArrayAdapter<String>(
            this@DonateFundsActivity,
            R.layout.support_simple_spinner_dropdown_item,
            donation_types_array
        )
        sp_donation_type_id.adapter = adapter
        tv_min_value_id = findViewById(R.id.tv_min_value_id)
        customSeekBar = findViewById(R.id.customSeekBar)
        customSeekBar.setMaxF(1000f)
        customSeekBar.setMinF(5f)
        customSeekBar.setOnSeekBarChangeListener(object : SeekBarFloat.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBarFloat: SeekBarFloat?,
                progress: Float,
                fromUser: Boolean
            ) {
                val progressS = String.format("%.2f", progress)
                val progressF = (progress * 100.0).roundToInt().toFloat() / 100.0.toFloat()
                Log.d("value", "$progress======$progressS====$progressF====$fromUser")
                if (fromUser) {
                    et_amount_id.setText(progressS)
                }
                if (progressF.toString().equals("") || progressF.toString().equals("0")) {
                    tv_donate_amount_id.text = "$0"
                    tv_total_amount_id.text = "0"
                } else {
                    /*3.2% plus 30 cents per transaction (calculation based on that) */
                    if (payType.equals(0)) {
                        /*1% plus 20 cents per transaction (calculation based on that) */
                        // val tax = round(()+(20/100))

                        val strpextracharge = (stripeBankExtracharges.toDouble() /*/ 100*/)
                        val piesaextracharge = (piesaBankExtracharges.toDouble() /*/ 100*/)
                        val decimal =
                            (progressF!! * (stripeBankPercentage / 100)) + (strpextracharge) + (progressF * (piesaBankPercentage / 100)) + piesaextracharge
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)

                        tv_tax_amount_id.text = "$$tax"
                        tv_tax_amount_id_payment.text = "$$tax"
                        tv_donate_amount_id.text =
                            "$" + "%.2f".format(progressF.toDouble() - tax.toDouble())
                        tv_donate_amount_id_payment.text =
                            "$" + "%.2f".format(progressF.toDouble() - tax.toDouble())
                        tv_total_amount_id.text = "%.2f".format(progressF.toDouble())
                        tv_total_amount_id_payment.text = "%.2f".format(progressF.toDouble())

                        Log.e("final_all", tax)
                        global_stripe_percent = stripeBankPercentage
                        global_stripe_total_fee =
                            "%.2f".format((progressF!! * stripeBankPercentage / 100)).toDouble()
                        global_ex_amount = (stripeBankExtracharges * 100).toInt()
                        global_piesa_percent = piesaBankPercentage
                        global_piesa_total_fee =
                            "%.2f".format((progressF!! * piesaBankPercentage / 100)).toDouble()
                        global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()

                        Log.e("global_ex_amount", "Bank: " + global_ex_amount.toString())

                    } else {
                        //tv_donate_amount_id.setText("$" + progress.toString())
                        val strpextracharge = (stripeCardExtracharges.toDouble() /*/ 100*/)
                        val piesaextracharge = (piesaCardExtracharges.toDouble() /*/ 100*/)
                        val decimal =
                            (progressF!! * (stripeCardPercentage / 100)) + (strpextracharge) + (progressF * (piesaCardPercentage / 100)) + piesaextracharge
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e(
                            "tax_ll",
                            tax + "--" + progressF + "---" + (progressF * (stripeCardPercentage / 100)) + "----" + ((stripeCardExtracharges)) + "-----" + (progressF * (piesagivingPercentage / 100))
                        )
                        tv_tax_amount_id.text = "$$tax"
                        tv_tax_amount_id_payment.text = "$$tax"
                        tv_donate_amount_id.text =
                            "$" + "%.2f".format(progressF.toDouble() - tax.toDouble())
                        tv_donate_amount_id_payment.text =
                            "$" + "%.2f".format(progressF.toDouble() - tax.toDouble())
                        tv_total_amount_id.text = "%.2f".format(progressF.toDouble())
                        tv_total_amount_id_payment.text =
                            "%.2f".format(progressF.toDouble()).toString()
                        Log.e("final_value", "%.2f".format((progressF!! * 0.032) + 0.3))
                        global_stripe_percent = stripeCardPercentage
                        global_stripe_total_fee =
                            "%.2f".format((progressF * stripeCardPercentage / 100)).toDouble()
                        global_ex_amount = (stripeCardExtracharges * 100).toInt()
                        global_piesa_percent = piesaCardPercentage
                        global_piesa_total_fee =
                            "%.2f".format((progressF * piesaCardPercentage / 100)).toDouble()
                        global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()

                        Log.e("global_ex_amount", "Card: " + global_ex_amount.toString())
                    }
                }
            }

            override fun onStartTrackingTouch(seekBarFloat: SeekBarFloat?) {}

            override fun onStopTrackingTouch(seekBarFloat: SeekBarFloat?) {}
        })

        sp_donation_type_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                payType = donation_types_array_ids.get(position)
                Log.e("type_name_stg", payType.toString())
                if (payType.equals(0)) {
                    /*1% plus 20 cents per transaction (calculation
    based on that) */
                    //tv_donate_amount_id.setText("$" + edit_amount.toString())
                    // val tax = round(()+(20/100))

                    val strpextracharge = (stripeBankExtracharges.toDouble()/* / 100*/)
                    val piesaextracharge = (piesaBankExtracharges.toDouble() /*/ 100*/)

                    val decimal =
                        (edit_amount!! * (stripeBankPercentage / 100)) + (strpextracharge) + (edit_amount!! * (piesaBankPercentage / 100)) + piesaextracharge
                    println(decimal)
                    // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                    val tax = "%.2f".format(decimal)
                    tv_tax_amount_id.text = "$$tax"
                    tv_tax_amount_id_payment.text = "$$tax"

                    tv_donate_amount_id.text = "$" + (edit_amount!!.toDouble() - tax.toDouble())
                    tv_donate_amount_id_payment.text =
                        "$" + (edit_amount!!.toDouble() - tax.toDouble())
                    tv_total_amount_id.text = String.format("%.2f", (edit_amount!!.toDouble()))
                    tv_total_amount_id_payment.text =
                        "%.2f".format((edit_amount!!.toDouble())).toString()

                    global_stripe_percent = stripeBankPercentage
                    global_stripe_total_fee =
                        "%.2f".format((edit_amount!! * stripeBankPercentage / 100)).toDouble()
                    global_ex_amount = (stripeBankExtracharges * 100).toInt()
                    global_piesa_percent = piesaBankPercentage
                    global_piesa_total_fee =
                        "%.2f".format((edit_amount!! * piesaBankPercentage / 100)).toDouble()
                    global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()


                    Log.e("final_all", tax)

                } else {
                    //tv_donate_amount_id.setText("$" + edit_amount.toString())
                    val strpextracharge = (stripeCardExtracharges.toDouble()/* / 100*/)
                    val piesaextracharge = (piesaCardExtracharges.toDouble() /*/ 100*/)

                    val decimal =
                        (edit_amount!! * (stripeCardPercentage / 100)) + (strpextracharge) + (edit_amount!! * (piesaCardPercentage / 100)) + piesaextracharge
                    println(decimal)
                    // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                    val tax = "%.2f".format(decimal)
                    Log.e("tax_ll", decimal.toString())
                    tv_tax_amount_id.text = "$$tax"
                    tv_tax_amount_id_payment.text = "$$tax"
                    tv_donate_amount_id.text = "$" + (edit_amount!!.toDouble() - tax.toDouble())
                    tv_donate_amount_id_payment.text =
                        "$" + (edit_amount!!.toDouble() - tax.toDouble())
                    tv_total_amount_id.text = String.format("%.2f", (edit_amount!!.toDouble()))
                    tv_total_amount_id_payment.text =
                        String.format("%.2f", (edit_amount!!.toDouble()))
                    Log.e("final_all", tax)
                    global_stripe_percent = stripeCardPercentage
                    global_stripe_total_fee =
                        "%.2f".format((edit_amount!! * stripeCardPercentage / 100)).toDouble()
                    global_ex_amount = (stripeCardExtracharges * 100).toInt()
                    global_piesa_percent = piesaCardPercentage
                    global_piesa_total_fee =
                        "%.2f".format((edit_amount!! * piesaCardPercentage / 100)).toDouble()
                    global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}

        }

        amount_seekbar_id.max = 1005
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            amount_seekbar_id.min = 5
        }

        amount_seekbar_id.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onStopTrackingTouch(seekBar: SeekBar) { // TODO Auto-generated method stub
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) { // TODO Auto-generated method stub
            }

            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {


                et_amount_id.setText(progress.toString())
                if (progress.toString().equals("") || progress.toString().equals("0")) {
                    tv_donate_amount_id.text = "$0"
                    tv_total_amount_id.text = "0"
                } else {
                    /*3.2% plus 30 cents per transaction (calculation
 based on that) */
                    if (payType.equals(0)) {
                        /*1% plus 20 cents per transaction (calculation
 based on that) */
                        // val tax = round(()+(20/100))

                        val strpextracharge = (stripeBankExtracharges.toDouble() /*/ 100*/)
                        val piesaextracharge = (piesaBankExtracharges.toDouble() /*/ 100*/)

                        val decimal =
                            (progress!! * (stripeBankPercentage / 100)) + (strpextracharge) + (progress * (piesaBankPercentage / 100)) + piesaextracharge
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)

                        tv_tax_amount_id.text = "$$tax"
                        tv_tax_amount_id_payment.text = "$$tax"
                        tv_donate_amount_id.text = "$" + (progress.toDouble() - tax.toDouble())
                        tv_donate_amount_id_payment.text =
                            "$" + (progress.toDouble() - tax.toDouble())
                        tv_total_amount_id.text = String.format("%.2f", (progress.toDouble()))
                        tv_total_amount_id_payment.text =
                            String.format("%.2f", (progress.toDouble()))

                        Log.e("final_all", tax)
                        global_stripe_percent = stripeBankPercentage
                        global_stripe_total_fee =
                            "%.2f".format((progress * stripeBankPercentage / 100)).toDouble()
                        global_ex_amount = (stripeBankExtracharges * 100).toInt()
                        global_piesa_percent = piesaBankPercentage
                        global_piesa_total_fee =
                            "%.2f".format((progress * piesaBankPercentage / 100)).toDouble()
                        global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()


                    } else {
                        //tv_donate_amount_id.setText("$" + progress.toString())
                        val strpextracharge = (stripeCardExtracharges.toDouble() /*/ 100*/)
                        val piesaextracharge = (piesaCardExtracharges.toDouble() /*/ 100*/)
                        val decimal =
                            (progress * (stripeCardPercentage / 100)) + (strpextracharge) + (progress * (piesaCardPercentage / 100)) + piesaextracharge
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e(
                            "tax_ll",
                            tax + "--" + progress + "---" + (progress * (stripeCardPercentage / 100)) + "----" + ((stripeCardExtracharges)) + "-----" + (progress * (piesagivingPercentage / 100))
                        )

                        tv_tax_amount_id.text = "$$tax"
                        tv_tax_amount_id_payment.text = "$$tax"
                        tv_donate_amount_id.text = "$" + (progress.toDouble() - tax.toDouble())
                        tv_donate_amount_id_payment.text =
                            "$" + (progress.toDouble() - tax.toDouble())
                        tv_total_amount_id.text = String.format("%.2f", (progress.toDouble()))
                        tv_total_amount_id_payment.text =
                            String.format("%.2f", (progress.toDouble()))
                        Log.e("final_value", "%.2f".format((progress!! * 0.032) + 0.3))
                        global_stripe_percent = stripeCardPercentage
                        global_stripe_total_fee =
                            "%.2f".format((progress * stripeCardPercentage / 100)).toDouble()
                        global_ex_amount = (stripeCardExtracharges * 100).toInt()
                        global_piesa_percent = piesaCardPercentage
                        global_piesa_total_fee =
                            "%.2f".format((progress * piesaCardPercentage / 100)).toDouble()
                        global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()

                    }
                }

                //t1.setTextSize(progress)
                /*Toast.makeText(applicationContext, progress.toString(), Toast.LENGTH_LONG)
                    .show()*/
            }
        })
        DonationFeeApi()

        et_amount_id.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    et_amount_id.setSelection(et_amount_id.length())
                    customSeekBar.setProgressF(et_amount_id.text.toString().toFloat(), true)
                    //amount_seekbar_id.setProgress((et_amount_id.text.toString()).toInt(), true)
                    edit_amount = (et_amount_id.text.toString()).toDouble()
                    if (edit_amount!!.toInt() >= 5) {
                        tv_min_value_id.text = "$" + String.format("%.2f", edit_amount)
                    } else {
                        tv_min_value_id.text = "$5.00"
                    }

                    if (et_amount_id.text.toString().equals("") || et_amount_id.text.toString()
                            .equals(
                                "0"
                            )
                    ) {
                        tv_donate_amount_id.text = "$0.00"
                        tv_total_amount_id.text = "0.00"
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Log.e("exception", et_amount_id.text.toString())
                    tv_donate_amount_id.text = "$0.00"
                    tv_total_amount_id.text = "0.00"
                    edit_amount = 5.00
                }
            }
        })
    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    if (payType == 0) {
                        bank_full_dialog.show()
                    } else {
                        full_dialog.show()
                    }
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    if (payType == 0) {
                        bank_full_dialog.show()
                    } else {
                        full_dialog.show()
                    }
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            if (payType == 0) {
                bank_full_dialog.show()
            } else {
                full_dialog.show()
            }
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)                               // true to use pin/pattern/password if biometric is not set
            .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@DonateFundsActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    if (payType == 0) {
                        bank_full_dialog.show()
                    } else {
                        full_dialog.show()
                    }
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())

                    /*if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {

                    } else if (errorCode == BiometricConstants.ERROR_CANCELED) {
                        Log.e("Biometrics", "biometric operation cancelled by user.")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT_PERMANENT) {
                        Log.e("Biometrics", "ERROR_LOCKOUT occurred too many times")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT) {
                        Log.e("Biometrics", "the API is locked out due to too many attempts")
                    } else if (errorCode == BiometricConstants.ERROR_NO_BIOMETRICS) {
                        Log.e("Biometrics", "user does not have any biometrics enrolled")
                    } else if (errorCode == BiometricConstants.ERROR_UNABLE_TO_PROCESS) {
                        Log.e("Biometrics", "the sensor was unable to process the current image")
                    }*/
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
                if (payType == 0) {
                    bank_full_dialog.show()
                } else {
                    full_dialog.show()
                }
                ll_check_email_passcode.visibility = View.GONE
            } else {
                Toast.makeText(
                    this@DonateFundsActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT
                ).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj = JSONObject()
            obj.put("passcode", passCode)
            obj.put("email", userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@DonateFundsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text =
                                getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun Bank_full_dilag() {
        bank_full_dialog = BottomSheetDialog(this)
        bank_full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_full_dialog.setCancelable(true)
        bank_full_dialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        bank_full_dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        // bank_full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);
        bank_full_dialog.setContentView(R.layout.bank_payment_dialog)
        passCodeDialog.dismissWithAnimation = true
        //btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        img_bank_close_id = bank_full_dialog.findViewById(R.id.img_bank_close_id)
        id_empty_data_id = bank_full_dialog.findViewById(R.id.id_empty_data_id)
        btn_saved_bank_pay = bank_full_dialog.findViewById(R.id.btn_saved_bank_pay)
        rv_account_details_id = bank_full_dialog.findViewById(R.id.rv_account_details_id)

        img_bank_close_id.setOnClickListener {
            bank_full_dialog.dismiss()
        }
        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject

        AccountsListApi(jObject)

        btn_saved_bank_pay.setOnClickListener {
            if (type_project.equals("project")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                )
                obj.put("projectId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 2)
                obj.put("bankAccountId", card_global_id)
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("cardSave", checkbox_savecard_checked)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("project_donation_input", jObject.toString())
                StripePaymentApi(jObject)
            }

            if (type_project.equals("special_cause")) {
                Log.e("mmmm", type_project)

                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                )
                obj.put("causeId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 2)
                obj.put("bankAccountId", card_global_id)
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("cardSave", checkbox_savecard_checked)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("project_details_input", jObject.toString())
                SpecialDontateNowApi(jObject)
            }

            if (type_project.equals("generalfund")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                )
                obj.put("projectId", "0")
                obj.put("orgId", organization_id)
                obj.put("amount", ((et_amount_id.text.toString()).toDouble() * 100).toInt())
                obj.put("paymentType", 2)
                obj.put("bankAccountId", card_global_id)
                obj.put("isReccurring", false)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("cardSave", checkbox_savecard_checked)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("generalfund_input", jObject.toString())
                StripePaymentApi(jObject)
            }
        }

    }

    private fun AccountsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_account_list = ArrayList()
        array_account_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<BankAccountListPojo>,
                response: Response<BankAccountListPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {
                        // toast(response!!.body()!!.result!!)
                        if (response.body()!!.bankAccountList!!.data!!.size.equals(0)) {
                            id_empty_data_id.visibility = View.VISIBLE
                        }
                        for (i in 0 until response.body()!!.bankAccountList!!.data!!.size) {
                            array_account_list.add(response.body()!!.bankAccountList!!.data!![i])
                        }
                        bank_default_source_id =
                            response.body()!!.customerDetails!!.default_source!!
                        rv_account_details_id.visibility = View.VISIBLE

                        val adapter = BankListAdapter(
                            this@DonateFundsActivity,
                            array_account_list,
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString(),
                            bank_default_source_id
                        )
                        rv_account_details_id.setHasFixedSize(true)
                        rv_account_details_id.adapter = adapter
                        adapter.notifyDataSetChanged()

                    } else {
                        id_empty_data_id.visibility = View.VISIBLE
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    inner class BankListAdapter(
        context: Context,
        val account_list: ArrayList<BankAccountsArrayList>,
        val user_id: String,
        var bank_default_source_id: String
    ) :
        RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_wallet_accounts, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_bank_name_id.text = account_list[position].bank_name
            holder.tv_ac_no_id.text = "*******" + account_list[position].last4
            holder.tv_ac_holder_name_id.text = account_list[position].account_holder_name
            holder.img_delete_id.visibility = View.GONE

            if (account_list[position].status.equals("new")) {
                //holder.img_verify_id.setImageDrawable(mContext!!.resources.getDrawable(R.drawable.ic_close))
                //holder.tv_verify_id.visibility=View.VISIBLE
                holder.cv_setupwallet.visibility = View.GONE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.tv_verify_id.visibility = View.GONE
                holder.img_verify_id.visibility = View.GONE
                holder.cv_setupwallet.visibility = View.VISIBLE
                holder.ll_verify_amount_id.visibility = View.GONE


            }
            holder.cv_setupwallet.setOnClickListener {
                card_global_id = account_list[position].id!!
                btn_saved_bank_pay.visibility = View.VISIBLE
                selectedPosition = position
                holder.img_verify_id.visibility = View.GONE

                notifyDataSetChanged()
                bank_default_source_id = ""
            }

            if (selectedPosition == position) {
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_list_bg));
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_normal_bg));
                holder.img_verify_id.visibility = View.INVISIBLE
            }

            if (bank_default_source_id.equals(account_list[position].id)) {
                card_global_id = account_list[position].id!!.toString()
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var cv_setupwallet: CardView = view.findViewById(R.id.cv_setupwallet)
            var img_verify_id: ImageView = view.findViewById(R.id.img_verify_id)
            var tv_bank_name_id: TextView = view.findViewById(R.id.tv_bank_name_id)
            var tv_ac_no_id: TextView = view.findViewById(R.id.tv_ac_no_id)
            var tv_ac_holder_name_id: TextView = view.findViewById(R.id.tv_ac_holder_name_id)
            var tv_verify_id: TextView = view.findViewById(R.id.tv_verify_id)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var ll_verify_amount_id: LinearLayout = view.findViewById(R.id.ll_verify_amount_id)
        }

    }

    private fun ProjectDonationTypeApi(org_id: String) {
//mani
        loading_dialog.show()

        only_names = ArrayList()
        only_types = ArrayList()
        only_names.clear()
        only_types.clear()
        only_ids = ArrayList()
        only_ids.clear()
        special_cause_names = ArrayList()
        special_cause_names.clear()
        projectDonationsArray = ArrayList()
        projectDonationsArray.clear()
        specialCauseDetailsArray = ArrayList()
        specialCauseDetailsArray.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectViewSpecialCausesApi(org_id)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<ProjectTypePojo> {
            override fun onFailure(call: Call<ProjectTypePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<ProjectTypePojo>,
                response: Response<ProjectTypePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        Log.e("from_screen_in_service", from_screen)

                        if (from_screen.equals("churchprofile")) {
                            Log.e("from_screen_in_service", from_screen)

                            only_names.add("General Fund")
                            only_ids.add("0")
                            only_types.add("generalfund")
                            for (i in 0 until response.body()!!.projectDetails!!.size) {
                                only_names.add(response.body()!!.projectDetails!!.get(i).projectName!!)
                                only_ids.add(response.body()!!.projectDetails!!.get(i).projectId!!)
                                projectDonationsArray.add(response.body()!!.projectDetails!!.get(i))
                                only_types.add("project")
                            }


                            for (i in 0 until response.body()!!.specialCauseDetails!!.size) {
                                only_names.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseName!!)
                                only_ids.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseId!!)
                                only_types.add("special_cause")
                                specialCauseDetailsArray.add(
                                    response.body()!!.specialCauseDetails!!.get(
                                        i
                                    )
                                )
                            }

                        }


                        if (from_screen.equals("project")) {
                            for (i in 0 until response.body()!!.projectDetails!!.size) {
                                only_names.add(response.body()!!.projectDetails!!.get(i).projectName!!)
                                only_ids.add(response.body()!!.projectDetails!!.get(i).projectId!!)
                                only_types.add("project")
                                projectDonationsArray.add(response.body()!!.projectDetails!!.get(i))
                            }
                        }

                        if (from_screen.equals("special_cause")) {
                            for (i in 0 until response.body()!!.specialCauseDetails!!.size) {
                                only_names.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseName!!)
                                only_ids.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseId!!)
                                only_types.add("special_cause")
                                specialCauseDetailsArray.add(
                                    response.body()!!.specialCauseDetails!!.get(
                                        i
                                    )
                                )
                            }
                        }

                        Log.e("from_screen_in", from_screen)


                        val adapter = ArrayAdapter<String>(
                            this@DonateFundsActivity,
                            R.layout.support_simple_spinner_dropdown_item,
                            only_names
                        )
                        sp_type_id.adapter = adapter
                        for (i in 0 until only_names.size) {
                            if (only_names[i] == project_name) {
                                sp_type_id.setSelection(i)
                                type_id_stg = only_ids[i]
                                type_name_stg = only_names[i]
                                type_project = only_types[i]
                            }
                        }

                    } else {
                        toast("Project Type Fetching Failed!")
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

        sp_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                type_id_stg = only_ids.get(position).toString()
                type_name_stg = only_names.get(position).toString()
                type_project = only_types.get(position).toString();
                Log.e("type_name_stg", type_name_stg + "---" + type_id_stg + "-----" + type_project)

                if (!type_name_stg!!.equals(0) || !type_name_stg!!.equals("0") || !type_name_stg!!.equals(
                        null
                    ) || !type_name_stg!!.equals("")
                ) {
                    tv_organisation_name_payment.setText("To " + display_name + "( " + type_name_stg + " )")
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        })

    }

    //Manibabu
    private fun DontateNowApi(jsonObj: JsonObject) {

        loading_dialog.show()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.RecrringDonationsApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {

                        val intent = Intent(this@DonateFundsActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "donate")
                        intent.putExtra("from_type", type_project)
                        intent.putExtra("display_name", display_name)
                        intent.putExtra("project_name", type_name_stg)
                        intent.putExtra("paid_amount", "%.2f".format(edit_amount!!.toDouble()))
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(
                            this@DonateFundsActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun SpecialDontateNowApi(jsonObj: JsonObject) {

        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.SpecialCauseRecrringDonationsApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {
                        val special_cause_id = response.body()!!.specialCauseId

                        Log.e("special_cause_id", special_cause_id.toString())

                        val intent = Intent(this@DonateFundsActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "donate")
                        intent.putExtra("from_type", type_project)
                        intent.putExtra("special_cause_id", special_cause_id)
                        intent.putExtra("display_name", display_name)
                        intent.putExtra("project_name", type_name_stg)
                        intent.putExtra("paid_amount", "%.2f".format(edit_amount!!.toDouble()))
                        startActivity(intent)
                        finish()

                    } else {
                        Log.e("failed_0", response!!.body()!!.result!!.toString())

                        Toast.makeText(
                            this@DonateFundsActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun DonationFeeApi() {

        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetExtraFeeApi()
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationFeePojo> {
            override fun onFailure(call: Call<DonationFeePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<DonationFeePojo>,
                response: Response<DonationFeePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {

                        piesaBankPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaBankPercentage!!
                        piesaBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaBankExtracharges!!
                        piesaCardPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaCardPercentage!!
                        piesaCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaCardExtracharges!!
                        piesagivingPercentage =
                            response.body()!!.stripeFeeDetails!!.piesagivingPercentage!!
                        stripeBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeBankExtracharges!!
                        stripeBankPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeBankPercentage!!
                        stripeCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeCardExtracharges!!
                        stripeCardPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeCardPercentage!!

                        if (payType.equals(0)) {
                            //tv_donate_amount_id.setText("$5")
                            // val tax = round(()+(20/100))

                            val strpextracharge = (stripeBankExtracharges.toDouble() /*/ 100*/)
                            val piesaextracharge = (piesaBankExtracharges.toDouble() /*/ 100*/)

                            val decimal =
                                (5 * (stripeBankPercentage / 100)) + (strpextracharge) + (5 * (piesaBankPercentage / 100)) + piesaextracharge
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            tv_tax_amount_id.text = "$$tax"
                            tv_tax_amount_id_payment.text = "$$tax"
                            tv_donate_amount_id.text = "$" + (5.00 - tax.toDouble())
                            tv_donate_amount_id_payment.text = "$" + (5.00 - tax.toDouble())
                            tv_total_amount_id.text = "%.2f".format(5.00)
                            tv_total_amount_id_payment.text = "%.2f".format(5.00)

                            Log.e("final_all", "%.2f".format((5!! * 0.01) + 0.3))

                            global_stripe_percent = stripeBankPercentage
                            global_stripe_total_fee =
                                "%.2f".format((5 * stripeBankPercentage / 100)).toDouble()
                            global_ex_amount = (stripeBankExtracharges * 100).toInt()
                            global_piesa_percent = piesaBankPercentage
                            global_piesa_total_fee =
                                "%.2f".format((5 * piesaBankPercentage / 100)).toDouble()
                            global_piesa_bank_ex_amount = (piesaBankExtracharges * 100).toInt()

                        } else {
                            //tv_donate_amount_id.setText("$5")
                            val strpextracharge = (stripeCardExtracharges.toDouble() /*/ 100*/)
                            val piesaextracharge = (piesaCardExtracharges.toDouble() /*/ 100*/)

                            val decimal =
                                (5 * (stripeCardPercentage / 100)) + (strpextracharge) + (5 * (piesagivingPercentage / 100)) + piesaextracharge
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            Log.e("tax_ll", decimal.toString())
                            tv_tax_amount_id.setText("$" + tax.toString())
                            tv_tax_amount_id_payment.setText("$" + tax.toString())
                            tv_donate_amount_id.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_donate_amount_id_payment.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_total_amount_id.setText(
                                String.format(
                                    "%.2f",
                                    (5.toDouble()).toString()
                                )
                            )
                            tv_total_amount_id_payment.setText(
                                String.format("%.2f", (5.toDouble())).toString()
                            )
                            Log.e("final_all", "%.2f".format((5!! * 0.032) + 0.3))
                            global_stripe_percent = stripeCardPercentage
                            global_stripe_total_fee =
                                "%.2f".format((5 * stripeCardPercentage / 100)).toDouble()
                            global_ex_amount = (stripeCardExtracharges * 100).toInt()
                            global_piesa_percent = piesaCardPercentage
                            global_piesa_total_fee =
                                "%.2f".format((5 * piesaCardPercentage / 100)).toDouble()
                            global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()

                        }

                    } else {
                        Log.e("failed_0", response!!.body()!!.result!!.toString())

                        Toast.makeText(
                            this@DonateFundsActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun buy() {
        val validation = get_card!!.validateCard()
        if (validation) {

            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
                number = get_card.number!!,
                exp_month = get_card.expMonth!!,
                exp_year = get_card.expYear!!,
                cvc = get_card.cvc!!,
                name = et_card_holder_id.text.toString()
            )
            Log.e(
                "input_card",
                get_card.number!! + "--" + get_card.expMonth!! + "--" + get_card.expYear!! + "--" + get_card.cvc!!
            )
            //Log.e("@response", call.toString())
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)

                }

                override fun onResponse(
                    call: Call<CardTokenPojo>,
                    response: Response<CardTokenPojo>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {
                        //response!!.body()!!.id!!.toString()

                        if (type_project.equals("project")) {
                            val obj = JSONObject()
                            obj.put(
                                "userId",
                                sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                                    .toString()
                            )
                            obj.put("projectId", type_id_stg)
                            obj.put("orgId", organization_id)
                            obj.put(
                                "amount",
                                ((et_amount_id.text.toString()).toDouble() * 100).toInt()
                            )
                            obj.put("paymentType", 1)
                            obj.put("cardId", "")
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("isReccurring", false)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("cardSave", checkbox_savecard_checked)
                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("project_donation_input", jObject.toString())
                            StripePaymentApi(jObject)
                        }

                        if (type_project.equals("generalfund")) {
                            val obj = JSONObject()
                            obj.put(
                                "userId",
                                sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                                    .toString()
                            )
                            obj.put("projectId", "0")
                            obj.put("orgId", organization_id)
                            obj.put(
                                "amount",
                                ((et_amount_id.text.toString()).toDouble() * 100).toInt()
                            )
                            obj.put("paymentType", 1)
                            obj.put("cardId", "")
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("isReccurring", false)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("cardSave", checkbox_savecard_checked)
                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("generalfund_input", jObject.toString())
                            StripePaymentApi(jObject)
                        }

                        if (type_project.equals("special_cause")) {
                            Log.e("mmmm", type_project)

                            val obj = JSONObject()
                            obj.put(
                                "userId",
                                sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                                    .toString()
                            )
                            obj.put("causeId", type_id_stg)
                            obj.put("orgId", organization_id)
                            obj.put(
                                "amount",
                                ((et_amount_id.text.toString()).toDouble() * 100).toInt()
                            )
                            obj.put("paymentType", 1)
                            obj.put("cardId", "")
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("isReccurring", false)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("cardSave", checkbox_savecard_checked)
                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("project_details_input", jObject.toString())
                            SpecialDontateNowApi(jObject)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            /*End Card Token API*/


/*
            Stripe(this).createToken(
                get_card!!,
                PUBLISHABLE_KEY,
                object : TokenCallback {
                    override fun onError(error: Exception) {
                        Log.d("Stripe", error.toString())
                    }

                    override fun onSuccess(token: Token) {
                        // finishProgress()
                        // charge(token)
                        loading_dialog.dismiss()
                        */
/*  Toast.makeText(
                              this@DonateFundsActivity,
                              "Success : " + token.getId(),
                              Toast.LENGTH_SHORT
                          ).show()*//*

                        Log.e(
                            "cardToken_lll",
                            token.getId() + "--" + token.card!!.customerId + "---" + token.card!!.last4 + "---" + token.card!!.expMonth + "---" + token.card!!.expYear
                        )

                        */
/*{
"orgId":"5ee31a5dec33d6a06b99289b",
"projectId":"5ee315307e75e5950f558f76",
"userId":"5ee30b48251376f63f6a18a8",
"amount":10000,
"paymentType":1,
"isReccurring":false,
"cardToken":"tok_1GtB6eCvsUQGNnuEDSzzbdRK"
}*//*

                        Log.e("TYPEOFPROJECT", type_project)


                    }
                })
*/
        } else if (!get_card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun StripePaymentApi(jsonObj: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.StripePaymentDonationApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<StripePaymentPojo> {
            override fun onFailure(call: Call<StripePaymentPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<StripePaymentPojo>,
                response: Response<StripePaymentPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {

                        val intent = Intent(this@DonateFundsActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "donate")
                        intent.putExtra("from_type", type_project)
                        intent.putExtra("special_cause_id", "")
                        intent.putExtra("display_name", display_name)
                        intent.putExtra("project_name", type_name_stg)
                        intent.putExtra("paid_amount", "%.2f".format(edit_amount!!.toDouble()))
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(
                            this@DonateFundsActivity,
                            "Payment Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    fun helpDialog() {
        val bank_help_dialog = Dialog(this)
        bank_help_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_help_dialog.setCancelable(false)

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        bank_help_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        //bank_help_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);
        bank_help_dialog.getWindow()!!.setBackgroundDrawableResource(R.color.transparent)

        bank_help_dialog.setContentView(R.layout.donation_help_screen_dialog)
        val img_help_close_id = bank_help_dialog.findViewById<ImageView>(R.id.img_help_close_id)
        val tv_help_id = bank_help_dialog.findViewById<TextView>(R.id.tv_help_id)
        tv_help_id.text = getString(R.string.transaction_fees_card_and_bank)
        img_help_close_id.setOnClickListener {
            bank_help_dialog.dismiss()
        }
        bank_help_dialog.show()
    }


    fun SnakbarAlert() {
        Snackbar.make(
            ll_donations_id,
            getString(R.string.transaction_fees_card_and_bank),
            Snackbar.LENGTH_LONG
        )
            .setActionTextColor(getResources().getColor(android.R.color.holo_red_light))
            .show();
    }


    private fun SavedCardsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_savedcards_list = ArrayList()
        array_savedcards_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.savedCardsList(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<SavedCardListResponse> {
            override fun onFailure(call: Call<SavedCardListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<SavedCardListResponse>,
                response: Response<SavedCardListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    val resp = response!!.body()
                    Log.e("STRIPERESPOMNSEDONATION", resp.toString())
                    if (response!!.body()!!.responseStatus == "1") {
                        // toast(response!!.body()!!.result!!)
                        for (i in 0 until response.body()!!.savedCardsList!!.data!!.size) {
                            array_savedcards_list.add(response.body()!!.savedCardsList!!.data!![i])
                        }
                        cards_default_source_id =
                            response.body()!!.customerDetails!!.default_source!!
                        Log.e("cards_default_source_id", cards_default_source_id)
                        rv_savedcardslist.visibility = View.VISIBLE

                        val adapter = SavedCardListAdapter(
                            this@DonateFundsActivity,
                            array_savedcards_list,
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString(),
                            "no", cards_default_source_id
                        )

                        rv_savedcardslist.adapter = adapter
                        adapter.notifyDataSetChanged()

                    } else {
                        toast(response!!.body()!!.result!!)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }


    inner class SavedCardListAdapter(
        context: Context,
        val account_list: ArrayList<SavedCardsArrayDataResponse>,
        val user_id: String, val delete_status: String, var default_source_id: String
    ) :
        RecyclerView.Adapter<SavedCardListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_savedcards, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_card_name: TextView = view.findViewById(R.id.tv_card_name)
            var iv_card_type: ImageView = view.findViewById(R.id.iv_card_type)
            var tv_card_number: TextView = view.findViewById(R.id.tv_card_number)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var img_check_id: ImageView = view.findViewById(R.id.img_check_id)
            var card_select_id: CardView = view.findViewById(R.id.card_select_id)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_card_name.text = account_list[position].name
            //holder.iv_card_type.text="*******"+account_list[position].last4
            holder.tv_card_number.text = "*****-" + account_list[position].last4

            if (delete_status.equals("yes")) {
                holder.img_delete_id.visibility = View.VISIBLE
            }



            holder.card_select_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                default_source_id = ""
            }


            if (selectedPosition == position) {
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_list_bg));
                (holder.img_check_id.getDrawable() as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_check_id.getDrawable() as Animatable).start()
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_normal_bg));
                holder.img_check_id.visibility = View.INVISIBLE
            }

            Log.e("default_source_id", default_source_id + "--" + account_list[position].id)
            if (default_source_id.equals(account_list[position].id)) {
                card_global_id = account_list[position].id!!.toString()
                (holder.img_check_id.getDrawable() as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }
            //img_check_id


        }


    }

    override fun onDestroy() {
        if (bank_full_dialog.isShowing){
            bank_full_dialog.dismiss()
        }
        if (full_dialog.isShowing) {
            full_dialog.dismiss()
        }
        super.onDestroy()
    }

}