package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.ChurchProjectUpdatesAdapter
import com.piesagiving.com.Adapters.ChurchUpdatesAdapter
import com.piesagiving.com.Adapters.DonationsAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class ProjectDetailedViewActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private var from_screen = ""
    private var organiz_id = ""
    private var orgName = ""
    private var project_id = ""
    private var project_name = ""
    private var display_name = ""
    private var specialCauseId = ""
    var isFollowing = false
    val FOLLOW = 1
    val UNFOLLOW = 0

    lateinit var img_project_profile_id: ImageView
    lateinit var tv_hosted_by: TextView
    lateinit var header: TextView
    lateinit var tv_amount_received: TextView
    lateinit var tv_total_amount: TextView
    lateinit var tv_last_day: TextView
    lateinit var tv_projects: TextView
    lateinit var tv_updates_empty: TextView
    lateinit var tv_donations_empty: TextView
    lateinit var tv_church_bio: TextView
    lateinit var tv_donated_by: TextView
    lateinit var tv_donated_amount: TextView
    lateinit var tv_follow: TextView
    lateinit var tv_donate: TextView
    lateinit var ll_hosted: LinearLayout
    lateinit var ll_total_donations: LinearLayout
    lateinit var ll_last_day: LinearLayout
    lateinit var seekBar: SeekBar
    lateinit var rv_updates: RecyclerView
    lateinit var rv_donations: RecyclerView
    lateinit var iv_back: ImageView
    lateinit var lm_updates: LinearLayoutManager
    lateinit var lm_donations: LinearLayoutManager
    lateinit var donationsAdapter: DonationsAdapter
    lateinit var specialcauseUpdatesAdapter: ChurchUpdatesAdapter
    lateinit var loading_dialog: Dialog
    lateinit var userNotifications: HashMap<String, String>

    var isclickable: Boolean = false


    lateinit var projectDonationsArrayList: ArrayList<projectDonationsArrayResponse>
    lateinit var projectUpdatesArrayList: ArrayList<projectUpdatesArrayResponse>
    lateinit var specialcauseUpdatesArrayList: ArrayList<SpecialCauseUpdatesArrayResponse>
    lateinit var specialcauseDonationsArrayList: ArrayList<SpecialCauseDonationsArrayResponse>
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_project_detailed_view)
        @Suppress("DEPRECATION")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        from_screen = intent.getStringExtra("from_screen")!!
        organiz_id = intent.getStringExtra("organiz_id")!!
        project_id = intent.getStringExtra("project_id")!!
        project_name = intent.getStringExtra("project_name")!!
        display_name = intent.getStringExtra("display_name")!!
        Log.e("NOTIPROJECTID", project_id + "------------------" + project_name)
        /*from_screen = intent.getStringExtra("from_screen")!!
        organiz_id = intent.getStringExtra("organiz_id")!!
        project_id = intent.getStringExtra("project_id")!!
        project_name = intent.getStringExtra("project_name")!!
        display_name = intent.getStringExtra("display_name")!!*/
        specialCauseId = intent.getStringExtra("project_id")!!
        project_name = intent.getStringExtra("project_name")!!
        Log.e(
            "project_details_input",
            from_screen + "-----" + organiz_id + "----" + project_id + "----" + specialCauseId
        )
        initialize()
        header.text = project_name
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        /*  if (from_screen == "project") {
              donationsAdapter = DonationsAdapter(this@ProjectDetailedViewActivity, specialcauseDonationsArrayList)
          } else if (from_screen == "special_cause") {
              donationsAdapter = DonationsAdapter(this@ProjectDetailedViewActivity, specialcauseDonationsArrayList)
              specialcauseUpdatesAdapter = ChurchUpdatesAdapter(this@ProjectDetailedViewActivity, specialcauseUpdatesArrayList)
          }
          rv_donations.adapter = donationsAdapter*/
        if (from_screen == "project") {
            ll_hosted.visibility = View.GONE
            ll_last_day.visibility = View.GONE
            tv_follow.visibility = View.GONE
            tv_church_bio.visibility = View.VISIBLE
            tv_projects.text = getString(R.string.project_updates)

            val obj = JSONObject()
            obj.put("userId", userId)
            obj.put("projectId", project_id)
            val jParser = JsonParser()
            val jObject = jParser.parse(obj.toString()) as JsonObject
            Log.e("project_details_input", jObject.toString())
            ProfileDetailsApi(jObject)
            //  donationsAdapter = DonationsAdapter(this@ProjectDetailedViewActivity, specialcauseDonationsArrayList)
        } else if (from_screen == "special_cause") {
            ll_hosted.visibility = View.VISIBLE
            ll_last_day.visibility = View.VISIBLE
            tv_follow.visibility = View.VISIBLE
            tv_church_bio.visibility = View.VISIBLE
            tv_projects.text = getString(R.string.special_cause_updates)

            val objspl = JSONObject()
            objspl.put("userId", userId)
            objspl.put("specialCauseId", specialCauseId)
            val jParserspl = JsonParser()
            val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
            Log.e("project_details_input", jObjectspl.toString())
            ProfileDetailsSpecialCauseApi(jObjectspl)

            //  donationsAdapter = DonationsAdapter(this@ProjectDetailedViewActivity, specialcauseDonationsArrayList)
            // specialcauseUpdatesAdapter = ChurchUpdatesAdapter(this@ProjectDetailedViewActivity, specialcauseUpdatesArrayList)
        }

        ll_total_donations.setOnClickListener {

            Log.e("isclickable", isclickable.toString() + "---" + from_screen)
            if (isclickable) {
                val intent =
                    Intent(this@ProjectDetailedViewActivity, DonorsDetailViewActivity::class.java)
                intent.putExtra("from_screen", from_screen)
                intent.putExtra("organiz_id", organiz_id)
                intent.putExtra("project_id", project_id)
                intent.putExtra("project_name", project_name)
                startActivity(intent)
            } else {
                // Toast.makeText(applicationContext, "No Donations Found", Toast.LENGTH_SHORT).show()
            }


        }

        tv_follow.setOnClickListener {
            if (isFollowing) {
                Haptics().vibrate(this)
                setFollowingDisabled()
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("causeId", specialCauseId)
                followobj.put("status", UNFOLLOW)
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                followingApi(followjObject)
            } else {
                Haptics().vibrate(this)
                setFollowingEnabled()
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("causeId", specialCauseId)
                followobj.put("status", FOLLOW)
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                followingApi(followjObject)
            }
        }

        tv_donate.setOnClickListener {
            Haptics().vibrate(this)
            Log.e("from_screen_funds", from_screen)
            val intent = Intent(this@ProjectDetailedViewActivity, DonateFundsActivity::class.java)
            intent.putExtra("organiz_id", organiz_id)
            intent.putExtra("display_name", display_name)
            intent.putExtra("from_screen", from_screen)
            intent.putExtra("project_name", project_name)
            startActivity(intent)
            /*if (from_screen == "project") {
                finish()
            }*/
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet

        userNotifications = sessionManager.getUserNotifications

        img_project_profile_id = findViewById(R.id.img_project_profile_id)
        header = findViewById(R.id.header)
        tv_hosted_by = findViewById(R.id.tv_hosted_by)
        ll_hosted = findViewById(R.id.ll_hosted)
        tv_amount_received = findViewById(R.id.tv_amount_received)
        tv_total_amount = findViewById(R.id.tv_total_amount)
        seekBar = findViewById(R.id.seekBar)
        ll_last_day = findViewById(R.id.ll_last_day)
        ll_total_donations = findViewById(R.id.ll_total_donations)
        tv_last_day = findViewById(R.id.tv_last_day)
        tv_church_bio = findViewById(R.id.tv_church_bio)
        tv_projects = findViewById(R.id.tv_projects)
        rv_updates = findViewById(R.id.rv_updates)
        tv_updates_empty = findViewById(R.id.tv_updates_empty)
        tv_donated_by = findViewById(R.id.tv_donated_by)
        tv_donated_amount = findViewById(R.id.tv_donated_amount)
        rv_donations = findViewById(R.id.rv_donations)
        tv_donations_empty = findViewById(R.id.tv_donations_empty)
        tv_follow = findViewById(R.id.tv_follow)
        tv_donate = findViewById(R.id.tv_donate)
        iv_back = findViewById(R.id.iv_back)
        tv_last_day.setTypeface(tv_last_day.typeface, Typeface.ITALIC)
        lm_updates =
            LinearLayoutManager(this@ProjectDetailedViewActivity, RecyclerView.VERTICAL, false)
        lm_donations =
            LinearLayoutManager(this@ProjectDetailedViewActivity, RecyclerView.VERTICAL, false)

        rv_updates.layoutManager = lm_updates
        rv_donations.layoutManager = lm_donations

        rv_updates.isNestedScrollingEnabled = false
        rv_donations.isNestedScrollingEnabled = false


        //donationsAdapter.notifyDataSetChanged()


    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setFollowingDisabled() {
        tv_follow.setTextColor(ContextCompat.getColor(this, R.color.login_button_background))
        tv_follow.background = ContextCompat.getDrawable(this, R.drawable.button_background_white)
        tv_follow.text = getString(R.string.follow)
        isFollowing = false
    }

    private fun setFollowingEnabled() {
        tv_follow.setTextColor(ContextCompat.getColor(this, R.color.white))
        tv_follow.background = ContextCompat.getDrawable(this, R.drawable.button_background_blue)
        tv_follow.text = getString(R.string.following)
        isFollowing = true
    }

    private fun ProfileDetailsApi(json_obj: JsonObject) {
        loading_dialog.show()

        projectDonationsArrayList = ArrayList()
        projectDonationsArrayList.clear()
        projectUpdatesArrayList = ArrayList()
        projectUpdatesArrayList.clear()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectDetailsApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                loading_dialog.dismiss()
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val project_details = jsondata.project!![0]

                        if (!jsondata.projectUpdates!!.size.equals(null)) {
                            for (i in 0 until jsondata.projectUpdates.size) {
                                projectUpdatesArrayList.add(response.body()!!.projectUpdates!![i])
                            }
                        }
                        val adapter_updates = ChurchProjectUpdatesAdapter(
                            this@ProjectDetailedViewActivity,
                            projectUpdatesArrayList
                        )
                        rv_updates.adapter = adapter_updates
                        adapter_updates.notifyDataSetChanged()

                        isclickable =
                            !(jsondata.projectDonations!!.size.equals(null) || jsondata.projectDonations.size.equals(
                                ""
                            ) || jsondata.projectDonations.size.equals(0) || jsondata.projectDonations.size.equals(
                                "0"
                            ))

                        Log.e("PROJECT", isclickable.toString())
                        if (project_details.projectPicture.toString() == "") {
                            Picasso.with(this@ProjectDetailedViewActivity)
                                .load(R.drawable.ic_default_project)
                                .into(img_project_profile_id)
                        } else {
                            Picasso.with(this@ProjectDetailedViewActivity)
                                .load(project_details.projectPicture.toString())
                                .error(R.drawable.ic_default_project)
                                .into(img_project_profile_id)
                        }
                        val donationByLength = jsondata.donationDetails!!.donationsByLength
                        val totalAmount = jsondata.donationDetails.totalAmount
                        tv_church_bio.text = project_details.description.toString()
                        tv_donated_by.text =
                            donationByLength.toString() + " people have made donation"
                        tv_donated_amount.text = donationByLength.toString()

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun ProfileDetailsSpecialCauseApi(json_obj: JsonObject) {
        loading_dialog.show()
        specialcauseDonationsArrayList = ArrayList()
        specialcauseUpdatesArrayList = ArrayList()
        specialcauseDonationsArrayList.clear()
        specialcauseUpdatesArrayList.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectDetailsSpecialCauseApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                loading_dialog.dismiss()
                try {
                    val jsondata = response!!.body()!!
                    val donationDetails = jsondata.donationDetails
                    val specialCauseDonations = jsondata.specialCauseDonations
                    var donationByLength = 0
                    var totalAmount = 0
                    if (donationDetails != null) {
                        donationByLength = donationDetails.donationsByLength!!
                        totalAmount = donationDetails.totalAmount!!.toInt()
                    }
                    if (specialCauseDonations != null) {
                        isclickable =
                            !(specialCauseDonations.size.equals(null) || specialCauseDonations.size.equals(
                                ""
                            ) || specialCauseDonations.size.equals(0))
                    }

                    Log.e("SPECIAL_CAUSE", isclickable.toString())

                    tv_donated_by.text = "$donationByLength people have made donation"
                    tv_donated_amount.text = donationByLength.toString()
                    if (jsondata.responseStatus!! == 1) {
                        val specialcausedetails_details = jsondata.specialCause!![0]
                        val name = specialcausedetails_details.name
                        val description = specialcausedetails_details.description
                        val endDate = specialcausedetails_details.endDate
                        val goalAmount = specialcausedetails_details.goalAmount
                        val orgId = specialcausedetails_details.orgId
                        orgName = specialcausedetails_details.orgName.toString()
                        val startDate = specialcausedetails_details.startDate
                        val status = specialcausedetails_details.status
                        val followStatus = specialcausedetails_details.followStatus

                        tv_church_bio.text = description
                        //seekBar.progress = goalAmount!!.toInt()
                        seekBar.max = goalAmount!!
                        if (totalAmount > goalAmount) {
                            seekBar.progress = goalAmount
                        } else {
                            seekBar.progress = totalAmount
                        }
                        tv_total_amount.text = "$ $goalAmount"
                        tv_amount_received.text = "$ $totalAmount"
                        tv_hosted_by.text = "Hosted By $orgName"
                        if (followStatus == "0") {
                            setFollowingDisabled()
                        } else if (followStatus == "1") {
                            setFollowingEnabled()
                        }

                        /*if (endDate != null) {
                            val formatter =
                                SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.US)
                            val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                            val date = formatter.parse(endDate)!!
                            val date_stg = sdf.format(date)
                            Log.d("date", "date $date $date_stg")
                            tv_last_day.setText("Last Day to Donate: " + date_stg)
                        }*/
                        tv_last_day.text = "Last Day to Donate: $endDate"
                        if (specialcausedetails_details.projectPicture.toString() == "") {
                            Picasso.with(this@ProjectDetailedViewActivity)
                                .load(R.drawable.ic_default_spl_causes)
                                .into(img_project_profile_id)
                        } else {
                            Picasso.with(this@ProjectDetailedViewActivity)
                                .load(specialcausedetails_details.projectPicture.toString())
                                .error(R.drawable.ic_default_spl_causes)
                                .into(img_project_profile_id)
                        }
                        if (!jsondata.specialCauseDonations!!.size.equals(null)) {
                            for (i in 0 until jsondata.specialCauseDonations.size) {
                                specialcauseDonationsArrayList.add(response.body()!!.specialCauseDonations!![i])
                            }
                        } else {

                        }
                        if (!jsondata.specialCauseUpdates!!.size.equals(null)) {
                            for (i in 0 until jsondata.specialCauseUpdates.size) {
                                specialcauseUpdatesArrayList.add(response.body()!!.specialCauseUpdates!![i])
                            }
                        }

                        val adapter = DonationsAdapter(
                            this@ProjectDetailedViewActivity,
                            specialcauseDonationsArrayList
                        )
                        rv_donations.adapter = adapter
                        adapter.notifyDataSetChanged()

                        val adapter_updates = ChurchUpdatesAdapter(
                            this@ProjectDetailedViewActivity,
                            specialcauseUpdatesArrayList
                        )
                        rv_updates.adapter = adapter_updates
                        adapter.notifyDataSetChanged()

                    } else {
                        Toast.makeText(
                            this@ProjectDetailedViewActivity,
                            jsondata.result, Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun followingApi(final_object: JsonObject) {

        val api = ApiInterface.create()
        val call = api.causeFollowingApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProjectDetailedViewActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus!! == "1") {
                        /*Toast.makeText(
                            this@ProjectDetailedViewActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()*/
                    } else {
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
