package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.concurrent.Executor
import java.util.regex.Pattern

@SuppressLint("SetTextI18n")
class ProfileEmailActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var et_currentemail: EditText
    lateinit var et_newemail: EditText
    lateinit var et_confirmemail: EditText
    lateinit var loading_dialog: Dialog
    lateinit var tv_submit: TextView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var user_details: HashMap<String, String>
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var userId = ""
    var userEmail = ""
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_profile_email)
        initialize()
        et_currentemail.setText(userEmail)
        tv_submit.setOnClickListener {
            if (et_currentemail.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileEmailActivity,
                    "Email cannot be empty!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (!isValidEmail(et_currentemail.text.toString())) {
                Toast.makeText(this, "Email is not valid!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_newemail.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileEmailActivity,
                    "New email cannot be empty!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (!isValidEmail(et_newemail.text.toString())) {
                Toast.makeText(this, "New Email is not valid!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_confirmemail.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileEmailActivity,
                    "Confirmation Email cannot be empty!",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (!isValidEmail(et_confirmemail.text.toString())) {
                Toast.makeText(this, "Confirmation  Email is not valid!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_newemail.text.toString() != et_confirmemail.text.toString()) {
                Toast.makeText(this, "Emails do not match!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                showBiometricPrompt()
            }
        }
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        et_currentemail = findViewById(R.id.et_currentemail)
        et_newemail = findViewById(R.id.et_newemail)
        et_confirmemail = findViewById(R.id.et_confirmemail)
        tv_submit = findViewById(R.id.tv_submit)
        loadingDialog()

        passCodeDialog()

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
        userEmail = user_details[SessionManager.EMAIL_KEY]!!
        passCode = user_details[SessionManager.PASSCODE_KEY]!!
        biometricManager = BiometricManager.from(this@ProfileEmailActivity)
        executor = ContextCompat.getMainExecutor(this@ProfileEmailActivity)
        biometrics = user_details[SessionManager.FACEID_KEY]!!
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ProfileEmailActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ProfileEmailActivity)
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    emailJson()
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    emailJson()
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            emailJson()
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)                               // true to use pin/pattern/password if biometric is not set
            .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@ProfileEmailActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    emailJson()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                    // showToast("Auth failed")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())

                    /*if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {

                    } else if (errorCode == BiometricConstants.ERROR_CANCELED) {
                        Log.e("Biometrics", "biometric operation cancelled by user.")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT_PERMANENT) {
                        Log.e("Biometrics", "ERROR_LOCKOUT occurred too many times")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT) {
                        Log.e("Biometrics", "the API is locked out due to too many attempts")
                    } else if (errorCode == BiometricConstants.ERROR_NO_BIOMETRICS) {
                        Log.e("Biometrics", "user does not have any biometrics enrolled")
                    } else if (errorCode == BiometricConstants.ERROR_UNABLE_TO_PROCESS) {
                        Log.e("Biometrics", "the sensor was unable to process the current image")
                    }*/
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
         et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
                emailJson()
            } else {
                ll_check_email_passcode.visibility = View.GONE
                Toast.makeText(
                    this@ProfileEmailActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT
                ).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj=JSONObject()
            obj.put("passcode",passCode)
            obj.put("email",userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun emailJson() {
        if (isNetworkAvailable) {
            val jObject = JSONObject()
            jObject.put("uid", userId)
            jObject.put("email", et_currentemail.text.toString())
            jObject.put("newEmail", et_newemail.text.toString())
            val jParser = JsonParser()
            val final_object = jParser.parse(jObject.toString()) as JsonObject
            updateEmail(final_object)
        } else {
            Toast.makeText(
                this@ProfileEmailActivity,
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun updateEmail(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateEmailApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileEmailActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            sessionManager.updateEmail(et_newemail.text.toString())
                            et_currentemail.setText(et_newemail.text.toString())
                            et_newemail.setText("")
                            et_confirmemail.setText("")
                            Toast.makeText(
                                this@ProfileEmailActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileEmailActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ProfileEmailActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text = getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
