package com.piesagiving.com.Activities

import android.content.IntentSender
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationServicesActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    lateinit var iv_back: ImageView
    lateinit var switch_location: SwitchMaterial
    lateinit var tv_gps_enable: TextView
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_locationservices)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        switch_location.setOnCheckedChangeListener { compoundButton, b ->

            if (b) {
                // The toggle is enabled
                EnableGPSAutoMatically()
                tv_gps_enable.text = getString(R.string.on)
                val objspl = JSONObject()
                objspl.put("userId", userId)
                objspl.put("geotagging", 1)
                val jParserspl = JsonParser()
                val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
                Log.e("LocationGeotagging", jObjectspl.toString())
                LocationServiceApi(jObjectspl)
            } else {
                // The toggle is disabled
                tv_gps_enable.text = getString(R.string.enable_geotagging)
            }

        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        switch_location = findViewById(R.id.switch_location)
        tv_gps_enable = findViewById(R.id.tv_gps_enable)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun EnableGPSAutoMatically() {
        var googleApiClient: GoogleApiClient? = null
        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build()
            googleApiClient!!.connect()
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = (30 * 1000).toLong()
            locationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            // **************************
            builder.setAlwaysShow(true) // this is the key ingredient
            // **************************

            val result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build())
            result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                override fun onResult(result: LocationSettingsResult) {
                    val status = result.status
                    val state = result
                        .locationSettingsStates
                    when (status.statusCode) {
                        LocationSettingsStatusCodes.SUCCESS -> toast("Success")


                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            toast("GPS is not on")
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(this@LocationServicesActivity, 1000)

                            } catch (e: IntentSender.SendIntentException) {
                                // Ignore the error.
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> toast("Setting change not allowed")
                    }// All location settings are satisfied. The client can
                    // initialize location
                    // requests here.
                    // Location settings are not satisfied. However, we have
                    // no way to fix the
                    // settings so we won't show the dialog.
                }
            })
        }
    }

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    private fun toast(message: String) {
        try {
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            // log("Window has been closed")
        }

    }



    private fun LocationServiceApi(json_obj: JsonObject) {
        //  loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.LocationServiceApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }
            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata=response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result=jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }else{
                        val result=jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


}
