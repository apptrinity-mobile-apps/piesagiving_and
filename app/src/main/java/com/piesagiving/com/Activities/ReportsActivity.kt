package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.SectionRecyclerViewAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.DonationReportsResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception

@Suppress("DEPRECATION")
class ReportsActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    lateinit var iv_back: ImageView
    lateinit var sp_room_no_id: Spinner
    lateinit var loading_dialog: Dialog
    lateinit var tv_empty: TextView

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    var userId = ""
    lateinit var report_types_array: ArrayList<String>
    lateinit var report_types_array_ids: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_reports)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()

        iv_back.setOnClickListener {
            onBackPressed()
        }

        val adapter = ArrayAdapter(
            this, R.layout.support_simple_spinner_dropdown_item, report_types_array
        )
        sp_room_no_id.adapter = adapter

        sp_room_no_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                val type = report_types_array_ids[position]
                val dataObj = JSONObject()
                dataObj.put("userId", userId)
                dataObj.put("reportType", type)
                val jParser = JsonParser()
                val final_object = jParser.parse(dataObj.toString()) as JsonObject
                Log.d("type_stg", final_object.toString())
                getReportsApi(final_object)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!

        report_types_array = ArrayList()
        report_types_array_ids = ArrayList()

        report_types_array_ids.add("month")
        report_types_array_ids.add("year")

        report_types_array.add("Monthly Reports")
        report_types_array.add("Year End Reports")

        iv_back = findViewById(R.id.iv_back)
        recyclerView = findViewById(R.id.recycler_view)
        tv_empty = findViewById(R.id.tv_empty)
        sp_room_no_id = findViewById(R.id.sp_room_no_id)
        val layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ReportsActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ReportsActivity)
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getReportsApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getDonationsReportsApi(final_object)
        call.enqueue(object : Callback<DonationReportsResponse> {
            override fun onFailure(call: Call<DonationReportsResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()

                    tv_empty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                    Toast.makeText(
                        this@ReportsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<DonationReportsResponse>,
                response: Response<DonationReportsResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val history = resp.history
                            if (history!!.size > 0) {
                                tv_empty.visibility = View.GONE
                                recyclerView.visibility = View.VISIBLE
                            } else {
                                tv_empty.visibility = View.VISIBLE
                                recyclerView.visibility = View.GONE
                            }
                            val adapter =
                                SectionRecyclerViewAdapter(this@ReportsActivity, history)
                            recyclerView.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }
                        else -> {
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
