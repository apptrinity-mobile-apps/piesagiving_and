package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R

class HelpActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_faqs: LinearLayout
    lateinit var ll_contactus: LinearLayout
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        
        setContentView(R.layout.activity_help)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        ll_faqs.setOnClickListener {
            val intent = Intent(this, BrowseFaqs::class.java)
            intent.putExtra("from_intent","")
            startActivity(intent)
        }
        ll_contactus.setOnClickListener {
            val intent = Intent(this, ContactUsActivity::class.java)
            startActivity(intent)
        }


    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        ll_faqs = findViewById(R.id.ll_faqs)
        ll_contactus = findViewById(R.id.ll_contactus)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


}
