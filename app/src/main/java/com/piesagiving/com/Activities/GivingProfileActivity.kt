package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.ChurchesProfileAdapter
import com.piesagiving.com.Adapters.FollowersAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.*
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class GivingProfileActivity : AppCompatActivity() {

    lateinit var iv_share: ImageView
    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    lateinit var tv_follow_church: TextView
    lateinit var tv_church_name: TextView
    lateinit var tv_church_location: TextView
    lateinit var ll_yourcircle: LinearLayout
    lateinit var rv_spl_causes: RecyclerView
    lateinit var rv_churches: RecyclerView
    lateinit var rv_charities: RecyclerView
    lateinit var lm_churches: LinearLayoutManager
    lateinit var lm_charities: LinearLayoutManager
    lateinit var lm_spl_causes: LinearLayoutManager
    lateinit var lm_projects: LinearLayoutManager
    lateinit var iv_profile: ImageView
    lateinit var cv_profile: CardView
    lateinit var loading_dialog: Dialog
    lateinit var tabLayout: TabLayout
    lateinit var ll_following_organizations: LinearLayout
    lateinit var ll_followers: LinearLayout
    lateinit var rv_followers: RecyclerView
    lateinit var tv_empty_followers: TextView
    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView
    lateinit var ivTabOne: ImageView
    lateinit var ivTabTwo: ImageView
    lateinit var followersList: ArrayList<UsersDataResponse>
    lateinit var followersAdapter: FollowersAdapter
    lateinit var tv_churches_empty: TextView
    lateinit var tv_charities_empty: TextView
    lateinit var tv_spl_causes_empty: TextView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    var isFollowingChurch = false
    lateinit var sessionManager: SessionManager
    var from_screen = ""
    lateinit var mChurchesListData: ArrayList<CharityListDataResponse>
    lateinit var mCharitiesListData: ArrayList<CharityListDataResponse>
    lateinit var mSpecialCausesListData: ArrayList<CharityListDataResponse>
    var user_name = ""
    var user_location = ""
    var user_img = ""
    var userId = ""
    var first_name = ""
    var last_name = ""
    lateinit var user_details: HashMap<String, String>
    val REQUEST_IMAGE = 100
    lateinit var bitmap_data: Bitmap
    lateinit var bytearray_img: ByteArray
    lateinit var encodedImage: String
    var imageFileName = "PiesaGivingImage"
    var MY_CAMERA_REQUEST_CODE = 1000

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_giving_profile)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()

        //iv_profile.setImageResource(R.drawable.ic_church_profile)
        tv_church_name.text = user_name
        tv_header.text = "$first_name $last_name"
        tv_church_location.text = user_location

        if (isNetworkAvailable) {
            val obj = JSONObject()
            obj.put("userId", userId)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            getFollowingUsersListApi(final_object)
        } else {
            Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                .show()
        }

        /*if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_GRANTED
        ) {
            // do nothing
        } else {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
//                Toast.makeText(applicationContext, "Permission Needed.", Toast.LENGTH_LONG).show()
            }
            requestPermissions(
                arrayOf(
                    android.Manifest.permission.CAMERA*//*,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE*//*
                ), MY_CAMERA_REQUEST_CODE
            )
        }*/

        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }

        iv_profile.setOnClickListener {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED
            ) {
                showImagePickerOptions()
            } else {
                if (shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
//                    Toast.makeText(applicationContext, "Permission Needed.", Toast.LENGTH_LONG) .show()
                }
                requestPermissions(
                    arrayOf(
                        android.Manifest.permission.CAMERA/*,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE*/
                    ), MY_CAMERA_REQUEST_CODE
                )
            }
        }

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()

        user_location = user_details[SessionManager.STATE_KEY]!!
        user_img = user_details[SessionManager.PROFILEPIC_KEY]!!
        userId = user_details[SessionManager.ID_KEY]!!
        user_name = user_details[SessionManager.USERNAME_KEY]!!
        first_name = user_details[SessionManager.FIRST_NAME_KEY]!!
        last_name = user_details[SessionManager.LAST_NAME_KEY]!!

        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_church_name = findViewById(R.id.tv_church_name)
        tv_church_location = findViewById(R.id.tv_church_location)
        tv_spl_causes_empty = findViewById(R.id.tv_spl_causes_empty)
        tv_churches_empty = findViewById(R.id.tv_churches_empty)
        tv_charities_empty = findViewById(R.id.tv_charities_empty)
        ll_yourcircle = findViewById(R.id.ll_yourcircle)
        rv_spl_causes = findViewById(R.id.rv_spl_causes)
        rv_churches = findViewById(R.id.rv_churches)
        rv_charities = findViewById(R.id.rv_charities)
        iv_profile = findViewById(R.id.iv_profile)
        cv_profile = findViewById(R.id.cv_profile)
        ll_following_organizations = findViewById(R.id.ll_following_organizations)
        ll_followers = findViewById(R.id.ll_followers)
        rv_followers = findViewById(R.id.rv_followers)
        tv_empty_followers = findViewById(R.id.tv_empty_followers)
        tabLayout = findViewById(R.id.tabLayout)
        setupTabIcons()

        if (user_img == "") {
            Picasso.with(this).load(R.drawable.ic_church_profile_edited).into(iv_profile)
        } else {
            Picasso.with(this).load(user_img).error(R.drawable.ic_church_profile_edited)
                .into(iv_profile)
        }
        lm_churches =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_charities =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_spl_causes =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        rv_churches.layoutManager = lm_churches
        lm_projects =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_charities =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_spl_causes =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.HORIZONTAL, false)
        val layoutManager =
            LinearLayoutManager(this@GivingProfileActivity, RecyclerView.VERTICAL, false)
        rv_churches.layoutManager = lm_projects
        rv_charities.layoutManager = lm_charities
        rv_spl_causes.layoutManager = lm_spl_causes
        rv_followers.layoutManager = layoutManager

        getMyCircleListApi()
        ViewSpecialCauseDonation()

        rv_churches.addOnItemTouchListener(
            RecyclerItemClickListener(this@GivingProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        /*val intent = Intent(this@GivingProfileActivity!!, ChurchProfileActivity::class.java)
                        intent.putExtra("from_screen", "profile")
                        intent.putExtra("display_name", mChurchesListData[position].displayName.toString())
                        intent.putExtra("reco_organizationId", mChurchesListData[position].id)
                        startActivity(intent)*/
                    }
                })
        )

        rv_charities.addOnItemTouchListener(
            RecyclerItemClickListener(this@GivingProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        /*val intent = Intent(this@GivingProfileActivity!!, ChurchProfileActivity::class.java)
                        intent.putExtra("from_screen", "profile")
                        intent.putExtra("display_name", mChurchesListData[position].displayName.toString())
                        intent.putExtra("reco_organizationId", mChurchesListData[position].id)
                        startActivity(intent)*/
                    }
                })
        )

        rv_spl_causes.addOnItemTouchListener(
            RecyclerItemClickListener(this@GivingProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        /* val intent = Intent(this@GivingProfileActivity!!, ChurchProfileActivity::class.java)
                         intent.putExtra("from_screen", "profile")
                         intent.putExtra("display_name", mChurchesListData[position].displayName.toString())
                         intent.putExtra("reco_organizationId", mChurchesListData[position].id)
                         startActivity(intent)*/
                    }
                })
        )

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when {
                    tab!!.position == 0 -> {
                        tabOne.setTextColor(
                            ContextCompat.getColor(
                                this@GivingProfileActivity,
                                R.color.login_button_background
                            )
                        )
                        tabTwo.setTextColor(
                            ContextCompat.getColor(
                                this@GivingProfileActivity,
                                R.color.black
                            )
                        )
                        ivTabOne.setImageResource(R.drawable.ic_fav_selected_new)
                        ivTabTwo.setImageResource(R.drawable.ic_friends_unselected_new)
                        ll_following_organizations.visibility = View.VISIBLE
                        ll_followers.visibility = View.GONE
                    }
                    tab.position == 1 -> {
                        tabOne.setTextColor(
                            ContextCompat.getColor(
                                this@GivingProfileActivity,
                                R.color.black
                            )
                        )
                        tabTwo.setTextColor(
                            ContextCompat.getColor(
                                this@GivingProfileActivity,
                                R.color.login_button_background
                            )
                        )
                        ivTabOne.setImageResource(R.drawable.ic_fav_unselected_new)
                        ivTabTwo.setImageResource(R.drawable.ic_friends_selected_new)
                        ll_following_organizations.visibility = View.GONE
                        ll_followers.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@GivingProfileActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@GivingProfileActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setupTabIcons() {
        val view =
            LayoutInflater.from(this@GivingProfileActivity).inflate(R.layout.tab_header, null)
        val view1 =
            LayoutInflater.from(this@GivingProfileActivity).inflate(R.layout.tab_header, null)
        tabOne = view.findViewById(R.id.tv_tab)
        tabTwo = view1.findViewById(R.id.tv_tab)
        ivTabOne = view.findViewById(R.id.iv_tab)
        ivTabTwo = view1.findViewById(R.id.iv_tab)
        ivTabOne.visibility = View.VISIBLE
        ivTabTwo.visibility = View.VISIBLE
        tabOne.visibility = View.GONE
        tabTwo.visibility = View.GONE
        ivTabOne.setImageResource(R.drawable.ic_fav_selected_new)
        ivTabTwo.setImageResource(R.drawable.ic_friends_unselected_new)

        tabOne.text = getString(R.string.organizations)
        tabTwo.text = getString(R.string.people)
        tabOne.setTextColor(
            ContextCompat.getColor(this@GivingProfileActivity, R.color.login_button_background)
        )
        tabLayout.addTab(tabLayout.newTab().setCustomView(view), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1))
        ll_following_organizations.visibility = View.VISIBLE
        ll_followers.visibility = View.GONE
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED /*&& grantResults[1] == PackageManager.PERMISSION_GRANTED*/) {
                //showImagePickerOptions()
            } else {
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
                    bitmap_data = bitmap
                    val bm = BitmapFactory.decodeFile(ImageFilePath.getPath(this, uri!!))
                    val baos = ByteArrayOutputStream()
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
                    bytearray_img = baos.toByteArray()
                    var encodedImage = Base64.encodeToString(bytearray_img, Base64.DEFAULT)
                    Log.e(
                        "upload_url",
                        "-----" + ImageFilePath.getPath(this, uri) + "-----" + encodedImage
                    )
                    encodedImage = encodedImage.replace("\n", "")
                    val dataObj = JSONObject()
                    dataObj.put("userId", userId)
                    dataObj.put("profilePic", encodedImage)
                    val jParser = JsonParser()
                    val finalObject = jParser.parse(dataObj.toString()) as JsonObject
                    Log.e("upload_url", "-----$finalObject")
                    uploadProfilePicApi(finalObject)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(
            this,
            object : ImagePickerActivity.PickerOptionListener {
                override fun onTakeCameraSelected() {
                    launchCameraIntent()
                }

                override fun onChooseGallerySelected() {
                    launchGalleryIntent()
                }
            })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this@GivingProfileActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_IMAGE_CAPTURE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 400)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 400)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this@GivingProfileActivity, ImagePickerActivity::class.java)
        intent.putExtra(
            ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION,
            ImagePickerActivity.REQUEST_GALLERY_IMAGE
        )

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun uploadProfilePicApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateProfilePicApi(final_object)
        call.enqueue(object : Callback<ProfilePicResponse> {
            override fun onFailure(call: Call<ProfilePicResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@GivingProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ProfilePicResponse>,
                response: Response<ProfilePicResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()!!
                    if (resp.responseStatus == "1") {
                        sessionManager.updateProfilePic(resp.profilePic!!)
                        Toast.makeText(
                            this@GivingProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                        iv_profile.setImageBitmap(bitmap_data)
                    } else {
                        Toast.makeText(
                            this@GivingProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getMyCircleListApi() {

        val dataObj = JSONObject()
        dataObj.put("userId", userId)
        Log.d("TAG", "dataObj===$dataObj")
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject

        loading_dialog.show()
        mChurchesListData = ArrayList()
        mCharitiesListData = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetMyCirclesApi(finalJsonObject)
        call.enqueue(object : Callback<GetMyCirclePojo> {
            override fun onFailure(call: Call<GetMyCirclePojo>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@GivingProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMyCirclePojo>,
                response: Response<GetMyCirclePojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.charityList
                            val data2 = resp.churchList
                            if (data2!!.size > 0) {

                                for (i in 0 until response.body()!!.churchList!!.size) {
                                    mChurchesListData.add(response.body()!!.churchList!!.get(i))
                                }
                                // mChurchesListData = data2
                                mChurchesListData.reverse()
                                rv_churches.layoutManager = lm_churches
                                rv_churches.setHasFixedSize(true)
                                val adapter = ChurchesProfileAdapter(
                                    this@GivingProfileActivity,
                                    mChurchesListData
                                )
                                rv_churches.adapter = adapter
                                adapter.notifyDataSetChanged()

                                val linear = SnapHelperOneByOne()
                                linear.attachToRecyclerView(rv_churches)
                                if (mChurchesListData.size > 0) {
                                    tv_churches_empty.visibility = View.GONE
                                    rv_churches.visibility = View.VISIBLE
                                } else {
                                    tv_churches_empty.visibility = View.VISIBLE
                                    rv_churches.visibility = View.GONE
                                }
                            } else {
                                tv_churches_empty.visibility = View.VISIBLE
                                rv_churches.visibility = View.GONE
                            }

                            if (data!!.size > 0) {
                                for (i in 0 until response.body()!!.charityList!!.size) {
                                    mCharitiesListData.add(response.body()!!.charityList!!.get(i))
                                }
                                // mCharitiesListData = data
                                mCharitiesListData.reverse()
                                rv_charities.layoutManager = lm_charities
                                rv_charities.setHasFixedSize(true)

                                val adapter = ChurchesProfileAdapter(
                                    this@GivingProfileActivity,
                                    mCharitiesListData
                                )
                                rv_charities.adapter = adapter
                                adapter.notifyDataSetChanged()

                                val linear = SnapHelperOneByOne()
                                linear.attachToRecyclerView(rv_charities)
                                if (mCharitiesListData.size > 0) {
                                    tv_charities_empty.visibility = View.GONE
                                    rv_charities.visibility = View.VISIBLE
                                } else {
                                    tv_charities_empty.visibility = View.VISIBLE
                                    rv_charities.visibility = View.GONE
                                }
                            } else {
                                tv_charities_empty.visibility = View.VISIBLE
                                rv_charities.visibility = View.GONE
                            }
                        }
                        else -> {
                            tv_charities_empty.visibility = View.VISIBLE
                            rv_charities.visibility = View.GONE
                            tv_churches_empty.visibility = View.VISIBLE
                            rv_churches.visibility = View.GONE
                            /* Toast.makeText(
                                 this@GivingProfileActivity!!,
                                 getString(R.string.no_record_available),
                                 Toast.LENGTH_SHORT
                             ).show()*/
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun ViewSpecialCauseDonation() {

        val dataObj = JSONObject()
        dataObj.put("userId", userId)
        Log.d("TAG", "dataObj===$dataObj")
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject

        loading_dialog.show()
        mSpecialCausesListData = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.ViewSpecialCauseDonationApi(finalJsonObject)
        call.enqueue(object : Callback<GetMyCirclePojo> {
            override fun onFailure(call: Call<GetMyCirclePojo>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@GivingProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMyCirclePojo>,
                response: Response<GetMyCirclePojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.SpecialCauses
                            if (data!!.size > 0) {

                                for (i in 0 until response.body()!!.SpecialCauses!!.size) {
                                    mSpecialCausesListData.add(
                                        response.body()!!.SpecialCauses!![i]
                                    )
                                }
                                mSpecialCausesListData.reverse()
                                rv_spl_causes.layoutManager = lm_spl_causes
                                rv_spl_causes.setHasFixedSize(true)

                                val adapter = ChurchesProfileAdapter(
                                    this@GivingProfileActivity,
                                    mSpecialCausesListData
                                )
                                rv_spl_causes.adapter = adapter
                                adapter.notifyDataSetChanged()

                                val linear = SnapHelperOneByOne()
                                linear.attachToRecyclerView(rv_spl_causes)
                                if (mSpecialCausesListData.size > 0) {
                                    tv_spl_causes_empty.visibility = View.GONE
                                    rv_spl_causes.visibility = View.VISIBLE
                                } else {
                                    tv_spl_causes_empty.visibility = View.VISIBLE
                                    rv_spl_causes.visibility = View.GONE
                                }
                            } else {
                                tv_spl_causes_empty.visibility = View.VISIBLE
                                rv_spl_causes.visibility = View.GONE
                            }
                        }
                        else -> {
                            /*Toast.makeText(
                                this@GivingProfileActivity!!,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            ).show()*/
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun getFollowingUsersListApi(final_object: JsonObject) {
        loading_dialog.show()
        followersList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getFollowingUsersListApi(final_object)
        call.enqueue(object : Callback<AllUsersResponse> {
            override fun onFailure(call: Call<AllUsersResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@GivingProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllUsersResponse>,
                response: Response<AllUsersResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.follwers_list
                        followersList = data!!
                        followersAdapter =
                            FollowersAdapter(this@GivingProfileActivity, followersList, userId)
                        rv_followers.adapter = followersAdapter
                        followersAdapter.notifyDataSetChanged()
                        if (followersList.size > 0) {
                            rv_followers.visibility = View.VISIBLE
                            tv_empty_followers.visibility = View.GONE
                        } else {
                            rv_followers.visibility = View.GONE
                            tv_empty_followers.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@GivingProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
