package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SecurityActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_signinhistory: LinearLayout
    lateinit var switch_faceid: SwitchMaterial
    lateinit var tv_face_id: TextView
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var BIOMETRIC_STATUS = 0
    private var biometrics = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_security)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        when (biometrics) {
            "1" -> {
                switch_faceid.isChecked = true
                tv_face_id.text = getString(R.string.on)
                tv_face_id.setTextColor(
                    ContextCompat.getColor(
                        this@SecurityActivity,
                        R.color.colorAccent
                    )
                )
            }
            "0" -> {
                switch_faceid.isChecked = false
                tv_face_id.text = getString(R.string.enable_fingerprint)
                tv_face_id.setTextColor(
                    ContextCompat.getColor(
                        this@SecurityActivity,
                        R.color.light_grey
                    )
                )
            }
            else -> {
                switch_faceid.isChecked = false
                tv_face_id.text = getString(R.string.enable_fingerprint)
                tv_face_id.setTextColor(
                    ContextCompat.getColor(
                        this@SecurityActivity,
                        R.color.light_grey
                    )
                )
            }
        }

        switch_faceid.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                BIOMETRIC_STATUS = 1
                val objspl = JSONObject()
                objspl.put("userId", userId)
                objspl.put("faceId", BIOMETRIC_STATUS)
                val jParserspl = JsonParser()
                val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
                Log.e("UserPrivacyApi", jObjectspl.toString())
                UserSecurityApi(jObjectspl, BIOMETRIC_STATUS)
                tv_face_id.text = getString(R.string.on)
                tv_face_id.setTextColor(
                    ContextCompat.getColor(
                        this@SecurityActivity,
                        R.color.colorAccent
                    )
                )
            } else {
                // The toggle is disabled
                BIOMETRIC_STATUS = 0
                val objspl = JSONObject()
                objspl.put("userId", userId)
                objspl.put("faceId", BIOMETRIC_STATUS)
                val jParserspl = JsonParser()
                val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
                Log.e("UserPrivacyApi", jObjectspl.toString())
                UserSecurityApi(jObjectspl, BIOMETRIC_STATUS)
                tv_face_id.text = getString(R.string.enable_fingerprint)
                tv_face_id.setTextColor(
                    ContextCompat.getColor(
                        this@SecurityActivity,
                        R.color.light_grey
                    )
                )
            }
        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        ll_signinhistory = findViewById(R.id.ll_signinhistory)
        switch_faceid = findViewById(R.id.switch_faceid)
        tv_face_id = findViewById(R.id.tv_face_id)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

        biometrics = user_details[SessionManager.FACEID_KEY]!!

        ll_signinhistory.setOnClickListener {
            val intent = Intent(this, LoginHistoryActivity::class.java)
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun UserSecurityApi(json_obj: JsonObject, biometricStatus: Int) {
        //  loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.UserSecurityApi(json_obj)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                        sessionManager.updateBiometrics(biometricStatus = biometricStatus)
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


}
