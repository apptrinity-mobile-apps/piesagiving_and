package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.CitiesAdapter
import com.piesagiving.com.Adapters.CountriesAdapter
import com.piesagiving.com.Adapters.StatesAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.CustomDatePickerDialog
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class RegisterAccountActivity : AppCompatActivity() {

    lateinit var et_first_name: EditText
    lateinit var et_last_name: EditText
    lateinit var et_email: EditText
    lateinit var et_street_address: EditText
    lateinit var et_city_name: EditText
    lateinit var et_state_name: EditText
    lateinit var et_country_name: EditText
    lateinit var et_zip_code: EditText
    lateinit var et_password: EditText
    lateinit var et_confirm_password: EditText
    lateinit var et_birthday: EditText
    lateinit var tv_sign_in: TextView
    lateinit var tv_submit: TextView
    lateinit var checkBox_terms: CheckBox
    lateinit var iv_back: ImageView
    lateinit var loading_dialog: Dialog
    lateinit var countriesRecyclerView: RecyclerView
    lateinit var statesRecyclerView: RecyclerView
    lateinit var citiesRecyclerView: RecyclerView
    lateinit var tv_emptyCountries: TextView
    lateinit var tv_emptyStates: TextView
    lateinit var tv_emptyCities: TextView
    lateinit var countyLoaderDialog: LinearLayout
    lateinit var stateLoaderDialog: LinearLayout
    lateinit var cityLoaderDialog: LinearLayout
    lateinit var tv_terms_conditions: TextView
    lateinit var et_user_name: EditText
    lateinit var checkBox_privacy_policy: CheckBox
    lateinit var tv_privacy_policy: TextView

    var showDatePickerDialog = CustomDatePickerDialog()
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    var isTermsChecked = false
    val TAG = RegisterAccountActivity::class.java.simpleName
    var selectedCountry = ""
    var selectedCountryId = ""
    var selectedState = ""
    var selectedStateId = ""
    var selectedCity = ""
    var selectedCityId = ""
    var dialogHeight = 0
    var dialogWidth = 0
    var birthday = ""
    var isPrivacyChecked = false

    lateinit var countriesList: ArrayList<CountriesDataResponse>
    lateinit var statesList: ArrayList<StatesDataResponse>
    lateinit var citiesList: ArrayList<CitiesDataResponse>
    lateinit var cityAdapter: CitiesAdapter
    lateinit var countriesAdapter: CountriesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_register_account)
        val metrics = resources.displayMetrics
        dialogWidth = (metrics.widthPixels * 0.85).toInt()
        dialogHeight = (metrics.heightPixels * 0.85).toInt()

        initialize()

        // get all countries list
        getCountries()

       checkBox_terms.isChecked = false
        isTermsChecked = checkBox_terms.isChecked
        checkBox_privacy_policy.isChecked = false
        isPrivacyChecked = checkBox_privacy_policy.isChecked

        checkBox_terms.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                isTermsChecked = isChecked
            }
        })
        checkBox_privacy_policy.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener {
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                isPrivacyChecked = isChecked
            }
        })

        // capitalise first letter of edit text
        et_first_name.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        et_last_name.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        et_street_address.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

        // underline content in textView
        tv_terms_conditions.paintFlags = tv_terms_conditions.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_privacy_policy.paintFlags = tv_privacy_policy.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        // move to login screen
        tv_sign_in.setOnClickListener {
            val intent = Intent(this@RegisterAccountActivity, LoginActivity::class.java)
            intent.putExtra("from_intent", "")
            startActivity(intent)
        }

        et_country_name.setOnClickListener {
            // show country picker
            // showCountriesDialog()
        }

        et_state_name.setOnClickListener {
            // show state picker
            if (selectedCountry == "") {
                Toast.makeText(
                    this@RegisterAccountActivity,
                    getString(R.string.select_your_country),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                showStatesDialog()
            }
        }

        et_city_name.setOnClickListener {
            // show city picker
           /* if (selectedState == "") {
                Toast.makeText(
                    this@RegisterAccountActivity,
                    getString(R.string.select_your_state),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                showCitiesDialog()
            }*/
        }

        // show calendar
        et_birthday.setOnClickListener {
            showDatePickerDialog.showDatePicker(this@RegisterAccountActivity, "", et_birthday)
        }

        // go to back screen
        iv_back.setOnClickListener {
            onBackPressed()
        }

        // show terms & conditions
        tv_terms_conditions.setOnClickListener {
            val intent = Intent(this, TermsAndConditions::class.java)
            startActivity(intent)
        }

        // show privacy policy
        tv_privacy_policy.setOnClickListener {
            val intent = Intent(this, PrivacyPolicyActivity::class.java)
            startActivity(intent)
        }

        // register profile
        tv_submit.setOnClickListener {
            if (isNetworkAvailable) {
                if (isValidForm()) {
                    val dataObj = JSONObject()
                    dataObj.put("firstName", et_first_name.text.toString().trim())
                    dataObj.put("lastName", et_last_name.text.toString().trim())
                    dataObj.put("userName", et_user_name.text.toString().trim())
                    dataObj.put("email", et_email.text.toString().trim())
                    dataObj.put("password", et_password.text.toString().trim())
                    dataObj.put("streetAddress", et_street_address.text.toString().trim())
                    dataObj.put("city", /*selectedCityId*/et_city_name.text.toString().trim())
                    dataObj.put("state", selectedStateId)
                    dataObj.put("country", selectedCountryId)
                    dataObj.put("zipcode", et_zip_code.text.toString().trim())
                    dataObj.put("birthday", et_birthday.text.toString().trim())
                    dataObj.put("terms", isTermsChecked)
                    Log.d(TAG, "dataObj===$dataObj")
                    val jsonParser = JsonParser()
                    val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                    registerUserApi(finalJsonObject)
                }
            } else {
                Toast.makeText(
                    this@RegisterAccountActivity,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        isNetworkAvailable = cd.isConnectingToInternet

        et_user_name = findViewById(R.id.et_user_name)
        et_first_name = findViewById(R.id.et_first_name)
        et_last_name = findViewById(R.id.et_last_name)
        et_email = findViewById(R.id.et_email)
        et_street_address = findViewById(R.id.et_street_address)
        et_city_name = findViewById(R.id.et_city_name)
        et_state_name = findViewById(R.id.et_state_name)
        et_zip_code = findViewById(R.id.et_zip_code)
        et_password = findViewById(R.id.et_password)
        et_confirm_password = findViewById(R.id.et_confirm_password)
        et_birthday = findViewById(R.id.et_birthday)
        et_country_name = findViewById(R.id.et_country_name)
        tv_sign_in = findViewById(R.id.tv_sign_in)
        tv_submit = findViewById(R.id.tv_submit)
        checkBox_terms = findViewById(R.id.checkBox)
        iv_back = findViewById(R.id.iv_back)
        tv_terms_conditions = findViewById(R.id.tv_terms_conditions)
        tv_privacy_policy = findViewById(R.id.tv_privacy_policy)
        checkBox_privacy_policy = findViewById(R.id.checkBox_privacy_policy)

    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun isValidPassword(password: String): Boolean {

        /* password matcher condition
        (? = .*\d)         must contains one digit from 0-9
        (? = .*[a-z])      must contains one lowercase characters
        (? = .*[A-Z])      must contains one uppercase characters
        (? = .*[@#$%])     must contains one special symbols in the list "@#$%"
        .                  match anything with previous condition checking
        { 6, 20 }          length at least 6 characters and maximum of 20*/

        val passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#\$%&*()-+=^.])(?=\\S+$).{8,}$"
        val pattern = Pattern.compile(passwordPattern)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun isValidForm(): Boolean {
        if (et_user_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "User name should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_first_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "First name should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_last_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Last name should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_email.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "E-mail should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!isValidEmail(et_email.text.toString().trim())) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Invalid E-mail address",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_street_address.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Address should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_city_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "City name should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_state_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "State name should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_country_name.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Country should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_zip_code.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Zip Code should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_password.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Password should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!isValidPassword(et_password.text.toString().trim())) {
            Toast.makeText(
                this@RegisterAccountActivity,
                getString(R.string.password_requirement),
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_confirm_password.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Password should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_password.text.toString() != et_confirm_password.text.toString()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Password must be same",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_birthday.text.toString().isEmpty()) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Birthday date should not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!checkBox_terms.isChecked) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Please Accept Terms & Conditions",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!checkBox_privacy_policy.isChecked) {
            Toast.makeText(
                this@RegisterAccountActivity,
                "Please Accept Privacy Policy",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            return true
        }
        return false
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@RegisterAccountActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RegisterAccountActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showCountriesDialog() {

        val dialog = Dialog(this@RegisterAccountActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RegisterAccountActivity)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(false)

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        tv_emptyCountries = view.findViewById(R.id.tv_empty)
        countriesRecyclerView = view.findViewById(R.id.recyclerView)
        countyLoaderDialog = view.findViewById(R.id.ll_loading)
        val layoutManager =
            LinearLayoutManager(this@RegisterAccountActivity, RecyclerView.VERTICAL, false)
        countriesRecyclerView.setHasFixedSize(true)
        countriesRecyclerView.layoutManager = layoutManager

//        getCountries()
        val adapter =
            CountriesAdapter(this@RegisterAccountActivity, countriesList)
        countriesRecyclerView.adapter = adapter
        if (countriesList.size > 0) {
            tv_emptyCountries.visibility = View.GONE
            countriesRecyclerView.visibility = View.VISIBLE
        } else {
            tv_emptyCountries.visibility = View.VISIBLE
            countriesRecyclerView.visibility = View.GONE
        }

        tv_header.text = getString(R.string.select_country)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_country_name.setText(selectedCountry)
            dialog.dismiss()
        }

        countriesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this@RegisterAccountActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedCountry = countriesAdapter.getItemAt(position).name!!
                        val id = countriesAdapter.getItemAt(position).countryId!!
                        selectedCountryId = id.toString()
                        Log.d(TAG, "selected country===$selectedCountry===$selectedCountryId")
                        et_country_name.setText(selectedCountry)
                        dialog.dismiss()
                    }
                })
        )
        dialog.show()
    }

    private fun showStatesDialog() {

        val dialog = Dialog(this@RegisterAccountActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RegisterAccountActivity)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(true)

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        tv_emptyStates = view.findViewById(R.id.tv_empty)
        stateLoaderDialog = view.findViewById(R.id.ll_loading)
        statesRecyclerView = view.findViewById(R.id.recyclerView)
        val layoutManager =
            LinearLayoutManager(this@RegisterAccountActivity, RecyclerView.VERTICAL, false)
        statesRecyclerView.setHasFixedSize(true)
        statesRecyclerView.layoutManager = layoutManager

        val dataObj = JSONObject()
        dataObj.put("countryId", selectedCountryId)
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
        getStates(finalJsonObject)

        tv_header.text = getString(R.string.select_state)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_state_name.setText(selectedState)
            dialog.dismiss()
        }

        statesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this@RegisterAccountActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedState = statesList[position].name!!
                        val id = statesList[position].stateId!!
                        selectedStateId = id.toString()
                        Log.d(TAG, "selected state===$selectedState===$selectedStateId")
                        et_state_name.setText(selectedState)
                        dialog.dismiss()
                    }
                })
        )
        dialog.show()
    }

    private fun showCitiesDialog() {

        val dialog = Dialog(this@RegisterAccountActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RegisterAccountActivity)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(true)
        dialog.show()

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        val ll_search = view.findViewById(R.id.ll_search) as LinearLayout
        val et_search = view.findViewById(R.id.et_search) as EditText
        ll_search.visibility = View.VISIBLE
        tv_emptyCities = view.findViewById(R.id.tv_empty)
        cityLoaderDialog = view.findViewById(R.id.ll_loading)
        citiesRecyclerView = view.findViewById(R.id.recyclerView)
        val layoutManager =
            LinearLayoutManager(this@RegisterAccountActivity, RecyclerView.VERTICAL, false)
        citiesRecyclerView.setHasFixedSize(true)
        citiesRecyclerView.layoutManager = layoutManager

        val dataObj = JSONObject()
        dataObj.put("stateId", selectedStateId)
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
        getCities(finalJsonObject)

        tv_header.text = getString(R.string.select_city)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_city_name.setText(selectedCity)
            dialog.dismiss()
        }
        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                cityAdapter.filter.filter(s)
                cityAdapter.removeSelectionAt(-1)
            }
        })

        citiesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this@RegisterAccountActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedCity = cityAdapter.getItemAt(position).name!!
                        val id = cityAdapter.getItemAt(position).cityId!!
                        selectedCityId = id.toString()
                        Log.d(TAG, "selected city===$selectedCity===$selectedCityId")
                        et_city_name.setText(selectedCity)
                        dialog.dismiss()
                    }
                })
        )
    }

    private fun registerUserApi(final_object: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.registerUserApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Log.d(TAG, "registerApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@RegisterAccountActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>, response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                "Verification Email sent",
                                Toast.LENGTH_SHORT
                            ).show()
                            /*Toast.makeText(
                                this@RegisterAccountActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()*/
                            val intent =
                                Intent(this@RegisterAccountActivity, LoginActivity::class.java)
                            intent.putExtra("from_intent", "")
                            startActivity(intent)
                            finish()
                        }
                        "0" -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.something_went_wrong),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getCountries() {
        loading_dialog.show()
        //countyLoaderDialog.visibility = View.VISIBLE
        countriesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getCountriesApi()
        call.enqueue(object : Callback<CountriesResponse> {
            override fun onFailure(call: Call<CountriesResponse>, t: Throwable) {
                try {
                    Log.d(TAG, "countriesApi error $t")
                    //countyLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@RegisterAccountActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CountriesResponse>,
                response: Response<CountriesResponse>
            ) {
                try {
                    //countyLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.countries
                            if (data!!.size > 0) {
                                countriesList = data
//                                countriesList.reverse()
//                                 countriesAdapter =
//                                    CountriesAdapter(this@RegisterAccountActivity, countriesList)
//                                countriesRecyclerView.adapter = adapter
//                                if (countriesList.size > 0) {
//                                    tv_emptyCountries.visibility = View.GONE
//                                    countriesRecyclerView.visibility = View.VISIBLE
//                                } else {
//                                    tv_emptyCountries.visibility = View.VISIBLE
//                                    countriesRecyclerView.visibility = View.GONE
//                                }
                                for (i in 0 until countriesList.size) {
                                    if (countriesList[i].iso3!!.equals("USA", ignoreCase = false)) {
                                        selectedCountry = countriesList[i].name!!
                                        et_country_name.setText(selectedCountry)
                                        val id = countriesList[i].countryId!!
                                        selectedCountryId = id.toString()
                                        Log.d(
                                            TAG,
                                            "selected country def ===$selectedCountry===$selectedCountryId"
                                        )
//                                        break
                                    }
                                }
                                // adding only USA to list
                                val list: ArrayList<CountriesDataResponse> = ArrayList()
                                for (i in 0 until countriesList.size) {
                                    if (countriesList[i].iso3!!.equals("USA", ignoreCase = false)) {
                                        list.add(countriesList[i])
                                    }
                                }

                            } else {
                                Toast.makeText(
                                    this@RegisterAccountActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun getStates(final_object: JsonObject) {
        stateLoaderDialog.visibility = View.VISIBLE
//        loading_dialog.show()
        statesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getStatesApi(final_object)
        call.enqueue(object : Callback<StatesResponse> {
            override fun onFailure(call: Call<StatesResponse>, t: Throwable) {
                try {
                    Log.d(TAG, "statesApi error $t")
                    stateLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@RegisterAccountActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    )

                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<StatesResponse>,
                response: Response<StatesResponse>
            ) {
                try {
                    stateLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.states
                            if (data!!.size > 0) {
                                statesList = data
//                                statesList.reverse()
                                val adapter =
                                    StatesAdapter(this@RegisterAccountActivity, statesList)
                                statesRecyclerView.adapter = adapter
                                if (countriesList.size > 0) {
                                    tv_emptyStates.visibility = View.GONE
                                    statesRecyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_emptyStates.visibility = View.VISIBLE
                                    statesRecyclerView.visibility = View.GONE
                                }
                            } else {
                                Toast.makeText(
                                    this@RegisterAccountActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun getCities(final_object: JsonObject) {
        cityLoaderDialog.visibility = View.VISIBLE
//        loading_dialog.show()
        citiesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getCitiesApi(final_object)
        call.enqueue(object : Callback<CitiesResponse> {
            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                try {
                    Log.d(TAG, "citiesApi error $t")
                    cityLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@RegisterAccountActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                try {
                    cityLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.cities
                            if (data!!.size > 0) {
                                citiesList = data
//                                citiesList.reverse()
                                cityAdapter =
                                    CitiesAdapter(this@RegisterAccountActivity, citiesList)
                                citiesRecyclerView.adapter = cityAdapter
                                if (citiesList.size > 0) {
                                    tv_emptyCities.visibility = View.GONE
                                    citiesRecyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_emptyCities.visibility = View.VISIBLE
                                    citiesRecyclerView.visibility = View.GONE
                                }
                            } else {
                                Toast.makeText(
                                    this@RegisterAccountActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@RegisterAccountActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

}
