package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.res.Resources
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.*
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.TicketsListAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.OrganizationsFollowingListResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@Suppress("DEPRECATION")
class TicketsListActivity : AppCompatActivity() {

    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>
    private var userId = ""
    private lateinit var cd: ConnectionDetector
    private var isNetworkAvailable = false
    private var type_id_stg = ""
    private var type_name_stg = ""

    private lateinit var iv_back: ImageView
    private lateinit var rv_tickets: RecyclerView
    private lateinit var tv_no_tickets: TextView
    private lateinit var ll_create_ticket: CardView
    private lateinit var createTicketDialog: BottomSheetDialog
    private lateinit var loading_dialog: Dialog
    private lateinit var mFollowingList: ArrayList<OrganizationsFollowingListResponse>
    private lateinit var only_names: ArrayList<String>
    private lateinit var only_ids: ArrayList<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_tickets_list)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        if (isNetworkAvailable) {
            val dataObj = JSONObject()
            dataObj.put("userId", userId)
            val jsonParser = JsonParser()
            val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
            getAllTickets(finalJsonObject)
            getFollowingOrganizations(finalJsonObject)
        } else {
            Toast.makeText(
                this,
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        iv_back.setOnClickListener {
            onBackPressed()
        }

        ll_create_ticket.setOnClickListener {
            showCreateTicketDialog()
        }

    }

    private fun initialize() {
        loadingDialog()
        sessionManager = SessionManager(this)
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!
        cd = ConnectionDetector(this)
        isNetworkAvailable = cd.isConnectingToInternet

        iv_back = findViewById(R.id.iv_back)
        rv_tickets = findViewById(R.id.rv_tickets)
        tv_no_tickets = findViewById(R.id.tv_no_tickets)
        ll_create_ticket = findViewById(R.id.ll_create_ticket)

        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_tickets.layoutManager = layoutManager

        mFollowingList = ArrayList()
        only_ids = ArrayList()
        only_names = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun showCreateTicketDialog() {
        createTicketDialog = BottomSheetDialog(this)
        createTicketDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        createTicketDialog.setCancelable(true)
//        createTicketDialog.window!!.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        createTicketDialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        createTicketDialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
        val view = LayoutInflater.from(this).inflate(R.layout.create_ticket_dialog, null)
        createTicketDialog.setContentView(view)
        val bottomSheetBehavior = BottomSheetBehavior.from(view.parent as View)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        bottomSheetBehavior.peekHeight = Resources.getSystem().displayMetrics.heightPixels
        createTicketDialog.dismissWithAnimation = true
        val adapter = ArrayAdapter<String>(
            this@TicketsListActivity,
            R.layout.support_simple_spinner_dropdown_item,
            only_names
        )
        val iv_back = view.findViewById(R.id.iv_back) as ImageView
        val sp_following_org = view.findViewById(R.id.sp_following_org) as Spinner
        val et_subject = view.findViewById(R.id.et_subject) as EditText
        val et_message = view.findViewById(R.id.et_message) as EditText
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        sp_following_org.adapter = adapter
        iv_back.setOnClickListener {
            createTicketDialog.dismiss()
        }
        sp_following_org.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                type_id_stg = only_ids[position]
                type_name_stg = only_names[position]
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {}
        }

        tv_submit.setOnClickListener {
            when {
                et_subject.text.toString().isEmpty() -> {
                    Toast.makeText(this, "Subject is required!", Toast.LENGTH_SHORT).show()
                }
                et_message.text.toString().isEmpty() -> {
                    Toast.makeText(this, "Message is required!", Toast.LENGTH_SHORT).show()
                }
                else -> {
                    if (isNetworkAvailable) {
                        val dataObj = JSONObject()
                        dataObj.put("userId", userId)
                        dataObj.put("orgId", type_id_stg)
                        dataObj.put("subject", et_subject.text.toString().trim())
                        dataObj.put("message", et_message.text.toString().trim())
                        val jsonParser = JsonParser()
                        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                        createNewTicketApi(finalJsonObject)
                    } else {
                        Toast.makeText(
                            this,
                            resources.getString(R.string.no_network_available),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        }
        createTicketDialog.show()
    }

    private fun getFollowingOrganizations(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.getOrganizationFollowingApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TicketsListActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val list = resp.organisations
                        mFollowingList = list!!
                        if (mFollowingList.size > 0) {
                            for (i in 0 until mFollowingList.size) {
                                only_names.add(mFollowingList[i].orgName!!)
                                only_ids.add(mFollowingList[i].orgId!!)
                            }
                        }
                    } else {
                        // do nothing
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAllTickets(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getAllTicketsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TicketsListActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    rv_tickets.visibility = View.GONE
                    tv_no_tickets.visibility = View.VISIBLE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val list = resp.tickets
                        val adapter = TicketsListAdapter(this@TicketsListActivity, list!!)
                        rv_tickets.adapter = adapter
                        adapter.notifyDataSetChanged()
                        if (list.size > 0) {
                            tv_no_tickets.visibility = View.GONE
                            rv_tickets.visibility = View.VISIBLE
                        } else {
                            rv_tickets.visibility = View.GONE
                            tv_no_tickets.visibility = View.VISIBLE
                        }
                    } else {
                        rv_tickets.visibility = View.GONE
                        tv_no_tickets.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun createNewTicketApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.addTicketApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TicketsListActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        Toast.makeText(this@TicketsListActivity, resp.result, Toast.LENGTH_SHORT)
                            .show()
                        val dataObj = JSONObject()
                        dataObj.put("userId", userId)
                        val jsonParser = JsonParser()
                        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                        getAllTickets(finalJsonObject)
                        createTicketDialog.dismiss()
                    } else {
                        // do nothing
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}