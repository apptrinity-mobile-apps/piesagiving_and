package com.piesagiving.com.Activities

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.GetNotificationSettingsResponse
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class NewProjectActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_church: LinearLayout
    lateinit var ll_charities: LinearLayout
    lateinit var ll_dce: LinearLayout
    lateinit var ll_ude: LinearLayout
    lateinit var ll_dpe: LinearLayout
    lateinit var ll_confirmationpage: LinearLayout

    lateinit var ll_dcp: LinearLayout
    lateinit var ll_udp: LinearLayout
    lateinit var ll_dpp: LinearLayout
    lateinit var switch_dcp: SwitchMaterial
    lateinit var switch_dpp: SwitchMaterial
    lateinit var switch_udp: SwitchMaterial
    lateinit var tv_dpp: TextView
    lateinit var tv_udp: TextView
    lateinit var tv_dcp: TextView

    lateinit var switch_church: SwitchMaterial
    lateinit var switch_charities: SwitchMaterial
    lateinit var switch_dce: SwitchMaterial
    lateinit var switch_ude: SwitchMaterial
    lateinit var switch_dpe: SwitchMaterial
    lateinit var tv_church: TextView
    lateinit var tv_dpe: TextView
    lateinit var tv_ude: TextView
    lateinit var tv_dce: TextView
    lateinit var tv_charities: TextView
    lateinit var tv_toolbarname: TextView
    lateinit var ll_donation_processed_email: LinearLayout
    lateinit var ll_donation_processing_phone: LinearLayout
    lateinit var tv_donation_processed_email: TextView
    lateinit var switch_donation_processed_email: SwitchMaterial
    lateinit var tv_donation_processing_phone: TextView
    lateinit var switch_donation_processing_phone: SwitchMaterial

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""

    private var recurringdonation_val = ""
    private var newpiesarecommend_val = ""
    private var trendingnow_val = ""
    private var someonefollowed_val = ""
    private var haptics_val = ""
    private var newProjectChurch = ""
    private var newProjectCharity = ""
    private var newSpecialCausesChurch = ""
    private var newSpecialCausesCharity = ""
    private var donationConfirmationEmail = ""
    private var upcomingDonationEmail = ""
    private var DonationProcessedEmail = ""
    private var DonationProcessingEmail = ""
    private var donationConfirmationPhone = ""
    private var upcomingDonationPhone = ""
    private var donationProcessedPhone = ""
    private var donationProcessingPhone = ""
    lateinit var notification_list: HashMap<String, String>
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_new_porject)
        initialize()

        notification_list = sessionManager.getUserNotifications
        recurringdonation_val = notification_list[SessionManager.NOTIFY_RECURRINDONATION_KEY]!!
        newpiesarecommend_val = notification_list[SessionManager.NOTIFY_NEWPIESARECOMMEND_KEY]!!
        trendingnow_val = notification_list[SessionManager.NOTIFY_TRENDINGNOW_KEY]!!
        newProjectChurch = notification_list[SessionManager.NOTIFY_NEWPROJECTCHURCH_KEY]!!
        haptics_val = notification_list[SessionManager.NOTIFY_HAPTICS_KEY]!!
        newProjectCharity = notification_list[SessionManager.NOTIFY_NEWPROJECTCHARITY_KEY]!!
        someonefollowed_val = notification_list[SessionManager.NOTIFY_SOMEONEFOLLOWED_KEY]!!
        newSpecialCausesChurch =
            notification_list[SessionManager.NOTIFY_NEWSPECIALCAUSECHURCH_KEY]!!
        newSpecialCausesCharity =
            notification_list[SessionManager.NOTIFY_NEWSPECIALCAUSECHARITY_KEY]!!
        DonationProcessedEmail =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSEDEMAIL_KEY]!!
        donationProcessedPhone =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSEDPHONE_KEY]!!
        DonationProcessingEmail =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSINGEMAIL_KEY]!!
        donationProcessingPhone =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSINGPHONE_KEY]!!
        getNotificationsApi()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        switch_church.isChecked = true
        tv_church.text = getString(R.string.on)
        switch_charities.isChecked = true
        tv_charities.text = getString(R.string.on)
        switch_dce.isChecked = true
        tv_dce.text = getString(R.string.on)
        switch_ude.isChecked = true
        tv_ude.text = getString(R.string.on)
        switch_donation_processed_email.isChecked = true
        tv_donation_processed_email.text = getString(R.string.on)
        switch_donation_processing_phone.isChecked = true
        tv_donation_processing_phone.text = getString(R.string.on)
        switch_dpe.isChecked = true
        tv_dpe.text = getString(R.string.on)
        switch_dpp.isChecked = true
        tv_dpp.text = getString(R.string.on)
        switch_udp.isChecked = true
        tv_udp.text = getString(R.string.on)
        switch_dcp.isChecked = true
        tv_dcp.text = getString(R.string.on)
        NotificationsApi()

        switch_church.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_church.text = getString(R.string.on)
                tv_church.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                newProjectChurch = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()

            } else {
                // The toggle is disabled
                tv_church.text = getString(R.string.off)
                tv_church.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                newProjectChurch = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }
        switch_charities.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_charities.text = getString(R.string.on)
                tv_charities.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                newProjectCharity = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_charities.text = getString(R.string.off)
                tv_charities.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                newProjectCharity = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }
        switch_dce.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_dce.text = getString(R.string.on)
                tv_dce.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                donationConfirmationEmail = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_dce.text = getString(R.string.off)
                tv_dce.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                donationConfirmationEmail = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }

        switch_ude.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_ude.text = getString(R.string.on)
                tv_ude.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                upcomingDonationEmail = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_ude.text = getString(R.string.off)
                tv_ude.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                upcomingDonationEmail = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }
        switch_donation_processed_email.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_donation_processed_email.text = getString(R.string.on)
                tv_donation_processed_email.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                DonationProcessedEmail = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_donation_processed_email.text = getString(R.string.off)
                tv_donation_processed_email.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                DonationProcessedEmail = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }

        switch_donation_processing_phone.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_donation_processing_phone.text = getString(R.string.on)
                tv_donation_processing_phone.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                donationProcessingPhone = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_donation_processing_phone.text = getString(R.string.off)
                tv_donation_processing_phone.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                donationProcessingPhone = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }

        switch_dpe.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_dpe.text = getString(R.string.on)
                tv_dpe.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                DonationProcessingEmail = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_dpe.text = getString(R.string.off)
                tv_dpe.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                DonationProcessingEmail = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }

        switch_dcp.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_dcp.text = getString(R.string.on)
                tv_dcp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                donationConfirmationPhone = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_dcp.text = getString(R.string.off)
                tv_dcp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                donationConfirmationPhone = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }

        switch_dpp.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_dpp.text = getString(R.string.on)
                tv_dpp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                donationProcessedPhone = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_dpp.text = getString(R.string.off)
                tv_dpp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                donationProcessedPhone = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }
        switch_udp.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_udp.text = getString(R.string.on)
                tv_udp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.colorAccent
                    )
                )
                upcomingDonationPhone = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            } else {
                // The toggle is disabled
                tv_udp.text = getString(R.string.off)
                tv_udp.setTextColor(
                    ContextCompat.getColor(
                        this@NewProjectActivity,
                        R.color.light_grey
                    )
                )
                upcomingDonationPhone = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi()
            }
        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        from_intent = intent.getStringExtra("activity_name")!!

        iv_back = findViewById(R.id.iv_back)
        tv_toolbarname = findViewById(R.id.tv_toolbarname)
        ll_dcp = findViewById(R.id.ll_dcp)
        ll_udp = findViewById(R.id.ll_udp)
        ll_dpp = findViewById(R.id.ll_dpp)
        tv_dpp = findViewById(R.id.tv_dpp)
        tv_udp = findViewById(R.id.tv_udp)
        tv_dcp = findViewById(R.id.tv_dcp)
        switch_dcp = findViewById(R.id.switch_dcp)
        switch_dpp = findViewById(R.id.switch_dpp)
        switch_udp = findViewById(R.id.switch_udp)

        ll_dce = findViewById(R.id.ll_dce)
        ll_ude = findViewById(R.id.ll_ude)
        ll_dpe = findViewById(R.id.ll_dpe)
        ll_church = findViewById(R.id.ll_church)
        ll_charities = findViewById(R.id.ll_charities)
        ll_confirmationpage = findViewById(R.id.ll_confirmationpage)
        switch_church = findViewById(R.id.switch_church)
        switch_charities = findViewById(R.id.switch_charities)
        switch_dce = findViewById(R.id.switch_dce)
        switch_ude = findViewById(R.id.switch_ude)
        switch_dpe = findViewById(R.id.switch_dpe)
        tv_church = findViewById(R.id.tv_church)
        tv_charities = findViewById(R.id.tv_charities)
        tv_dpe = findViewById(R.id.tv_dpe)
        tv_ude = findViewById(R.id.tv_ude)
        tv_dce = findViewById(R.id.tv_dce)

        ll_donation_processed_email = findViewById(R.id.ll_donation_processed_email)
        ll_donation_processing_phone = findViewById(R.id.ll_donation_processing_phone)
        tv_donation_processed_email = findViewById(R.id.tv_donation_processed_email)
        switch_donation_processed_email = findViewById(R.id.switch_donation_processed_email)
        tv_donation_processing_phone = findViewById(R.id.tv_donation_processing_phone)
        switch_donation_processing_phone = findViewById(R.id.switch_donation_processing_phone)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

        if (from_intent.equals("newproject")) {
            ll_confirmationpage.visibility = View.GONE
        } else if (from_intent.equals("newspecialcase")) {
            ll_confirmationpage.visibility = View.GONE
            tv_toolbarname.text = getString(R.string.new_special_cause)
        } else if (from_intent.equals("confirmationemails")) {
            tv_toolbarname.text = getString(R.string.donation_alerts)
            ll_church.visibility = View.GONE
            ll_charities.visibility = View.GONE
            ll_confirmationpage.visibility = View.VISIBLE
        }
    }

    private fun NotificationsApi() {
        //  loading_dialog.show()
        val objspl = JSONObject()
        val notobject = JSONObject()
        notobject.put("recurringDonationReminder", recurringdonation_val.toInt())
        notobject.put("newPiesaRecommendation", newpiesarecommend_val.toInt())
        notobject.put("newTrendingNow", trendingnow_val.toInt())
        notobject.put("someoneFollowedYou", someonefollowed_val.toInt())
        notobject.put("hapticss", haptics_val.toInt())
        notobject.put("newProjectChurch", newProjectChurch.toInt())
        notobject.put("newProjectCharity", newProjectCharity.toInt())
        notobject.put("newSpecialCausesChurch", newSpecialCausesChurch.toInt())
        notobject.put("newSpecialCausesCharity", newSpecialCausesCharity.toInt())
        notobject.put("DonationProcessedEmail", DonationProcessedEmail.toInt())
        notobject.put("donationProcessedPhone", donationProcessedPhone.toInt())
        notobject.put("donationProcessingEmail", DonationProcessingEmail.toInt())
        notobject.put("donationProcessingPhone", donationProcessingPhone.toInt())
        objspl.put("userId", userId)
        objspl.put("notifications", notobject)
        val jParserspl = JsonParser()
        val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
        Log.e("UserPrivacyApi", jObjectspl.toString())

        val apiInterface = ApiInterface.create()
        val call = apiInterface.NotificationsApi(jObjectspl)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result = jsondata.result!!
                        //Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                        tv_church.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_charities.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_donation_processed_email.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_dce.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_ude.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_dpp.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_dpe.setTextColor(
                            ContextCompat.getColor(
                                this@NewProjectActivity,
                                R.color.colorAccent
                            )
                        )
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun getNotificationsApi() {
        //  loading_dialog.show()
        val objspl = JSONObject()
        objspl.put("userId", userId)
        val jParserspl = JsonParser()
        val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
        Log.e("NOTIFICATIONOBJECT", jObjectspl.toString())

        val apiInterface = ApiInterface.create()
        val call = apiInterface.getUserNotificationsSettings(jObjectspl)
        call.enqueue(object : Callback<GetNotificationSettingsResponse> {
            override fun onFailure(call: Call<GetNotificationSettingsResponse>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GetNotificationSettingsResponse>,
                response: Response<GetNotificationSettingsResponse>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!!.toString() == "1") {
                        val result = jsondata.result!!
                        if (result.donationProcessingEmail.equals("1")) {
                            switch_dpe.isChecked = true
                            tv_dpe.text = getString(R.string.on)
                            tv_dpe.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_dpe.isChecked = false
                            tv_dpe.text = getString(R.string.off)
                            tv_dpe.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }

                        if (result.donationProcessingPhone.equals("1")) {
                            switch_donation_processing_phone.isChecked = true
                            tv_donation_processing_phone.text = getString(R.string.on)
                            tv_donation_processing_phone.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_donation_processing_phone.isChecked = false
                            tv_donation_processing_phone.text = getString(R.string.off)
                            tv_donation_processing_phone.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.DonationProcessedEmail.equals("1")) {
                            switch_donation_processed_email.isChecked = true
                            tv_donation_processed_email.text = getString(R.string.on)
                            tv_donation_processed_email.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_donation_processed_email.isChecked = false
                            tv_donation_processed_email.text = getString(R.string.off)
                            tv_donation_processed_email.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.donationProcessedPhone.equals("1")) {
                            switch_dpp.isChecked = true
                            tv_dpp.text = getString(R.string.on)
                            tv_dpp.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_dpp.isChecked = false
                            tv_dpp.text = getString(R.string.off)
                            tv_dpp.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.newProjectCharity.equals("1")) {
                            switch_charities.isChecked = true
                            tv_charities.text = getString(R.string.on)
                            tv_charities.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_charities.isChecked = false
                            tv_charities.text = getString(R.string.off)
                            tv_charities.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.newProjectChurch.equals("1")) {
                            switch_church.isChecked = true
                            tv_church.text = getString(R.string.on)
                            tv_church.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_church.isChecked = false
                            tv_church.text = getString(R.string.off)
                            tv_church.setTextColor(
                                ContextCompat.getColor(
                                    this@NewProjectActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                    } else {
                        //Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
