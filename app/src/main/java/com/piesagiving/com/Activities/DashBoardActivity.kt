package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Fragments.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.DataStorage
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@Suppress("DEPRECATION")
class DashBoardActivity : BaseActivity() {

    lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var mFragmentManager: FragmentManager
    lateinit var loading_dialog: Dialog

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    var userId = ""
    var passCode = ""
    var to_fragment = ""
    var cat_id = ""

    lateinit var bank_account_dialog: Dialog

    lateinit var ll_paymentlist: LinearLayout
    lateinit var ll_banktype: LinearLayout
    lateinit var sp_payment_type_id: Spinner
    lateinit var et_holder_name: EditText
    lateinit var et_card_holder_id: EditText
    lateinit var et_routinnumber: EditText
    lateinit var et_accountnumber: EditText
    lateinit var et_confirmacctnumber: EditText
    lateinit var et_nameonaccount: EditText
    lateinit var checkBox: CheckBox
    lateinit var tv_submit: TextView
    lateinit var iv_back: ImageView

    var account_holder_name = ""
    var account_routing_no = ""
    var account_number = ""
    var account_conf_number = ""
    var show_lock = "yes"
    lateinit var dataStorage: DataStorage
    var deviceToken = ""
    private var appPinCode = ""

    var payment_type_id_stg = ""
    var payment_type_stg = ""
    lateinit var select_payment_array: ArrayList<String>
    lateinit var select_pay_type_id_array: ArrayList<String>

    lateinit var ll_credit_card_id: LinearLayout
    lateinit var ll_bank_account_id: LinearLayout

    lateinit var stripe_card_id: CardInputWidget
    lateinit var id_save_card_id: Button
    lateinit var get_card: Card
    lateinit var createPassCodeDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_dash_board)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(this, object : OnSuccessListener<InstanceIdResult?> {
                override fun onSuccess(instanceIdResult: InstanceIdResult?) {
                    deviceToken = instanceIdResult!!.token
                    Log.e("NewFcm Token dashboard", deviceToken)
                    if (isNetworkAvailable) {
                        val jObj = JSONObject()
                        jObj.put("userId", userId)
                        jObj.put("deviceToken", deviceToken)
                        jObj.put("deviceType", "ANDROID")
                        val jParser = JsonParser()
                        val final_object = jParser.parse(jObj.toString()) as JsonObject
                        updateDeviceToken(final_object)
                    }
                }
            })
        //deviceToken = dataStorage.getFcmToken(this)

        to_fragment = intent.getStringExtra("to_fragment")!!
        cat_id = intent.getStringExtra("cat_id")!!

        show_lock = intent.getStringExtra("show_lock")!!
        if (show_lock == "yes") {
            showBiometricPrompt()
        }
        if (to_fragment == "") {
            if (isNetworkAvailable) {
                val dataObj = JSONObject()
                dataObj.put("userId", userId)
                val jsonParser = JsonParser()
                val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                getUserProfileApi(finalJsonObject)
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        if (savedInstanceState == null) {
            if (to_fragment == "") {
                // setting default home item
                bottomNavigationView.menu.getItem(0).isChecked = true
                bottomNavigationView.menu.getItem(0).setIcon(R.drawable.ic_bottom_home_selected)
                val tx = supportFragmentManager.beginTransaction()
                tx.replace(R.id.content_frame, HomeFragment())
                tx.commit()
            } else if (to_fragment == "search") {
                bottomNavigationView.menu.getItem(1).isChecked = true
                bottomNavigationView.menu.getItem(1).setIcon(R.drawable.ic_bottom_search_selected)
                val tx = supportFragmentManager.beginTransaction()
                val searchFragment = SearchFragment()
                val bundle = Bundle()
                bundle.putString("cat_id", cat_id)
                searchFragment.arguments = bundle
                tx.replace(R.id.content_frame, searchFragment)
                tx.commit()
            } else if (to_fragment == "wallet") {
                bottomNavigationView.menu.getItem(3).isChecked = true
                bottomNavigationView.menu.getItem(3).setIcon(R.drawable.ic_bottom_wallet_selected)
                val tx = supportFragmentManager.beginTransaction()
                tx.replace(R.id.content_frame, WalletFragment())
                tx.commit()
            }
        }
        if (to_fragment == "") {
            // setting default home item
            bottomNavigationView.menu.getItem(0).isChecked = true
            bottomNavigationView.menu.getItem(0).setIcon(R.drawable.ic_bottom_home_selected)
        }
        // remove color tint for item
        bottomNavigationView.itemIconTintList = null
        bottomNavigationView.setOnNavigationItemSelectedListener { item -> // setting default icons on every item click
            bottomNavigationView.menu.getItem(0).setIcon(R.drawable.ic_bottom_home_unselected)
            bottomNavigationView.menu.getItem(1).setIcon(R.drawable.ic_bottom_search_unselected)
            bottomNavigationView.menu.getItem(2)
                .setIcon(R.drawable.ic_bottom_favourites_unselected)
            bottomNavigationView.menu.getItem(3).setIcon(R.drawable.ic_bottom_wallet_unselected)
            bottomNavigationView.menu.getItem(4)
                .setIcon(R.drawable.ic_bottom_settings_unselected)
            when (item.itemId) {
                R.id.nav_home -> {
                    item.setIcon(R.drawable.ic_bottom_home_selected)
                    changeFragment(HomeFragment(), addToBackStack = false)
                    true
                }
                R.id.nav_search -> {
                    val searchFragment = SearchFragment()
                    val bundle = Bundle()
                    bundle.putString("cat_id", "")
                    searchFragment.arguments = bundle
                    changeFragment(searchFragment, addToBackStack = false)
                    item.setIcon(R.drawable.ic_bottom_search_selected)
                    true
                }
                R.id.nav_favourites -> {
                    changeFragment(FavouritesFragmentNew(), addToBackStack = true)
                    item.setIcon(R.drawable.ic_bottom_favourites_selected)
                    true
                }
                R.id.nav_wallet -> {
                    changeFragment(WalletFragment(), addToBackStack = true)
                    item.setIcon(R.drawable.ic_bottom_wallet_selected)
                    true
                }
                R.id.nav_settings -> {
                    changeFragment(SettingsFragment(), true)
                    item.setIcon(R.drawable.ic_bottom_settings_selected)
                    true
                }
                else -> false
            }
        }
        if (passCode == "") {
            createPassCodeDialog.show()
        } else  {
            BankAccountSetUp()
        }
//        BankAccountSetUp()
    }

    private fun initialize() {
        loadingDialog()
        createPassCodeDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        dataStorage = DataStorage()
        isNetworkAvailable = cd.isConnectingToInternet
        mFragmentManager = supportFragmentManager
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!
        passCode = userDetails[SessionManager.PASSCODE_KEY]!!

        bottomNavigationView = findViewById(R.id.bottomNavigationView)
    }

    private fun changeFragment(fragment: Fragment, addToBackStack: Boolean) {
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        mFragmentTransaction.replace(R.id.content_frame, fragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)

        mFragmentTransaction.commit()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    override fun onBackPressed() {
        try {
            val selectedId = bottomNavigationView.selectedItemId
            if (to_fragment == "search") {
                super.onBackPressed()
            } else if (selectedId != R.id.nav_home) {
                changeFragment(HomeFragment(), false)
                bottomNavigationView.menu.getItem(0).isChecked = true
                bottomNavigationView.menu.getItem(0).setIcon(R.drawable.ic_bottom_home_selected)
                bottomNavigationView.menu.getItem(1)
                    .setIcon(R.drawable.ic_bottom_search_unselected)
                bottomNavigationView.menu.getItem(2)
                    .setIcon(R.drawable.ic_bottom_favourites_unselected)
                bottomNavigationView.menu.getItem(3)
                    .setIcon(R.drawable.ic_bottom_wallet_unselected)
                bottomNavigationView.menu.getItem(4)
                    .setIcon(R.drawable.ic_bottom_settings_unselected)
            } else {
                finishAffinity()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getUserProfileApi(final_object: JsonObject) {
//        loading_dialog.show()
        var city: String
        var country: String
        var email: String
        var firstName: String
        var geotagging: String
        var faceId: String
        var id: String
        var language: String
        var lastName: String
        var profilePic: String
        var state: String
        var streetAddress: String
        var terms: String
        var zipcode: String
        var dob: String
        var cityId: String
        var stateId: String
        var countryId: String
        var userName: String

        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetUserProfileApi(final_object)
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                try {

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("TAG", "profileapi error $t")
                    Toast.makeText(
                        this@DashBoardActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<UserProfileResponse>,
                response: Response<UserProfileResponse>
            ) {
                try {
//                    if (loading_dialog.isShowing)
//                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.userList
                            city = data!!.city.toString()
                            country = data.country.toString()
                            email = data.email.toString()
                            firstName = data.firstName.toString()
                            geotagging = data.geotagging.toString()
                            faceId = data.faceId.toString()
                            id = data.id.toString()
                            language = data.language.toString()
                            lastName = data.lastName.toString()
                            profilePic = data.profilePic.toString()
                            state = data.state.toString()
                            streetAddress = data.streetAddress.toString()
                            terms = data.terms.toString()
                            zipcode = data.zipcode.toString()
                            cityId = data.cityId.toString()
                            stateId = data.stateId.toString()
                            countryId = data.countryId.toString()
                            dob = data.birthday.toString()
                            userName = data.userName.toString()

                            /*if (dob == "") {
                                birthday = ""
                            } else {
                                val formatter =
                                    SimpleDateFormat("E, dd MMM yyyy HH:mm:ss z", Locale.US)
                                val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                                val date = formatter.parse(dob)!!
                                birthday = sdf.format(date)
                            }*/
                            sessionManager.userProfileUpdate(
                                firstName = firstName,
                                lastName = lastName,
                                email = email,
                                id = id,
                                state = state,
                                country = country,
                                city = city,
                                street = streetAddress,
                                zip = zipcode,
                                terms = terms,
                                birthday = dob,
                                faceid = faceId,
                                geotag = geotagging,
                                language = language,
                                profilePic = profilePic,
                                cityId = cityId,
                                stateId = stateId,
                                countryId = countryId,
                                userName = userName
                            )
                        }
                        else -> {
                            /*Toast.makeText(
                                this@DashBoardActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            ).show()*/
                        }
                    }
                    changeFragment(HomeFragment(), addToBackStack = false)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    /* pass code setup */
    private fun createPassCodeDialog() {
        createPassCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.create_passcode_dialog, null)
        createPassCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        createPassCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        createPassCodeDialog.setCancelable(false)
        createPassCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        //createPassCodeDialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        createPassCodeDialog.setContentView(view)
        createPassCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val et_passcode = view.findViewById(R.id.et_passcode) as EditText
        val et_re_passcode = view.findViewById(R.id.et_re_passcode) as EditText
        tv_submit.setOnClickListener {
            when {
                et_passcode.text.toString() == "" -> {
                    Toast.makeText(
                        this@DashBoardActivity,
                        "Pin Code must not be empty!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_re_passcode.text.toString() == "" -> {
                    Toast.makeText(
                        this@DashBoardActivity,
                        "Pin Code must not be empty!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_passcode.text.toString() != et_re_passcode.text.toString() -> {
                    Toast.makeText(
                        this@DashBoardActivity,
                        "Pin Code must be same!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    appPinCode = et_passcode.text.toString()
                    /* sessionManager.updatePasscodeStatus("1")
                     sessionManager.updatePasscode(et_passcode.text.toString())
                     createPassCodeDialog.dismiss()
                     BankAccountSetUp()*/
                    val jObj = JSONObject()
                    jObj.put("userId", userId)
                    jObj.put("devicePinCode", appPinCode)
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(jObj.toString()) as JsonObject
                    updateAppPinCode(finalObj)
                }
            }
        }

    }

    /*Bank Account Setup*/
    fun BankAccountSetUp() {
        bank_account_dialog = Dialog(this, R.style.DialogFullScreen)
        bank_account_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // bank_account_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        bank_account_dialog.setCancelable(false)

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // bank_account_dialog.window!!.setLayout(width,width)
        bank_account_dialog.window!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        bank_account_dialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)

        bank_account_dialog.setContentView(R.layout.activity_setupwallet)
        val tv_skip_id = bank_account_dialog.findViewById<TextView>(R.id.tv_skip_id)
        tv_skip_id.visibility = View.VISIBLE
        tv_skip_id.setOnClickListener {
            bank_account_dialog.dismiss()
            sessionManager.profileUpdate("1")
        }
        iv_back = bank_account_dialog.findViewById(R.id.iv_back)
        ll_paymentlist = bank_account_dialog.findViewById(R.id.ll_paymentlist)
        ll_banktype = bank_account_dialog.findViewById(R.id.ll_banktype)
        sp_payment_type_id = bank_account_dialog.findViewById(R.id.sp_payment_type_id)
        et_holder_name = bank_account_dialog.findViewById(R.id.et_holder_name)
        et_routinnumber = bank_account_dialog.findViewById(R.id.et_routinnumber)
        et_accountnumber = bank_account_dialog.findViewById(R.id.et_accountnumber)
        et_confirmacctnumber = bank_account_dialog.findViewById(R.id.et_confirmacctnumber)
        et_nameonaccount = bank_account_dialog.findViewById(R.id.et_nameonaccount)
        checkBox = bank_account_dialog.findViewById(R.id.checkBox)
        val img_routing_help_id =
            bank_account_dialog.findViewById(R.id.img_routing_help_id) as ImageView
        val img_account_help_id =
            bank_account_dialog.findViewById(R.id.img_account_help_id) as ImageView
        tv_submit = bank_account_dialog.findViewById(R.id.tv_submit)


        ll_bank_account_id = bank_account_dialog.findViewById(R.id.ll_bank_account_id)
        ll_credit_card_id = bank_account_dialog.findViewById(R.id.ll_credit_card_id)
        stripe_card_id = bank_account_dialog.findViewById(R.id.stripe_card_id)
        id_save_card_id = bank_account_dialog.findViewById(R.id.id_save_card_id)
        et_card_holder_id = bank_account_dialog.findViewById(R.id.et_card_holder_id)
        //bank_account_dialog.show()

        iv_back.visibility = View.GONE
        ll_paymentlist.visibility = View.VISIBLE

        img_routing_help_id.setOnClickListener {
            helpDialog()
        }
        img_account_help_id.setOnClickListener {
            helpDialog()
        }
        select_payment_array = ArrayList()
        select_payment_array.add("Select Bank Account")
        select_payment_array.add("Select Credit Card")

        select_pay_type_id_array = ArrayList()
        select_pay_type_id_array.add("0")
        select_pay_type_id_array.add("1")

        /*amount_int=intent.getStringExtra("amount")!!
        organization_id_stg=intent.getStringExtra("organization_id")!!
        display_name_stg=intent.getStringExtra("display_name")!!
        from_screen_stg=intent.getStringExtra("from_screen")!!
        type_project_stg=intent.getStringExtra("type_project")!!
        projectId_stg=intent.getStringExtra("projectId")!!*/

        val adapter = ArrayAdapter<String>(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            select_payment_array
        )
        sp_payment_type_id.adapter = adapter

        sp_payment_type_id.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                payment_type_stg = select_payment_array.get(position)
                payment_type_id_stg = select_pay_type_id_array.get(position).toString()
                Log.e("payment_type_id_stg", payment_type_id_stg + "---" + payment_type_stg)
                if (payment_type_id_stg.equals("0")) {
                    ll_bank_account_id.visibility = View.VISIBLE
                    ll_credit_card_id.visibility = View.GONE
                } else {
                    ll_bank_account_id.visibility = View.GONE
                    ll_credit_card_id.visibility = View.VISIBLE
                }
            }
            override fun onNothingSelected(arg0: AdapterView<*>) {
            }
        }
        tv_submit.setOnClickListener {
            account_holder_name = et_nameonaccount.text.toString()
            account_routing_no = et_routinnumber.text.toString()
            account_number = et_accountnumber.text.toString()
            account_conf_number = et_confirmacctnumber.text.toString()
            Log.e("---", account_number + "---" + account_conf_number)
            if (account_holder_name == "") {
                toast("Enter Account Holder Name")
            } else if (account_routing_no == "") {
                toast("Enter Valid Routing Number")
            } else if (account_routing_no.length <= 8) {
                toast("Routing number must have a minimum of 9 digits")
            } else if (account_number == "") {
                toast("Enter Valid Account Number")
            } else if (account_number.length <= 8) {
                toast("Account number must have a minimum of 9 digits")
            } else if (account_number != account_conf_number) {
                toast("Enter Valid Account Number")
            } else {
                CreatBankToken()
            }
        }
        /*tv_submit.setOnClickListener {
            account_holder_name = et_nameonaccount.text.toString()
            account_routing_no = et_routinnumber.text.toString()
            account_number = et_accountnumber.text.toString()
            account_conf_number = et_confirmacctnumber.text.toString()
            Log.e("---", account_number + "---" + account_conf_number)
            if (account_holder_name.equals("")) {
                toast("Enter Account Holder Name")
            } else if (account_routing_no.equals("")) {
                toast("Enter Valid Routing Number")
            } else if (account_routing_no.length <= 8) {
                toast("Routing number must have 9 digits")
            } else if (account_number.equals("")) {
                toast("Enter Valid Account Number")
            } else if (account_number != account_conf_number) {
                toast("Enter Valid Account Number")
            } else {
                CreatBankToken()
            }

        }*/

        id_save_card_id.setOnClickListener {
            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                toast("Invalid Card Data")
            } else if (et_card_holder_id.text.toString().trim().equals("")) {
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                loading_dialog.show()
                buy()
            }
        }

        val account_skip = sessionManager.getBankAccountSkip()
        val account_status = account_skip[SessionManager.BANK_AC_SKIP]!!
        if (!account_status.equals("1")) {
            AccountsListApi()
        }
    }

    fun CreatBankToken() {
        loading_dialog.show()
        val stripe_apiInterface = StripeInterFace.create()
        val call = stripe_apiInterface.StripeCreatBankAccount(
            country = "US",
            currency = "usd",
            user_name = account_holder_name,
            type = "individual",
            route = account_routing_no,
            acnumber = account_number
        )
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<CreateBankAccountPojo> {
            override fun onFailure(call: Call<CreateBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<CreateBankAccountPojo>,
                response: Response<CreateBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (!response!!.body()!!.id!!.equals(null)) {

                        Log.e("data_token", response.body()!!.id!!.toString())

                        val obj = JSONObject()
                        obj.put(
                            "userId",
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                        )
                        obj.put("bankToken", response.body()!!.id!!.toString())
                        obj.put("preferred", true)
                        val jParser = JsonParser()
                        val jObject = jParser.parse(obj.toString()) as JsonObject
                        sessionManager.profileUpdate("1")
                        NewAccountSetUp(jObject)
                    } else {
                        toast("Account Details Not Valid")
                    }


                    if (!response.body()!!.error!!.equals(null)) {
                        toast(response.body()!!.error!!.message!!.toString())
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun NewAccountSetUp(jsonObj: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.AddBankAccountApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<NewBankAccountPojo> {
            override fun onFailure(call: Call<NewBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<NewBankAccountPojo>,
                response: Response<NewBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response.body()!!.responseStatus == 1) {
                        toast(response.body()!!.result!!)
                        val intent = Intent(this@DashBoardActivity, WalletActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        toast(response.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /*Bank Accoun List Run Only First Time of LOGIN*/
    private fun AccountsListApi() {
        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject

        // loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jObject)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<BankAccountListPojo>,
                response: Response<BankAccountListPojo>?
            ) {

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response.body()!!.responseStatus == 1) {
                        // toast( response!!.body()!!.result!!)

                        if (response.body()!!.bankAccountList!!.data!!.size > 0) {
                            sessionManager.profileUpdate("1")
                        } else {
                            bank_account_dialog.show()
                        }
                    } else {
                        bank_account_dialog.show()
                        //toast( response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun buy() {
        val validation = get_card.validateCard()
        if (validation) {
            Log.e(
                "input_card",
                get_card.number!! + "--" + get_card.expMonth!! + "--" + get_card.expYear!! + "--" + get_card.cvc!!
            )

            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
                number = get_card.number!!,
                exp_month = get_card.expMonth!!,
                exp_year = get_card.expYear!!,
                cvc = get_card.cvc!!,
                name = et_card_holder_id.text.toString()
            )
            Log.e(
                "input_card",
                get_card.number!! + "--" + get_card.expMonth!! + "--" + get_card.expYear!! + "--" + get_card.cvc!!
            )
            //Log.e("@response", call.toString())
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)

                }

                override fun onResponse(
                    call: Call<CardTokenPojo>,
                    response: Response<CardTokenPojo>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {

                        val obj = JSONObject()
                        obj.put(
                            "userId",
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                        )
                        obj.put("cardToken", response!!.body()!!.id!!.toString())
                        obj.put("preferred", true)
                        val jParser = JsonParser()
                        val jObject = jParser.parse(obj.toString()) as JsonObject
                        Log.e("save_card_input", jObject.toString())
                        SaveNewCardApi(jObject)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            /*End Card Token API*/


        } else if (!get_card.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun SaveNewCardApi(jsonObj: JsonObject) {
        Log.e("id_error", "error idsss")

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.SaveCardApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<NewBankAccountPojo> {
            override fun onFailure(call: Call<NewBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<NewBankAccountPojo>,
                response: Response<NewBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response.body()!!.responseStatus == 1) {
                        toast(response.body()!!.result!!)
                        val intent = Intent(this@DashBoardActivity, WalletActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        toast(response.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    fun helpDialog() {
        val bank_help_dialog = Dialog(this)
        bank_help_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_help_dialog.setCancelable(false)

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        bank_help_dialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        //bank_help_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);
        bank_help_dialog.window!!.setBackgroundDrawableResource(R.color.transparent)


        bank_help_dialog.setContentView(R.layout.bank_help_dilaog_layout)
        val img_help_close_id = bank_help_dialog.findViewById<ImageView>(R.id.img_help_close_id)
        img_help_close_id.setOnClickListener {
            bank_help_dialog.dismiss()
        }
        bank_help_dialog.show()
    }

    // updating device token
    private fun updateDeviceToken(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.updateDeviceTokenApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Log.d("deviceToken", "error $t")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    Log.d("deviceToken", "success ${resp!!.responseStatus}")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateAppPinCode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.updateAppPinApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        sessionManager.updatePasscode(appPinCode)
                        createPassCodeDialog.dismiss()
                        BankAccountSetUp()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
