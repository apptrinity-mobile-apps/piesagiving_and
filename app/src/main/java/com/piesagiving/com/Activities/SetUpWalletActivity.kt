package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SetUpWalletActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView

    lateinit var ll_paymentlist: LinearLayout
    lateinit var ll_banktype: LinearLayout
    lateinit var et_paymentlist: EditText
    lateinit var et_holder_name: EditText
    lateinit var et_routinnumber: EditText
    lateinit var et_accountnumber: EditText
    lateinit var et_confirmacctnumber: EditText
    lateinit var et_nameonaccount: EditText
    lateinit var et_card_holder_id: EditText
    lateinit var checkBox: CheckBox
    lateinit var cardCheckBox: CheckBox
    lateinit var tv_submit: TextView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var payment_type_id_stg = ""
    var payment_type_stg = ""
    lateinit var select_payment_array: ArrayList<String>
    lateinit var select_pay_type_id_array: ArrayList<String>
    lateinit var sp_payment_type_id: Spinner

    lateinit var ll_credit_card_id: LinearLayout
    lateinit var ll_bank_account_id: LinearLayout

    lateinit var stripe_card_id: CardInputWidget
    lateinit var id_save_card_id: Button
    lateinit var get_card: Card


    var amount_int = ""
    var organization_id_stg = ""
    var projectId_stg = ""
    var from_screen_stg = ""
    var type_project_stg = ""
    var display_name_stg = ""

    var account_holder_name = ""
    var account_routing_no = ""
    var account_number = ""
    var account_conf_number = ""
    var account_holder_type = ""
    lateinit var loading_dialog: Dialog
    lateinit var img_routing_help_id: ImageView
    lateinit var img_account_help_id: ImageView
    var preferredCard = true
    var preferredBankAccount = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_setupwallet)
        initialize()

        select_payment_array = ArrayList()
        select_payment_array.add("Select Bank Account")
        select_payment_array.add("Select Credit Card")

        select_pay_type_id_array = ArrayList()
        select_pay_type_id_array.add("0")
        select_pay_type_id_array.add("1")


        /*amount_int=intent.getStringExtra("amount")!!
        organization_id_stg=intent.getStringExtra("organization_id")!!
        display_name_stg=intent.getStringExtra("display_name")!!
        from_screen_stg=intent.getStringExtra("from_screen")!!
        type_project_stg=intent.getStringExtra("type_project")!!
        projectId_stg=intent.getStringExtra("projectId")!!*/

        //make preferred account - required api.
        checkBox.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                preferredBankAccount = isChecked
            }
        })

        cardCheckBox.setOnCheckedChangeListener(object :CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                preferredCard = isChecked
            }
        })

        val adapter = ArrayAdapter<String>(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            select_payment_array
        )
        sp_payment_type_id.adapter = adapter

        sp_payment_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                payment_type_stg = select_payment_array.get(position)
                payment_type_id_stg = select_pay_type_id_array.get(position).toString()
                Log.e("payment_type_id_stg", payment_type_id_stg + "---" + payment_type_stg)

                if (payment_type_id_stg.equals("0")) {
                    ll_bank_account_id.visibility = View.VISIBLE
                    ll_credit_card_id.visibility = View.GONE
                } else {
                    ll_bank_account_id.visibility = View.GONE
                    ll_credit_card_id.visibility = View.VISIBLE
                }

            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        tv_submit.setOnClickListener {
            account_holder_name = et_nameonaccount.text.toString()
            account_routing_no = et_routinnumber.text.toString()
            account_number = et_accountnumber.text.toString()
            account_conf_number = et_confirmacctnumber.text.toString()
            Log.e("---", account_number + "---" + account_conf_number)
            if (account_holder_name == "") {
                toast("Enter Account Holder Name")
            } else if (account_routing_no == "") {
                toast("Enter Valid Routing Number")
            } else if (account_routing_no.length <= 8) {
                toast("Routing number must have a minimum of 9 digits")
            } else if (account_number == "") {
                toast("Enter Valid Account Number")
            } else if (account_number.length <= 8) {
                toast("Account number must have a minimum of 9 digits")
            } else if (account_number != account_conf_number) {
                toast("Enter Valid Account Number")
            } else {
                CreatBankToken()
            }

        }

        id_save_card_id.setOnClickListener {

            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                toast("Invalid Card Data")
            }else if(et_card_holder_id.text.toString().trim().equals("")){
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                loading_dialog.show()
                buy()
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        ll_paymentlist = findViewById(R.id.ll_paymentlist)
        ll_banktype = findViewById(R.id.ll_banktype)
        sp_payment_type_id = findViewById(R.id.sp_payment_type_id)
        et_holder_name = findViewById(R.id.et_holder_name)
        et_routinnumber = findViewById(R.id.et_routinnumber)
        et_accountnumber = findViewById(R.id.et_accountnumber)
        et_confirmacctnumber = findViewById(R.id.et_confirmacctnumber)
        et_nameonaccount = findViewById(R.id.et_nameonaccount)
        checkBox = findViewById(R.id.checkBox)
        cardCheckBox = findViewById(R.id.cardCheckBox)
        tv_submit = findViewById(R.id.tv_submit)
        et_card_holder_id = findViewById(R.id.et_card_holder_id)
        img_routing_help_id = findViewById(R.id.img_routing_help_id)
        img_account_help_id = findViewById(R.id.img_account_help_id)
        isNetworkAvailable = cd.isConnectingToInternet

        ll_bank_account_id = findViewById(R.id.ll_bank_account_id)
        ll_credit_card_id = findViewById(R.id.ll_credit_card_id)
        stripe_card_id = findViewById(R.id.stripe_card_id)
        id_save_card_id = findViewById(R.id.id_save_card_id)

        ll_paymentlist.visibility=View.VISIBLE


        img_routing_help_id.setOnClickListener {
            helpDialog()
        }
        img_account_help_id.setOnClickListener {
            helpDialog()
        }


    }

    fun helpDialog(){
        val bank_help_dialog = Dialog(this)
        bank_help_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_help_dialog.setCancelable(false)

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        bank_help_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        //bank_help_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);
        bank_help_dialog.getWindow()!!.setBackgroundDrawableResource(R.color.transparent)

        bank_help_dialog.setContentView(R.layout.bank_help_dilaog_layout)
        val img_help_close_id=bank_help_dialog.findViewById<ImageView>(R.id.img_help_close_id)
        img_help_close_id.setOnClickListener {
            bank_help_dialog.dismiss()
        }
        bank_help_dialog.show()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun CreatBankToken() {
        /*val bankAccount: MutableMap<String, Any> = HashMap()
        bankAccount["country"] = "US"
        bankAccount["currency"] = "usd"
        bankAccount["account_holder_name"] = "Jenny Rosen"
        bankAccount["account_holder_type"] = "individual"
        bankAccount["routing_number"] = "110000000"
        bankAccount["account_number"] = "000123456789"
        val params: MutableMap<String, Any> = HashMap()
        params["bank_account"] = bankAccount

        Log.e("params",params.toString())*/


        loading_dialog.show()

        val stripe_apiInterface = StripeInterFace.create()
        val call = stripe_apiInterface.StripeCreatBankAccount(
            country = "US",
            currency = "usd",
            user_name = account_holder_name,
            type = "individual",
            route = account_routing_no,
            acnumber = account_number
        )
        //Log.e("@response", call.toString())
        call.enqueue(object : Callback<CreateBankAccountPojo> {
            override fun onFailure(call: Call<CreateBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<CreateBankAccountPojo>,
                response: Response<CreateBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if(response!!.body()!!.equals(null)){
                        if (!response!!.body()!!.error!!.equals(null)) {
                            toast(response!!.body()!!.error!!.message!!.toString())
                        }
                        toast("Account Details Not Valid")

                    }
                    if (!response!!.body()!!.id!!.equals(null)) {

                        Log.e("data_token", response!!.body()!!.id!!.toString())

                        val obj = JSONObject()
                        obj.put(
                            "userId",
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                        )
                        obj.put("bankToken", response!!.body()!!.id!!.toString())
                        obj.put("preferred", preferredBankAccount)
                        val jParser = JsonParser()
                        val jObject = jParser.parse(obj.toString()) as JsonObject
                        Log.e("error_id", response!!.body()!!.id!!.toString())
                        NewAccountSetUp(jObject)
                    } else {
                        toast("Account Details Not Valid")
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    private fun NewAccountSetUp(jsonObj: JsonObject) {
        Log.e("id_error", "error idsss")

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.AddBankAccountApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<NewBankAccountPojo> {
            override fun onFailure(call: Call<NewBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<NewBankAccountPojo>,
                response: Response<NewBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {
                        toast(response!!.body()!!.result!!)
                        val intent = Intent(this@SetUpWalletActivity, WalletActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    private fun buy() {
        val validation = get_card!!.validateCard()
        if (validation) {
            Log.e("input_card",get_card.number!!+"--"+get_card.expMonth!!+"--"+get_card.expYear!!+"--"+get_card.cvc!!)

            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
               number = get_card.number!!, exp_month = get_card.expMonth!!, exp_year = get_card.expYear!!, cvc = get_card.cvc!!, name = et_card_holder_id.text.toString()
            )
            Log.e("input_card",get_card.number!!+"--"+get_card.expMonth!!+"--"+get_card.expYear!!+"--"+get_card.cvc!!)
            //Log.e("@response", call.toString())
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)

                }

                override fun onResponse(
                    call: Call<CardTokenPojo>,
                    response: Response<CardTokenPojo>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {

                        val obj = JSONObject()
                        obj.put(
                            "userId",
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                        )
                        obj.put("cardToken", response!!.body()!!.id!!.toString())
                        obj.put("preferred", preferredCard)
                        val jParser = JsonParser()
                        val jObject = jParser.parse(obj.toString()) as JsonObject
                        Log.e("save_card_input",jObject.toString())
                        SaveNewCardApi(jObject)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            /*End Card Token API*/


        } else if (!get_card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }


    private fun SaveNewCardApi(jsonObj: JsonObject) {
        Log.e("id_error", "error idsss")

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.SaveCardApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<NewBankAccountPojo> {
            override fun onFailure(call: Call<NewBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<NewBankAccountPojo>,
                response: Response<NewBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {
                        toast(response!!.body()!!.result!!)
                        val intent = Intent(this@SetUpWalletActivity, WalletActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }
    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}
