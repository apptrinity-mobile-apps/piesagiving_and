package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.LocationHistoryAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.HistoryResultsResponse
import com.piesagiving.com.ApiInterface.LoginHistoryPojo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginHistoryActivity : AppCompatActivity() {
    lateinit var rv_location_history_id: RecyclerView
    lateinit var ll_no_data: LinearLayout
    lateinit var sesstion: SessionManager
    lateinit var loading_dialog: Dialog
    lateinit var historyArraylist: ArrayList<HistoryResultsResponse>

    lateinit var iv_back: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login_history)
        sesstion = SessionManager(this)
        rv_location_history_id = findViewById(R.id.rv_location_history_id)
        ll_no_data = findViewById(R.id.ll_no_data)

        iv_back = findViewById(R.id.iv_back)
        val userId = sesstion.getUserDetails()[SessionManager.ID_KEY]!!

        loadingDialog()

        val obj = JSONObject()
        obj.put("userId", userId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("history_input", jObject.toString())
        SigninHistoryResponse(jObject)

        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun SigninHistoryResponse(json_obj: JsonObject) {

        loading_dialog.show()
        historyArraylist = ArrayList()
        historyArraylist.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.SigninHistoryApi(json_obj)
        call.enqueue(object : Callback<LoginHistoryPojo> {
            override fun onFailure(call: Call<LoginHistoryPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<LoginHistoryPojo>,
                response: Response<LoginHistoryPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())

                    if (response!!.body()!!.responseStatus!!.equals(1)) {
                        for (i in 0 until response.body()!!.result!!.size) {
                            historyArraylist.add(
                                response.body()!!.result!!.get(
                                    i
                                )
                            )
                            val LayoutManager =
                                LinearLayoutManager(this@LoginHistoryActivity!!, RecyclerView.VERTICAL, false)
                            rv_location_history_id.layoutManager = LayoutManager
                            rv_location_history_id.setHasFixedSize(true)
                            val locationadapter = LocationHistoryAdapter(this@LoginHistoryActivity, historyArraylist)

                            rv_location_history_id.adapter = locationadapter
                            locationadapter.notifyDataSetChanged()
                        }
                        ll_no_data.visibility= View.GONE
                    }else{
                        ll_no_data.visibility= View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@LoginHistoryActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@LoginHistoryActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }
}