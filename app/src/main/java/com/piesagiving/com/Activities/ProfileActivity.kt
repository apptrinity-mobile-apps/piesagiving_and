package com.piesagiving.com.Activities

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.CitiesAdapter
import com.piesagiving.com.Adapters.CountriesAdapter
import com.piesagiving.com.Adapters.StatesAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.*
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ProfileActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var et_firstname: EditText
    lateinit var et_lastname: EditText
    lateinit var et_streetaddress: EditText
    lateinit var et_cityname: EditText
    lateinit var et_country_name: EditText
    lateinit var et_statename: EditText
    lateinit var et_zipcode: EditText
    lateinit var et_gender: EditText
    lateinit var et_dateofbirth: EditText
    lateinit var tv_submit: TextView
    lateinit var et_username: EditText
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""

    var selectedCountry = ""
    var selectedCountryId = ""
    var selectedState = ""
    var selectedStateId = ""
    var selectedCity = ""
    var selectedCityId = ""
    var dialogHeight = 0
    var dialogWidth = 0

    lateinit var loading_dialog: Dialog
    lateinit var countriesRecyclerView: RecyclerView
    lateinit var statesRecyclerView: RecyclerView
    lateinit var citiesRecyclerView: RecyclerView
    lateinit var tv_emptyCountries: TextView
    lateinit var tv_emptyStates: TextView
    lateinit var tv_emptyCities: TextView
    lateinit var countyLoaderDialog: LinearLayout
    lateinit var stateLoaderDialog: LinearLayout
    lateinit var cityLoaderDialog: LinearLayout

    var showDatePikerDialog = CustomDatePickerDialog()

    lateinit var countriesList: ArrayList<CountriesDataResponse>
    lateinit var statesList: ArrayList<StatesDataResponse>
    lateinit var userdataList: ArrayList<UserProfileDataResponse>
    lateinit var citiesList: ArrayList<CitiesDataResponse>
    lateinit var countriesAdapter: CountriesAdapter
    lateinit var cityAdapter: CitiesAdapter
    var birthday = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_profile)

        val metrics = resources.displayMetrics
        dialogWidth = (metrics.widthPixels * 0.85).toInt()
        dialogHeight = (metrics.heightPixels * 0.85).toInt()
        initialize()

        if (isNetworkAvailable) {
            val dataObj = JSONObject()
            dataObj.put("userId", userId)
            val jsonParser = JsonParser()
            val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
            getUserProfileApi(finalJsonObject)
        } else {
            Toast.makeText(
                this,
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        // set first letter CAPS
        et_firstname.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        et_lastname.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        et_streetaddress.inputType =
            InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

        // login to app
        tv_submit.setOnClickListener {
            if (isNetworkAvailable) {
                if (isValidForm()) {
                    /*try {
                        val inputSDF = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                        val outputSDF = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        val date = inputSDF.parse(et_dateofbirth.text.toString())
                        birthday = outputSDF.format(date!!)
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }*/
                    val dataObj = JSONObject()
                    dataObj.put("uid", userId)
                    dataObj.put("firstName", et_firstname.text.toString().trim())
                    dataObj.put("lastName", et_lastname.text.toString().trim())
                    dataObj.put("streetAddress", et_streetaddress.text.toString().trim())
                    dataObj.put("city", /*selectedCityId*/et_cityname.text.toString().trim())
                    dataObj.put("state", selectedStateId)
                    dataObj.put("country", selectedCountryId)
                    dataObj.put("zipcode", et_zipcode.text.toString().trim())
                    dataObj.put("birthday", et_dateofbirth.text.toString().trim())
                    dataObj.put("userName",et_username.text.toString().trim())
                    Log.d("TAG", "dataObj===$dataObj")
                    val jsonParser = JsonParser()
                    val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                    ProfileApi(finalJsonObject)
                }
            } else {
                Toast.makeText(
                    this,
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        et_firstname = findViewById(R.id.et_firstname)
        et_lastname = findViewById(R.id.et_lastname)
        et_streetaddress = findViewById(R.id.et_streetaddress)
        et_cityname = findViewById(R.id.et_cityname)
        et_country_name = findViewById(R.id.et_country_name)
        et_statename = findViewById(R.id.et_statename)
        et_zipcode = findViewById(R.id.et_zipcode)
        et_gender = findViewById(R.id.et_gender)
        et_dateofbirth = findViewById(R.id.et_dateofbirth)
        tv_submit = findViewById(R.id.tv_submit)
        et_username = findViewById(R.id.et_username)


        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

        et_country_name.setOnClickListener {
            // show country picker
           // showCountriesDialog()
        }

        et_statename.setOnClickListener {
            // show state picker
            if (selectedCountry == "") {
                Toast.makeText(
                    this,
                    getString(R.string.select_your_country),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                showStatesDialog()
            }
        }

        et_cityname.setOnClickListener {
            // show city picker
            /*if (selectedState == "") {
                Toast.makeText(
                    this,
                    getString(R.string.select_your_state),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                showCitiesDialog()
            }*/
        }

        // show calendar
        et_dateofbirth.setOnClickListener {
            if (et_dateofbirth.text.toString() == "") {
                showDatePikerDialog.showDatePicker(this, "", et_dateofbirth)
            } else {
                showDatePikerDialog.showDatePicker(this, et_dateofbirth.text.toString(), et_dateofbirth)
            }
        }


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun isValidForm(): Boolean {
        when {
            et_firstname.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "First name should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_lastname.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "Last name should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_streetaddress.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "Address should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_country_name.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "Country should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_statename.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "State name should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_cityname.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "City name should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_zipcode.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "Zip Code should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            et_dateofbirth.text.toString().isEmpty() -> {
                Toast.makeText(
                    this,
                    "Birthday date should not be empty",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {
                return true
            }
        }
        return false
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


    private fun showCountriesDialog() {

        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(true)

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        tv_emptyCountries = view.findViewById(R.id.tv_empty)
        countriesRecyclerView = view.findViewById(R.id.recyclerView)
        countyLoaderDialog = view.findViewById(R.id.ll_loading)
        val layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        countriesRecyclerView.setHasFixedSize(true)
        countriesRecyclerView.layoutManager = layoutManager

        getCountries()

        tv_header.text = getString(R.string.select_country)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_country_name.setText(selectedCountry)
            dialog.dismiss()
        }

        countriesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedCountry = countriesAdapter.getItemAt(position).name!!
                        val id = countriesAdapter.getItemAt(position).countryId!!
                        selectedCountryId = id.toString()
                        Log.d("TAG", "selected country===$selectedCountry===$selectedCountryId")
                        et_country_name.setText(selectedCountry)
                        dialog.dismiss()
                    }
                })
        )
        dialog.show()
    }


    private fun getCountries() {
        // change loading dialog later
        //loading_dialog.show()
        countyLoaderDialog.visibility = View.VISIBLE
        countriesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getCountriesApi()
        call.enqueue(object : Callback<CountriesResponse> {
            override fun onFailure(call: Call<CountriesResponse>, t: Throwable) {
                try {
                    Log.d("TAG", "countriesApi error $t")
                    countyLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CountriesResponse>,
                response: Response<CountriesResponse>
            ) {
                try {
                    countyLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.countries
                            if (data!!.size > 0) {
                                countriesList = data
//                                countriesList.reverse()
                                /* countriesAdapter =
                                    CountriesAdapter(this@ProfileActivity, countriesList)
                                countriesRecyclerView.adapter = adapter*/
                                // adding only USA to list
                                val list: ArrayList<CountriesDataResponse> = ArrayList()
                                for (i in 0 until countriesList.size) {
                                    if (countriesList[i].iso3!!.equals("USA", ignoreCase = false)) {
                                        list.add(countriesList[i])
                                    }
                                }
                                countriesAdapter =
                                    CountriesAdapter(this@ProfileActivity, list)
                                countriesRecyclerView.adapter = countriesAdapter
                                if (countriesList.size > 0) {
                                    tv_emptyCountries.visibility = View.GONE
                                    countriesRecyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_emptyCountries.visibility = View.VISIBLE
                                    countriesRecyclerView.visibility = View.GONE
                                }
                            } else {
                                Toast.makeText(
                                    this@ProfileActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun getStates(final_object: JsonObject) {
        stateLoaderDialog.visibility = View.VISIBLE
//        loading_dialog.show()
        statesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getStatesApi(final_object)
        call.enqueue(object : Callback<StatesResponse> {
            override fun onFailure(call: Call<StatesResponse>, t: Throwable) {
                try {
                    Log.d("TAG", "statesApi error $t")
                    stateLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    )

                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<StatesResponse>,
                response: Response<StatesResponse>
            ) {
                try {
                    stateLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.states
                            if (data!!.size > 0) {
                                statesList = data
//                                statesList.reverse()
                                val adapter =
                                    StatesAdapter(this@ProfileActivity, statesList)
                                statesRecyclerView.adapter = adapter
                                if (statesList.size > 0) {
                                    tv_emptyStates.visibility = View.GONE
                                    statesRecyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_emptyStates.visibility = View.VISIBLE
                                    statesRecyclerView.visibility = View.GONE
                                }
                            } else {
                                Toast.makeText(
                                    this@ProfileActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun getCities(final_object: JsonObject) {
        cityLoaderDialog.visibility = View.VISIBLE
//        loading_dialog.show()
        citiesList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getCitiesApi(final_object)
        call.enqueue(object : Callback<CitiesResponse> {
            override fun onFailure(call: Call<CitiesResponse>, t: Throwable) {
                try {
                    Log.d("TAG", "citiesApi error $t")
                    cityLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CitiesResponse>,
                response: Response<CitiesResponse>
            ) {
                try {
                    cityLoaderDialog.visibility = View.GONE
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.cities
                            if (data!!.size > 0) {
                                citiesList = data
//                                citiesList.reverse()
                                cityAdapter =
                                    CitiesAdapter(this@ProfileActivity, citiesList)
                                citiesRecyclerView.adapter = cityAdapter
                                if (citiesList.size > 0) {
                                    tv_emptyCities.visibility = View.GONE
                                    citiesRecyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_emptyCities.visibility = View.VISIBLE
                                    citiesRecyclerView.visibility = View.GONE
                                }
                            } else {
                                Toast.makeText(
                                    this@ProfileActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun showStatesDialog() {

        val dialog = Dialog(this@ProfileActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ProfileActivity)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(true)

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        tv_emptyStates = view.findViewById(R.id.tv_empty)
        stateLoaderDialog = view.findViewById(R.id.ll_loading)
        statesRecyclerView = view.findViewById(R.id.recyclerView)
        val layoutManager =
            LinearLayoutManager(this@ProfileActivity, RecyclerView.VERTICAL, false)
        statesRecyclerView.setHasFixedSize(true)
        statesRecyclerView.layoutManager = layoutManager

        val dataObj = JSONObject()
        dataObj.put("countryId", selectedCountryId)
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
        getStates(finalJsonObject)

        tv_header.text = getString(R.string.select_state)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_statename.setText(selectedState)
            dialog.dismiss()
        }

        statesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this@ProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedState = statesList[position].name!!
                        val id = statesList[position].stateId!!
                        selectedStateId = id.toString()
                        Log.d("TAG", "selected state===$selectedState===$selectedStateId")
                        et_statename.setText(selectedState)
                        dialog.dismiss()
                    }
                })
        )
        dialog.show()
    }

    private fun showCitiesDialog() {

        val dialog = Dialog(this@ProfileActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ProfileActivity)
                .inflate(R.layout.countries_dialog, null)
        dialog.setContentView(view)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window!!.setLayout(dialogWidth, dialogHeight)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val iv_close = view.findViewById(R.id.iv_close) as ImageView
        val tv_ok = view.findViewById(R.id.tv_ok) as TextView
        val tv_header = view.findViewById(R.id.tv_header) as TextView
        tv_emptyCities = view.findViewById(R.id.tv_empty)
        cityLoaderDialog = view.findViewById(R.id.ll_loading)
        citiesRecyclerView = view.findViewById(R.id.recyclerView)
        val ll_search = view.findViewById(R.id.ll_search) as LinearLayout
        val et_search = view.findViewById(R.id.et_search) as EditText
        ll_search.visibility = View.VISIBLE
        val layoutManager =
            LinearLayoutManager(this@ProfileActivity, RecyclerView.VERTICAL, false)
        citiesRecyclerView.setHasFixedSize(true)
        citiesRecyclerView.layoutManager = layoutManager

        val dataObj = JSONObject()
        dataObj.put("stateId", selectedStateId)
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
        getCities(finalJsonObject)

        tv_header.text = getString(R.string.select_state)
        iv_close.setOnClickListener {
            dialog.dismiss()
        }
        tv_cancel.setOnClickListener {
            dialog.dismiss()
        }
        tv_ok.setOnClickListener {
            et_cityname.setText(selectedCity)
            dialog.dismiss()
        }
        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                cityAdapter.filter.filter(s)
                cityAdapter.removeSelectionAt(-1)
            }
        })

        citiesRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedCity = cityAdapter.getItemAt(position).name!!
                        val id = cityAdapter.getItemAt(position).cityId!!
                        selectedCityId = id.toString()
                        Log.d("TAG", "selected city===$selectedCity===$selectedCityId")
                        et_cityname.setText(selectedCity)
                        dialog.dismiss()
                    }
                })
        )
    }

    private fun ProfileApi(final_object: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProfileApi(final_object)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                try {
                    Log.d("TAG", "registerApi error $t")

                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>, response: Response<ProfileDetailsPojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val dataObj = JSONObject()
                            dataObj.put("userId", userId)
                            val jsonParser = JsonParser()
                            val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject
                            getUserProfileApi(finalJsonObject)
                            val alertDialog = AlertDialog.Builder(this@ProfileActivity)
                            alertDialog.setTitle("Alert!")
                            alertDialog.setMessage("Profile Updated!")
                            alertDialog.setPositiveButton(getString(R.string.ok)
                            ) { dialog, which ->
                                dialog!!.dismiss()
                            }
                            alertDialog.create()
                            alertDialog.show()
                        }
                        0 -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                getString(R.string.something_went_wrong),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getUserProfileApi(final_object: JsonObject) {
        loading_dialog.show()
        userdataList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetUserProfileApi(final_object)
        call.enqueue(object : Callback<UserProfileResponse> {
            override fun onFailure(call: Call<UserProfileResponse>, t: Throwable) {
                try {
                    Log.d("TAG", "statesApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<UserProfileResponse>,
                response: Response<UserProfileResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            val data = resp.userList

                            selectedCity = data!!.city.toString()
                            selectedCountry = data.country.toString()
                            val firstName = data.firstName
                            val lastName = data.lastName
                            selectedState = data.state.toString()
                            val streetAddress = data.streetAddress
                            val zipcode = data.zipcode
                            val dob = data.birthday
                            val userName = data.userName
                            selectedCityId= data.cityId.toString()
                            selectedStateId = data.stateId.toString()
                            selectedCountryId = data.countryId.toString()
                            et_firstname.setText(firstName)
                            et_lastname.setText(lastName)
                            et_streetaddress.setText(streetAddress)
                            et_cityname.setText(selectedCity)
                            et_country_name.setText(selectedCountry)
                            et_statename.setText(selectedState)
                            et_zipcode.setText(zipcode)
                            et_gender.setText("")
                            et_dateofbirth.setText(dob)
                            et_username.setText(userName)
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileActivity,
                                getString(R.string.no_record_available),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


}
