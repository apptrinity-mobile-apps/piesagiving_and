package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_account_profile: LinearLayout
    lateinit var ll_account_email: LinearLayout
    lateinit var ll_account_changepassword: LinearLayout
    lateinit var ll_account_delete: LinearLayout
    lateinit var ll_account_change_pin: LinearLayout
    lateinit var loading_dialog: Dialog
    lateinit var changePassCodeDialog: BottomSheetDialog

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var userId = ""
    var pinCode = ""
    var appPinCode = ""
    var newAppPinCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_account)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        // navigate to forgot password activity
        ll_account_profile.setOnClickListener {
            val intent = Intent(this@AccountActivity, ProfileActivity::class.java)
            startActivity(intent)
        }
        // navigate to register profile
        ll_account_email.setOnClickListener {
            val intent = Intent(this@AccountActivity, ProfileEmailActivity::class.java)
            startActivity(intent)
        }
        ll_account_changepassword.setOnClickListener {
            val intent = Intent(this@AccountActivity, ProfileChangePasswordActivity::class.java)
            startActivity(intent)
        }
        ll_account_change_pin.setOnClickListener {
            changePassCodeDialog.show()
        }
        ll_account_delete.setOnClickListener {
            AlertDialog.Builder(this@AccountActivity)
                .setTitle(getString(R.string.delete_account))
                .setMessage("Deleting your account will wipe out your donation methods, donation history, and account settings. \nPlease confirm you want to delete your account.")
                .setPositiveButton(getString(R.string.delete_account)) { dialog, which ->
                    if (isNetworkAvailable) {
                        val jObj = JSONObject()
                        jObj.put("userId", userId)
                        val jParser = JsonParser()
                        val final_obj = jParser.parse(jObj.toString()) as JsonObject
                        deleteAccount(final_obj)
                    } else {
                        Toast.makeText(
                            this@AccountActivity,
                            getString(R.string.no_network_available),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dialog!!.dismiss()
                }
                .setNegativeButton(getString(R.string.cancel)) { dialog, which -> dialog!!.dismiss() }
                .create()
                .show()
        }
    }

    private fun initialize() {
        loadingDialog()
        changePinDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!
        pinCode = userDetails[SessionManager.PASSCODE_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        ll_account_profile = findViewById(R.id.ll_account_profile)
        ll_account_email = findViewById(R.id.ll_account_email)
        ll_account_changepassword = findViewById(R.id.ll_account_changepassword)
        ll_account_delete = findViewById(R.id.ll_account_delete)
        ll_account_change_pin = findViewById(R.id.ll_account_change_pin)
        isNetworkAvailable = cd.isConnectingToInternet
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@AccountActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@AccountActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun changePinDialog() {
        changePassCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.change_passcode_dialog, null)
        changePassCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        changePassCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        changePassCodeDialog.setCancelable(true)
        changePassCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
//        changePassCodeDialog.window!!.setWindowAnimations(R.style.AppTheme_Slide)
        changePassCodeDialog.setContentView(view)
        changePassCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val et_passcode = view.findViewById(R.id.et_passcode) as EditText
        val et_re_passcode = view.findViewById(R.id.et_re_passcode) as EditText
        val et_old_passcode = view.findViewById(R.id.et_old_passcode) as EditText
        tv_submit.setOnClickListener {
            when {
                et_old_passcode.text.toString() == "" -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "Pin Code must not be empty!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_passcode.text.toString() == "" -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "Pin Code must not be empty!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_re_passcode.text.toString() == "" -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "Pin Code must not be empty!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_old_passcode.text.toString() != pinCode -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "Current Pin Code is in-correct!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_old_passcode.text.toString() == et_passcode.text.toString() -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "New Pin Code must not be same as old Pin Code!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                et_passcode.text.toString() != et_re_passcode.text.toString() -> {
                    Toast.makeText(
                        this@AccountActivity,
                        "Pin Code must be same!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    appPinCode = et_old_passcode.text.toString()
                    newAppPinCode = et_passcode.text.toString()
                    Log.d("pincode", "$pinCode === $appPinCode === $newAppPinCode")
                    val jObj = JSONObject()
                    jObj.put("userId", userId)
                    jObj.put("devicePinCode", newAppPinCode)
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(jObj.toString()) as JsonObject
                    changeAppPinCode(finalObj)
                }
            }
        }
        changePassCodeDialog.setOnDismissListener {
            et_old_passcode.setText("")
            et_passcode.setText("")
            et_re_passcode.setText("")
        }

    }

    private fun deleteAccount(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.deleteAccountApi(finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        sessionManager.logout()
                        val intent = Intent(this@AccountActivity, SignUpSignInActivity::class.java)
                        startActivity(intent)
                        finish()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun changeAppPinCode(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.updateAppPinApi(finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        sessionManager.updatePasscode(newAppPinCode)
                        changePassCodeDialog.dismiss()
                        Toast.makeText(this@AccountActivity, resp.result, Toast.LENGTH_SHORT)
                            .show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
