package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.AllNotificationsAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class NotificationsListActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_friend_request: LinearLayout
    lateinit var tv_friends_count: TextView
    lateinit var loading_dialog: Dialog
    lateinit var rv_notifications: RecyclerView
    lateinit var tv_empty_notifications: TextView
    lateinit var ll_announcements: LinearLayout

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    lateinit var dataList: ArrayList<UserNotificationsData>
    lateinit var adapter: AllNotificationsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_notificationslist)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()

        if (isNetworkAvailable) {
            val jObj = JSONObject()
            jObj.put("userId", userId)
            val jParser = JsonParser()
            val jsonObject = jParser.parse(jObj.toString()) as JsonObject
            getNotifications(jsonObject)
        } else {
            Toast.makeText(
                this@NotificationsListActivity, getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        ll_friend_request.setOnClickListener {
            val intent = Intent(this, SearchPeopleActivity::class.java)
            intent.putExtra("from_screen", "request")
            startActivity(intent)
        }
        ll_announcements.setOnClickListener {
            val intent = Intent(this, AnnouncementsActivity::class.java)
            startActivity(intent)
        }

        rv_notifications.addOnItemTouchListener(
            RecyclerItemClickListener(this@NotificationsListActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {}
                })
        )
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        loadingDialog()

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

        tv_friends_count = findViewById(R.id.tv_friends_count)
        ll_friend_request = findViewById(R.id.ll_friend_request)
        rv_notifications = findViewById(R.id.rv_notifications)
        tv_empty_notifications = findViewById(R.id.tv_empty_notifications)
        ll_announcements = findViewById(R.id.ll_announcements)
        val layoutManager =
            LinearLayoutManager(this@NotificationsListActivity, RecyclerView.VERTICAL, false)
        rv_notifications.layoutManager = layoutManager
        rv_notifications.hasFixedSize()

        dataList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getNotifications(final_object: JsonObject) {
        dataList = ArrayList()
        var friendReqUnReadCount: Int
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getNotificationsApi(final_object)
        call.enqueue(object : Callback<GetNotificationsResponse> {
            override fun onFailure(call: Call<GetNotificationsResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@NotificationsListActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    tv_empty_notifications.visibility = View.GONE
                    rv_notifications.visibility = View.VISIBLE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetNotificationsResponse>,
                response: Response<GetNotificationsResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val notifications = resp.user_notifications!!
                        for (i in 0 until notifications.size) {
                            if (notifications[i].donation!!.isNotEmpty() || notifications[i].follow_request!!.isNotEmpty() || notifications[i].organisation!!.isNotEmpty()) {
                                dataList.add(notifications[i])
                            }
                        }
                        friendReqUnReadCount = resp.friend_requests_count!!
                        tv_friends_count.text = friendReqUnReadCount.toString()
                        val unReadList = ArrayList<NotificationsData>()
                        for (i in 0 until dataList.size) {
                            unReadList.addAll(dataList[i].donation!!)
                            unReadList.addAll(dataList[i].follow_request!!)
                            unReadList.addAll(dataList[i].organisation!!)
                        }
                        Log.d("unReadList",unReadList.size.toString())
                        for (i in 0 until unReadList.size) {
                            if (unReadList[i].read_status == 1) {
                                val jObj = JSONObject()
                                jObj.put("notificationId", unReadList[i].notificationId)
                                val jParser = JsonParser()
                                val fObject = jParser.parse(jObj.toString()) as JsonObject
                                Log.d("unReadList",jObj.toString())
                                updateNotificationStatus(fObject)
                            }
                        }
                        adapter = AllNotificationsAdapter(this@NotificationsListActivity, dataList)
                        rv_notifications.adapter = adapter
                        adapter.notifyDataSetChanged()
                        if (dataList.size > 0) {
                            tv_empty_notifications.visibility = View.GONE
                            rv_notifications.visibility = View.VISIBLE
                        } else {
                            tv_empty_notifications.visibility = View.VISIBLE
                            rv_notifications.visibility = View.GONE
                        }
                    } else if (resp.responseStatus == "0") {
                        tv_empty_notifications.visibility = View.VISIBLE
                        rv_notifications.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateNotificationStatus(jsonObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.readNotificationsApi(jsonObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        Log.d("status", "updated")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        if (isNetworkAvailable) {
            val jObj = JSONObject()
            jObj.put("userId", userId)
            val jParser = JsonParser()
            val jsonObject = jParser.parse(jObj.toString()) as JsonObject
            getNotifications(jsonObject)
        } else {
            Toast.makeText(
                this@NotificationsListActivity, getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }
    }

}
