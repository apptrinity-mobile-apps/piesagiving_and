package com.piesagiving.com.Activities

import android.content.Intent
import android.graphics.drawable.Animatable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.piesagiving.com.R

class AccountVerificationSuccess : AppCompatActivity() {
    lateinit var tv_home:TextView
    lateinit var tv_message_id:TextView
    lateinit var img_verify_id:ImageView

    var msg=""
    var ac_no=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_verification_success)



        msg=intent.getStringExtra("message")!!
        ac_no=intent.getStringExtra("account_no")!!

        tv_home=findViewById(R.id.tv_home)
        tv_message_id=findViewById(R.id.tv_message_id)
        img_verify_id=findViewById(R.id.img_verify_id)

        (img_verify_id.getDrawable() as Animatable).start()


        tv_message_id.text="*****"+ac_no+"\n"+msg

        tv_home.setOnClickListener {
            val intent = Intent(this@AccountVerificationSuccess, DashBoardActivity::class.java)
            intent.putExtra("to_fragment","")
            intent.putExtra("cat_id","")
            intent.putExtra("show_lock","no")
            startActivity(intent)
            finish()
        }
    }
}
