package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class ContactUsActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var tv_submit: TextView
    lateinit var et_name: EditText
    lateinit var et_email: EditText
    lateinit var et_subject: EditText
    lateinit var et_comments: EditText

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var userEmail = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_contactus)
        initialize()

        // delete after test
        et_email.setText(userEmail)
        // login to app
        tv_submit.setOnClickListener {
            if (et_name.text.toString().isEmpty()) {
                Toast.makeText(this, "Name cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_email.text.toString().isEmpty()) {
                Toast.makeText(this, "Email cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } else if (!isValidEmail(et_email.text.toString())) {
                Toast.makeText(this, "Email is not valid!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_subject.text.toString().isEmpty()) {
                Toast.makeText(this, "Subject cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_comments.text.toString().isEmpty()) {
                Toast.makeText(this, "Comments cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } else {
                if (isNetworkAvailable) {
                    ContactUsApi()
                } else {
                    Toast.makeText(
                        this, resources.getString(R.string.no_network_available), Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        userEmail = user_details[SessionManager.EMAIL_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tv_submit = findViewById(R.id.tv_submit)
        et_name = findViewById(R.id.et_name)
        et_email = findViewById(R.id.et_email)
        et_subject = findViewById(R.id.et_subject)
        et_comments = findViewById(R.id.et_comments)
        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun ContactUsApi() {
        //  loading_dialog.show()
        val objspl = JSONObject()
        objspl.put("name", et_name.text.toString())
        objspl.put("email", et_email.text.toString())
        objspl.put("subject", et_subject.text.toString())
        objspl.put("comment", et_comments.text.toString())
        objspl.put("userId", userId)
        val jParserspl = JsonParser()
        val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
        Log.e("CONTACTUS", jObjectspl.toString())

        val apiInterface = ApiInterface.create()
        val call = apiInterface.ContactUsApi(jObjectspl)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                        val intent = Intent(this@ContactUsActivity, DashBoardActivity::class.java)
                        intent.putExtra("to_fragment", "")
                        intent.putExtra("cat_id", "")
                        intent.putExtra("show_lock", "no")
                        startActivity(intent)
                        finish()
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
