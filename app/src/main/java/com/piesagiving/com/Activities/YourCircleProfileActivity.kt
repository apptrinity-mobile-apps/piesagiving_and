package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.*
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.SnapHelperOneByOne
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class YourCircleProfileActivity : AppCompatActivity() {

    lateinit var iv_share: ImageView
    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    lateinit var tv_follow: TextView
    lateinit var tv_name: TextView
    lateinit var tv_location: TextView
    lateinit var iv_profile_pic: ImageView
    lateinit var rv_spl_causes: RecyclerView
    lateinit var rv_churches: RecyclerView
    lateinit var rv_charities: RecyclerView
    lateinit var lm_projects: LinearLayoutManager
    lateinit var lm_charity: LinearLayoutManager
    lateinit var lm_spl_causes: LinearLayoutManager
    lateinit var tv_follow_spl_causes: TextView
    lateinit var tv_follow_charities: TextView
    lateinit var tv_follow_churches: TextView
    lateinit var tv_request: TextView
    lateinit var ll_profile_details: LinearLayout
    lateinit var ll_profile_private: LinearLayout
    lateinit var ll_follow_request: LinearLayout
    lateinit var tv_follow_name: TextView
    lateinit var tv_reject: TextView
    lateinit var tv_accept: TextView
    lateinit var loading_dialog: Dialog
    lateinit var tv_request_rejected: TextView
    lateinit var tv_empty_churches: TextView
    lateinit var tv_empty_charities: TextView
    lateinit var tv_empty_specialcauses: TextView
    lateinit var ll_organizations: LinearLayout
    lateinit var ll_people: LinearLayout
    lateinit var ll_follow_organizations: LinearLayout
    lateinit var followingOrganizationsAdapter: FollowingOrganizationsAdapter
    lateinit var followersAdapter: FollowersAdapter
    lateinit var followingOrganizationsList: ArrayList<SearchOrganizationDataResponse>
    lateinit var followersList: ArrayList<UsersDataResponse>
    lateinit var tv_empty_organizations: TextView
    lateinit var rv_organizations: RecyclerView
    lateinit var tv_empty_followers: TextView
    lateinit var rv_followers: RecyclerView

    lateinit var tabLayout: TabLayout
    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView
    lateinit var tabThree: TextView
    lateinit var ivTabOne: ImageView
    lateinit var ivTabTwo: ImageView
    lateinit var ivTabThree: ImageView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    var isFollowing = false
    var isRequested = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    lateinit var userNotifications: HashMap<String, String>
    var userId = ""
    var otherUserId = ""
    var firstName = ""
    var lastName = ""
    var name = ""
    var privacy = ""
    var profilePic = ""
    var from_screen = ""
    var location = ""
    var userName = ""
    var notificationId = ""
    val REQUEST_SENT = 0
    val REQUEST_ACCEPT = 1
    val REQUEST_REJECT = 2
    val REQUEST_CANCEL = 3

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_yourcircle_profile)

        initialize()
        otherUserId = intent.getStringExtra("id")!!
        firstName = intent.getStringExtra("firstName")!!
        lastName = intent.getStringExtra("lastName")!!
        privacy = intent.getStringExtra("privacy")!!
        profilePic = intent.getStringExtra("profilePic")!!
        from_screen = intent.getStringExtra("from_screen")!!
        userName = intent.getStringExtra("userName")!!.toString()
        notificationId = intent.getStringExtra("notificationId")!!.toString()
        name = "$firstName $lastName"

        tv_header.text = name
        tv_name.text = userName
        tv_location.visibility = View.GONE
        tv_follow_churches.text = firstName + " " + getString(R.string.follows_these_places)
        tv_follow_charities.text = firstName + " " + getString(R.string.follows_these_charities)
        tv_follow_spl_causes.text =
            firstName + " " + getString(R.string.follows_these_special_causes)
        tv_follow_name.text = firstName + " " + getString(R.string.wants_to_follow)
        tv_empty_churches.text = firstName + " " + getString(R.string.not_following_these_churches)
        tv_empty_charities.text =
            firstName + " " + getString(R.string.not_following_these_charities)
        tv_empty_specialcauses.text =
            firstName + " " + getString(R.string.not_following_these_special_causes)
        tv_empty_followers.text = getString(R.string.no_friends)

        if (from_screen == "search") {
            if (privacy == "private" || privacy == "onlyme") {
                ll_profile_private.visibility = View.VISIBLE
                tv_request_rejected.visibility = View.GONE
                ll_profile_details.visibility = View.GONE
                ll_follow_request.visibility = View.GONE
            } else {
                ll_profile_private.visibility = View.GONE
                tv_request_rejected.visibility = View.GONE
                ll_profile_details.visibility = View.VISIBLE
                ll_follow_request.visibility = View.GONE
                ll_follow_organizations.visibility = View.VISIBLE
                ll_organizations.visibility = View.GONE
                ll_people.visibility = View.GONE
            }
        } else if (from_screen == "request") {
            if (privacy == "private" || privacy == "onlyme") {
                ll_profile_private.visibility = View.GONE
                tv_request_rejected.visibility = View.GONE
                ll_profile_details.visibility = View.GONE
                ll_follow_request.visibility = View.VISIBLE
            } else {
                ll_profile_private.visibility = View.GONE
                tv_request_rejected.visibility = View.GONE
                ll_profile_details.visibility = View.GONE
                ll_follow_request.visibility = View.VISIBLE
                ll_follow_organizations.visibility = View.VISIBLE
                ll_organizations.visibility = View.GONE
                ll_people.visibility = View.GONE
            }
        }

        if (profilePic == "") {
            Picasso.with(this).load(R.drawable.ic_church_profile_edited)
                .error(R.drawable.ic_church_profile_edited)
                .into(iv_profile_pic)
        } else {
            Picasso.with(this).load(profilePic)
                .error(R.drawable.ic_church_profile_edited)
                .into(iv_profile_pic)
        }

        if (from_screen == "search") {
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                obj.put("otherUserId", otherUserId)
                val jParser = JsonParser()
                val final_object = jParser.parse(obj.toString()) as JsonObject
                getUserDetailViewApi(final_object)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        if (isNetworkAvailable) {
            val obj = JSONObject()
            obj.put("userId", otherUserId)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            getUserDonatedListApi(final_object)
            getFollowingUsersListApi(final_object)
        } else {
            Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                .show()
        }
        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }

        tv_follow.setOnClickListener {
            Haptics().vibrate(this)
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                obj.put("otherUserId", otherUserId)
                val jParser = JsonParser()
                val final_object = jParser.parse(obj.toString()) as JsonObject
                userFollowingApi(final_object)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        tv_request.setOnClickListener {
            tv_request_rejected.visibility = View.GONE
            if (isNetworkAvailable) {
                if (isRequested) {
                    // cancel request
                    val obj = JSONObject()
                    obj.put("userId", userId)
                    obj.put("otherUserId", otherUserId)
                    obj.put("acceptStatus", REQUEST_CANCEL)
                    val jParser = JsonParser()
                    val final_object = jParser.parse(obj.toString()) as JsonObject
                    sendFriendRequestApi(final_object)
                } else {
                    // send request
                    val obj = JSONObject()
                    obj.put("userId", userId)
                    obj.put("otherUserId", otherUserId)
                    obj.put("acceptStatus", REQUEST_SENT)
                    val jParser = JsonParser()
                    val final_object = jParser.parse(obj.toString()) as JsonObject
                    sendFriendRequestApi(final_object)
                }
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        tv_accept.setOnClickListener {
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                obj.put("requestId", otherUserId)
                obj.put("acceptStatus", REQUEST_ACCEPT)
                val jParser = JsonParser()
                val final_object = jParser.parse(obj.toString()) as JsonObject
                friendRequestApi(final_object)

                val jObj = JSONObject()
                jObj.put("notificationId", notificationId)
                val jsonParser = JsonParser()
                val fObject = jsonParser.parse(jObj.toString()) as JsonObject
                updateNotificationStatus(fObject)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        tv_reject.setOnClickListener {
            if (isNetworkAvailable) {
                val obj = JSONObject()
                obj.put("userId", userId)
                obj.put("requestId", otherUserId)
                obj.put("acceptStatus", REQUEST_REJECT)
                val jParser = JsonParser()
                val final_object = jParser.parse(obj.toString()) as JsonObject
                friendRequestApi(final_object)

                val jObj = JSONObject()
                jObj.put("notificationId", notificationId)
                val jsonParser = JsonParser()
                val fObject = jsonParser.parse(jObj.toString()) as JsonObject
                updateNotificationStatus(fObject)
            } else {
                Toast.makeText(this, getString(R.string.no_network_available), Toast.LENGTH_SHORT)
                    .show()
            }
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when {
                    tab!!.position == 0 -> {
                        ll_follow_organizations.visibility = View.VISIBLE
                        ll_organizations.visibility = View.GONE
                        ll_people.visibility = View.GONE
                        ivTabOne.setImageResource(R.drawable.ic_fav_selected_new)
                        ivTabTwo.setImageResource(R.drawable.ic_activity_unselected)
                        ivTabThree.setImageResource(R.drawable.ic_friends_unselected_new)
                    }
                    /*tab.position == 1 -> {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            tabOne.setTextColor(resources.getColor(R.color.black, theme))
                            tabTwo.setTextColor(
                                resources.getColor(R.color.login_button_background, theme)
                            )
                            tabThree.setTextColor(resources.getColor(R.color.black, theme))
                        } else {
                            tabOne.setTextColor(resources.getColor(R.color.black))
                            tabTwo.setTextColor(resources.getColor(R.color.login_button_background))
                            tabThree.setTextColor(resources.getColor(R.color.black))
                        }
                        ll_follow_organizations.visibility = View.GONE
                        ll_organizations.visibility = View.VISIBLE
                        ll_people.visibility = View.GONE
                        ivTabOne.setImageResource(R.drawable.ic_fav_unselected)
                        ivTabTwo.setImageResource(R.drawable.ic_activity_selected)
                        ivTabThree.setImageResource(R.drawable.ic_friends_unselected)
                    }*/
                    tab.position == 1 -> {
                        ll_follow_organizations.visibility = View.GONE
                        ll_organizations.visibility = View.GONE
                        ll_people.visibility = View.VISIBLE
                        ivTabOne.setImageResource(R.drawable.ic_fav_unselected_new)
                        ivTabTwo.setImageResource(R.drawable.ic_activity_unselected)
                        ivTabThree.setImageResource(R.drawable.ic_friends_selected_new)
                    }
                }
            }
        })


    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        userDetails = sessionManager.getUserDetails()
        userNotifications = sessionManager.getUserNotifications
        userId = userDetails[SessionManager.ID_KEY]!!
        loadingDialog()

        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_name = findViewById(R.id.tv_name)
        tv_location = findViewById(R.id.tv_location)
        tv_follow = findViewById(R.id.tv_follow)
        iv_profile_pic = findViewById(R.id.iv_profile_pic)
//        tv_user_circle = findViewById(R.id.tv_user_circle)
        rv_spl_causes = findViewById(R.id.rv_spl_causes)
        rv_churches = findViewById(R.id.rv_churches)
        rv_charities = findViewById(R.id.rv_charities)
        tv_follow_spl_causes = findViewById(R.id.tv_follow_spl_causes)
        tv_follow_charities = findViewById(R.id.tv_follow_charities)
        tv_follow_churches = findViewById(R.id.tv_follow_churches)
        ll_profile_details = findViewById(R.id.ll_profile_details)
        ll_profile_private = findViewById(R.id.ll_profile_private)
        tv_request = findViewById(R.id.tv_request)
        ll_follow_request = findViewById(R.id.ll_follow_request)
        tv_follow_name = findViewById(R.id.tv_follow_name)
        tv_reject = findViewById(R.id.tv_reject)
        tv_accept = findViewById(R.id.tv_accept)
        tv_request_rejected = findViewById(R.id.tv_request_rejected)
        tv_empty_churches = findViewById(R.id.tv_empty_churches)
        tv_empty_charities = findViewById(R.id.tv_empty_charities)
        tv_empty_specialcauses = findViewById(R.id.tv_empty_specialcauses)
        ll_organizations = findViewById(R.id.ll_organizations)
        ll_people = findViewById(R.id.ll_people)
        ll_follow_organizations = findViewById(R.id.ll_follow_organizations)
        rv_organizations = findViewById(R.id.rv_organizations)
        tv_empty_organizations = findViewById(R.id.tv_empty_organizations)
        rv_followers = findViewById(R.id.rv_followers)
        tv_empty_followers = findViewById(R.id.tv_empty_followers)

        tabLayout = findViewById(R.id.tabLayout)
        setupTabIcons()

        lm_projects =
            LinearLayoutManager(this@YourCircleProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_charity =
            LinearLayoutManager(this@YourCircleProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_spl_causes =
            LinearLayoutManager(this@YourCircleProfileActivity, RecyclerView.HORIZONTAL, false)
        val lm_org =
            LinearLayoutManager(this@YourCircleProfileActivity, RecyclerView.VERTICAL, false)
        val lm_followers =
            LinearLayoutManager(this@YourCircleProfileActivity, RecyclerView.VERTICAL, false)
        rv_churches.layoutManager = lm_projects
        rv_charities.layoutManager = lm_charity
        rv_spl_causes.layoutManager = lm_spl_causes
        rv_organizations.layoutManager = lm_org
        rv_followers.layoutManager = lm_followers

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


    private fun setFollowingEnabled() {
        tv_follow.text = getString(R.string.following)
        tv_follow.setTextColor(
            ContextCompat.getColor(
                this@YourCircleProfileActivity,
                R.color.white
            )
        )
        tv_follow.background =
            ContextCompat.getDrawable(
                this@YourCircleProfileActivity,
                R.drawable.button_background_blue
            )
        isFollowing = true
    }

    private fun setUnFollowingEnabled() {
        tv_follow.text = getString(R.string.follow)
        tv_follow.setTextColor(
            ContextCompat.getColor(
                this@YourCircleProfileActivity,
                R.color.login_button_background
            )
        )
        tv_follow.background =
            ContextCompat.getDrawable(
                this@YourCircleProfileActivity,
                R.drawable.button_background_white
            )
        isFollowing = false
    }

    private fun setupTabIcons() {
        val view = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        val view1 = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        val view2 = LayoutInflater.from(this).inflate(R.layout.tab_header, null)
        tabOne = view.findViewById(R.id.tv_tab)
        tabTwo = view1.findViewById(R.id.tv_tab)
        tabThree = view2.findViewById(R.id.tv_tab)
        ivTabOne = view.findViewById(R.id.iv_tab)
        ivTabTwo = view1.findViewById(R.id.iv_tab)
        ivTabThree = view2.findViewById(R.id.iv_tab)
        ivTabOne.visibility = View.VISIBLE
        ivTabTwo.visibility = View.VISIBLE
        ivTabThree.visibility = View.VISIBLE
        tabOne.visibility = View.GONE
        tabTwo.visibility = View.GONE
        tabThree.visibility = View.GONE
        ivTabOne.setImageResource(R.drawable.ic_fav_selected_new)
        ivTabTwo.setImageResource(R.drawable.ic_activity_unselected)
        ivTabThree.setImageResource(R.drawable.ic_friends_unselected_new)

        tabLayout.addTab(tabLayout.newTab().setCustomView(view), true)
        //  tabLayout.addTab(tabLayout.newTab().setCustomView(view1))
        tabLayout.addTab(tabLayout.newTab().setCustomView(view2))
    }

    private fun userFollowingApi(final_object: JsonObject) {
        // loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.userFollowingApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        if (isFollowing) {
                            setUnFollowingEnabled()
                        } else {
                            setFollowingEnabled()
                        }
                    } else {
                        Toast.makeText(
                            this@YourCircleProfileActivity, resp.result, Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getUserDetailViewApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getUserDetailViewApi(final_object)
        call.enqueue(object : Callback<UserDetailResponse> {
            override fun onFailure(call: Call<UserDetailResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<UserDetailResponse>,
                response: Response<UserDetailResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val profile = resp.profile
                        val acceptStatus = profile!!.acceptStatus.toString()
                        val privacy = profile.privacy
                        location = profile.location.toString()
                        userName = profile.userName.toString()
                        var state = ""
                        if (location.contains(",")) {
                            state = location.substring(location.lastIndexOf(",") + 1).trim()
                            tv_location.text = state
                        } else {
                            tv_location.text = location
                        }
                        tv_location.visibility = View.VISIBLE
                        tv_name.text = userName
                        tv_header.text = profile.name
                        if (profilePic == "") {
                            Picasso.with(this@YourCircleProfileActivity)
                                .load(R.drawable.ic_church_profile_edited)
                                .error(R.drawable.ic_church_profile_edited)
                                .into(iv_profile_pic)
                        } else {
                            Picasso.with(this@YourCircleProfileActivity)
                                .load(profile.profilePic.toString())
                                .error(R.drawable.ic_church_profile_edited)
                                .into(iv_profile_pic)
                        }

                        when (acceptStatus) {
                            "3" -> {
                                ll_profile_private.visibility = View.VISIBLE
                                tv_request_rejected.visibility = View.GONE
                                ll_profile_details.visibility = View.GONE
                            }
                            "0" -> {
                                if (privacy == "private" || privacy == "onlyme") {
//                                  pending
                                    ll_profile_private.visibility = View.VISIBLE
                                    tv_request_rejected.visibility = View.GONE
                                    ll_profile_details.visibility = View.GONE
                                    // tv_request_rejected.text = getString(R.string.request_pending)
                                    isRequested = true
                                    tv_request.text = getString(R.string.requested)
                                    tv_request.setTextColor(
                                        ContextCompat.getColor(
                                            this@YourCircleProfileActivity,
                                            R.color.white
                                        )
                                    )
                                    tv_request.background =
                                        ContextCompat.getDrawable(
                                            this@YourCircleProfileActivity,
                                            R.drawable.button_background_blue
                                        )
                                } else if (privacy == "public") {
//                                    public
                                    val followStatus = profile.follwStatus
                                    val churchList = profile.church_list
                                    val charityList = profile.charity_list
                                    val causesList = profile.special_cause_list
                                    if (followStatus == "1") {
                                        setFollowingEnabled()
                                    } else {
                                        setUnFollowingEnabled()
                                    }
                                    val adapter =
                                        FollowingChurchesAdapter(
                                            this@YourCircleProfileActivity,
                                            churchList!!
                                        )
                                    val adapter1 =
                                        FollowingChurchesAdapter(
                                            this@YourCircleProfileActivity,
                                            charityList!!
                                        )
                                    val adapter2 =
                                        FollowingChurchesAdapter(
                                            this@YourCircleProfileActivity,
                                            causesList!!
                                        )
                                    rv_churches.adapter = adapter
                                    rv_charities.adapter = adapter1
                                    rv_spl_causes.adapter = adapter2
                                    adapter.notifyDataSetChanged()
                                    adapter1.notifyDataSetChanged()
                                    adapter2.notifyDataSetChanged()

                                    val linear = SnapHelperOneByOne()
                                    linear.attachToRecyclerView(rv_churches)
                                    val linear_trending = SnapHelperOneByOne()
                                    linear_trending.attachToRecyclerView(rv_charities)
                                    val linear_circles = SnapHelperOneByOne()
                                    linear_circles.attachToRecyclerView(rv_spl_causes)

                                    if (churchList.size > 0) {
                                        rv_churches.visibility = View.VISIBLE
                                        tv_empty_churches.visibility = View.GONE
                                    } else {
                                        rv_churches.visibility = View.GONE
                                        tv_empty_churches.visibility = View.VISIBLE
                                    }

                                    if (charityList.size > 0) {
                                        rv_charities.visibility = View.VISIBLE
                                        tv_empty_charities.visibility = View.GONE
                                    } else {
                                        rv_charities.visibility = View.GONE
                                        tv_empty_charities.visibility = View.VISIBLE
                                    }

                                    if (causesList.size > 0) {
                                        rv_spl_causes.visibility = View.VISIBLE
                                        tv_empty_specialcauses.visibility = View.GONE
                                    } else {
                                        rv_spl_causes.visibility = View.GONE
                                        tv_empty_specialcauses.visibility = View.VISIBLE
                                    }

                                    ll_follow_organizations.visibility = View.VISIBLE
                                    ll_organizations.visibility = View.GONE
                                    ll_people.visibility = View.GONE
                                    ll_profile_private.visibility = View.GONE
                                    tv_request_rejected.visibility = View.GONE
                                    ll_profile_details.visibility = View.VISIBLE
                                }
                            }
                            "1" -> {
//                                private & accepted
                                val followStatus = profile.follwStatus
                                val churchList = profile.church_list
                                val charityList = profile.charity_list
                                val causesList = profile.special_cause_list
                                if (followStatus == "1") {
                                    setFollowingEnabled()
                                } else {
                                    setUnFollowingEnabled()
                                }
                                val adapter =
                                    FollowingChurchesAdapter(
                                        this@YourCircleProfileActivity,
                                        churchList!!
                                    )
                                val adapter1 =
                                    FollowingChurchesAdapter(
                                        this@YourCircleProfileActivity,
                                        charityList!!
                                    )
                                val adapter2 =
                                    FollowingChurchesAdapter(
                                        this@YourCircleProfileActivity,
                                        causesList!!
                                    )
                                rv_churches.adapter = adapter
                                rv_charities.adapter = adapter1
                                rv_spl_causes.adapter = adapter2
                                adapter.notifyDataSetChanged()
                                adapter1.notifyDataSetChanged()
                                adapter2.notifyDataSetChanged()

                                val linear = SnapHelperOneByOne()
                                linear.attachToRecyclerView(rv_churches)
                                val linear_trending = SnapHelperOneByOne()
                                linear_trending.attachToRecyclerView(rv_charities)
                                val linear_circles = SnapHelperOneByOne()
                                linear_circles.attachToRecyclerView(rv_spl_causes)

                                if (churchList.size > 0) {
                                    rv_churches.visibility = View.VISIBLE
                                    tv_empty_churches.visibility = View.GONE
                                } else {
                                    rv_churches.visibility = View.GONE
                                    tv_empty_churches.visibility = View.VISIBLE
                                }

                                if (charityList.size > 0) {
                                    rv_charities.visibility = View.VISIBLE
                                    tv_empty_charities.visibility = View.GONE
                                } else {
                                    rv_charities.visibility = View.GONE
                                    tv_empty_charities.visibility = View.VISIBLE
                                }

                                if (causesList.size > 0) {
                                    rv_spl_causes.visibility = View.VISIBLE
                                    tv_empty_specialcauses.visibility = View.GONE
                                } else {
                                    rv_spl_causes.visibility = View.GONE
                                    tv_empty_specialcauses.visibility = View.VISIBLE
                                }

                                ll_follow_organizations.visibility = View.VISIBLE
                                ll_organizations.visibility = View.GONE
                                ll_people.visibility = View.GONE
                                ll_profile_private.visibility = View.GONE
                                tv_request_rejected.visibility = View.GONE
                                ll_profile_details.visibility = View.VISIBLE

                            }
                            "2" -> {
//                                rejected
                                if (from_screen == "search") {
                                    tv_request_rejected.text =
                                        firstName + " " + getString(R.string.rejected_your_request)
                                } else if (from_screen == "request") {
                                    tv_request_rejected.text =
                                        getString(R.string.you_rejected_request) + " " + firstName
                                }
                                tv_request_rejected.visibility = View.VISIBLE
                                ll_profile_private.visibility = View.VISIBLE
                                ll_profile_details.visibility = View.GONE
                                tv_request.isEnabled = false
                            }
                            else -> {
                                ll_profile_private.visibility = View.GONE
                                tv_request_rejected.visibility = View.GONE
                                ll_profile_details.visibility = View.VISIBLE
                            }
                        }
                    } else if (resp.responseStatus == "0") {
                        Toast.makeText(
                            this@YourCircleProfileActivity, resp.result, Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun sendFriendRequestApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.sendFriendRequestApi(final_object)
        call.enqueue(object : Callback<UserDetailResponse> {
            override fun onFailure(call: Call<UserDetailResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<UserDetailResponse>,
                response: Response<UserDetailResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        if (isRequested) {
                            tv_request.text = getString(R.string.request)
                            tv_request.setTextColor(
                                ContextCompat.getColor(
                                    this@YourCircleProfileActivity,
                                    R.color.login_button_background
                                )
                            )
                            tv_request.background =
                                ContextCompat.getDrawable(
                                    this@YourCircleProfileActivity,
                                    R.drawable.button_background_white
                                )
                            isRequested = false
                        } else {
                            isRequested = true
                            tv_request.text = getString(R.string.requested)
                            tv_request.setTextColor(
                                ContextCompat.getColor(
                                    this@YourCircleProfileActivity,
                                    R.color.white
                                )
                            )
                            tv_request.background =
                                ContextCompat.getDrawable(
                                    this@YourCircleProfileActivity,
                                    R.drawable.button_background_blue
                                )
                        }
                    } else {
                        Toast.makeText(
                            this@YourCircleProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun friendRequestApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.acceptRejectApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        ll_follow_request.visibility = View.GONE
                        val obj = JSONObject()
                        obj.put("userId", userId)
                        obj.put("otherUserId", otherUserId)
                        val jParser = JsonParser()
                        val finalObject = jParser.parse(obj.toString()) as JsonObject
                        getUserDetailViewApi(finalObject)
                    } else {
                        Toast.makeText(
                            this@YourCircleProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getUserDonatedListApi(final_object: JsonObject) {
        loading_dialog.show()
        followingOrganizationsList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getUserDonatedListApi(final_object)
        call.enqueue(object : Callback<SearchOrganizationResponse> {
            override fun onFailure(call: Call<SearchOrganizationResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<SearchOrganizationResponse>,
                response: Response<SearchOrganizationResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.donated_organisations
                        followingOrganizationsList = data!!
                        followingOrganizationsAdapter = FollowingOrganizationsAdapter(
                            this@YourCircleProfileActivity,
                            followingOrganizationsList, userId
                        )
                        rv_organizations.adapter = followingOrganizationsAdapter
                        followingOrganizationsAdapter.notifyDataSetChanged()
                        if (followingOrganizationsList.size > 0) {
                            rv_organizations.visibility = View.VISIBLE
                            tv_empty_organizations.visibility = View.GONE
                        } else {
                            rv_organizations.visibility = View.GONE
                            tv_empty_organizations.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@YourCircleProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getFollowingUsersListApi(final_object: JsonObject) {
        loading_dialog.show()
        followersList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getFollowingUsersListApi(final_object)
        call.enqueue(object : Callback<AllUsersResponse> {
            override fun onFailure(call: Call<AllUsersResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@YourCircleProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<AllUsersResponse>,
                response: Response<AllUsersResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.follwers_list
                        followersList = data!!
                        followersAdapter =
                            FollowersAdapter(this@YourCircleProfileActivity, followersList, userId)
                        rv_followers.adapter = followersAdapter
                        followersAdapter.notifyDataSetChanged()
                        if (followersList.size > 0) {
                            rv_followers.visibility = View.VISIBLE
                            tv_empty_followers.visibility = View.GONE
                        } else {
                            rv_followers.visibility = View.GONE
                            tv_empty_followers.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(
                            this@YourCircleProfileActivity,
                            resp.result,
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun updateNotificationStatus(jsonObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.readNotificationsApi(jsonObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        Log.d("status", "updated")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
