package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.*
import android.telephony.PhoneNumberUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.AnnouncementsAdapter
import com.piesagiving.com.Adapters.FundRaiserAdapter
import com.piesagiving.com.Adapters.ProfileAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.SnapHelperOneByOne
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.Executor

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class ChurchProfileActivity : AppCompatActivity() {

    lateinit var img_profile_pic_id: ImageView
    lateinit var iv_more: ImageView
    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    lateinit var tv_follow_church: TextView
    lateinit var tv_church_name: TextView
    lateinit var tv_church_location: TextView
    lateinit var tv_donate: TextView
    lateinit var tv_church_bio: TextView
    lateinit var tv_church_address: TextView
    lateinit var tv_church_email: TextView
    lateinit var tv_church_phone: TextView
    lateinit var tv_church_website: TextView
    lateinit var tv_pastor_ceo: TextView
    lateinit var tv_pastor_ceo_name: TextView
    lateinit var tv_pastor_ceo_phone: TextView
    lateinit var tv_pastor_ceo_email: TextView
    lateinit var tv_treasurer_cfo: TextView
    lateinit var tv_treasurer_cfo_name: TextView
    lateinit var tv_treasurer_cfo_phone: TextView
    lateinit var tv_treasurer_cfo_email: TextView
    lateinit var tv_projects: TextView
    lateinit var tv_projects_empty: TextView
    lateinit var tv_spl_causes_empty: TextView
    lateinit var ll_email: LinearLayout
    lateinit var ll_spl_causes: LinearLayout
    lateinit var ll_projects: LinearLayout
    lateinit var ll_fundraisers: LinearLayout
    lateinit var rv_spl_causes: RecyclerView
    lateinit var rv_projects: RecyclerView
    lateinit var rv_fundraisers: RecyclerView
    lateinit var lm_projects: LinearLayoutManager
    lateinit var lm_spl_causes: LinearLayoutManager
    lateinit var lm_fundraisers: LinearLayoutManager
    lateinit var ll_org_phone_number: LinearLayout
    lateinit var ll_org_email: LinearLayout
    lateinit var iv_pastor: ImageView
    lateinit var iv_treasurer: ImageView
    lateinit var iv_announcements: ImageView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    var isFollowingChurch = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    var from_screen = ""
    var reco_organiz_id = ""
    var organisation_id = ""
    var project_id = ""
    var display_name = ""
    var categoryId = ""
    lateinit var loading_dialog: Dialog

    lateinit var churchProjectArray: ArrayList<ChurchProjectArrayResponse>
    lateinit var specialCausesArray: ArrayList<ChurchProjectArrayResponse>
    lateinit var fundRaisersArray: ArrayList<FundRaisersArrayResponse>

    lateinit var tv_report: TextView
    lateinit var tv_share: TextView
    lateinit var tv_org_followers: TextView
    lateinit var ll_report: LinearLayout
    lateinit var ll_share_report: LinearLayout
    lateinit var et_description: EditText
    lateinit var tv_submit: TextView

    var church_project_id = ""
    var spl_cause_project_id = ""

    var isclickable: Boolean = false
    lateinit var userNotifications: java.util.HashMap<String, String>
    private lateinit var annoucements_dialog: Dialog
    private lateinit var mAnnouncements: ArrayList<AnnouncementsResponse>
    private lateinit var rv_announcements: RecyclerView
    private lateinit var sw_notifications: SwitchMaterial
    private lateinit var tv_no_announcements: TextView

    //   biometric
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var userEmail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_church_profile)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }

        initialize()


        val data: Uri? = intent?.data
        if (data == null) {
            reco_organiz_id = intent.getStringExtra("reco_organizationId")!!
            from_screen = intent.getStringExtra("from_screen")!!
            display_name = intent.getStringExtra("display_name")!!
        } else {
            reco_organiz_id = data.toString().substringAfter("id=","")
            from_screen = "profile"
        }


        val obj = JSONObject()
        obj.put("userId", userId)
        obj.put("organizationId", reco_organiz_id)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("dash_bard_input", jObject.toString())

        ProfileResponse(jObject)
        Log.e("DisplayName", display_name)
        tv_header.text = display_name
        announcementsDialog()
        when (from_screen) {
            "profile" -> {
                tv_projects.text = getString(R.string.church_project)
                tv_church_bio.visibility = View.VISIBLE
                ll_email.visibility = View.GONE
            }
            "charities" -> {
                tv_projects.text = getString(R.string.charity_project)
                tv_church_bio.visibility = View.GONE
                ll_email.visibility = View.VISIBLE
            }
            "announcement" -> {
                annoucements_dialog.show()
            }
            else -> {
                tv_church_bio.visibility = View.VISIBLE
            }
        }

        sw_notifications.isChecked = true
        if (isNetworkAvailable) {
            val jObj = JSONObject()
            jObj.put("userId", userId)
            jObj.put("orgId", reco_organiz_id)
            val jsonParser = JsonParser()
            val finalObj = jsonParser.parse(jObj.toString()) as JsonObject
            getAnnouncementsApi(finalObj)
        } else {
            Toast.makeText(
                this@ChurchProfileActivity,
                getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }

        // share this profile
        iv_more.setOnClickListener {
            showChangeLangDialog()
        }

        // follow church
        tv_follow_church.setOnClickListener {

            if (isFollowingChurch) {
                tv_follow_church.setTextColor(
                    ContextCompat.getColor(this, R.color.login_button_background)
                )
                tv_follow_church.background =
                    ContextCompat.getDrawable(this, R.drawable.button_background_white)
                tv_follow_church.text = getString(R.string.follow)
                isFollowingChurch = false
                iv_announcements.visibility = View.GONE
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("organizationId", reco_organiz_id)
                followobj.put("status", 0)
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                Log.e("dash_bard_input", followjObject.toString())
                FollowingApiCall(followjObject)
            } else {
                tv_follow_church.setTextColor(ContextCompat.getColor(this, R.color.white))
                tv_follow_church.background =
                    ContextCompat.getDrawable(this, R.drawable.button_background_blue)
                tv_follow_church.text = getString(R.string.following)
                isFollowingChurch = true
                iv_announcements.visibility = View.VISIBLE
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("organizationId", reco_organiz_id)
                followobj.put("status", 1)
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                Log.e("dash_bard_input", followjObject.toString())
                FollowingApiCall(followjObject)
            }
        }

        // navigate to donation page
        tv_donate.setOnClickListener {
            Haptics().vibrate(this)
            showBiometricPrompt()
            /*Log.e("isclickable", isclickable.toString())

            val intent = Intent(this, DonateFundsActivity::class.java)
            intent.putExtra("from_screen", "churchprofile")
            intent.putExtra("organiz_id", organisation_id)
            intent.putExtra("display_name", display_name)
            intent.putExtra("project_name", "")
            startActivity(intent)*/

        }

        tv_org_followers.setOnClickListener {
            val intent = Intent(this, OrganisationFollowersActivity::class.java)
            intent.putExtra("reco_organizationId", reco_organiz_id)
            intent.putExtra("display_name", display_name)
            startActivity(intent)
        }

        rv_projects.addOnItemTouchListener(
            RecyclerItemClickListener(this@ChurchProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(
                            this@ChurchProfileActivity,
                            ProjectDetailedViewActivity::class.java
                        )
                        intent.putExtra("from_screen", "project")
                        intent.putExtra("organiz_id", organisation_id)
                        intent.putExtra("project_id", church_project_id)
                        intent.putExtra("project_name", churchProjectArray[position].name)
                        intent.putExtra("display_name", display_name)
                        startActivity(intent)
                    }
                })
        )

        rv_spl_causes.addOnItemTouchListener(
            RecyclerItemClickListener(this@ChurchProfileActivity,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(
                            this@ChurchProfileActivity,
                            ProjectDetailedViewActivity::class.java
                        )
                        intent.putExtra("from_screen", "special_cause")
                        intent.putExtra("organiz_id", organisation_id)
                        intent.putExtra("project_id", spl_cause_project_id)
                        intent.putExtra("project_name", specialCausesArray[position].name)
                        intent.putExtra("display_name", display_name)

                        // intent.putExtra("specialCauseId", "5ed9c441826ad1927a11c6cf")
                        startActivity(intent)
                    }
                })
        )
        rv_fundraisers.addOnItemTouchListener(
            RecyclerItemClickListener(this@ChurchProfileActivity, object : RecyclerItemClickListener.OnItemClickListener {
                override fun onItemClick(view: View, position: Int) {
                    Log.d("id---", "${fundRaisersArray[position].id}")
                    val intent = Intent(this@ChurchProfileActivity, FundRaisersActivity::class.java)
                    intent.putExtra("fundraiser_id", fundRaisersArray[position].id)
                    startActivity(intent)
                }
            })
        )

        tv_church_name.setOnClickListener {
            val intent = Intent(this@ChurchProfileActivity, DashBoardActivity::class.java)
            intent.putExtra("to_fragment", "search")
            intent.putExtra("cat_id", categoryId)
            intent.putExtra("show_lock", "no")
            startActivity(intent)
        }

        tv_church_phone.setOnClickListener {
            if (tv_church_phone.text.toString().isNotEmpty()) {
                val callIntent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", tv_church_phone.text.toString().trim(), null)
                )
                startActivity(callIntent)
            }
        }

        tv_church_website.setOnClickListener {
            var url = tv_church_website.text.toString()
            if (!url.startsWith("http://") && !url.startsWith("https://"))
                url = "http://$url"
            try {
                val myIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(Intent.createChooser(myIntent, "Open with"))
            } catch (e: ActivityNotFoundException) {
                e.printStackTrace()
            }
        }

        tv_treasurer_cfo_phone.setOnClickListener {
            if (tv_treasurer_cfo_phone.text.toString().isNotEmpty()) {
                val callIntent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", tv_treasurer_cfo_phone.text.toString().trim(), null)
                )
                startActivity(callIntent)
            }
        }

        tv_pastor_ceo_phone.setOnClickListener {
            if (tv_pastor_ceo_phone.text.toString().isNotEmpty()) {
                val callIntent = Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", tv_pastor_ceo_phone.text.toString().trim(), null)
                )
                startActivity(callIntent)
            }
        }

        iv_announcements.setOnClickListener {
            annoucements_dialog.show()
        }
    }

    private fun initialize() {
        loadingDialog()
        passCodeDialog()
        cd = ConnectionDetector(this)
        isNetworkAvailable = cd.isConnectingToInternet
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userNotifications = sessionManager.getUserNotifications
        userId = user_details[SessionManager.ID_KEY]!!
        biometrics = user_details[SessionManager.FACEID_KEY]!!
        passCode = user_details[SessionManager.PASSCODE_KEY]!!
        biometricManager = BiometricManager.from(this@ChurchProfileActivity)
        executor = ContextCompat.getMainExecutor(this@ChurchProfileActivity)
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }
        img_profile_pic_id = findViewById(R.id.img_profile_pic_id)
        iv_more = findViewById(R.id.iv_more)
        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_church_name = findViewById(R.id.tv_church_name)
        tv_church_location = findViewById(R.id.tv_church_location)
        tv_follow_church = findViewById(R.id.tv_follow_church)
        tv_donate = findViewById(R.id.tv_donate)
        tv_org_followers = findViewById(R.id.tv_org_followers)
        tv_church_bio = findViewById(R.id.tv_church_bio)
        tv_church_address = findViewById(R.id.tv_church_address)
        ll_email = findViewById(R.id.ll_email)
        tv_church_email = findViewById(R.id.tv_church_email)
        tv_church_phone = findViewById(R.id.tv_church_phone)
        tv_church_website = findViewById(R.id.tv_church_website)
        tv_pastor_ceo = findViewById(R.id.tv_pastor_ceo)
        tv_pastor_ceo_name = findViewById(R.id.tv_pastor_ceo_name)
        tv_pastor_ceo_phone = findViewById(R.id.tv_pastor_ceo_phone)
        tv_pastor_ceo_email = findViewById(R.id.tv_pastor_ceo_email)
        tv_treasurer_cfo = findViewById(R.id.tv_treasurer_cfo)
        tv_treasurer_cfo_name = findViewById(R.id.tv_treasurer_cfo_name)
        tv_treasurer_cfo_phone = findViewById(R.id.tv_treasurer_cfo_phone)
        tv_treasurer_cfo_email = findViewById(R.id.tv_treasurer_cfo_email)
        tv_projects = findViewById(R.id.tv_projects)
        ll_spl_causes = findViewById(R.id.ll_spl_causes)
        ll_projects = findViewById(R.id.ll_projects)
        tv_projects_empty = findViewById(R.id.tv_projects_empty)
        tv_spl_causes_empty = findViewById(R.id.tv_spl_causes_empty)
        rv_spl_causes = findViewById(R.id.rv_spl_causes)
        rv_projects = findViewById(R.id.rv_projects)
        ll_org_phone_number = findViewById(R.id.ll_org_phone_number)
        ll_org_email = findViewById(R.id.ll_org_email)
        iv_pastor = findViewById(R.id.iv_pastor)
        iv_treasurer = findViewById(R.id.iv_treasurer)
        iv_announcements = findViewById(R.id.iv_announcements)
        ll_fundraisers = findViewById(R.id.ll_fundraisers)
        rv_fundraisers = findViewById(R.id.rv_fundraisers)

        lm_projects =
            LinearLayoutManager(this@ChurchProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_spl_causes =
            LinearLayoutManager(this@ChurchProfileActivity, RecyclerView.HORIZONTAL, false)
        lm_fundraisers =
            LinearLayoutManager(this@ChurchProfileActivity, RecyclerView.HORIZONTAL, false)
        rv_projects.layoutManager = lm_projects
        rv_spl_causes.layoutManager = lm_spl_causes

        mAnnouncements = ArrayList()
    }

    private fun ProfileResponse(json_obj: JsonObject) {

        loading_dialog.show()
        churchProjectArray = ArrayList()
        churchProjectArray.clear()
        specialCausesArray = ArrayList()
        specialCausesArray.clear()
        fundRaisersArray = ArrayList()
        fundRaisersArray.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.OrganizationDetailsApi(json_obj)
        call.enqueue(object : Callback<OrganizationDetailsPojo> {
            override fun onFailure(call: Call<OrganizationDetailsPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(call: Call<OrganizationDetailsPojo>, response: Response<OrganizationDetailsPojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val jsondata = response.body()!!

                    if (jsondata.organisation_follwers_count.toString()
                            .equals("") || jsondata.organisation_follwers_count.toString()
                            .equals(0) || jsondata.organisation_follwers_count.toString()
                            .equals(null)
                    ) {
                        tv_org_followers.setText("0" + " followers")
                    } else {
                        tv_org_followers.setText(jsondata.organisation_follwers_count.toString() + " followers")
                    }


                    if (jsondata.responseStatus!! == 1) {
                        val organiz_details = jsondata.organization!![0]
                        display_name = organiz_details.displayName!!
                        tv_header.text = display_name


                        if (organiz_details.organizationType.equals("charity")) {
                            // tv_header.text="Charity Profile"

                            organisation_id = organiz_details.id.toString()
                            tv_church_name.text = organiz_details.categoryName.toString()
                            tv_church_location.text = organiz_details.streetAddress.toString()
                            tv_church_address.text = organiz_details.streetAddress.toString()
                            tv_church_bio.text = organiz_details.bio.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_church_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.phoneNumber, "US"
                                ))
                            } else {
                                tv_church_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.phoneNumber)); //Deprecated method
                            }
                            tv_church_website.text = organiz_details.websiteLink.toString()
                            tv_projects.text = getString(R.string.charity_project)
                            tv_pastor_ceo.text = organiz_details.ceoType.toString()
                            tv_pastor_ceo_name.text = organiz_details.ceoName.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_pastor_ceo_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.ceoMobileNumber, "US"
                                ))
                            } else {
                                tv_pastor_ceo_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.ceoMobileNumber)); //Deprecated method
                            }
                            tv_pastor_ceo_email.text = organiz_details.ceoEmail.toString()
                            tv_treasurer_cfo.text = organiz_details.cfoType.toString()
                            tv_treasurer_cfo_name.text = organiz_details.cfoName.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_treasurer_cfo_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.cfoMobileNumber, "US"
                                ))
                            } else {
                                tv_treasurer_cfo_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.cfoMobileNumber)); //Deprecated method
                            }
                            tv_treasurer_cfo_email.text = organiz_details.cfoEmail.toString()
                            Picasso.with(this@ChurchProfileActivity)
                                .load(organiz_details.profilePic.toString())
                                .error(R.drawable.ic_default_officials)
                                .into(img_profile_pic_id)
                            if (organiz_details.ceoImage.toString() == "") {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(R.drawable.ic_default_officials)
                                    .into(iv_pastor)
                            } else {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(organiz_details.ceoImage.toString())
                                    .error(R.drawable.ic_default_officials)
                                    .into(iv_pastor)
                            }
                            if (organiz_details.cfoImage.toString() == "") {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(R.drawable.ic_default_officials)
                                    .into(iv_treasurer)
                            } else {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(organiz_details.cfoImage.toString())
                                    .error(R.drawable.ic_default_officials)
                                    .into(iv_treasurer)
                            }
                            if (organiz_details.ceoMobileNumber.toString() == "") {
                                tv_pastor_ceo_phone.visibility = View.GONE
                            }
                            if (organiz_details.ceoEmail.toString() == "") {
                                tv_pastor_ceo_email.visibility = View.GONE
                            }
                            if (organiz_details.cfoMobileNumber.toString() == "") {
                                tv_treasurer_cfo_phone.visibility = View.GONE
                            }
                            if (organiz_details.cfoEmail.toString() == "") {
                                tv_treasurer_cfo_email.visibility = View.GONE
                            }
                            if (organiz_details.phoneNumber.toString() == "") {
                                ll_org_phone_number.visibility = View.GONE
                            }
                            if (organiz_details.websiteLink.toString() == "") {
                                ll_org_email.visibility = View.GONE
                            }
                            if (organiz_details.bio.toString() == "") {
                                tv_church_bio.visibility = View.GONE
                            }
                            Log.e("pic_profile", organiz_details.profilePic.toString())
                        } else {
                            organisation_id = organiz_details.id.toString()
                            tv_church_name.text = organiz_details.categoryName.toString()
                            tv_church_bio.text = organiz_details.bio.toString()
                            tv_church_location.text = organiz_details.streetAddress.toString()
                            tv_church_address.text = organiz_details.streetAddress.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_church_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.phoneNumber, "US"
                                ))
                            } else {
                                tv_church_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.phoneNumber)); //Deprecated method
                            }
                            tv_church_website.text = organiz_details.websiteLink.toString()
                            tv_pastor_ceo_name.text = organiz_details.pastorName.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_pastor_ceo_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.pastorMobileNumber, "US"
                                ))
                            } else {
                                tv_pastor_ceo_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.pastorMobileNumber)); //Deprecated method
                            }
                            tv_pastor_ceo_email.text = organiz_details.pastorEmail.toString()
//                            tv_pastor_ceo.text = organiz_details.pastorType.toString()
                            tv_pastor_ceo.text = getString(R.string.pastor)
                            tv_treasurer_cfo_name.text = organiz_details.treasurerName.toString()
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                tv_treasurer_cfo_phone.text = (PhoneNumberUtils.formatNumber(
                                    organiz_details.treasurerMobileNumber, "US"
                                ))
                            } else {
                                tv_treasurer_cfo_phone.text =
                                    (PhoneNumberUtils.formatNumber(organiz_details.treasurerMobileNumber)); //Deprecated method
                            }
                            tv_treasurer_cfo.text = organiz_details.treasurerType.toString()
                            tv_treasurer_cfo_email.text = organiz_details.treasurerEmail.toString()
                            Picasso.with(this@ChurchProfileActivity)
                                .load(organiz_details.profilePic.toString())
                                .error(R.drawable.ic_default_officials)
                                .into(img_profile_pic_id)
                            if (organiz_details.pastorImage.toString() == "") {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(R.drawable.ic_default_officials)
                                    .into(iv_pastor)
                            } else {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(organiz_details.pastorImage.toString())
                                    .error(R.drawable.ic_default_officials)
                                    .into(iv_pastor)
                            }
                            if (organiz_details.treasurerImage.toString() == "") {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(R.drawable.ic_default_officials)
                                    .into(iv_treasurer)
                            } else {
                                Picasso.with(this@ChurchProfileActivity)
                                    .load(organiz_details.treasurerImage.toString())
                                    .error(R.drawable.ic_default_officials)
                                    .into(iv_treasurer)
                            }
                            if (organiz_details.pastorMobileNumber.toString() == "") {
                                tv_pastor_ceo_phone.visibility = View.GONE
                            }
                            if (organiz_details.pastorEmail.toString() == "") {
                                tv_pastor_ceo_email.visibility = View.GONE
                            }
                            if (organiz_details.treasurerMobileNumber.toString() == "") {
                                tv_treasurer_cfo_phone.visibility = View.GONE
                            }
                            if (organiz_details.treasurerEmail.toString() == "") {
                                tv_treasurer_cfo_email.visibility = View.GONE
                            }
                            if (organiz_details.phoneNumber.toString() == "") {
                                ll_org_phone_number.visibility = View.GONE
                            }
                            if (organiz_details.websiteLink.toString() == "") {
                                ll_org_email.visibility = View.GONE
                            }
                            if (organiz_details.bio.toString() == "") {
                                tv_church_bio.visibility = View.GONE
                            }
                            Log.e("pic_profile", organiz_details.profilePic.toString())
                        }
                        categoryId = organiz_details.categoryId.toString()
                        if (organiz_details.followStatus.toString().equals("0")) {
                            tv_follow_church.setTextColor(
                                ContextCompat.getColor(
                                    this@ChurchProfileActivity,
                                    R.color.login_button_background
                                )
                            )
                            tv_follow_church.background =
                                ContextCompat.getDrawable(
                                    this@ChurchProfileActivity,
                                    R.drawable.button_background_white
                                )
                            tv_follow_church.text = getString(R.string.follow)
                            isFollowingChurch = false
                            iv_announcements.visibility = View.GONE
                        } else {
                            tv_follow_church.setTextColor(
                                ContextCompat.getColor(
                                    this@ChurchProfileActivity,
                                    R.color.white
                                )
                            )
                            tv_follow_church.background =
                                ContextCompat.getDrawable(
                                    this@ChurchProfileActivity,
                                    R.drawable.button_background_blue
                                )
                            tv_follow_church.text = getString(R.string.following)
                            isFollowingChurch = true
                            iv_announcements.visibility = View.VISIBLE
                        }

                        if (jsondata.churchProject!!.size.equals(null) || jsondata.churchProject!!.size.equals(
                                ""
                            ) || jsondata.churchProject!!.size.equals(0) || jsondata.churchProject!!.size.equals(
                                "0"
                            )
                        ) {
                            isclickable = false
                        } else {
                            isclickable = true
                        }

                        /*if (jsondata.specialCauses!!.size.equals(null) || jsondata.specialCauses!!.size.equals(
                                ""
                            ) || jsondata.specialCauses!!.size.equals(0) || jsondata.specialCauses!!.size.equals(
                                "0"
                            )
                        ) {
                            isclickable = false
                        } else {
                            isclickable = true
                        }*/

                        if (!jsondata.churchProject!!.size.equals(null)) {
                            for (i in 0 until jsondata.churchProject.size) {
                                churchProjectArray.add(response.body()!!.churchProject!!.get(i))
                                church_project_id = churchProjectArray[i].id.toString()
                            }

                        }
                        /*if (!jsondata.specialCauses!!.size.equals(null)) {
                            for (i in 0 until jsondata.specialCauses.size) {
                                specialCausesArray.add(response.body()!!.specialCauses!!.get(i))
                                spl_cause_project_id = specialCausesArray[i].id.toString()

                            }
                        }*/
                        if (!jsondata.fundraisers_list!!.size.equals(null)) {
                            if (jsondata.fundraisers_list.isNotEmpty()) {
                                fundRaisersArray.addAll(jsondata.fundraisers_list)
                            }
                        }
                        rv_projects.setHasFixedSize(true)
                        rv_spl_causes.setHasFixedSize(true)
                        rv_fundraisers.setHasFixedSize(true)

                        val adapter = ProfileAdapter(this@ChurchProfileActivity, churchProjectArray)

                        val adapter1 = ProfileAdapter(this@ChurchProfileActivity, specialCausesArray)
                        val fundsRaisersAdapter = FundRaiserAdapter(this@ChurchProfileActivity, fundRaisersArray)

                        rv_projects.adapter = adapter
                        rv_spl_causes.adapter = adapter1
                        rv_fundraisers.adapter = fundsRaisersAdapter
                        adapter.notifyDataSetChanged()
                        adapter1.notifyDataSetChanged()

                        val linear = SnapHelperOneByOne()
                        linear.attachToRecyclerView(rv_projects)
                        linear.attachToRecyclerView(rv_spl_causes)
                        linear.attachToRecyclerView(rv_fundraisers)
                        if (churchProjectArray.isEmpty()) {
                            ll_projects.visibility = View.GONE
                        }
                        if (specialCausesArray.isEmpty()) {
                            ll_spl_causes.visibility = View.GONE
                        }
                        if (fundRaisersArray.isEmpty()) {
                            ll_fundraisers.visibility = View.GONE
                        }

                    } else {
                        /*Toast.makeText(
                            this@ChurchProfileActivity,
                            "Data Fetching Failed",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun FollowingApiCall(json_obj: JsonObject) {
        val apiInterface = ApiInterface.create()
        val call = apiInterface.FollowingApi(json_obj)
        call.enqueue(object : Callback<FollowPojo> {
            override fun onFailure(call: Call<FollowPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(call: Call<FollowPojo>, response: Response<FollowPojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun ReportApiCall(json_obj: JsonObject) {

        loading_dialog.show()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.reportApi(json_obj)
        call.enqueue(object : Callback<FollowPojo> {
            override fun onFailure(call: Call<FollowPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(call: Call<FollowPojo>, response: Response<FollowPojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response.body()!!.responseStatus!! == 1) {
                        Toast.makeText(this@ChurchProfileActivity, response.body()!!.result.toString(), Toast.LENGTH_SHORT).show()
                        ll_report.visibility = View.GONE
                        ll_share_report.visibility = View.VISIBLE
                        et_description.text = null
                    } else {
                        /*Toast.makeText(
                            this@ChurchProfileActivity,
                            "Data Fetching Failed",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getAnnouncementsApi(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.getAnnouncementsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {}

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val list = resp.announcements
                        mAnnouncements = list!!
                        if (mAnnouncements.size > 0) {
                            rv_announcements.visibility = View.VISIBLE
                            tv_no_announcements.visibility = View.GONE
                        } else {
                            rv_announcements.visibility = View.GONE
                            tv_no_announcements.visibility = View.VISIBLE
                        }
                        val adapter =
                            AnnouncementsAdapter(this@ChurchProfileActivity, mAnnouncements)
                        rv_announcements.adapter = adapter
                        adapter.notifyDataSetChanged()
                        sw_notifications.isChecked = true
                    } else if (resp.responseStatus == "2") {
                        sw_notifications.isChecked = false
                        rv_announcements.visibility = View.GONE
                        tv_no_announcements.visibility = View.VISIBLE
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun blockAnnouncements(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.blockAnnouncementsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {}

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {

                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun announcementsDialog() {
        annoucements_dialog = Dialog(this)
        annoucements_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.announcement_dialog, null)
        annoucements_dialog.setContentView(view)
        annoucements_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        annoucements_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        annoucements_dialog.setCanceledOnTouchOutside(true)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val displayWidth: Int = displayMetrics.widthPixels
        val displayHeight: Int = displayMetrics.heightPixels
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(annoucements_dialog.window!!.attributes)
        val dialogWindowWidth = (displayWidth * 0.8f).toInt()
        val dialogWindowHeight = (displayHeight * 0.75f).toInt()
        layoutParams.width = dialogWindowWidth
        layoutParams.height = dialogWindowHeight
        annoucements_dialog.window!!.attributes = layoutParams
        val cv_toast: CardView = view.findViewById(R.id.cv_toast)
        val tv_toast_header: TextView = view.findViewById(R.id.tv_toast_header)
        sw_notifications = view.findViewById(R.id.sw_notifications)
        tv_no_announcements = view.findViewById(R.id.tv_no_announcements)
        rv_announcements = view.findViewById(R.id.rv_announcements)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_announcements.layoutManager = layoutManager

        cv_toast.visibility = View.INVISIBLE
        sw_notifications.setOnClickListener {
            if (isNetworkAvailable) {
                val jObj = JSONObject()
                jObj.put("userId", userId)
                jObj.put("orgId", reco_organiz_id)
                val jsonParser = JsonParser()
                val finalObj = jsonParser.parse(jObj.toString()) as JsonObject
                blockAnnouncements(finalObj)
            } else {
                Toast.makeText(
                    this@ChurchProfileActivity,
                    getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        sw_notifications.setOnCheckedChangeListener { buttonView, isChecked ->
            cv_toast.visibility = View.VISIBLE
            if (isChecked) {
                tv_toast_header.text =
                    getString(R.string.you_will_receive_announcement_notifications)
            } else {
                tv_toast_header.text =
                    getString(R.string.you_won_t_receive_announcement_notifications)
            }
            Handler().postDelayed({
                cv_toast.visibility = View.INVISIBLE
            }, 2000)
        }

    }

    fun showChangeLangDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        dialogBuilder.setView(dialogView)
        val b = dialogBuilder.create()

        tv_report = dialogView.findViewById(R.id.tv_report) as TextView
        tv_share = dialogView.findViewById(R.id.tv_share) as TextView
        ll_report = dialogView.findViewById(R.id.ll_report) as LinearLayout
        ll_share_report = dialogView.findViewById(R.id.ll_share_report) as LinearLayout
        et_description = dialogView.findViewById(R.id.et_description) as EditText
        tv_submit = dialogView.findViewById(R.id.tv_submit) as TextView

        tv_report.setOnClickListener(View.OnClickListener {
            ll_report.visibility = View.VISIBLE
            ll_share_report.visibility = View.GONE

        })

        tv_submit.setOnClickListener {
            if (et_description.text.toString().equals("")) {
                Toast.makeText(
                    this@ChurchProfileActivity,
                    "Please Enter Description",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("description", et_description.text.toString())
                followobj.put("status", 0)
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                Log.e("dash_bard_input", followjObject.toString())
                ReportApiCall(followjObject)
            }
        }
        tv_share.setOnClickListener {
            share()
        }
        b.show()

    }

    private fun share() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            val shareMessage =
                "${ApiInterface.BASE}home/org_profile/%7Bid%7D?id=$reco_organiz_id"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "Share via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    val intent = Intent(this, DonateFundsActivity::class.java)
                    intent.putExtra("from_screen", "churchprofile")
                    intent.putExtra("organiz_id", organisation_id)
                    intent.putExtra("display_name", display_name)
                    intent.putExtra("project_name", "")
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    val intent = Intent(this, DonateFundsActivity::class.java)
                    intent.putExtra("from_screen", "churchprofile")
                    intent.putExtra("organiz_id", organisation_id)
                    intent.putExtra("display_name", display_name)
                    intent.putExtra("project_name", "")
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            val intent = Intent(this, DonateFundsActivity::class.java)
            intent.putExtra("from_screen", "churchprofile")
            intent.putExtra("organiz_id", organisation_id)
            intent.putExtra("display_name", display_name)
            intent.putExtra("project_name", "")
            startActivity(intent)
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)
            .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@ChurchProfileActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    val intent = Intent(this@ChurchProfileActivity, DonateFundsActivity::class.java)
                    intent.putExtra("from_screen", "churchprofile")
                    intent.putExtra("organiz_id", organisation_id)
                    intent.putExtra("display_name", display_name)
                    intent.putExtra("project_name", "")
                    startActivity(intent)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())

                    /*if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {

                    } else if (errorCode == BiometricConstants.ERROR_CANCELED) {
                        Log.e("Biometrics", "biometric operation cancelled by user.")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT_PERMANENT) {
                        Log.e("Biometrics", "ERROR_LOCKOUT occurred too many times")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT) {
                        Log.e("Biometrics", "the API is locked out due to too many attempts")
                    } else if (errorCode == BiometricConstants.ERROR_NO_BIOMETRICS) {
                        Log.e("Biometrics", "user does not have any biometrics enrolled")
                    } else if (errorCode == BiometricConstants.ERROR_UNABLE_TO_PROCESS) {
                        Log.e("Biometrics", "the sensor was unable to process the current image")
                    }*/
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()

                ll_check_email_passcode.visibility = View.GONE
            } else {
                Toast.makeText(
                    this@ChurchProfileActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT
                ).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj = JSONObject()
            obj.put("passcode", passCode)
            obj.put("email", userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@ChurchProfileActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text =
                                getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}
