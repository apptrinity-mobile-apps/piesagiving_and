package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.ProjectTypePojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class ThanksActivity : AppCompatActivity() {

    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private var from_screen = ""
    private var from_type = ""
    private var paid_amount = ""
    private var project_name = ""
    private var display_name = ""
    private var special_cause_id = ""

    lateinit var tv_home: TextView
    lateinit var tv_thanks_note: TextView
    lateinit var tv_thanks_projectname: TextView
    lateinit var tv_success_note: TextView
    lateinit var ll_comment_id: LinearLayout
    lateinit var et_comments_id: EditText
    lateinit var tv_submit: TextView
    lateinit var loading_dialog: Dialog


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_thanks)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        from_screen = intent.getStringExtra("from_screen")!!

        if (intent.getStringExtra("from_type")!! !== "") {
            from_type = intent.getStringExtra("from_type")!!
        }
        if (intent.getStringExtra("paid_amount")!! !== "") {
            paid_amount = Formatter().format("%,.2f",intent.getStringExtra("paid_amount")!!.toDouble()).toString()
        }
        if (intent.getStringExtra("project_name")!! !== "") {
            project_name = intent.getStringExtra("project_name")!!
        }
        try {
            if (intent.getStringExtra("special_cause_id")!! != "") {
                special_cause_id = intent.getStringExtra("special_cause_id")!!
            }
        } catch (e: Exception) {
            special_cause_id = ""
        }
        display_name = intent.getStringExtra("display_name")!!

        Log.e("ProjectName", project_name + "----" + display_name)

        if (from_screen == "recurring") {
            if (project_name == "General Fund") {
                tv_thanks_projectname.visibility = View.GONE
                tv_thanks_note.text = "$display_name Thanks You For Your Recurring Donation of $$paid_amount"
            } else {
                tv_thanks_projectname.text = display_name
                tv_thanks_note.text = "$project_name Thanks You For Your Recurring Donation of $$paid_amount"
            }
        } else if (from_screen == "donate") {
            if (project_name == "General Fund") {
                tv_thanks_projectname.visibility = View.GONE
                tv_thanks_note.text = "$display_name Thanks You For Your Donation of $$paid_amount"
            } else {
                tv_thanks_projectname.text = display_name
                tv_thanks_note.text = "$project_name Thanks You For Your Donation of $$paid_amount"
            }
        }  else if (from_screen == "fundraiser") {
            tv_thanks_projectname.visibility = View.GONE
            tv_thanks_note.text = "Thanks You For Your Donation \n Your donation for $display_name of $$paid_amount has been processed!"
        }

        if (from_type == "project") {
            ll_comment_id.visibility = View.GONE
        } else if (from_type == "special_cause") {
            ll_comment_id.visibility = View.VISIBLE
            if (from_screen == "recurring") {
                tv_thanks_projectname.text = project_name
                tv_thanks_note.text = "$display_name Thanks You For Your Donation of $$paid_amount"
            } else {
                tv_thanks_projectname.text = project_name
                tv_thanks_note.text = "$display_name Thanks You For Your Donation of $$paid_amount"
            }
        }

        /*if (from_screen == "recurring") {
            tv_thanks_projectname.text = display_name
            tv_thanks_note.text = project_name + ", Thanks You For Your Recurring Donation of Amount "+ "$"+paid_amount
        } else if (from_screen == "donate") {
            tv_thanks_projectname.text = display_name
            tv_thanks_note.text = project_name + ", Thanks You For Your Donation of Amount " + "$"+paid_amount
        }*/


        tv_submit.setOnClickListener {
            if (et_comments_id.text.equals("")) {
                Toast.makeText(this@ThanksActivity, "Please Enter Comment", Toast.LENGTH_SHORT).show()
            } else {
                updateSpecialCauseComment(special_cause_id)
            }
        }

        tv_home.setOnClickListener {
            val intent = Intent(this@ThanksActivity, DashBoardActivity::class.java)
            intent.putExtra("to_fragment", "")
            intent.putExtra("cat_id", "")
            intent.putExtra("show_lock", "no")
            startActivity(intent)
            finish()
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isInternetConnected = cd.isConnectingToInternet

        tv_home = findViewById(R.id.tv_home)
        tv_thanks_note = findViewById(R.id.tv_thanks_note)
        tv_thanks_projectname = findViewById(R.id.tv_thanks_projectname)
        ll_comment_id = findViewById(R.id.ll_comment_id)
        et_comments_id = findViewById(R.id.et_comments_id)
        tv_submit = findViewById(R.id.tv_submit)
        tv_success_note = findViewById(R.id.tv_success_note)
    }

    override fun onBackPressed() {
        val intent = Intent(this@ThanksActivity, DashBoardActivity::class.java)
        intent.putExtra("to_fragment", "")
        intent.putExtra("cat_id", "")
        intent.putExtra("show_lock", "no")
        startActivity(intent)
        finish()
    }

    private fun updateSpecialCauseComment(special_cause_id: String) {
        val obj = JSONObject()
        obj.put("comment", et_comments_id.text.toString())
        obj.put("spl_cause_id", special_cause_id)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("COMMENT_OBJECT", jObject.toString())
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.UpdateSpecialCauseComment(jObject)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<ProjectTypePojo> {
            override fun onFailure(call: Call<ProjectTypePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(call: Call<ProjectTypePojo>, response: Response<ProjectTypePojo>) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response.body()!!.responseStatus!! == 1) {
                        Log.e("COMMENTRESPONSE", response.body()!!.result!!)
                        tv_success_note.visibility = View.VISIBLE
                        tv_thanks_note.visibility = View.GONE
                        tv_thanks_projectname.visibility = View.GONE
                        ll_comment_id.visibility = View.GONE
                    } else {
                        Toast.makeText(this@ThanksActivity, "Project Type Fetching Failed!", Toast.LENGTH_SHORT).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}
