package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.graphics.Paint
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG
import androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.concurrent.Executor

@SuppressLint("SetTextI18n")
open class BaseActivity : AppCompatActivity() {

    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private lateinit var sessionManager: SessionManager
    private lateinit var userDetails: HashMap<String, String>
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var eMail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        initialize()
    }

    private fun initialize() {
        passCodeDialog()
        sessionManager = SessionManager(this@BaseActivity)
        biometricManager = BiometricManager.from(this@BaseActivity)
        executor = ContextCompat.getMainExecutor(this@BaseActivity)
        userDetails = sessionManager.getUserDetails()
        biometrics = userDetails[SessionManager.FACEID_KEY]!!
        passCode = userDetails[SessionManager.PASSCODE_KEY]!!
        eMail = userDetails[SessionManager.EMAIL_KEY]!!
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }
    }

    open fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                }//showToast("No Biometric hardware available")
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                } //showToast("Biometric hardware unavailable")
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }

        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            .setDescription(getString(R.string.authenticate_to_proceed))               // description of usage purpose
//            .setDeviceCredentialAllowed(true)                               // true to use pin/pattern/password if biometric is not set
            .setAllowedAuthenticators(BIOMETRIC_STRONG or DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@BaseActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    // showToast("Success")
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                    // showToast("Auth failed")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())
                    /*if (errorCode == BiometricConstants.ERROR_USER_CANCELED) {
                        finish()
                    } else if (errorCode == BiometricConstants.ERROR_CANCELED) {
                        Log.e("Biometrics", "biometric operation cancelled by user.")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT_PERMANENT) {
                        Log.e("Biometrics", "ERROR_LOCKOUT occurred too many times")
                    } else if (errorCode == BiometricConstants.ERROR_LOCKOUT) {
                        Log.e("Biometrics", "the API is locked out due to too many attempts")
                    } else if (errorCode == BiometricConstants.ERROR_NO_BIOMETRICS) {
                        Log.e("Biometrics", "user does not have any biometrics enrolled")
                    } else if (errorCode == BiometricConstants.ERROR_UNABLE_TO_PROCESS) {
                        Log.e("Biometrics", "the sensor was unable to process the current image")
                    }*/
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
            } else {
                ll_check_email_passcode.visibility = View.GONE
                Toast.makeText(this@BaseActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            finish()
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj=JSONObject()
            obj.put("passcode",passCode)
            obj.put("email",eMail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener {
            if (et_passcode.text.toString() == "") {
                passCodeDialog.dismiss()
                finish()
            }
        }
    }

    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(
                        this@BaseActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            et_passcode.setText("")
                            tv_passcode_email.text = getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) +": $eMail"
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}