package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionSplash
import com.piesagiving.com.R


class SplashWelcomeActivity : AppCompatActivity() {

    lateinit var tv_sign_in:TextView
    lateinit var tv_create_account:TextView
    lateinit var cd:ConnectionDetector
    var isNetworkAvailable = false
    lateinit var session:SessionSplash
    private var splashLoaded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_splashwelcome)

        initialize()
        session = SessionSplash(this)
        session.setSplashScreen("1")
        val account_skip = session.getSplashScreen()
        val account_status = account_skip[SessionSplash.BANK_AC_SKIP]!!
        Log.e("SPLASHVALUE",account_status)
        scheduleSplashScreen()

    }


    private fun scheduleSplashScreen() {
        if (!splashLoaded) {

            val getsplash = session.getSplashScreen()
                val secondsDelayed = 2
                Handler().postDelayed({

                    val intent = Intent(this, DashBoardActivity::class.java)
                    intent.putExtra("to_fragment","")
                    intent.putExtra("cat_id","")
                    intent.putExtra("show_lock","no")
                    startActivity(intent)
                    finish()
                }, (secondsDelayed * 1000).toLong())

                splashLoaded = true




        }
    }

    private fun initialize(){
        cd = ConnectionDetector(this)

        isNetworkAvailable = cd.isConnectingToInternet


    }

    override fun onBackPressed() {
        finishAffinity()
    }


}
