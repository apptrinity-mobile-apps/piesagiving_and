package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.ApiInterface.ScheduleArrayList
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class RecurringDonationDetailedViewActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_screen = ""
    var church_addr = ""
    var church_name = ""
    var freq = ""
    var amount = ""
    var donation_details = ""
    var donationId = ""
    var start_date = ""
    var end_date = ""
    var screen_type = ""
    var paymentType = ""
    var sourceId = ""
    var projectName = ""
    var subscriptionAmount = ""
    var isHasEnd = ""
    var activeStatus = ""
    var totalAmount = ""
    private var userId = ""
    private lateinit var user_details: HashMap<String, String>

    lateinit var schedule_array: ArrayList<ScheduleArrayList>
    lateinit var loading_dialog: Dialog

    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    lateinit var tv_church_name: TextView
    lateinit var tv_church_address: TextView
    lateinit var tv_amount: TextView
    lateinit var tv_frequency: TextView
    lateinit var tv_donation_details: TextView
    lateinit var ll_edit_recurring: LinearLayout
    lateinit var ll_delete_id: LinearLayout
    lateinit var rv_schedule_id: RecyclerView
    lateinit var tv_start_date_id: TextView
    lateinit var tv_end_date_id: TextView
    lateinit var tv_payment_type_id: TextView
    lateinit var ll_buttons: LinearLayout
    lateinit var tv_total_amount: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.new_recurring_donation_detailed_view)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        val i = intent
        freq = i.getStringExtra("reccurringType")!!
        val cardHolderName = i.getStringExtra("cardHolderName")!!
        church_name = i.getStringExtra("orgName")!!
        val orgStreetAddress = i.getStringExtra("orgStreetAddress")!!
        val orgStateId = i.getStringExtra("orgStateId")!!
        val cardLast4Digits = i.getStringExtra("cardLast4Digits")!!
        amount = i.getStringExtra("amount")!!
        donationId = i.getStringExtra("donationId")!!
        start_date = i.getStringExtra("start_date")!!
        end_date = i.getStringExtra("end_date")!!
        screen_type = i.getStringExtra("screen_type")!!
        paymentType = i.getStringExtra("paymentType")!!
        sourceId = i.getStringExtra("sourceId")!!
        projectName = i.getStringExtra("projectName")!!
        subscriptionAmount = i.getStringExtra("subscriptionAmount")!!
        isHasEnd = i.getStringExtra("isHasEnd")!!
        activeStatus = i.getStringExtra("activeStatus")!!
        totalAmount = i.getStringExtra("totalAmount")!!
        if (projectName.equals("general_fund")) {
            projectName = "General Fund"
        }
        if (activeStatus == "inactive") {
            ll_buttons.visibility = View.GONE
        } else {
            ll_buttons.visibility = View.VISIBLE
        }
        schedule_array =
            intent.getSerializableExtra("schedule_array") as ArrayList<ScheduleArrayList>


        val special_rv_LayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_schedule_id.layoutManager = special_rv_LayoutManager
        val adapter = ScheduleAdapterNew(this, schedule_array)
        rv_schedule_id.adapter = adapter
        adapter.notifyDataSetChanged()

        church_addr = "$orgStreetAddress, $orgStateId"
        donation_details = "$cardHolderName, $cardLast4Digits"

        tv_header.text = getString(R.string.recurring_donations_for) + " " + church_name
        tv_church_name.text = church_name
        tv_church_address.text = church_addr
        tv_amount.text = "$ ${Formatter().format("%,.2f",subscriptionAmount.toDouble())}"
        tv_frequency.text = freq + " " + getString(R.string.recurring)
        tv_donation_details.text = donation_details
        tv_start_date_id.text = start_date
        //tv_end_date_id.text = end_date
        if (totalAmount == "") {
            tv_total_amount.text = "$ 0.00"
        } else {
            tv_total_amount.text = "$ ${Formatter().format("%,.2f",totalAmount.toDouble())}"
        }
        if (paymentType.equals("bank_account")) {
            tv_payment_type_id.text = "Bank Account"

        } else {
            tv_payment_type_id.text = "Credit Card"

        }

        if (end_date == "") {
            tv_end_date_id.text = "Not Scheduled"
        } else {
            tv_end_date_id.text = end_date

        }

        iv_back.setOnClickListener {
            onBackPressed()
        }

/*{
"userId":"5ee30b48251376f63f6a18a8",
"amount":"100",
"donationId":"5ee9c0161bafebe804120c9f",
"reccurringType":"week",
"cardToken":"tok_1GwnzPKijxtVwMc3tMEmA0zh",
"end*/
        ll_edit_recurring.setOnClickListener {
            val intent = Intent(
                this@RecurringDonationDetailedViewActivity,
                EditRecurringDonationActivity::class.java
            )
            intent.putExtra("amount", amount)
            intent.putExtra("donationId", donationId)
            intent.putExtra("reccurringType", freq)
            intent.putExtra("start_date", start_date)
            intent.putExtra("end_date", end_date)
            intent.putExtra("cardLast4Digits", cardLast4Digits)
            intent.putExtra("screen_type", screen_type)
            intent.putExtra("church_name", church_name)
            intent.putExtra("paymentType", paymentType)
            intent.putExtra("sourceId", sourceId)
            intent.putExtra("projectName", projectName)
            intent.putExtra("subscriptionAmount", subscriptionAmount)
            intent.putExtra("isHasEnd", isHasEnd)

            startActivity(intent)
        }

        ll_delete_id.setOnClickListener {
            if (isNetworkAvailable) {
                DeleteOrNot()
            } else {
                Toast.makeText(
                    this!!,
                    getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }


    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@RecurringDonationDetailedViewActivity)
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun deleteRecurringDonation(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.deleteRecurringDonationApi(finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("delete_recurring", "error $t")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val intent = Intent(
                            this@RecurringDonationDetailedViewActivity,
                            RecurringDonationsActivity::class.java
                        )
                        startActivity(intent)
                        finish()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }


    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_donation_details = findViewById(R.id.tv_donation_details)
        tv_frequency = findViewById(R.id.tv_frequency)
        tv_amount = findViewById(R.id.tv_amount)
        tv_church_address = findViewById(R.id.tv_church_address)
        tv_church_name = findViewById(R.id.tv_church_name)
        ll_edit_recurring = findViewById(R.id.ll_edit_recurring)
        ll_delete_id = findViewById(R.id.ll_delete_id)
        tv_end_date_id = findViewById(R.id.tv_end_date_id)
        tv_start_date_id = findViewById(R.id.tv_start_date_id)
        rv_schedule_id = findViewById(R.id.rv_schedule_id)
        tv_payment_type_id = findViewById(R.id.tv_payment_type_id)
        ll_buttons = findViewById(R.id.ll_buttons)
        tv_total_amount = findViewById(R.id.tv_total_amount)

    }

    @SuppressLint("SetTextI18n")
    inner class ScheduleAdapterNew(
        context: Context,
        list: ArrayList<ScheduleArrayList>
    ) :
        RecyclerView.Adapter<ScheduleAdapterNew.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<ScheduleArrayList>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.schedule_list_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_date_id.text = mList!![position].date.toString()
            holder.tv_amount_id.text = "$${Formatter().format("%,.2f",mList!![position].amount)}"


            val j = mList!![position].term!!.toInt() % 10
            val k = mList!![position].term!!.toInt() % 100
            if (j == 1 && k != 11) {
                holder.tv_term_id.text = (mList!![position].term.toString() + "st Term")
                return
            }
            if (j == 2 && k != 12) {
                holder.tv_term_id.text = (mList!![position].term.toString() + "nd Term")
                return
            }
            if (j == 3 && k != 13) {
                holder.tv_term_id.text = (mList!![position].term.toString() + "rd Term")
                return
            }
            holder.tv_term_id.text = mList!![position].term.toString() + "th Term"

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_term_id = view.findViewById(R.id.tv_term_id) as TextView
            var tv_date_id = view.findViewById(R.id.tv_date_id) as TextView
            var tv_amount_id = view.findViewById(R.id.tv_amount_id) as TextView

        }

    }

    fun DeleteOrNot() {
        val text =
            "Do you really want to delete these Donation? This process cannot be undone."
        //val message = SpannableString(text)
        //val boldSpan = StyleSpan(Typeface.BOLD)
        //message.setSpan(boldSpan, 0, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        AlertDialog.Builder(this)
            .setTitle("Are you sure?")
            .setMessage(text)
            .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                    val obj = JSONObject()
                    obj.put("userId", userId)
                    obj.put("donationId", donationId)
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(obj.toString()) as JsonObject
                    deleteRecurringDonation(finalObj)
                }
            })
            .setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                }
            })
            .show()
    }
}
