package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.VibrationEffect
import android.os.Vibrator
import android.text.SpannableString
import android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE
import android.text.style.AbsoluteSizeSpan
import android.text.style.StyleSpan
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.FundRaiserDonorsAdapter
import com.piesagiving.com.Adapters.RecentFundRaiserDonorsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.FundraisersResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.Haptics
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.Executor
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class FundRaisersActivity : AppCompatActivity() {
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var fundraiserId = ""
    private var fundraiserName = ""
    private var orgId = ""
    lateinit var loading_dialog: Dialog
    private lateinit var iv_back: ImageView
    private lateinit var iv_fundraiser: ImageView
    private lateinit var tv_header: TextView
    private lateinit var tv_fundraiser_name: TextView
    private lateinit var tv_fundraiser_desc: TextView
    private lateinit var tv_fundraiser_created: TextView
    private lateinit var tv_fundraiser_category: TextView
    private lateinit var tv_total_donations: TextView
    private lateinit var tv_fundraiser_amount_raised: TextView
    private lateinit var seekBar: SeekBar
    private lateinit var tv_all_donations: TextView
    private lateinit var tv_top_donations: TextView
//    private lateinit var tv_fundraiser_latest_name: TextView
//    private lateinit var tv_fundraiser_latest_amount: TextView
//    private lateinit var tv_fundraiser_top_name: TextView
//    private lateinit var tv_fundraiser_top_amount: TextView
//    private lateinit var tv_fundraiser_first_name: TextView
//    private lateinit var tv_fundraiser_first_amount: TextView
    private lateinit var ll_donations: LinearLayout
    private lateinit var tv_share: TextView
    private lateinit var tv_follow: TextView
    private lateinit var tv_donate: TextView
    var shareUrl = ""
    private lateinit var rv_recent_donations: RecyclerView
    private var isFundraiserFollowing = false

    //   biometric
    lateinit var userNotifications: HashMap<String, String>
    private lateinit var executor: Executor
    private lateinit var biometricManager: BiometricManager
    private var isLockEnabled = false
    private var biometrics = ""
    private var passCode = ""
    private var userEmail = ""
    private lateinit var biometricPrompt: BiometricPrompt
    private lateinit var passCodeDialog: BottomSheetDialog
    private lateinit var ll_check_email_passcode: LinearLayout
    private lateinit var et_passcode: EditText
    private lateinit var tv_passcode_email: TextView
    var isShowingMore = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_fund_raisers)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        val data: Uri? = intent?.data
        Log.d("data uri", data.toString())
        if (data == null) {
            fundraiserId = intent.getStringExtra("fundraiser_id")!!
        } else {
            fundraiserId = data.toString().substringAfter("id=","")
        }

        getData()
        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }
        tv_all_donations.setOnClickListener {
            val intent = Intent(this@FundRaisersActivity, FundraiserDonationsListActivity::class.java)
            intent.putExtra("type", "all")
            intent.putExtra("fundraiser_id", fundraiserId)
            startActivity(intent)
        }
        tv_top_donations.setOnClickListener {
            val intent = Intent(this@FundRaisersActivity, FundraiserDonationsListActivity::class.java)
            intent.putExtra("type", "top")
            intent.putExtra("fundraiser_id", fundraiserId)
            startActivity(intent)
        }
        tv_donate.setOnClickListener {
            Haptics().vibrate(this)
            showBiometricPrompt()
        }
        tv_share.setOnClickListener {
            share()
        }
        tv_follow.setOnClickListener {
            Haptics().vibrate(this)
            isFundraiserFollowing = !isFundraiserFollowing
            if (isFundraiserFollowing) {
                tv_follow.text = getString(R.string.follow)
            } else {
                tv_follow.text = getString(R.string.following)
            }
            val obj = JSONObject()
            obj.put("userId", userId)
            obj.put("fundraiserId", fundraiserId)
            val jParser = JsonParser()
            val jObject = jParser.parse(obj.toString()) as JsonObject
            fundraisersFollowing(jObject)
        }
        // to show or hide more data
        /*tv_fundraiser_desc.setOnClickListener {
            if (!isShowingMore) {
                tv_fundraiser_desc.maxLines = Integer.MAX_VALUE
                isShowingMore = true
            } else {
                tv_fundraiser_desc.maxLines = 4
                isShowingMore = false
            }
        }*/
    }

    fun initialize() {
        loadingDialog()
        passCodeDialog()
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userNotifications = sessionManager.getUserNotifications
        userId = user_details[SessionManager.ID_KEY]!!
        userEmail = user_details[SessionManager.EMAIL_KEY]!!
        biometrics = user_details[SessionManager.FACEID_KEY]!!
        passCode = user_details[SessionManager.PASSCODE_KEY]!!
        biometricManager = BiometricManager.from(this@FundRaisersActivity)
        executor = ContextCompat.getMainExecutor(this@FundRaisersActivity)
        isLockEnabled = when (biometrics) {
            "1" -> {
                true
            }
            "0" -> {
                false
            }
            else -> {
                false
            }
        }

        iv_back = findViewById(R.id.iv_back)
        iv_fundraiser = findViewById(R.id.iv_fundraiser)
        tv_header = findViewById(R.id.tv_header)
        tv_fundraiser_name = findViewById(R.id.tv_fundraiser_name)
        tv_fundraiser_desc = findViewById(R.id.tv_fundraiser_desc)
        tv_fundraiser_created = findViewById(R.id.tv_fundraiser_created)
        tv_fundraiser_category = findViewById(R.id.tv_fundraiser_category)
        tv_total_donations = findViewById(R.id.tv_total_donations)
        tv_fundraiser_amount_raised = findViewById(R.id.tv_fundraiser_amount_raised)
        seekBar = findViewById(R.id.seekBar)
        tv_all_donations = findViewById(R.id.tv_all_donations)
        tv_top_donations = findViewById(R.id.tv_top_donations)
        ll_donations = findViewById(R.id.ll_donations)
        /*tv_fundraiser_latest_name = findViewById(R.id.tv_fundraiser_latest_name)
        tv_fundraiser_latest_amount = findViewById(R.id.tv_fundraiser_latest_amount)
        tv_fundraiser_top_name = findViewById(R.id.tv_fundraiser_top_name)
        tv_fundraiser_top_amount = findViewById(R.id.tv_fundraiser_top_amount)
        tv_fundraiser_first_name = findViewById(R.id.tv_fundraiser_first_name)
        tv_fundraiser_first_amount = findViewById(R.id.tv_fundraiser_first_amount)*/
        tv_donate = findViewById(R.id.tv_donate)
        tv_share = findViewById(R.id.tv_share)
        tv_follow = findViewById(R.id.tv_follow)
        rv_recent_donations = findViewById(R.id.rv_recent_donations)
        val lm_fundraiser_donors = LinearLayoutManager(this@FundRaisersActivity, RecyclerView.VERTICAL, false)
        rv_recent_donations.layoutManager = lm_fundraiser_donors
        rv_recent_donations.setHasFixedSize(true)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view = LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT)
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getData() {
        val obj = JSONObject()
        obj.put("userId", userId)
        obj.put("fundraiserId", fundraiserId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        getFundraiserDetails(jObject)
    }

    private fun share() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            val shareMessage = "https://piesagiving.com/home/fundraiser_single_view/%7Bid%7D?id=$fundraiserId"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareUrl)
            startActivity(Intent.createChooser(shareIntent, "Share via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showBiometricPrompt() {
        if (isLockEnabled) {
            when (biometricManager.canAuthenticate()) {
                BiometricManager.BIOMETRIC_SUCCESS -> authUser(executor)
                BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> {
                    val intent = Intent(this@FundRaisersActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> {
                    val intent = Intent(this@FundRaisersActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }
                BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> {
                    passCodeDialog.show()
                }
            }
        } else {
            val intent = Intent(this@FundRaisersActivity, FundraiserDonateActivity::class.java)
            intent.putExtra("fundraiser_id", fundraiserId)
            intent.putExtra("fundraiser_name", fundraiserName)
            intent.putExtra("org_id", orgId)
            startActivity(intent)
        }
    }

    private fun authUser(executor: Executor) {
        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(getString(R.string.app_name))                         // Title of dialog
            //.setSubtitle("Authentication required to use app")              // sub title of dialog, if any
            .setDescription("Please authenticate to proceed")               // description of usage purpose
//            .setDeviceCredentialAllowed(true)
            .setAllowedAuthenticators(BiometricManager.Authenticators.BIOMETRIC_STRONG or BiometricManager.Authenticators.DEVICE_CREDENTIAL)
            .setConfirmationRequired(false)                                 // confirmation buttons
            .build()

        biometricPrompt = BiometricPrompt(this@FundRaisersActivity, executor,
            object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    val intent = Intent(this@FundRaisersActivity, FundraiserDonateActivity::class.java)
                    intent.putExtra("fundraiser_id", fundraiserId)
                    intent.putExtra("fundraiser_name", fundraiserName)
                    intent.putExtra("org_id", orgId)
                    startActivity(intent)
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    Log.e("Biometrics", "the biometric is valid but not recognized")
                }

                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    Log.e("Biometrics", errString.toString())
                }
            })

        biometricPrompt.authenticate(promptInfo)
    }

    /* pass code setup */
    private fun passCodeDialog() {
        passCodeDialog = BottomSheetDialog(this)
        val view =
            LayoutInflater.from(this).inflate(R.layout.enter_passcode_dialog, null)
        passCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        passCodeDialog.window!!.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        passCodeDialog.setCancelable(true)
        passCodeDialog.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
        passCodeDialog.setContentView(view)
        passCodeDialog.dismissWithAnimation = true
        val tv_submit = view.findViewById(R.id.tv_submit) as TextView
        val tv_cancel = view.findViewById(R.id.tv_cancel) as TextView
        val tv_forgot_passcode = view.findViewById(R.id.tv_forgot_passcode) as TextView
        tv_passcode_email = view.findViewById(R.id.tv_passcode_email) as TextView
        ll_check_email_passcode = view.findViewById(R.id.ll_check_email_passcode) as LinearLayout
        et_passcode = view.findViewById(R.id.et_passcode) as EditText
        tv_forgot_passcode.paintFlags = tv_forgot_passcode.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        tv_submit.setOnClickListener {
            if (et_passcode.text.toString() == passCode) {
                passCodeDialog.dismiss()
                val intent = Intent(this@FundRaisersActivity, FundraiserDonateActivity::class.java)
                intent.putExtra("fundraiser_id", fundraiserId)
                intent.putExtra("fundraiser_name", fundraiserName)
                intent.putExtra("org_id", orgId)
                startActivity(intent)
                ll_check_email_passcode.visibility = View.GONE
            } else {
                Toast.makeText(this@FundRaisersActivity, "In-correct Pin Code!", Toast.LENGTH_SHORT).show()
            }
        }
        tv_cancel.setOnClickListener {
            passCodeDialog.dismiss()
            et_passcode.setText("")
        }
        tv_forgot_passcode.setOnClickListener {
            ll_check_email_passcode.visibility = View.GONE
            val obj = JSONObject()
            obj.put("passcode", passCode)
            obj.put("email", userEmail)
            val jParser = JsonParser()
            val final_object = jParser.parse(obj.toString()) as JsonObject
            forgotPasscode(final_object)
        }
        passCodeDialog.setOnDismissListener { dialog ->
            dialog!!.dismiss()
            et_passcode.setText("")
        }
    }

    // forgot passcode api call
    private fun forgotPasscode(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.forgotPasscodeApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Toast.makeText(this@FundRaisersActivity, getString(R.string.try_again), Toast.LENGTH_SHORT).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            tv_passcode_email.text =
                                getString(R.string.your_app_pincode_will_be_sent_to_your_registered_e_mail) + ": $userEmail"
                            et_passcode.setText("")
                            ll_check_email_passcode.visibility = View.VISIBLE
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    // fundraisers api call
    private fun getFundraiserDetails(obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getFundraiserDetailsApi(obj)
        call.enqueue(object : Callback<FundraisersResponse> {
            override fun onFailure(call: Call<FundraisersResponse>, t: Throwable) {
                loading_dialog.dismiss()
                Toast.makeText(
                    this@FundRaisersActivity,
                    getString(R.string.try_again),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(call: Call<FundraisersResponse>, response: Response<FundraisersResponse>) {
                loading_dialog.dismiss()
                try {
                    val data = response.body()!!
                    if (data.responseStatus == 1) {
                        val fundraiserData = data.fundraiserData
                        orgId = fundraiserData!!.orgId!!
                        fundraiserName = fundraiserData.name!!
                        tv_fundraiser_name.text = fundraiserData.name
                        tv_header.text = fundraiserData.name
                        shareUrl = fundraiserData.fundraiserLink!!
                        isFundraiserFollowing = fundraiserData.followStatus!!
                        if (isFundraiserFollowing) {
                            tv_follow.text = getString(R.string.following)
                        } else {
                            tv_follow.text = getString(R.string.follow)
                        }
                        if (fundraiserData.image.toString() == "") {
                            Picasso.with(this@FundRaisersActivity).load(R.drawable.ic_default_project).centerInside().into(iv_fundraiser)
                        } else {
                            Picasso.with(this@FundRaisersActivity).load(fundraiserData.image.toString()).error(R.drawable.ic_default_project)
                                .into(iv_fundraiser)
                        }
                        tv_fundraiser_desc.text = fundraiserData.description
                        tv_fundraiser_created.text = "Created ${fundraiserData.createdAgo} days ago"
                        tv_fundraiser_category.text = fundraiserData.category
                        tv_total_donations.text = "${fundraiserData.donationsCount} ${getString(R.string.donations)}"
                        val total = Formatter().format("%,.2f", fundraiserData.goalAmount)
                        val completed = Formatter().format("%,.2f", fundraiserData.amountRaised)
                        val text = "$$completed raised of $$total goal"
                        val textDisplay = SpannableString(text)
                        textDisplay.setSpan(AbsoluteSizeSpan(20, true), 0, completed.toString().length + 1, SPAN_INCLUSIVE_INCLUSIVE)
                        textDisplay.setSpan(AbsoluteSizeSpan(16, true), completed.toString().length + 1, text.length, SPAN_INCLUSIVE_INCLUSIVE)
                        textDisplay.setSpan(StyleSpan(Typeface.BOLD), 0, completed.toString().length + 1, SPAN_INCLUSIVE_INCLUSIVE)
                        tv_fundraiser_amount_raised.text = textDisplay
                        if (fundraiserData.amountRaisedPercent != null) {
                            if (fundraiserData.amountRaisedPercent!!.toInt() > 100) {
                                seekBar.progress = 100
                            } else {
                                seekBar.progress = fundraiserData.amountRaisedPercent!!.toInt()
                            }
                        } else {
                            seekBar.progress = 0
                        }
                        /*if (fundraiserData.firstDonationRecord == null) {
                            ll_donations.visibility = View.GONE
                        } else {
                            ll_donations.visibility = View.VISIBLE
                            tv_fundraiser_first_name.text =
                                "${fundraiserData.firstDonationRecord!!.firstName} ${fundraiserData.firstDonationRecord!!.lastName}"
                            tv_fundraiser_first_amount.text = "$${Formatter().format("%.2f", fundraiserData.firstDonationRecord!!.donationAmount)}"
                            tv_fundraiser_latest_name.text =
                                "${fundraiserData.latestDonationRecord!!.firstName} ${fundraiserData.latestDonationRecord!!.lastName}"
                            tv_fundraiser_latest_amount.text = "$${Formatter().format("%.2f", fundraiserData.latestDonationRecord!!.donationAmount)}"
                            tv_fundraiser_top_name.text =
                                "${fundraiserData.topDonationRecord!!.firstName} ${fundraiserData.topDonationRecord!!.lastName}"
                            tv_fundraiser_top_amount.text = "$${Formatter().format("%.2f", fundraiserData.topDonationRecord!!.donationAmount)}"
                        }*/
                        if (fundraiserData.latestDonationsList!!.isNotEmpty()) {
                            rv_recent_donations.visibility = View.VISIBLE
                        } else {
                            rv_recent_donations.visibility = View.GONE
                        }
                        val adapter = RecentFundRaiserDonorsAdapter(this@FundRaisersActivity, fundraiserData.latestDonationsList!!)
                        rv_recent_donations.adapter = adapter
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    // fundraisers following api call
    private fun fundraisersFollowing(obj: JsonObject) {
        val apiInterface = ApiInterface.create()
        val call = apiInterface.fundraiserFollowingApi(obj)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                Toast.makeText(
                    this@FundRaisersActivity,
                    getString(R.string.try_again),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(call: Call<RegisterUserResponse>, response: Response<RegisterUserResponse>) {
                try {
                    val data = response.body()!!
                    if (data.responseStatus == "1") {
                        isFundraiserFollowing = !isFundraiserFollowing
                        if (isFundraiserFollowing) {
                            tv_follow.text = getString(R.string.follow)
                        } else {
                            tv_follow.text = getString(R.string.following)
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}