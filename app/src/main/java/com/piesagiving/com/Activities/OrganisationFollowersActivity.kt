package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.OrganisationFollowAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.OrganisationFollowersArrayResponse
import com.piesagiving.com.ApiInterface.OrganizationDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrganisationFollowersActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var tv_header: TextView
    lateinit var tv_spl_causes_empty: TextView

    lateinit var rv_org_followers: RecyclerView
    lateinit var lm_spl_causes: LinearLayoutManager

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    var from_screen = ""
    var reco_organiz_id = ""

    var display_name = ""

    lateinit var loading_dialog: Dialog
    lateinit var specialCausesArray: ArrayList<OrganisationFollowersArrayResponse>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_organisation_followers)

        initialize()

        display_name = intent.getStringExtra("display_name")!!
        reco_organiz_id = intent.getStringExtra("reco_organizationId")!!
        val obj = JSONObject()
        obj.put("userId", userId)
        obj.put("organizationId", reco_organiz_id)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("dash_bard_input", jObject.toString())
        ProfileResponse(jObject)
        Log.e("DisplayName", display_name)
        tv_header.text = display_name

        // go back
        iv_back.setOnClickListener {
            onBackPressed()
        }


    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        isNetworkAvailable = cd.isConnectingToInternet

        iv_back = findViewById(R.id.iv_back)
        tv_header = findViewById(R.id.tv_header)
        tv_spl_causes_empty = findViewById(R.id.tv_spl_causes_empty)
        rv_org_followers = findViewById(R.id.rv_org_followers)

        lm_spl_causes =
            LinearLayoutManager(this@OrganisationFollowersActivity, RecyclerView.VERTICAL, false)
        rv_org_followers.layoutManager = lm_spl_causes

    }


    private fun ProfileResponse(json_obj: JsonObject) {

        loading_dialog.show()
        specialCausesArray = ArrayList()
        specialCausesArray.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.OrganizationDetailsApi(json_obj)
        call.enqueue(object : Callback<OrganizationDetailsPojo> {
            override fun onFailure(call: Call<OrganizationDetailsPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<OrganizationDetailsPojo>,
                response: Response<OrganizationDetailsPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val organiz_followers = jsondata.organisation_follwers!!
                        if (!jsondata.organisation_follwers!!.size.equals(null)) {
                            for (i in 0 until jsondata.organisation_follwers!!.size) {
                                specialCausesArray.add(
                                    response.body()!!.organisation_follwers!!.get(
                                        i
                                    )
                                )
                            }
                        }
                        rv_org_followers.setHasFixedSize(true)
                        val adapter1 = OrganisationFollowAdapter(
                            this@OrganisationFollowersActivity,
                            specialCausesArray
                        )
                        rv_org_followers.adapter = adapter1
                        adapter1.notifyDataSetChanged()

                    } else {
                        /*Toast.makeText(
                            this@OrganisationFollowersActivity,
                            "Data Fetching Failed",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


}
