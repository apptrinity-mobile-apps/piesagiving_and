package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.*
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class CreateRecurringActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager

    lateinit var tv_save: TextView
    lateinit var checkBox: CheckBox
    lateinit var ll_name: LinearLayout
    lateinit var ll_project: LinearLayout
    lateinit var ll_frequency: LinearLayout
    lateinit var rl_start_date: RelativeLayout
    lateinit var rl_end_date: RelativeLayout
    var payType = 0
    lateinit var et_name: EditText
    lateinit var et_project: EditText
    lateinit var et_amount: EditText
    lateinit var et_frequency: EditText
    lateinit var et_start_date: EditText
    lateinit var img_start_date_hint_id: ImageView
    lateinit var ll_end_date_id: LinearLayout
    lateinit var et_end_date: EditText
    lateinit var iv_back: ImageView
    lateinit var datePicker: ShowDatePikerDialog
    lateinit var sp_project_type_id: Spinner
    lateinit var sp_freq_type_id: Spinner
    lateinit var sp_donation_type_id: Spinner

    lateinit var sp_weeks_id: Spinner
    lateinit var sp_days_id: Spinner
    lateinit var ll_week_id: LinearLayout
    lateinit var ll_days_id: LinearLayout

    var stripeCardExtracharges = 0.00
    var stripeCardPercentage = 0.00
    var piesaCardExtracharges = 0.00
    var piesaCardPercentage = 0.00
    var piesagivingPercentage = 0.00
    var edit_amount = 0.00
    lateinit var tv_organisation_name_payment: TextView
    lateinit var tv_tax_amount_id_payment: TextView
    lateinit var tv_donate_amount_id_payment: TextView
    lateinit var tv_total_amount_id_payment: TextView
    lateinit var et_card_holder_id: EditText
    lateinit var ll_bottom_sheet: LinearLayout
    lateinit var tv_bsheet_amount_id: TextView
    lateinit var tv_bsheet_cancel_id: TextView
    lateinit var tv_bsheet_pay_id: TextView
    lateinit var img_bsheet_close_id: ImageView

    lateinit var days_array: ArrayList<String>
    lateinit var week_array: ArrayList<String>
    lateinit var week_array_ids: ArrayList<String>
    lateinit var freq_names: ArrayList<String>
    lateinit var freq_names_ids: ArrayList<String>
    lateinit var only_names: ArrayList<String>
    lateinit var only_types: ArrayList<String>
    lateinit var only_ids: ArrayList<String>
    lateinit var special_cause_names: ArrayList<String>
    lateinit var projectDonationsArray: ArrayList<DropDownProjectDetailsArrayResponse>
    lateinit var specialCauseDetailsArray: ArrayList<DropDownSpecialCauseDetailsArrayResponse>
    lateinit var loading_dialog: Dialog

    lateinit var donation_types_array: ArrayList<String>
    lateinit var donation_types_array_ids: ArrayList<Int>

    lateinit var array_savedcards_list: ArrayList<SavedCardsArrayDataResponse>
    lateinit var rv_savedcardslist: RecyclerView
    var from_screen = ""
    var organization_id = ""
    var display_name = ""
    var type_project = ""
    var type_id_stg = ""
    var type_name_stg = ""
    var freq_mode = ""
    var freq_mode_id = ""
    var week_freq_id = ""
    var day_freq_id = ""
    var check_box = false
    var startDate = ""
    var endDate = ""

    var donation_amount = ""
    var payment_type = ""
    var type_id = ""
    var type_name = ""

    lateinit var full_dialog: Dialog
    lateinit var bank_full_dialog: Dialog
    lateinit var img_bank_close_id: ImageView
    lateinit var btn_saved_bank_pay: Button
    lateinit var id_empty_data_id: TextView
    lateinit var rv_account_details_id: RecyclerView
    lateinit var array_account_list: ArrayList<BankAccountsArrayList>

    var stripeBankExtracharges = 0.00
    var stripeBankPercentage = 0.00
    var piesaBankExtracharges = 0.00
    var piesaBankPercentage = 0.00

    var global_stripe_percent = 0.00
    var global_stripe_total_fee = 0.00
    var global_ex_amount = 0
    var global_bank_ex_amount = 0.00
    var global_piesa_percent = 0.00
    var global_piesa_total_fee = 0.00
    var global_piesa_card_ex_amount = 0
    var global_piesa_bank_ex_amount = 0.00
    var card_global_id = ""
    var bank_card_token = ""

    var cards_default_source_id = ""
    var bank_default_source_id = ""

    lateinit var stripe_card_id: CardInputWidget
    lateinit var btn_pay_id: Button
    lateinit var btn_saved_card_pay: Button
    lateinit var img_close_id: ImageView
    lateinit var get_card: Card

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_create_recurring)

        freq_names = ArrayList()
        freq_names.add("Daily")
        freq_names.add("Weekly")
        freq_names.add("Monthly")
        /*freq_names.add("Quarterly")
        freq_names.add("Halfyearly")
        freq_names.add("Yearly")*/

        freq_names_ids = ArrayList()
        freq_names_ids.add("day")
        freq_names_ids.add("week")
        freq_names_ids.add("month")
        /*freq_names_ids.add("quarter_year")
        freq_names_ids.add("half_year")
        freq_names_ids.add("year")*/

        week_array = ArrayList()
        week_array.add("Sunday")
        week_array.add("Monday")
        week_array.add("Tuesday")
        week_array.add("Wednesday")
        week_array.add("Thursday")
        week_array.add("Friday")
        week_array.add("Saturday")

        /* week_array_ids=ArrayList()
         week_array_ids.add("sunday")
         week_array_ids.add("monday")
         week_array_ids.add("Tuesday")
         week_array_ids.add("Wednesday")
         week_array_ids.add("Thursday")
         week_array_ids.add("Friday")
         week_array_ids.add("Saturday")*/

        days_array = ArrayList()
        for (i in 1 until 32) {
            days_array.add(i.toString())
        }

        donation_types_array = ArrayList()
        // donation_types_array.add("ACH")
        donation_types_array.add("Select Bank Account")
        donation_types_array.add("Select Credit Card")

        donation_types_array_ids = ArrayList()
        donation_types_array_ids.add(0)
        donation_types_array_ids.add(1)


        initialize()

        organization_id = intent.getStringExtra("organization_id")!!
        from_screen = intent.getStringExtra("from_screen")!!
        display_name = intent.getStringExtra("display_name")!!
        type_project = intent.getStringExtra("type_project")!!

        donation_amount = intent.getStringExtra("donation_amount")!!
        payment_type = intent.getStringExtra("payment_type")!!
        type_id = intent.getStringExtra("type_id")!!
        type_name = intent.getStringExtra("type_name")!!

        et_amount.setText(donation_amount)

        Log.e(
            "get_data",
            donation_amount + "-1-" + payment_type + "-1-" + type_id + "-1-" + type_name + "-1-" + display_name
        )
        Log.e("get_reccurringType_22", payment_type)

        if (payment_type.equals("0")) {
            sp_donation_type_id.setSelection(0)
            Log.e("get_reccurringType_11", payment_type)
        } else {
            sp_donation_type_id.setSelection(1)
        }


        Log.e("organization_id_recu", organization_id)

        ProjectDonationTypeApi(organization_id)
        DonationFeeApi()

        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("project_donation_input", jObject.toString())
        SavedCardsListApi(jObject)

        et_name.setText(display_name)
        iv_back.setOnClickListener {
            onBackPressed()
        }

        ll_name.setOnClickListener {
            //open dialog
        }

        val c: Date = Calendar.getInstance().getTime()
        println("Current time => $c")
        val df = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
        val formattedDate = df.format(c)

        et_start_date.setText(formattedDate)
        /*et_start_date.setOnClickListener {
            datePicker.showPiker(this@CreateRecurringActivity, "", et_start_date)
        }*/

        et_end_date.setOnClickListener {
            datePicker.showPiker(this@CreateRecurringActivity, "", et_end_date)
        }

        img_start_date_hint_id.setOnClickListener {
            helpDialog()
        }

        /*rl_start_date.setOnClickListener {
            et_start_date.performClick()
        }*/

        rl_end_date.setOnClickListener {
            et_end_date.performClick()
        }
        rl_end_date.isClickable = false
        et_end_date.isEnabled = false
        et_end_date.isClickable = false
        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {

                ll_end_date_id.visibility = View.GONE
                rl_end_date.isClickable = false
                et_end_date.isEnabled = false
                et_end_date.isClickable = false
                et_end_date.setTextColor(
                    ContextCompat.getColor(this, R.color.login_text_hint)
                )
                check_box = false
            } else {
                ll_end_date_id.visibility = View.VISIBLE
                rl_end_date.isClickable = true
                et_end_date.isEnabled = true
                et_end_date.isClickable = true
                et_end_date.setTextColor(
                    ContextCompat.getColor(this, R.color.login_text)
                )

                check_box = true
            }
        }

        full_dialog = Dialog(this, R.style.DialogTheme)
        full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // full_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        full_dialog.setCancelable(false);

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        full_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);

        full_dialog.setContentView(R.layout.stripe_payment_dialog)
        stripe_card_id = full_dialog.findViewById(R.id.stripe_card_id)
        btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        tv_organisation_name_payment = full_dialog.findViewById(R.id.tv_organisation_name_payment)
        tv_tax_amount_id_payment = full_dialog.findViewById(R.id.tv_tax_amount_id_payment)
        tv_donate_amount_id_payment = full_dialog.findViewById(R.id.tv_donate_amount_id_payment)
        tv_total_amount_id_payment = full_dialog.findViewById(R.id.tv_total_amount_id_payment)
        et_card_holder_id = full_dialog.findViewById(R.id.et_card_holder_id)
        img_close_id = full_dialog.findViewById<ImageView>(R.id.img_close_id)
        rv_savedcardslist = full_dialog.findViewById(R.id.rv_savedcardslist)

        btn_saved_card_pay = full_dialog.findViewById(R.id.btn_saved_card_pay)


        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_savedcardslist.layoutManager = layoutManager
        rv_savedcardslist.setHasFixedSize(true)

        rv_savedcardslist.addOnItemTouchListener(
            RecyclerItemClickListener(this!!,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        card_global_id = array_savedcards_list[position].id!!.toString()

                        btn_saved_card_pay.visibility = View.VISIBLE
                    }
                })
        )


        img_close_id.setOnClickListener {
            full_dialog.dismiss()
        }

        btn_saved_card_pay.setOnClickListener {
            endDate = et_end_date.text.toString()

            startDate = et_start_date.text.toString()!!

            if (type_project.equals("project")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                        .toString()
                )
                obj.put("projectId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                obj.put("paymentType", 1)
                obj.put("isReccurring", true)
                obj.put("cardId", card_global_id)
                obj.put("cardToken", "")
                obj.put("cardSave", true)
                obj.put("reccurringType", freq_mode_id)
                obj.put("startDate", startDate)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("isHasEnd", check_box)
                if (check_box.equals(true)) {
                    obj.put("endDate", endDate)
                } else {
                    obj.put("endDate", startDate)
                }

                if (freq_mode.equals("Weekly")) {
                    obj.put("weekDay", week_freq_id)
                } else if (freq_mode.equals("Monthly")) {
                    obj.put("dateNumber", day_freq_id.toInt())
                }

                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("project_details_input", jObject.toString())
                StripePaymentApi(jObject)
            }


            if (type_project.equals("generalfund")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                        .toString()
                )
                obj.put("projectId", "0")
                obj.put("orgId", organization_id)
                obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                obj.put("paymentType", 1)
                obj.put("isReccurring", true)
                obj.put("cardId", card_global_id)
                obj.put("cardToken", "")
                obj.put("cardSave", true)
                obj.put("reccurringType", freq_mode_id)
                obj.put("startDate", startDate)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("isHasEnd", check_box)
                if (check_box.equals(true)) {
                    obj.put("endDate", endDate)
                } else {
                    obj.put("endDate", startDate)
                }

                if (freq_mode.equals("Weekly")) {
                    obj.put("weekDay", week_freq_id)
                } else if (freq_mode.equals("Monthly")) {
                    obj.put("dateNumber", day_freq_id.toInt())
                }

                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("recurrinGeneralFund_input", jObject.toString())
                StripePaymentApi(jObject)
            }


        }

        btn_pay_id.setOnClickListener {

            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                Toast.makeText(
                    this@CreateRecurringActivity,
                    "Invalid Card Data",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (et_card_holder_id.text.toString().trim().equals("")) {
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                loading_dialog.show()
                buy()
            }
        }

        TooltipCompat.setTooltipText(btn_pay_id, "Payment Button")


        val dialogView = getLayoutInflater().inflate(R.layout.payment_confirmation_layout, null);
        val dialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
        dialog.setContentView(dialogView)
        dialog.dismiss()

        ll_bottom_sheet = findViewById(R.id.ll_bottom_sheet)
        tv_bsheet_amount_id = dialog!!.findViewById(R.id.tv_bsheet_amount_id)!!
        tv_bsheet_cancel_id = dialog.findViewById(R.id.tv_bsheet_cancel_id)!!
        tv_bsheet_pay_id = dialog.findViewById(R.id.tv_bsheet_pay_id)!!
        val bottomSheetBehavior = BottomSheetBehavior.from(ll_bottom_sheet)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        img_bsheet_close_id = dialog.findViewById(R.id.img_bsheet_close_id)!!

        img_bsheet_close_id.setOnClickListener {
            dialog.dismiss()
        }
        tv_bsheet_pay_id.setOnClickListener {
            dialog.dismiss()
            full_dialog.show()
        }
        tv_bsheet_cancel_id.setOnClickListener {
            dialog.dismiss()
        }
        tv_save.setOnClickListener {

            /*{
   "userId": "5ee30feb251376f63f6a18ae",
   "projectId": "5ee315307e75e5950f558f76",
   "orgId": "5ee30fe1251376f63f6a18ad",
   "amount": 262,
   "paymentType": 1,
   "cardToken": "tok_1GtDPnKijxtVwMc3dl2UUUNA",
   "isReccurring": true,
   "reccurringType":"month",
   "isHasEnd":false
}*/

            if (et_amount.text.toString().trim().equals("") || et_amount.text.toString()
                    .toDouble() <= 4.99
            ) {
                toast("You must enter donation amount min $5")
            } else if (et_start_date.text.toString().equals("")) {
                toast("You must enter Start Date")
            } else if (check_box.equals(true) && et_end_date.text.toString().equals("")) {
                toast("You must enter End Date")
            } else {
                if (payType.equals(1)) {

                    // bank_full_dialog.show()
                    // full_dialog.show()
                    dialog.show()

                    edit_amount = (et_amount.text.toString()).toDouble()
                    val tax =
                        "%.2f".format((edit_amount!! * stripeCardPercentage / 100) + (stripeCardExtracharges.toDouble()) + (piesaCardExtracharges.toDouble()) + (edit_amount!! * piesaCardPercentage / 100))

                    Log.e("tax_lll", tax.toString())
                    tv_tax_amount_id_payment.text = "$" + tax
                    tv_total_amount_id_payment.text = et_amount.text.toString()
                    tv_donate_amount_id_payment.setText("$" + (edit_amount!!.toDouble() - tax!!.toDouble()))
                    Log.e("to_double", edit_amount.toString() + "--" + et_amount.text.toString())

                    val full_data =
                        "Send " + "<b>" + "$" + et_amount.text.toString() + "</b> " + "to $display_name"
                    // tv_bsheet_amount_id.text="Send $"+edit_amount.toString()+" to $display_name"
                    tv_bsheet_amount_id.text = Html.fromHtml(full_data)

                } else {

                    bank_full_dialog.show()

                    /* full_dialog.show()
                     edit_amount = (et_amount.text.toString()).toInt()
                     val tax = "%.2f".format((edit_amount!! * stripeCardPercentage/100) + (stripeCardExtracharges.toDouble()/100)+(edit_amount!! * piesagivingPercentage/100))
                     tv_tax_amount_id_payment.text ="$" +  tax
                     tv_total_amount_id_payment.text =  edit_amount!!.toString()
                     tv_donate_amount_id_payment.setText("$" + (edit_amount!!.toDouble() - tax!!.toDouble()))*/

                }


            }

        }


    }

    fun helpDialog() {
        val bank_help_dialog = Dialog(this)
        bank_help_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_help_dialog.setCancelable(false)

        bank_help_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        bank_help_dialog.getWindow()!!.setBackgroundDrawableResource(R.color.transparent)

        bank_help_dialog.setContentView(R.layout.donation_help_screen_dialog)
        val img_help_close_id = bank_help_dialog.findViewById<ImageView>(R.id.img_help_close_id)
        val tv_help_id = bank_help_dialog.findViewById<TextView>(R.id.tv_help_id)
        tv_help_id.text =
            "Your first donation will happen today, after that it will occur only on the day you have selected."
        img_help_close_id.setOnClickListener {
            bank_help_dialog.dismiss()
        }
        bank_help_dialog.show()
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet

        tv_save = findViewById(R.id.tv_save)
        checkBox = findViewById(R.id.checkBox)
        ll_name = findViewById(R.id.ll_name)
        et_name = findViewById(R.id.et_name)

        et_amount = findViewById(R.id.et_amount)

        sp_donation_type_id = findViewById(R.id.sp_donation_type_id)
        rl_start_date = findViewById(R.id.rl_start_date)
        et_start_date = findViewById(R.id.et_start_date)
        img_start_date_hint_id = findViewById(R.id.img_start_date_hint_id)
        rl_end_date = findViewById(R.id.rl_end_date)
        ll_end_date_id = findViewById(R.id.ll_end_date_id)
        et_end_date = findViewById(R.id.et_end_date)
        iv_back = findViewById(R.id.iv_back)
        sp_project_type_id = findViewById(R.id.sp_project_type_id)
        sp_freq_type_id = findViewById(R.id.sp_freq_type_id)
        sp_weeks_id = findViewById(R.id.sp_weeks_id)
        sp_days_id = findViewById(R.id.sp_days_id)
        ll_week_id = findViewById(R.id.ll_week_id)
        ll_days_id = findViewById(R.id.ll_days_id)
        datePicker = ShowDatePikerDialog()

        et_amount.setFilters(arrayOf(DecimalDigitsInputFilter(5, 2)))

        et_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {

                    et_amount.setSelection(et_amount.length())
                    //amount_seekbar_id.setProgress((et_amount.text.toString()).toInt(), true)
                    edit_amount = (et_amount.text.toString()).toDouble()
                    if (et_amount.text.toString().equals("") || et_amount.text.toString().equals(
                            "0.00"
                        )
                    ) {
                        // tv_donate_amount_id.setText("$0.00")
                        // tv_total_amount_id.setText("0.00")
                    }

                } catch (e: java.lang.Exception) {
                    Log.e("exception", et_amount.text.toString())
                    //  tv_donate_amount_id.setText("$0.00")
                    // tv_total_amount_id.setText("0.00")
                    edit_amount = 5.00
                }

            }
        })



        Bank_full_dilag()

        val donation_type_adapter = ArrayAdapter<String>(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            donation_types_array
        )
        sp_donation_type_id.setAdapter(donation_type_adapter)
        sp_donation_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                payType = donation_types_array_ids.get(position)
                Log.e("type_name_stg", payType.toString())

                if (payType.equals(1)) {
                    if (edit_amount!!.toDouble() > 0.00) {
                        val strpextracharge = (stripeCardExtracharges.toDouble())
                        val decimal =
                            (edit_amount!! * (stripeCardPercentage / 100)) + (strpextracharge) + (piesaCardExtracharges.toDouble()) + (edit_amount!! * (piesaCardPercentage / 100))
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e("tax_ll", decimal.toString())
                        //tv_tax_amount_id.setText("$" + tax.toString())
                        tv_tax_amount_id_payment.setText("$" + tax.toString())
                        //tv_donate_amount_id.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        tv_donate_amount_id_payment.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        //tv_total_amount_id.setText((edit_amount!!.toDouble()).toString())
                        tv_total_amount_id_payment.setText((edit_amount!!.toDouble()).toString())
                        Log.e("final_all", tax)

                        global_stripe_percent = stripeCardPercentage
                        global_stripe_total_fee =
                            "%.2f".format((edit_amount!! * stripeCardPercentage / 100)).toDouble()
                        global_ex_amount = (stripeCardExtracharges * 100).toInt()
                        global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                        global_piesa_percent = piesaCardPercentage
                        global_piesa_total_fee =
                            "%.2f".format((edit_amount!! * piesaCardPercentage / 100)).toDouble()
                    }


                } else {
                    if (edit_amount!!.toDouble() > 0.00) {
                        val strpextracharge = (stripeBankExtracharges.toDouble())
                        val decimal =
                            (edit_amount!! * (stripeBankPercentage / 100)) + (strpextracharge) + (piesaBankExtracharges.toDouble()) + (edit_amount!! * (piesaBankPercentage / 100))
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e("tax_ll", decimal.toString())
                        //tv_tax_amount_id.setText("$" + tax.toString())
                        tv_tax_amount_id_payment.setText("$" + tax.toString())
                        //tv_donate_amount_id.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        tv_donate_amount_id_payment.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        //tv_total_amount_id.setText((edit_amount!!.toDouble()).toString())
                        tv_total_amount_id_payment.setText((edit_amount!!.toDouble()).toString())
                        Log.e("final_all", tax)

                        global_stripe_percent = stripeBankPercentage
                        global_stripe_total_fee =
                            "%.2f".format((edit_amount!! * stripeBankPercentage / 100)).toDouble()
                        global_bank_ex_amount = (stripeBankExtracharges)
                        global_piesa_bank_ex_amount = (piesaBankExtracharges)
                        global_piesa_percent = piesaBankPercentage
                        global_piesa_total_fee =
                            "%.2f".format((edit_amount!! * piesaBankPercentage / 100)).toDouble()
                    }
                }

            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })

        val adapter = ArrayAdapter<String>(
            this@CreateRecurringActivity,
            R.layout.support_simple_spinner_dropdown_item,
            freq_names
        )
        sp_freq_type_id.setAdapter(adapter)

        sp_freq_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                freq_mode = freq_names.get(position).toString()
                freq_mode_id = freq_names_ids.get(position).toString()
                Log.e("freq_mode_stg", freq_mode)
                if (freq_mode.equals("Weekly")) {
                    ll_week_id.visibility = View.VISIBLE
                    ll_days_id.visibility = View.GONE
                } else if (freq_mode.equals("Monthly")) {
                    ll_days_id.visibility = View.VISIBLE
                    ll_week_id.visibility = View.GONE
                } else {
                    ll_days_id.visibility = View.GONE
                    ll_week_id.visibility = View.GONE
                }
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })


        val weeks_adapter = ArrayAdapter<String>(
            this@CreateRecurringActivity,
            R.layout.support_simple_spinner_dropdown_item,
            week_array
        )
        sp_weeks_id.setAdapter(weeks_adapter)
        sp_weeks_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                week_freq_id = week_array.get(position).toLowerCase(Locale.getDefault())

                Log.e("week_freq_id", week_freq_id)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })

        val days_adapter = ArrayAdapter<String>(
            this@CreateRecurringActivity,
            R.layout.support_simple_spinner_dropdown_item,
            days_array
        )
        sp_days_id.setAdapter(days_adapter)
        sp_days_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                day_freq_id = days_array.get(position)

            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    private fun ProjectDonationTypeApi(org_id: String) {

        loading_dialog.show()


        only_types = ArrayList()
        only_types.clear()
        only_names = ArrayList()
        only_names.clear()
        only_ids = ArrayList()
        only_ids.clear()
        special_cause_names = ArrayList()
        special_cause_names.clear()
        projectDonationsArray = ArrayList()
        projectDonationsArray.clear()
        specialCauseDetailsArray = ArrayList()
        specialCauseDetailsArray.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.ProjectViewSpecialCausesApi(org_id)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<ProjectTypePojo> {
            override fun onFailure(call: Call<ProjectTypePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<ProjectTypePojo>,
                response: Response<ProjectTypePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        Log.e("from_screen_in_service", from_screen)

                        if (from_screen.equals("churchprofile")) {
                            Log.e("from_screen_in_service", from_screen)
                            only_names.add("General Fund")
                            only_ids.add("0")
                            only_types.add("generalfund")
                            for (i in 0 until response.body()!!.projectDetails!!.size) {
                                only_names.add(response.body()!!.projectDetails!!.get(i).projectName!!)
                                only_ids.add(response.body()!!.projectDetails!!.get(i).projectId!!)
                                projectDonationsArray.add(response.body()!!.projectDetails!!.get(i))
                                only_types.add("project")
                            }


                            /*for (i in 0 until response.body()!!.specialCauseDetails!!.size) {
                                only_names.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseName!!)
                                only_ids.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseId!!)
                                only_types.add("special_cause")
                                specialCauseDetailsArray.add(
                                    response.body()!!.specialCauseDetails!!.get(
                                        i
                                    )
                                )
                            }*/

                        }


                        if (from_screen.equals("project")) {
                            for (i in 0 until response.body()!!.projectDetails!!.size) {
                                only_names.add(response.body()!!.projectDetails!!.get(i).projectName!!)
                                only_ids.add(response.body()!!.projectDetails!!.get(i).projectId!!)
                                only_types.add("project")
                                projectDonationsArray.add(response.body()!!.projectDetails!!.get(i))
                            }
                        }

                        /*if (from_screen.equals("special_cause")) {
                            for (i in 0 until response.body()!!.specialCauseDetails!!.size) {
                                only_names.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseName!!)
                                only_ids.add(response.body()!!.specialCauseDetails!!.get(i).specialCauseId!!)
                                only_types.add("special_cause")
                                specialCauseDetailsArray.add(
                                    response.body()!!.specialCauseDetails!!.get(
                                        i
                                    )
                                )
                            }
                        }*/


                        Log.e("from_screen_in", from_screen)


                        val adapter = ArrayAdapter<String>(
                            this@CreateRecurringActivity,
                            R.layout.support_simple_spinner_dropdown_item,
                            only_names
                        )
                        sp_project_type_id.setAdapter(adapter)
                        projectSpinnerSelection()
                    } else {
                        Toast.makeText(
                            this@CreateRecurringActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    fun projectSpinnerSelection() {
        sp_project_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                type_id_stg = only_ids.get(position).toString()
                type_name_stg = only_names.get(position).toString()
                type_project = only_types.get(position);
                tv_organisation_name_payment.setText("To " + type_name_stg + "( " + display_name + " )")
                Log.e("type_name_stg", type_name_stg + "-----" + type_id_stg)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })

        if (type_id.equals("0")) {
            sp_project_type_id.setSelection(0)
            Log.e("get_ll_type_id", type_id)
        }

        for (i in 0 until only_names.size) {
            Log.e("type_name_lll", type_name + "---" + only_names[i].toString())
            if (type_name.equals(only_names[i].toString())) {
                sp_project_type_id.setSelection(i)
                type_id_stg = only_ids.get(i).toString()
            }
        }

    }


    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun buy() {
        val validation = get_card!!.validateCard()
        if (validation) {


            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
                number = get_card.number!!,
                exp_month = get_card.expMonth!!,
                exp_year = get_card.expYear!!,
                cvc = get_card.cvc!!,
                name = et_card_holder_id.text.toString()
            )
            Log.e(
                "input_card",
                get_card.number!! + "--" + get_card.expMonth!! + "--" + get_card.expYear!! + "--" + get_card.cvc!!
            )
            //Log.e("@response", call.toString())
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)

                }

                override fun onResponse(
                    call: Call<CardTokenPojo>,
                    response: Response<CardTokenPojo>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {
                        try {
                            val inputSDF = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                            val outputSDF = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                            val sDate = inputSDF.parse(et_start_date.text.toString())
                            if (et_end_date.text.toString().equals("")) {
                                endDate = ""
                            } else {
                                val eDate = inputSDF.parse(et_end_date.text.toString())
                                endDate = /*outputSDF.format(*/et_end_date.text.toString()/*)*/
                            }
                            startDate = /*outputSDF.format(*/et_start_date.text.toString()!!/*)*/
                            Log.e("Dates", startDate + "---" + endDate)
                        } catch (e: java.lang.Exception) {
                            Log.e("Dates", "----")
                            e.printStackTrace()
                        }

                        //response!!.body()!!.id!!.toString()
                        if (type_project.equals("project")) {
                            val obj = JSONObject()
                            obj.put(
                                "userId",
                                sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                                    .toString()
                            )
                            obj.put("projectId", type_id_stg)
                            obj.put("orgId", organization_id)
                            obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                            obj.put("paymentType", 1)
                            obj.put("isReccurring", true)
                            obj.put("cardId", "")
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("reccurringType", freq_mode_id)
                            obj.put("cardSave", true)
                            obj.put("startDate", startDate)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("isHasEnd", check_box)
                            if (check_box.equals(true)) {
                                obj.put("endDate", endDate)
                            } else {
                                obj.put("endDate", startDate)
                            }
                            if (freq_mode.equals("Weekly")) {
                                obj.put("weekDay", week_freq_id)
                            } else if (freq_mode.equals("Monthly")) {
                                obj.put("dateNumber", day_freq_id.toInt())
                            }

                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("project_recurring_input", jObject.toString())
                            StripePaymentApi(jObject)
                        }


                        if (type_project.equals("generalfund")) {
                            val obj = JSONObject()
                            obj.put(
                                "userId",
                                sessionManager.getUserDetails().get(SessionManager.ID_KEY)
                                    .toString()
                            )
                            obj.put("projectId", "0")
                            obj.put("orgId", organization_id)
                            obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                            obj.put("paymentType", 1)
                            obj.put("isReccurring", true)
                            obj.put("cardId", "")
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("reccurringType", freq_mode_id)
                            obj.put("startDate", startDate)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("isHasEnd", check_box)
                            obj.put("cardSave", true)
                            if (check_box.equals(true)) {
                                obj.put("endDate", endDate)
                            } else {
                                obj.put("endDate", startDate)
                            }
                            if (freq_mode.equals("Weekly")) {
                                obj.put("weekDay", week_freq_id)
                            } else if (freq_mode.equals("Monthly")) {
                                obj.put("dateNumber", day_freq_id.toInt())
                            }
                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("recur_GFund_input", jObject.toString())
                            StripePaymentApi(jObject)
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
            /*End Card Token API*/


        } else if (!get_card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun StripePaymentApi(jsonObj: JsonObject) {

        loading_dialog.show()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.StripePaymentDonationApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<StripePaymentPojo> {
            override fun onFailure(call: Call<StripePaymentPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<StripePaymentPojo>,
                response: Response<StripePaymentPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSERECURRING", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {

                        val intent =
                            Intent(this@CreateRecurringActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "donate")
                        intent.putExtra("from_type", type_project)
                        intent.putExtra("special_cause_id", "")
                        intent.putExtra("display_name", display_name)
                        intent.putExtra("project_name", type_name_stg)
                        intent.putExtra("paid_amount", edit_amount!!.toDouble().toString())
                        startActivity(intent)
                        finish()

                    } else {

                        Log.e("failed_0", response!!.body()!!.result!!.toString())
                        Toast.makeText(
                            this@CreateRecurringActivity,
                            response!!.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }


    private fun DonationFeeApi() {

        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetExtraFeeApi()
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationFeePojo> {
            override fun onFailure(call: Call<DonationFeePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<DonationFeePojo>,
                response: Response<DonationFeePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {

                        piesaCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaCardExtracharges!!
                        piesaCardPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaCardPercentage!!
                        piesagivingPercentage =
                            response.body()!!.stripeFeeDetails!!.piesagivingPercentage!!
                        piesaBankPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaBankPercentage!!
                        piesaBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaBankExtracharges!!
                        stripeBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeBankExtracharges!!
                        stripeBankPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeBankPercentage!!
                        stripeCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeCardExtracharges!!
                        stripeCardPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeCardPercentage!!
                        Log.e(
                            "STRIPECHARGES",
                            "" + stripeBankExtracharges + "-------" + stripeBankPercentage
                        )


                        if (payType.equals(1)) {
                            val strpextracharge = (stripeCardExtracharges.toDouble())
                            val decimal =
                                (5 * (stripeCardPercentage / 100)) + (strpextracharge) + (piesaCardExtracharges.toDouble()) + (5 * (piesaCardPercentage / 100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            Log.e("tax_ll", decimal.toString())
                            // tv_tax_amount_id.setText("$" + tax.toString())
                            tv_tax_amount_id_payment.setText("$" + tax.toString())
                            // tv_donate_amount_id.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_donate_amount_id_payment.setText("$" + (5.toDouble() - tax.toDouble()))
                            //  tv_total_amount_id.setText((5.toDouble()).toString())
                            tv_total_amount_id_payment.setText((5.toDouble()).toString())
                            Log.e("final_all", "%.2f".format((5!! * 0.032) + 0.3))
                            global_stripe_percent = stripeCardPercentage
                            global_stripe_total_fee =
                                "%.2f".format((5 * stripeCardPercentage / 100)).toDouble()
                            global_ex_amount = (stripeCardExtracharges * 100).toInt()
                            global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                            global_piesa_percent = piesaCardPercentage
                            global_piesa_total_fee =
                                "%.2f".format((5 * piesaCardPercentage / 100)).toDouble()
                        } else {
                            val strpextracharge = (stripeBankExtracharges.toDouble())
                            val decimal =
                                (5 * (stripeBankPercentage / 100)) + (strpextracharge) + (piesaBankExtracharges.toDouble()) + (5 * (piesaBankPercentage / 100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            Log.e("tax_ll", decimal.toString())
                            // tv_tax_amount_id.setText("$" + tax.toString())
                            tv_tax_amount_id_payment.setText("$" + tax.toString())
                            // tv_donate_amount_id.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_donate_amount_id_payment.setText("$" + (5.toDouble() - tax.toDouble()))
                            //  tv_total_amount_id.setText((5.toDouble()).toString())
                            tv_total_amount_id_payment.setText((5.toDouble()).toString())
                            Log.e("final_all", "%.2f".format((5!! * 0.032) + 0.3))
                            global_stripe_percent = stripeBankPercentage
                            global_stripe_total_fee =
                                "%.2f".format((5 * stripeBankPercentage / 100)).toDouble()
                            global_bank_ex_amount = (stripeBankExtracharges)
                            global_piesa_bank_ex_amount = (piesaBankExtracharges)
                            global_piesa_percent = piesaBankPercentage
                            global_piesa_total_fee =
                                "%.2f".format((5 * piesaBankPercentage / 100)).toDouble()
                        }

                        /*if (payType.equals(0)) {
                            //tv_donate_amount_id.setText("$5")
                            // val tax = round(()+(20/100))

                            val strpextracharge = (stripeBankExtracharges.toDouble()/100)
                            val decimal =(5 * (stripeBankPercentage/100)) + (strpextracharge)+(5* (piesagivingPercentage/100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            //tv_tax_amount_id.setText("$" + tax.toString())
                            tv_tax_amount_id_payment.setText("$" + tax.toString())
                           // tv_donate_amount_id.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_donate_amount_id_payment.setText("$" + (5.toDouble() - tax.toDouble()))
                            //tv_total_amount_id.setText((5.toDouble()).toString())
                            tv_total_amount_id_payment.setText((5.toDouble()).toString())

                            Log.e("final_all", "%.2f".format((5!! * 0.01) + 0.3))

                            global_stripe_percent = stripeBankPercentage
                            global_stripe_total_fee = "%.2f".format((5 * stripeBankPercentage/100)).toDouble()
                            global_ex_amount = stripeBankExtracharges
                            global_piesa_percent = piesagivingPercentage
                            global_piesa_total_fee = "%.2f".format((5* piesagivingPercentage/100)).toDouble()



                        } else {
                            //tv_donate_amount_id.setText("$5")
                            val strpextracharge = (stripeCardExtracharges.toDouble()/100)
                            val decimal =(5 * (stripeCardPercentage/100)) + (strpextracharge)+(5* (piesagivingPercentage/100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            Log.e("tax_ll", decimal.toString())
                           // tv_tax_amount_id.setText("$" + tax.toString())
                            tv_tax_amount_id_payment.setText("$" + tax.toString())
                           // tv_donate_amount_id.setText("$" + (5.toDouble() - tax.toDouble()))
                            tv_donate_amount_id_payment.setText("$" + (5.toDouble() - tax.toDouble()))
                          //  tv_total_amount_id.setText((5.toDouble()).toString())
                            tv_total_amount_id_payment.setText((5.toDouble()).toString())
                            Log.e("final_all", "%.2f".format((5!! * 0.032) + 0.3))
                            global_stripe_percent = stripeCardPercentage
                            global_stripe_total_fee = "%.2f".format((5 * stripeCardPercentage/100)).toDouble()
                            global_ex_amount = stripeCardExtracharges
                            global_piesa_percent = piesagivingPercentage
                            global_piesa_total_fee = "%.2f".format((5* piesagivingPercentage/100)).toDouble()

                        }*/

                    } else {
                        Log.e("failed_0", response!!.body()!!.result!!.toString())

                        Toast.makeText(
                            this@CreateRecurringActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }


    private fun SavedCardsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_savedcards_list = ArrayList()
        array_savedcards_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.savedCardsList(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<SavedCardListResponse> {
            override fun onFailure(call: Call<SavedCardListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<SavedCardListResponse>,
                response: Response<SavedCardListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    val resp = response!!.body()
                    Log.e("STRIPERESPOMNSEDONATION", resp.toString())
                    if (response!!.body()!!.responseStatus == "1") {
                        // toast( response!!.body()!!.result!!)
                        for (i in 0 until response.body()!!.savedCardsList!!.data!!.size) {
                            array_savedcards_list.add(response.body()!!.savedCardsList!!.data!![i])
                        }
                        cards_default_source_id =
                            response.body()!!.customerDetails!!.default_source!!
                        Log.e("cards_default_source_id", cards_default_source_id)
                        rv_savedcardslist.visibility = View.VISIBLE

                        val adapter = SavedCardListAdapter(
                            this@CreateRecurringActivity,
                            array_savedcards_list,
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString(),
                            cards_default_source_id
                        )

                        rv_savedcardslist.adapter = adapter
                        adapter.notifyDataSetChanged()

                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    inner class SavedCardListAdapter(
        context: Context,
        val account_list: ArrayList<SavedCardsArrayDataResponse>,
        val user_id: String,
        var cards_default_source_id: String
    ) :
        RecyclerView.Adapter<SavedCardListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_savedcards, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return account_list.size
        }


        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_card_name: TextView = view.findViewById(R.id.tv_card_name)
            var iv_card_type: ImageView = view.findViewById(R.id.iv_card_type)
            var img_check_id: ImageView = view.findViewById(R.id.img_check_id)
            var tv_card_number: TextView = view.findViewById(R.id.tv_card_number)
            var card_select_id: CardView = view.findViewById(R.id.card_select_id)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_card_name.text = account_list[position].name
            //holder.iv_card_type.text="*******"+account_list[position].last4
            holder.tv_card_number.text = "*****-" + account_list[position].last4

            holder.card_select_id.setOnClickListener {
                selectedPosition = position
                notifyDataSetChanged()
                cards_default_source_id = ""
            }

            if (selectedPosition == position) {
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_list_bg));
                (holder.img_check_id.getDrawable() as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_check_id.getDrawable() as Animatable).start()
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_normal_bg));
                holder.img_check_id.visibility = View.INVISIBLE
            }

            if (cards_default_source_id.equals(array_savedcards_list[position].id)) {
                card_global_id = array_savedcards_list[position].id!!.toString()
                (holder.img_check_id.getDrawable() as Animatable).start()
                holder.img_check_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }

        }

    }


    private fun Bank_full_dilag() {
        bank_full_dialog = Dialog(this, R.style.DialogTheme)
        bank_full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_full_dialog.setCancelable(false);

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        bank_full_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        bank_full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);

        bank_full_dialog.setContentView(R.layout.bank_payment_dialog)
        //btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        img_bank_close_id = bank_full_dialog.findViewById<ImageView>(R.id.img_bank_close_id)
        btn_saved_bank_pay = bank_full_dialog.findViewById<Button>(R.id.btn_saved_bank_pay)
        id_empty_data_id = bank_full_dialog.findViewById<TextView>(R.id.id_empty_data_id)

        rv_account_details_id =
            bank_full_dialog.findViewById<RecyclerView>(R.id.rv_account_details_id)

        img_bank_close_id.setOnClickListener {
            bank_full_dialog.dismiss()
        }
        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject

        AccountsListApi(jObject)

        btn_saved_bank_pay.setOnClickListener {

            endDate = et_end_date.text.toString()

            startDate = et_start_date.text.toString()!!
            if (type_project.equals("project")) {
                endDate = et_end_date.text.toString()

                startDate = et_start_date.text.toString()!!
                /*{
"userId": "5f2ba99a4cd097415e84c383",
"projectId": "0",
"orgId": "5f2ae2954ae9ea38e66263e2",
"amount": 1000,
"paymentType": 2,
"isReccurring": true,
"cardId": "",
"bankAccountId":"ba_1HMAmWKijxtVwMc3CTDBoHH8",
"cardToken": "",
"reccurringType": "day",
"startDate": "09-01-2020",
"stripeFeePercentage": 3.2,
"stripeFeeAmount": 0.16,
"stripeExtraCharges": 0.2,
"piesaFeePercentage": 0.3,
"piesaFeeAmount": 0.34,
"piesaExtraCharges":0.2,
"isHasEnd": true,
"endDate": "09-06-2020"
}*/

                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                )
                obj.put("projectId", type_id_stg)
                obj.put("orgId", organization_id)
                obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                obj.put("paymentType", 2)
                obj.put("bankAccountId", bank_card_token)
                obj.put("isReccurring", true)
                obj.put("cardId", "")
                obj.put("cardToken", "")
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_bank_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("cardSave", true)
                obj.put("startDate", startDate)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                obj.put("isHasEnd", check_box)
                obj.put("reccurringType", freq_mode_id)
                if (check_box.equals(true)) {
                    obj.put("endDate", endDate)
                } else {
                    obj.put("endDate", startDate)
                }
                if (freq_mode.equals("Weekly")) {
                    obj.put("weekDay", week_freq_id)
                } else if (freq_mode.equals("Monthly")) {
                    obj.put("dateNumber", day_freq_id.toInt())
                }
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("project_bankrecurring_input", jObject.toString())
                StripePaymentApi(jObject)
            }

            if (type_project.equals("generalfund")) {
                val obj = JSONObject()
                obj.put(
                    "userId",
                    sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                )
                obj.put("projectId", "0")
                obj.put("orgId", organization_id)
                obj.put("amount", (et_amount.text.toString()).toDouble() * 100)
                obj.put("paymentType", 2)
                obj.put("bankAccountId", bank_card_token)
                obj.put("isReccurring", true)
                obj.put("stripeFeePercentage", global_stripe_percent)
                obj.put("stripeFeeAmount", global_stripe_total_fee)
                obj.put("stripeExtraCharges", global_bank_ex_amount)
                obj.put("piesaFeePercentage", global_piesa_percent)
                obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                obj.put("piesaFeeAmount", global_piesa_total_fee)
                obj.put("isHasEnd", check_box)
                obj.put("startDate", startDate)
                obj.put("reccurringType", freq_mode_id)
                if (check_box.equals(true)) {
                    obj.put("endDate", endDate)
                } else {
                    obj.put("endDate", startDate)
                }
                if (freq_mode.equals("Weekly")) {
                    obj.put("weekDay", week_freq_id)
                } else if (freq_mode.equals("Monthly")) {
                    obj.put("dateNumber", day_freq_id.toInt())
                }
                obj.put("cardSave", true)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("recurbankgeneralfund_input", jObject.toString())
                StripePaymentApi(jObject)
            }
        }

    }

    private fun AccountsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_account_list = ArrayList()
        array_account_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<BankAccountListPojo>,
                response: Response<BankAccountListPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {
                        // toast( response!!.body()!!.result!!)
                        if (response.body()!!.bankAccountList!!.data!!.size.equals(0)) {
                            id_empty_data_id.visibility = View.VISIBLE
                        }
                        for (i in 0 until response.body()!!.bankAccountList!!.data!!.size) {
                            array_account_list.add(response.body()!!.bankAccountList!!.data!![i])
                        }
                        rv_account_details_id.visibility = View.VISIBLE
                        bank_default_source_id =
                            response.body()!!.customerDetails!!.default_source!!

                        val adapter = BankListAdapter(
                            this@CreateRecurringActivity,
                            array_account_list,
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString(),
                            bank_default_source_id
                        )
                        rv_account_details_id.setHasFixedSize(true)
                        rv_account_details_id.adapter = adapter
                        adapter.notifyDataSetChanged()

                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    inner class BankListAdapter(
        context: Context,
        val account_list: ArrayList<BankAccountsArrayList>,
        val user_id: String,
        var bank_default_source_id: String
    ) :
        RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_wallet_accounts, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_bank_name_id.text = account_list[position].bank_name
            holder.tv_ac_no_id.text = "*******" + account_list[position].last4
            holder.tv_ac_holder_name_id.text = account_list[position].account_holder_name
            holder.img_delete_id.visibility = View.GONE

            if (account_list[position].status.equals("new")) {
                //holder.img_verify_id.setImageDrawable(mContext!!.resources.getDrawable(R.drawable.ic_close))
                //holder.tv_verify_id.visibility=View.VISIBLE
                holder.cv_setupwallet.visibility = View.GONE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.tv_verify_id.visibility = View.GONE
                holder.img_verify_id.visibility = View.GONE
                holder.cv_setupwallet.visibility = View.VISIBLE
                holder.ll_verify_amount_id.visibility = View.GONE


            }
            holder.cv_setupwallet.setOnClickListener {
                bank_card_token = account_list[position].id!!
                btn_saved_bank_pay.visibility = View.VISIBLE
                selectedPosition = position
                holder.img_verify_id.visibility = View.GONE

                notifyDataSetChanged()
                bank_default_source_id = ""
            }

            if (selectedPosition == position) {
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_list_bg));
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_normal_bg));
                holder.img_verify_id.visibility = View.INVISIBLE
            }

            if (bank_default_source_id.equals(account_list[position].id)) {
                bank_card_token = account_list[position].id!!.toString()
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_card_pay.visibility = View.VISIBLE
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var cv_setupwallet: CardView = view.findViewById(R.id.cv_setupwallet)
            var img_verify_id: ImageView = view.findViewById(R.id.img_verify_id)
            var tv_bank_name_id: TextView = view.findViewById(R.id.tv_bank_name_id)
            var tv_ac_no_id: TextView = view.findViewById(R.id.tv_ac_no_id)
            var tv_ac_holder_name_id: TextView = view.findViewById(R.id.tv_ac_holder_name_id)
            var tv_verify_id: TextView = view.findViewById(R.id.tv_verify_id)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var ll_verify_amount_id: LinearLayout = view.findViewById(R.id.ll_verify_amount_id)
        }

    }


}
