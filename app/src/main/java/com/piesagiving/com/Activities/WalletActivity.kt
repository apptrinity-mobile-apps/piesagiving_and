package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@SuppressLint("SetTextI18n")
class WalletActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var cv_setupwallet: CardView
    lateinit var rv_wallet_accounts: RecyclerView
    lateinit var tv_wallet_account_empty: TextView
    lateinit var lm_spl_causes: LinearLayoutManager
    lateinit var rv_saved_cards_id: RecyclerView
    lateinit var ll_saved_cards_id: LinearLayout
    lateinit var ll_saved_bank_ac_id: LinearLayout
    lateinit var tv_header_saved_methods: TextView
    lateinit var ll_pending_verification: LinearLayout
    lateinit var rv_pending_verification: RecyclerView

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    lateinit var loading_dialog: Dialog

    lateinit var array_account_list: ArrayList<BankAccountsArrayList>
    lateinit var array_pending_account_list: ArrayList<BankAccountsArrayList>
    lateinit var array_savedcards_list: ArrayList<SavedCardsArrayDataResponse>

    var saved_cards_size: Int = 0
    var saved_bank_size: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_wallet)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        cv_setupwallet.setOnClickListener {
            val intent = Intent(this@WalletActivity, SetUpWalletActivity::class.java)
            intent.putExtra("amount", "200")
            intent.putExtra("organization_id", "1")
            intent.putExtra("display_name", "1")
            intent.putExtra("from_screen", "1")
            intent.putExtra("type_project", "1")
            intent.putExtra("projectId", "1")
            startActivity(intent)
        }

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        cv_setupwallet = findViewById(R.id.cv_setupwallet)
        rv_wallet_accounts = findViewById(R.id.rv_wallet_accounts)
        tv_wallet_account_empty = findViewById(R.id.tv_wallet_account_empty)
        rv_saved_cards_id = findViewById(R.id.rv_saved_cards_id)
        ll_saved_cards_id = findViewById(R.id.ll_saved_cards_id)
        ll_saved_bank_ac_id = findViewById(R.id.ll_saved_bank_ac_id)
        tv_header_saved_methods = findViewById(R.id.tv_header_saved_methods)
        ll_pending_verification = findViewById(R.id.ll_pending_verification)
        rv_pending_verification = findViewById(R.id.rv_pending_verification)
        isNetworkAvailable = cd.isConnectingToInternet

        val layoutManager =
            LinearLayoutManager(this@WalletActivity, RecyclerView.VERTICAL, false)
        rv_saved_cards_id.layoutManager = layoutManager
        rv_saved_cards_id.setHasFixedSize(true)

        lm_spl_causes = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_wallet_accounts.layoutManager = lm_spl_causes

        val pendingLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_pending_verification.layoutManager = pendingLayoutManager
        rv_pending_verification.hasFixedSize()

        rv_wallet_accounts.addOnItemTouchListener(
            RecyclerItemClickListener(this,
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                    }
                })
        )

/*{"userId":"5ef9d77d22514034db58c34d"}*/
        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        AccountsListApi(jObject)
        SavedCardsListApi(jObject)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this@WalletActivity, DashBoardActivity::class.java)
        intent.putExtra("to_fragment", "wallet")
        intent.putExtra("cat_id", "")
        intent.putExtra("show_lock", "no")
        startActivity(intent)
        finish()
    }

    private fun AccountsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_account_list = ArrayList()
        array_pending_account_list = ArrayList()
        array_account_list.clear()
        array_pending_account_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<BankAccountListPojo>,
                response: Response<BankAccountListPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response.body()!!.responseStatus == 1) {
                        val data = response.body()!!.bankAccountList!!.data!!
                        saved_bank_size = response.body()!!.bankAccountList!!.data!!.size
                        if (saved_bank_size == 0) {
                            ll_saved_bank_ac_id.visibility = View.GONE
                        } else {
                            ll_saved_bank_ac_id.visibility = View.VISIBLE
                        }

                        if (saved_bank_size > 0) {
                            tv_wallet_account_empty.visibility = View.GONE
                            for (i in 0 until saved_bank_size) {
                                if (data[i].status.toString() == "new") {
                                    array_pending_account_list.add(data[i])
                                } else /*if (data[i].status.toString() == "verified")*/ {
                                    array_account_list.add(data[i])
                                }
//                                array_account_list.add(response.body()!!.bankAccountList!!.data!![i])
                            }
                            if (array_account_list.size > 0) {
                                rv_wallet_accounts.visibility = View.VISIBLE
                                val adapter = WalletAccountsAdapter(
                                    this@WalletActivity,
                                    array_account_list,
                                    sessionManager.getUserDetails()[SessionManager.ID_KEY]
                                        .toString()
                                )
                                rv_wallet_accounts.setHasFixedSize(true)
                                rv_wallet_accounts.adapter = adapter
                                adapter.notifyDataSetChanged()
                            }
                            if (array_pending_account_list.size > 0) {
                                ll_pending_verification.visibility = View.VISIBLE
                                val adapter = WalletAccountsAdapter(
                                    this@WalletActivity,
                                    array_pending_account_list,
                                    sessionManager.getUserDetails()[SessionManager.ID_KEY].toString()
                                )
                                rv_pending_verification.adapter = adapter
                                adapter.notifyDataSetChanged()
                            } else {
                                ll_pending_verification.visibility = View.GONE
                            }
                        } else {
                            // tv_wallet_account_empty.visibility = View.VISIBLE
                        }
                        SavedListEmpty()

                    } else {
                        toast(response.body()!!.result!!)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


    inner class WalletAccountsAdapter(
        context: Context,
        val account_list: ArrayList<BankAccountsArrayList>,
        val user_id: String
    ) :
        RecyclerView.Adapter<WalletAccountsAdapter.ViewHolder>() {

        private var mContext: Context? = null

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_wallet_accounts, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_bank_name_id.text = account_list[position].bank_name
            holder.tv_ac_no_id.text = "*******" + account_list[position].last4
            holder.tv_ac_holder_name_id.text = account_list[position].account_holder_name

            if (account_list[position].status.equals("new")) {
                //holder.img_verify_id.setImageDrawable(mContext!!.resources.getDrawable(R.drawable.ic_close))
                holder.tv_verify_id.visibility = View.VISIBLE
                holder.img_verify_id.visibility = View.GONE
                holder.img_delete_id.visibility = View.GONE
                holder.ll_verify_amount_id.visibility = View.VISIBLE
            } else {
                (holder.img_verify_id.drawable as Animatable).start()
                holder.tv_verify_id.visibility = View.GONE
                holder.ll_verify_amount_id.visibility = View.GONE
                holder.img_verify_id.visibility = View.VISIBLE
                holder.img_delete_id.visibility = View.VISIBLE
            }

            holder.tv_verify_id.setOnClickListener {

                val value1 = holder.et_fst_amount_id.text.toString().trim()
                val value2 = holder.et_sec_amount_id.text.toString().trim()

                if (value1 != "" && value2 != "") {
                    val amount_1 = value1.toDouble() * 100
                    val amount_2 = value2.toDouble() * 100
                    val obj = JSONObject()
                    obj.put("userId", user_id)
                    obj.put("bankAccountId", account_list[position].id)
                    obj.put("verifyAmount1", amount_1.toInt())
                    obj.put("verifyAmount2", amount_2.toInt())
                    val jParser = JsonParser()
                    val jObject = jParser.parse(obj.toString()) as JsonObject
                    Log.e("verify_jObject", jObject.toString())
                    VerifyApi(jObject)
                    holder.progress_id.visibility = View.VISIBLE
                    holder.tv_verify_id.visibility = View.GONE
                    holder.ll_verify_amount_id.visibility = View.GONE
                }
            }

            holder.img_delete_id.setOnClickListener {
                val obj = JSONObject()
                obj.put("userId", sessionManager.getUserDetails()[SessionManager.ID_KEY].toString())
                obj.put("sourceId", account_list[position].id!!)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                DeleteCard(jObject)
            }

        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var img_verify_id: ImageView = view.findViewById(R.id.img_verify_id)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var tv_bank_name_id: TextView = view.findViewById(R.id.tv_bank_name_id)
            var tv_ac_no_id: TextView = view.findViewById(R.id.tv_ac_no_id)
            var tv_ac_holder_name_id: TextView = view.findViewById(R.id.tv_ac_holder_name_id)
            var tv_verify_id: TextView = view.findViewById(R.id.tv_verify_id)
            var progress_id: ProgressBar = view.findViewById(R.id.progress_id)
            var et_fst_amount_id: EditText = view.findViewById(R.id.et_fst_amount_id)
            var et_sec_amount_id: EditText = view.findViewById(R.id.et_sec_amount_id)
            var ll_verify_amount_id: LinearLayout = view.findViewById(R.id.ll_verify_amount_id)
        }

        private fun VerifyApi(jsonObj: JsonObject) {

            val apiInterface = ApiInterface.create()
            val call = apiInterface.BankAccountVerifyApi(jsonObj)
            Log.e("@response", call.toString())
            call.enqueue(object : Callback<BankAccountVerificationPojo> {
                override fun onFailure(call: Call<BankAccountVerificationPojo>, t: Throwable) {
                    Log.e("responseStatus", "Exception  " + call + "  " + t)
                }

                override fun onResponse(
                    call: Call<BankAccountVerificationPojo>,
                    response: Response<BankAccountVerificationPojo>?
                ) {
                    try {
                        Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                        if (response.body()!!.responseStatus == 1) {
                            mContext!!.toast(response.body()!!.result!!)
                            val intent = Intent(mContext, AccountVerificationSuccess::class.java)
                            intent.putExtra("message", response.body()!!.result!!)
                            intent.putExtra(
                                "account_no",
                                response.body()!!.accountDetails!!.last4!!
                            )
                            startActivity(intent)
                        } else {
                            mContext!!.toast(response.body()!!.result!!)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        }
    }

    /*Saved cards List API*/
    private fun SavedCardsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_savedcards_list = ArrayList()
        array_savedcards_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.savedCardsList(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<SavedCardListResponse> {
            override fun onFailure(call: Call<SavedCardListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<SavedCardListResponse>,
                response: Response<SavedCardListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    val resp = response!!.body()
                    Log.e("STRIPERESPOMNSEDONATION", resp.toString())
                    if (response.body()!!.responseStatus == "1") {
                        saved_cards_size = response.body()!!.savedCardsList!!.data!!.size
                        if (saved_cards_size == 0) {
                            ll_saved_cards_id.visibility = View.GONE
                        } else {
                            ll_saved_cards_id.visibility = View.VISIBLE
                        }
                        for (i in 0 until saved_cards_size) {
                            array_savedcards_list.add(response.body()!!.savedCardsList!!.data!![i])
                        }
                        rv_saved_cards_id.visibility = View.VISIBLE
                        val adapter =
                            SavedCardListAdapter(this@WalletActivity, array_savedcards_list)
                        rv_saved_cards_id.adapter = adapter
                        adapter.notifyDataSetChanged()
                        SavedListEmpty()
                    } else {
                        toast(response.body()!!.result!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    /*Cards list End*/
    inner class SavedCardListAdapter(
        context: Context,
        val account_list: ArrayList<SavedCardsArrayDataResponse>
    ) :
        RecyclerView.Adapter<SavedCardListAdapter.ViewHolder>() {

        private var mContext: Context? = null

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_savedcards, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_card_name: TextView = view.findViewById(R.id.tv_card_name)
            var tv_card_number: TextView = view.findViewById(R.id.tv_card_number)
            var tv_card_type: TextView = view.findViewById(R.id.tv_card_type)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tv_card_name.text = account_list[position].name
            //holder.iv_card_type.text="*******"+account_list[position].last4
            holder.tv_card_number.text = "**** **** **** " + account_list[position].last4
            holder.tv_card_type.text = "Card Type: " + account_list[position].brand.toString()
            holder.img_delete_id.visibility = View.VISIBLE

            holder.img_delete_id.setOnClickListener {
                /*{
                    "userId": "5f042ee4e5345fd2359d77b7",
                    "sourceId": "card_1H4nKMKijxtVwMc3V9vL87Zj"
                }*/
                val obj = JSONObject()
                obj.put("userId", sessionManager.getUserDetails()[SessionManager.ID_KEY].toString())
                obj.put("sourceId", account_list[position].id!!)
                val jParser = JsonParser()
                val jObject = jParser.parse(obj.toString()) as JsonObject
                Log.e("delete_json", jObject.toString())
                DeleteCard(jObject)
            }
        }
    }

    private fun DeleteCard(jsonObj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.RemoveCardBankApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<NewBankAccountPojo> {
            override fun onFailure(call: Call<NewBankAccountPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<NewBankAccountPojo>,
                response: Response<NewBankAccountPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    if (response!!.body()!!.responseStatus == 1) {
                        Log.e("Delete_success", "Deleted")
                        val obj = JSONObject()
                        obj.put(
                            "userId",
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
                        )
                        val jParser = JsonParser()
                        val jObject = jParser.parse(obj.toString()) as JsonObject
                        AccountsListApi(jObject)
                        SavedCardsListApi(jObject)
                    } else {
                        toast(response.body()!!.result!!)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    fun SavedListEmpty() {
        if (saved_cards_size == 0 && saved_bank_size == 0) {
            tv_wallet_account_empty.visibility = View.VISIBLE
        } else {
            tv_wallet_account_empty.visibility = View.GONE
        }
        if (saved_bank_size != 0 || saved_cards_size != 0) {
            tv_header_saved_methods.visibility = View.VISIBLE
        } else if (saved_bank_size == 0 && saved_cards_size == 0) {
            tv_header_saved_methods.visibility = View.GONE
        }
    }

}
