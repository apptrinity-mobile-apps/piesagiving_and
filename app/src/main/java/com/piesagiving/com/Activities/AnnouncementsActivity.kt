package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.OrganizationAnnouncementsResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

@SuppressLint("SetTextI18n")
class AnnouncementsActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""

    private lateinit var iv_back: ImageView
    private lateinit var loading_dialog: Dialog
    private lateinit var rv_announcements: RecyclerView
    private lateinit var tv_empty_announcements: TextView
    private lateinit var organizationsList: ArrayList<OrganizationAnnouncementsResponse>
    private lateinit var adapter: OrganizationsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_announcements)

        initialize()
        if (isNetworkAvailable) {
            val obj = JSONObject()
            obj.put("userId", userId)
            val jParser = JsonParser()
            val finalObj = jParser.parse(obj.toString()) as JsonObject
            getOrganizations(finalObj)
        } else {
            Toast.makeText(
                this@AnnouncementsActivity, getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        loadingDialog()

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)
        tv_empty_announcements = findViewById(R.id.tv_empty_announcements)
        rv_announcements = findViewById(R.id.rv_announcements)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_announcements.layoutManager = layoutManager
        rv_announcements.hasFixedSize()
        organizationsList = ArrayList()
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getOrganizations(final_object: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getFollowingOrganizationsAnnouncementsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@AnnouncementsActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                    tv_empty_announcements.visibility = View.GONE
                    rv_announcements.visibility = View.VISIBLE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        organizationsList = resp.org_list!!
                        adapter =
                            OrganizationsListAdapter(this@AnnouncementsActivity, organizationsList)
                        rv_announcements.adapter = adapter
                        adapter.notifyDataSetChanged()
                        if (organizationsList.size > 0) {
                            tv_empty_announcements.visibility = View.GONE
                            rv_announcements.visibility = View.VISIBLE
                        } else {
                            tv_empty_announcements.visibility = View.VISIBLE
                            rv_announcements.visibility = View.GONE
                        }
                    } else if (resp.responseStatus == "0") {
                        tv_empty_announcements.visibility = View.VISIBLE
                        rv_announcements.visibility = View.GONE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class OrganizationsListAdapter(
        context: Context, list: ArrayList<OrganizationAnnouncementsResponse>
    ) :
        RecyclerView.Adapter<OrganizationsListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<OrganizationAnnouncementsResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_favourites, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_name.text = mList!![position].orgName.toString()
            holder.tv_address.visibility = View.GONE
            if (mList!![position].profilePic == "") {
                Picasso.with(mContext!!).load(R.drawable.ic_profile)
                    .into(holder.iv_icon)
            } else {
                Picasso.with(mContext!!).load(mList!![position].profilePic.toString())
                    .error(R.drawable.ic_profile).into(holder.iv_icon)
            }
            holder.cv_header.setOnClickListener {
                val intent = Intent(mContext!!, AnnouncementsListActivity::class.java)
                intent.putExtra("orgName", mList!![position].orgName.toString())
                intent.putExtra("orgId", mList!![position].orgId.toString())
                mContext!!.startActivity(intent)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_name = view.findViewById(R.id.tv_name) as TextView
            var tv_address = view.findViewById(R.id.tv_address) as TextView
            var iv_icon = view.findViewById(R.id.iv_icon) as ImageView
            var cv_header = view.findViewById(R.id.cv_header) as CardView
        }
    }

}