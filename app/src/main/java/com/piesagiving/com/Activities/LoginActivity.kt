package com.piesagiving.com.Activities

import android.Manifest
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StyleSpan
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.LoginPojo
import com.piesagiving.com.ApiInterface.StripePaymentPojo
import com.piesagiving.com.Helpers.*
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {

    lateinit var ll_details_id: LinearLayout
    lateinit var iv_back: ImageView
    lateinit var tv_submit: TextView
    lateinit var tv_forgot_password: TextView
    lateinit var tv_register: TextView
    lateinit var et_email: EditText
    lateinit var et_password: EditText

    var device_token_stg = ""
    var device_type_stg = ""

    var dataStorage: DataStorage = DataStorage()

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var sessionSplash: SessionSplash
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    lateinit var loading_dialog: Dialog

    var login_city = ""
    private val PERMISSION_REQUEST_CODE = 23


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login)
        sessionSplash = SessionSplash(this)
        from_intent = intent.getStringExtra("from_intent")!!
        initialize()

        device_token_stg = dataStorage.getFcmToken(this)
        device_type_stg = "Android"

        FirebaseInstanceId.getInstance().getInstanceId()
            .addOnSuccessListener(this, object : OnSuccessListener<InstanceIdResult?> {
                override fun onSuccess(instanceIdResult: InstanceIdResult?) {
                    val newToken: String = instanceIdResult!!.getToken()
                    Log.e("NewFcm Token", newToken)
                }
            })

        // Log.d("LoginActivity", "fcm token : $device_token_stg")

        // delete after test
        // et_email.setText("test@gmail.com")
        // et_password.setText("123456")
        // login to app
        tv_submit.setOnClickListener {
            Log.e("login_city", login_city)
            if (!checkPermission()) {
                requestPermission()
            } else {
                Log.e("login_lll", "login")
                EnableGPSAutoMatically("login")
            }


            if (et_email.text.toString().isEmpty()) {
                Toast.makeText(this@LoginActivity, "Email cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } else if (et_password.text.toString().isEmpty()) {
                Toast.makeText(this@LoginActivity, "password cannot be empty!", Toast.LENGTH_SHORT)
                    .show()
            } /*else if (!isValidEmail(et_email.text.toString())) {
                Toast.makeText(this@LoginActivity, "Valid Email Required", Toast.LENGTH_SHORT)
                    .show()
            }*/ else {
                if (isNetworkAvailable) {

                    val obj = JSONObject()
                    obj.put("username", et_email.text.toString().trim())
                    obj.put("password", et_password.text.toString().trim())
                    val jParser = JsonParser()
                    val jObject = jParser.parse(obj.toString()) as JsonObject
                    Log.e("login_input_data", jObject.toString())

                    LoginRequest(jObject)
                } else {
                    Toast.makeText(
                        this@LoginActivity,
                        resources.getString(R.string.no_network_available),
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        }

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        // navigate to forgot password activity
        tv_forgot_password.setOnClickListener {
            val intent = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        // navigate to register profile
        tv_register.setOnClickListener {
            val intent = Intent(this@LoginActivity, RegisterAccountActivity::class.java)
            startActivity(intent)
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        ll_details_id = findViewById(R.id.ll_details_id)
        iv_back = findViewById(R.id.iv_back)
        tv_submit = findViewById(R.id.tv_submit)
        et_email = findViewById(R.id.et_email)
        et_password = findViewById(R.id.et_password)
        tv_forgot_password = findViewById(R.id.tv_forgot_password)
        tv_register = findViewById(R.id.tv_register)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

        if (!checkPermission()) {

            requestPermission()

        } else {
            Log.e("login_lll", "login")
            EnableGPSAutoMatically("login")

        }

    }

    private fun isValidEmail(email: String): Boolean {
        val emailPattern =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    override fun onBackPressed() {
        if (from_intent == "") {
            super.onBackPressed()
        } else if (from_intent == "reset") {
            // navigating activity to splash to avoid default onbackpress
            val intent = Intent(this@LoginActivity, SignUpSignInActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun checkPermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(
            applicationContext,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        return result == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        val text =
            "Piesa Giving needs to access your location to help you find Places of worship or charities near you."
        val message = SpannableString(text)
        val boldSpan = StyleSpan(Typeface.BOLD)
        message.setSpan(boldSpan, 0, 13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        AlertDialog.Builder(this)
            .setTitle("")
            .setMessage(message)
            .setPositiveButton("Allow", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                    ActivityCompat.requestPermissions(
                        this@LoginActivity,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSION_REQUEST_CODE
                    )
                }
            })
            .setNegativeButton("Dismiss", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    dialog!!.dismiss()
                }
            })
            .show()
        /*ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_CODE
        )*/

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0) {

                val locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED

                if (locationAccepted)
                    Snackbar.make(
                        ll_details_id,
                        "Permission Granted, Now you can access location.",
                        Snackbar.LENGTH_LONG
                    ).show()
                else {

                    Snackbar.make(
                        ll_details_id,
                        "Permission Denied, You cannot access location.",
                        Snackbar.LENGTH_LONG
                    ).show()

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                            // showMessageOKCancel("You need to allow access to both the permissions",
                            DialogInterface.OnClickListener { dialog, which ->
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(
                                        arrayOf(
                                            Manifest.permission.ACCESS_FINE_LOCATION
                                        ),
                                        PERMISSION_REQUEST_CODE
                                    )
                                }
                            }
                            return
                        }
                    }

                }
            }
        }
    }

    private fun LoginRequest(json_obj: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.loginApi(json_obj)
        call.enqueue(object : Callback<LoginPojo> {
            override fun onFailure(call: Call<LoginPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<LoginPojo>,
                response: Response<LoginPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        /* Toast.makeText(
                             this@LoginActivity,
                             "Signed In successfully!",
                             Toast.LENGTH_SHORT
                         ).show()*/
                        val resp = response.body()
                        val notifications = resp!!.result!!.notifications
                        sessionManager.loginSession(
                            firstName = resp.result!!.firstName.toString(),
                            lastName = resp.result.lastName.toString(),
                            email = resp.result.email.toString(),
                            id = resp.result.id.toString(),
                            state = resp.result.state.toString(),
                            country = resp.result.country.toString(),
                            city = resp.result.city.toString(),
                            street = resp.result.streetAddress.toString(),
                            zip = resp.result.zipcode.toString(),
                            terms = resp.result.terms.toString(),
                            privacy = resp.result.privacy.toString(),
                            language = resp.result.language.toString(),
                            geotag = resp.result.geotagging.toString(),
                            faceid = resp.result.faceId.toString(),
                            birthday = resp.result.birthday.toString(),
                            profilePic = resp.result.profilePic.toString(),
                            userName = resp.result.userName.toString(),
                            appPin = resp.result.devicePinCode.toString()
                        )

                        sessionManager.notificationSession(
                            recurringdonation = notifications!!.recurringDonationReminder.toString(),
                            newpiesarecommend = notifications!!.newPiesaRecommendation.toString(),
                            trendingnow = notifications.newTrendingNow.toString(),
                            someonefollowed = notifications.someoneFollowedYou.toString(),
                            haptics = notifications!!.hapticss.toString(),
                            newProjectChurch = notifications.newProjectChurch.toString(),
                            newProjectCharity = notifications.newProjectCharity.toString(),
                            newSpecialCausesChurch = notifications.newSpecialCausesChurch.toString(),
                            newSpecialCausesCharity = notifications.newSpecialCausesCharity.toString(),
                            DonationProcessedEmail = notifications.DonationProcessedEmail.toString(),
                            DonationProcessedPhone = notifications.donationProcessedPhone.toString(),
                            DonationProcessingEmail = notifications.donationProcessingEmail.toString(),
                            DonationProcessingPhone = notifications.donationProcessingPhone.toString()
                        )


                        /*{
"userId":"5ee4d4f244d1760dacd0a0d4",
"type": "web",
"location":"nizampet,hyderabad"
}*/
                        if (isNetworkAvailable) {

                            val obj = JSONObject()
                            obj.put("userId", resp.result.id.toString())
                            obj.put("type", "mobile")
                            obj.put("location", login_city)
                            val jParser = JsonParser()
                            val jObject = jParser.parse(obj.toString()) as JsonObject
                            Log.e("login_input_data", jObject.toString())

                            SigninUpdateResponse(
                                jObject
                            )

                            val account_skip = sessionSplash.getSplashScreen()
                            val account_status = account_skip[SessionSplash.BANK_AC_SKIP]!!
                            Log.e("LOGINSPLASHVALUE", account_status)

                            if (account_status.equals("1")) {
                                val intent =
                                    Intent(this@LoginActivity, DashBoardActivity::class.java)
                                intent.putExtra("to_fragment", "")
                                intent.putExtra("cat_id", "")
                                intent.putExtra("show_lock", "no")
                                startActivity(intent)
                                finish()
                            } else {
                                val intent =
                                    Intent(this@LoginActivity, SplashWelcomeActivity::class.java)
                                startActivity(intent)
                                finish()
                            }


                        } else {
                            Toast.makeText(
                                this@LoginActivity,
                                resources.getString(R.string.no_network_available),
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }


                    } else if (response.body()!!.responseStatus == 0) {
                        Toast.makeText(
                            this@LoginActivity,
                            "Email or Password Mismatch!",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (response.body()!!.responseStatus == 3) {
                        Toast.makeText(
                            this@LoginActivity,
                            response.body()!!.message.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            this@LoginActivity,
                            "Signed In Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun SigninUpdateResponse(json_obj: JsonObject) {

        loading_dialog.show()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.SigninUpdateApi(json_obj)
        call.enqueue(object : Callback<StripePaymentPojo> {
            override fun onFailure(call: Call<StripePaymentPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<StripePaymentPojo>,
                response: Response<StripePaymentPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    Log.e("update", response!!.body()!!.responseStatus!!.toString())


                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun EnableGPSAutoMatically(login_signup: String) {
        var googleApiClient: GoogleApiClient? = null
        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this@LoginActivity)
                .addOnConnectionFailedListener(this@LoginActivity).build()
            googleApiClient!!.connect()
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = (30 * 1000).toLong()
            locationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            // **************************
            builder.setAlwaysShow(true) // this is the key ingredient
            // **************************

            val result = LocationServices.SettingsApi
                .checkLocationSettings(googleApiClient, builder.build())
            result.setResultCallback(object : ResultCallback<LocationSettingsResult> {
                override fun onResult(result: LocationSettingsResult) {
                    val status = result.status
                    val state = result
                        .locationSettingsStates
                    when (status.statusCode) {
                        LocationSettingsStatusCodes.SUCCESS -> {
                            val gpsTracker = GPSTracker(this@LoginActivity)

                            val Dlatitude = gpsTracker.getLatitude()
                            val Dlongitude = gpsTracker.getLongitude()
                            LocationFullAddress(Dlatitude, Dlongitude)


                        }
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            // toast("GPS is not on")
                            // Location settings are not satisfied. But could be
                            // fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling
                                // startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(this@LoginActivity, 1000)

                            } catch (e: IntentSender.SendIntentException) {
                                // Ignore the error.
                            }

                        }
                        LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> toast("Setting change not allowed")
                    }
                }
            })
        }
    }

    fun LocationFullAddress(lat: Double, long: Double) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(this, Locale.getDefault())


        try {
            addresses = geocoder.getFromLocation(
                lat,
                long,
                1
            ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            // val address = addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

            val city = addresses[0].locality
            val state = addresses[0].adminArea
            val country = addresses[0].countryName
            val postalCode = addresses[0].postalCode
            val knownName = addresses[0].featureName

            // dllSessionManager.gpsLocationDetails(country, state, city)

            Log.e("citiesss", city + "---" + country)
            login_city = city.toString()

        } catch (e: java.lang.Exception) {
            //  getAdsLisrAPI("India", "Telangana", "Hyderabad", "Login")
            login_city = "Unknown"


            Log.e("login_ad_api", "dismiss")
        }

    }

    private fun toast(message: String) {
        try {
            Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            // log("Window has been closed")
        }

    }

    override fun onConnected(p0: Bundle?) {}

    override fun onConnectionSuspended(p0: Int) {}

    override fun onConnectionFailed(p0: ConnectionResult) {}

}
