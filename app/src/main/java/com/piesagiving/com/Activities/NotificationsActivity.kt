package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.GetNotificationSettingsResponse
import com.piesagiving.com.ApiInterface.ProfileDetailsPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@SuppressLint("SetTextI18n")
class NotificationsActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var switch_muteallnoti: SwitchMaterial
    lateinit var switch_recurringdonation: SwitchMaterial
    lateinit var switch_newpiesarecommend: SwitchMaterial
    lateinit var switch_trendingnow: SwitchMaterial
    lateinit var switch_someonefollowed: SwitchMaterial
    lateinit var switch_haptics: SwitchMaterial
    lateinit var tv_muteallnoti: TextView
    lateinit var tv_recurringdonation: TextView
    lateinit var tv_newpiesarecommend: TextView
    lateinit var tv_trendingnow: TextView
    lateinit var tv_someonefollowed: TextView
    lateinit var tv_haptics: TextView
    lateinit var ll_newproject: LinearLayout
    lateinit var ll_newspecialcase: LinearLayout
    lateinit var ll_confirmationemails: LinearLayout
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""

    var from_intent = ""
    private var recurringdonation_val = ""
    private var newpiesarecommend_val = ""
    private var trendingnow_val = ""
    private var someonefollowed_val = ""
    private var haptics_val = ""
    private var newProjectChurch = ""
    private var newProjectCharity = ""
    private var newSpecialCausesChurch = ""
    private var newSpecialCausesCharity = ""
    private var donationConfirmationEmail = ""
    private var upcomingDonationEmail = ""
    private var DonationProcessedEmail = ""
    private var donationProcessedPhone = ""
    private var DonationProcessingEmail = ""
    private var donationProcessingPhone = ""

    lateinit var notification_list: HashMap<String, String>

    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_notifications)
        initialize()

        notification_list = sessionManager.getUserNotifications
        recurringdonation_val = notification_list[SessionManager.NOTIFY_RECURRINDONATION_KEY]!!
        newpiesarecommend_val = notification_list[SessionManager.NOTIFY_NEWPIESARECOMMEND_KEY]!!
        trendingnow_val = notification_list[SessionManager.NOTIFY_TRENDINGNOW_KEY]!!
        newProjectChurch = notification_list[SessionManager.NOTIFY_NEWPROJECTCHURCH_KEY]!!
        haptics_val = notification_list[SessionManager.NOTIFY_HAPTICS_KEY]!!
        newProjectCharity = notification_list[SessionManager.NOTIFY_NEWPROJECTCHARITY_KEY]!!
        someonefollowed_val = notification_list[SessionManager.NOTIFY_SOMEONEFOLLOWED_KEY]!!
        newSpecialCausesChurch =
            notification_list[SessionManager.NOTIFY_NEWSPECIALCAUSECHURCH_KEY]!!
        newSpecialCausesCharity =
            notification_list[SessionManager.NOTIFY_NEWSPECIALCAUSECHARITY_KEY]!!
        DonationProcessedEmail =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSEDEMAIL_KEY]!!
        donationProcessedPhone =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSEDPHONE_KEY]!!
        DonationProcessingEmail =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSINGEMAIL_KEY]!!
        donationProcessingPhone =
            notification_list[SessionManager.NOTIFY_DONATIONPROCESSINGPHONE_KEY]!!
        getNotificationsApi(false)

        Log.e(
            "NOTIFYVALUES",
            recurringdonation_val + "" + newpiesarecommend_val + "" + trendingnow_val + "" + newProjectChurch + "" + newProjectCharity + "" + someonefollowed_val
        )

        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
        switch_muteallnoti.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_muteallnoti.text = getString(R.string.on)
                tv_muteallnoti.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
            } else {
                // The toggle is disabled
                tv_muteallnoti.text = getString(R.string.off)
                tv_muteallnoti.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
            }
        }
        switch_recurringdonation.isChecked = true
        switch_newpiesarecommend.isChecked = true
        switch_trendingnow.isChecked = true
        switch_someonefollowed.isChecked = true
        switch_haptics.isChecked = true
        tv_recurringdonation.text = getString(R.string.on)
        tv_newpiesarecommend.text = getString(R.string.on)
        tv_trendingnow.text = getString(R.string.on)
        tv_someonefollowed.text = getString(R.string.on)
        tv_haptics.text = getString(R.string.on)
        //NotificationsApi()
        switch_recurringdonation.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_recurringdonation.text = getString(R.string.on)
                tv_recurringdonation.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
                recurringdonation_val = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            } else {
                // The toggle is disabled
                tv_recurringdonation.text = getString(R.string.off)
                tv_recurringdonation.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
                recurringdonation_val = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            }
        }
        switch_newpiesarecommend.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_newpiesarecommend.text = getString(R.string.on)
                tv_newpiesarecommend.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
                newpiesarecommend_val = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            } else {
                // The toggle is disabled
                tv_newpiesarecommend.text = getString(R.string.off)
                tv_newpiesarecommend.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
                newpiesarecommend_val = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            }
        }
        switch_trendingnow.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_trendingnow.text = getString(R.string.on)
                tv_trendingnow.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
                trendingnow_val = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            } else {
                // The toggle is disabled
                tv_trendingnow.text = getString(R.string.off)
                tv_trendingnow.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
                trendingnow_val = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            }
        }
        switch_someonefollowed.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_someonefollowed.text = getString(R.string.on)
                tv_someonefollowed.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
                someonefollowed_val = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            } else {
                // The toggle is disabled
                tv_someonefollowed.text = getString(R.string.off)
                tv_someonefollowed.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
                someonefollowed_val = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            }
        }
        switch_haptics.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                // The toggle is enabled
                tv_haptics.text = getString(R.string.on)
                tv_haptics.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.colorAccent
                    )
                )
                haptics_val = "1"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            } else {
                // The toggle is disabled
                tv_haptics.text = getString(R.string.off)
                tv_haptics.setTextColor(
                    ContextCompat.getColor(
                        this@NotificationsActivity,
                        R.color.light_grey
                    )
                )
                haptics_val = "0"
                sessionManager.notificationSession(
                    recurringdonation_val,
                    newpiesarecommend_val,
                    trendingnow_val,
                    someonefollowed_val,
                    haptics_val,
                    newProjectChurch,
                    newProjectCharity,
                    newSpecialCausesChurch,
                    newSpecialCausesCharity,
                    DonationProcessedEmail,
                    donationProcessedPhone,
                    DonationProcessingEmail,
                    donationProcessingPhone
                )
                NotificationsApi(true)
            }
        }

        ll_newproject.setOnClickListener {
            val intent = Intent(this, NewProjectActivity::class.java)
            intent.putExtra("activity_name", "newproject")
            startActivity(intent)
        }
        ll_newspecialcase.setOnClickListener {
            val intent = Intent(this, NewProjectActivity::class.java)
            intent.putExtra("activity_name", "newspecialcase")
            startActivity(intent)
        }
        ll_confirmationemails.setOnClickListener {
            val intent = Intent(this, NewProjectActivity::class.java)
            intent.putExtra("activity_name", "confirmationemails")
            startActivity(intent)
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)

        switch_muteallnoti = findViewById(R.id.switch_muteallnoti)
        switch_recurringdonation = findViewById(R.id.switch_recurringdonation)
        switch_newpiesarecommend = findViewById(R.id.switch_newpiesarecommend)
        switch_trendingnow = findViewById(R.id.switch_trendingnow)
        switch_someonefollowed = findViewById(R.id.switch_someonefollowed)
        switch_haptics = findViewById(R.id.switch_haptics)
        tv_muteallnoti = findViewById(R.id.tv_muteallnoti)
        tv_recurringdonation = findViewById(R.id.tv_recurringdonation)
        tv_newpiesarecommend = findViewById(R.id.tv_newpiesarecommend)
        tv_trendingnow = findViewById(R.id.tv_trendingnow)
        tv_someonefollowed = findViewById(R.id.tv_someonefollowed)
        tv_haptics = findViewById(R.id.tv_haptics)
        ll_newproject = findViewById(R.id.ll_newproject)
        ll_newspecialcase = findViewById(R.id.ll_newspecialcase)
        ll_confirmationemails = findViewById(R.id.ll_confirmationemails)

        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)

    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun NotificationsApi(notify: Boolean) {
        //  loading_dialog.show()
        val objspl = JSONObject()
        val notobject = JSONObject()
        notobject.put("recurringDonationReminder", recurringdonation_val.toInt())
        notobject.put("newPiesaRecommendation", newpiesarecommend_val.toInt())
        notobject.put("newTrendingNow", trendingnow_val.toInt())
        notobject.put("someoneFollowedYou", someonefollowed_val.toInt())
        notobject.put("hapticss", haptics_val.toInt())
        notobject.put("newProjectChurch", newProjectChurch.toInt())
        notobject.put("newProjectCharity", newProjectCharity.toInt())
        notobject.put("newSpecialCausesChurch", newSpecialCausesChurch.toInt())
        notobject.put("newSpecialCausesCharity", newSpecialCausesCharity.toInt())
        notobject.put("DonationProcessedEmail", DonationProcessedEmail.toInt())
        notobject.put("donationProcessedPhone", donationProcessedPhone.toInt())
        notobject.put("donationProcessingEmail", DonationProcessingEmail.toInt())
        notobject.put("donationProcessingPhone", donationProcessingPhone.toInt())
        objspl.put("userId", userId)
        objspl.put("notifications", notobject)
        val jParserspl = JsonParser()
        val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
        Log.e("UserPrivacyApi", jObjectspl.toString())

        val apiInterface = ApiInterface.create()
        val call = apiInterface.NotificationsApi(jObjectspl)
        call.enqueue(object : Callback<ProfileDetailsPojo> {
            override fun onFailure(call: Call<ProfileDetailsPojo>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<ProfileDetailsPojo>,
                response: Response<ProfileDetailsPojo>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!! == 1) {
                        val result = jsondata.result!!
                        if (notify) {
                            //Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                        }
                        tv_recurringdonation.setTextColor(
                            ContextCompat.getColor(
                                this@NotificationsActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_newpiesarecommend.setTextColor(
                            ContextCompat.getColor(
                                this@NotificationsActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_trendingnow.setTextColor(
                            ContextCompat.getColor(
                                this@NotificationsActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_someonefollowed.setTextColor(
                            ContextCompat.getColor(
                                this@NotificationsActivity,
                                R.color.colorAccent
                            )
                        )
                        tv_haptics.setTextColor(
                            ContextCompat.getColor(
                                this@NotificationsActivity,
                                R.color.colorAccent
                            )
                        )
                    } else {
                        val result = jsondata.result!!
                        Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getNotificationsApi(notify: Boolean) {
        //  loading_dialog.show()
        val objspl = JSONObject()
        objspl.put("userId", userId)
        val jParserspl = JsonParser()
        val jObjectspl = jParserspl.parse(objspl.toString()) as JsonObject
        Log.e("NOTIFICATIONOBJECT", jObjectspl.toString())

        val apiInterface = ApiInterface.create()
        val call = apiInterface.getUserNotificationsSettings(jObjectspl)
        call.enqueue(object : Callback<GetNotificationSettingsResponse> {
            override fun onFailure(call: Call<GetNotificationSettingsResponse>, t: Throwable) {
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<GetNotificationSettingsResponse>,
                response: Response<GetNotificationSettingsResponse>?
            ) {
                try {
                    val jsondata = response!!.body()!!
                    if (jsondata.responseStatus!!.toString() == "1") {
                        val result = jsondata.result!!
                        if (result.hapticss.equals("1")) {
                            switch_haptics.isChecked = true
                            tv_haptics.text = getString(R.string.on)
                            tv_haptics.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_haptics.isChecked = false
                            tv_haptics.text = getString(R.string.off)
                            tv_haptics.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.newPiesaRecommendation.equals("1")) {
                            switch_newpiesarecommend.isChecked = true
                            tv_newpiesarecommend.text = getString(R.string.on)
                            tv_newpiesarecommend.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_newpiesarecommend.isChecked = true
                            tv_newpiesarecommend.text = getString(R.string.off)
                            tv_newpiesarecommend.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.newTrendingNow.equals("1")) {
                            switch_trendingnow.isChecked = true
                            tv_trendingnow.text = getString(R.string.on)
                            tv_trendingnow.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_trendingnow.isChecked = false
                            tv_trendingnow.text = getString(R.string.off)
                            tv_trendingnow.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.recurringDonationReminder.equals("1")) {
                            switch_recurringdonation.isChecked = true
                            tv_recurringdonation.text = getString(R.string.on)
                            tv_recurringdonation.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_recurringdonation.isChecked = false
                            tv_recurringdonation.text = getString(R.string.off)
                            tv_recurringdonation.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                        if (result.someoneFollowedYou.equals("1")) {
                            switch_someonefollowed.isChecked = true
                            tv_someonefollowed.text = getString(R.string.on)
                            tv_someonefollowed.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.colorAccent
                                )
                            )
                        } else {
                            switch_someonefollowed.isChecked = false
                            tv_someonefollowed.text = getString(R.string.off)
                            tv_someonefollowed.setTextColor(
                                ContextCompat.getColor(
                                    this@NotificationsActivity,
                                    R.color.light_grey
                                )
                            )
                        }
                    } else {
                        //Toast.makeText(applicationContext, result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
