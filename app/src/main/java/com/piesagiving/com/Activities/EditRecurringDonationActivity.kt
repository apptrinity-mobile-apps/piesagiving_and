package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.DecimalDigitsInputFilter
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.ShowDatePikerDialog
import com.piesagiving.com.R
import com.piesagiving.com.Utils.toast
import com.stripe.android.model.Card
import com.stripe.android.view.CardInputWidget
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class EditRecurringDonationActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager

    lateinit var tv_save: TextView
    lateinit var checkBox: CheckBox
    lateinit var ll_project: LinearLayout
    lateinit var ll_frequency: LinearLayout
    lateinit var rl_start_date: RelativeLayout
    lateinit var ll_end_date_id: LinearLayout
    lateinit var rl_end_date: RelativeLayout
    lateinit var et_project: EditText
    lateinit var et_amount: EditText
    lateinit var et_frequency: EditText
    lateinit var et_start_date: EditText
    lateinit var et_end_date: EditText
    lateinit var iv_back: ImageView
    lateinit var datePicker: ShowDatePikerDialog
    lateinit var sp_freq_type_id: Spinner

    lateinit var bank_full_dialog: Dialog

    lateinit var freq_names: ArrayList<String>
    lateinit var freq_names_ids: ArrayList<String>
    lateinit var only_names: ArrayList<String>
    lateinit var only_types: ArrayList<String>
    lateinit var only_ids: ArrayList<String>
    lateinit var special_cause_names: ArrayList<String>
    lateinit var projectDonationsArray: ArrayList<DropDownProjectDetailsArrayResponse>
    lateinit var specialCauseDetailsArray: ArrayList<DropDownSpecialCauseDetailsArrayResponse>
    lateinit var loading_dialog: Dialog

    lateinit var donation_types_array: ArrayList<String>
    lateinit var donation_types_array_ids: ArrayList<Int>


    var freq_mode = ""
    var freq_mode_id = ""
    var check_box = false
    var startDate = ""
    var endDate = ""
    var screen_type = ""
    var church_name = ""
    var paymentType = ""
    var sourceId = ""
    var projectName = ""
    var subscriptionAmount = ""
    var isHasEnd = ""
    var payType = 0

    var stripeCardExtracharges = 0.00
    var stripeCardPercentage = 0.00
    var piesaCardExtracharges = 0.00
    var piesaCardPercentage = 0.00
    var piesagivingPercentage = 0.00
    var edit_amount = 0.00
    var stripeBankExtracharges = 0.00
    var stripeBankPercentage = 0.00
    var piesaBankExtracharges = 0.00
    var piesaBankPercentage = 0.00

    var global_stripe_percent = 0.00
    var global_stripe_total_fee = 0.00
    var global_ex_amount = 0
    var global_bank_ex_amount = 0.00
    var global_piesa_percent = 0.00
    var global_piesa_total_fee = 0.00
    var global_piesa_card_ex_amount = 0
    var global_piesa_bank_ex_amount = 0.00
    var card_global_id = ""
    var bank_card_token = ""

    var cards_default_source_id = ""
    lateinit var full_dialog: Dialog


    lateinit var stripe_card_id: CardInputWidget
    lateinit var et_card_holder_id: EditText
    lateinit var btn_pay_id: Button
    lateinit var img_close_id: ImageView
    lateinit var tv_single_name_id: TextView
    lateinit var tv_single_card_no_id: TextView
    lateinit var ll_card_id: LinearLayout
    lateinit var get_card: Card
    val PUBLISHABLE_KEY = "pk_test_8AhIcIToh3eZFUBaX7WyqF3000Cge8sBFL"
    //val PUBLISHABLE_KEY = "pk_live_w4Epwz1onHynjfjaZFF2E7Dr00itQEIs9e"

    var get_amount = ""
    var get_donationId = ""
    var get_reccurringType = ""
    var get_start_date = ""
    var get_end_date = ""
    var get_cardLast4Digits = ""
    private var userId = ""
    private var userName = ""

    lateinit var week_array: ArrayList<String>
    lateinit var sp_weeks_id: Spinner
    lateinit var ll_week_id: LinearLayout
    var week_freq_id = ""
    lateinit var sp_donation_type_id: Spinner

    lateinit var tv_org_name: TextView
    lateinit var et_to_recurring_id: EditText
    lateinit var img_bank_close_id: ImageView
    lateinit var btn_saved_bank_pay: Button
    lateinit var id_empty_data_id: TextView
    lateinit var rv_account_details_id: RecyclerView
    lateinit var array_account_list: ArrayList<BankAccountsArrayList>


    //lateinit var btn_saved_card_pay: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_edit_recurring_donation)


        get_amount = intent.getStringExtra("amount")!!
        get_donationId = intent.getStringExtra("donationId")!!
        get_reccurringType = intent.getStringExtra("reccurringType")!!
        get_start_date = intent.getStringExtra("start_date")!!
        get_end_date = intent.getStringExtra("end_date")!!
        get_cardLast4Digits = intent.getStringExtra("cardLast4Digits")!!
        screen_type = intent.getStringExtra("screen_type")!!
        church_name = intent.getStringExtra("church_name")!!
        paymentType = intent.getStringExtra("paymentType")!!
        sourceId = intent.getStringExtra("sourceId")!!
        projectName = intent.getStringExtra("projectName")!!
        subscriptionAmount = intent.getStringExtra("subscriptionAmount")!!
        isHasEnd = intent.getStringExtra("isHasEnd")!!

        Log.e("get_reccurringType", get_reccurringType + "---" + paymentType+"-PNAME-"+projectName)
        when {
            get_reccurringType.equals("Day", ignoreCase = true) -> {
                get_reccurringType = "Daily"
            }
            get_reccurringType.equals("Quarter Year", ignoreCase = true) -> {
                get_reccurringType = "Quarterly"
            }
            get_reccurringType.equals("Half Year", ignoreCase = true) -> {
                get_reccurringType = "Halfyearly"
            }
        }




        freq_names = ArrayList()
        freq_names.add("Daily")
        freq_names.add("Weekly")
        freq_names.add("Monthly")
        /*freq_names.add("Quarterly")
        freq_names.add("Halfyearly")
        freq_names.add("Yearly")*/

        freq_names_ids = ArrayList()
        freq_names_ids.add("day")
        freq_names_ids.add("week")
        freq_names_ids.add("month")
        /*freq_names_ids.add("quarter_year")
        freq_names_ids.add("half_year")
        freq_names_ids.add("year")*/



        week_array = ArrayList()
        week_array.add("Now")
        week_array.add("Existing week")



        donation_types_array = ArrayList()
        // donation_types_array.add("ACH")
        donation_types_array.add("Select Bank Account")
        donation_types_array.add("Select Credit Card")

        donation_types_array_ids = ArrayList()
        donation_types_array_ids.add(0)
        donation_types_array_ids.add(1)

        initialize()

        DonationFeeApi()
        if (paymentType.equals("bank_account")) {
        } else {
            sp_donation_type_id.setSelection(1)
        }



        Bank_full_dilag()

        //val value = subscriptionAmount.toDouble().roundToInt()

        et_amount.setText(subscriptionAmount.toString())
        tv_org_name.setText("To "+church_name)
        et_to_recurring_id.setText(projectName)


        iv_back.setOnClickListener {
            onBackPressed()
        }

        et_start_date.setText(get_start_date)
        et_start_date.isEnabled = false
        et_start_date.isClickable = false

        et_end_date.setOnClickListener {
            datePicker.showPiker(this@EditRecurringDonationActivity, "", et_end_date)
        }



        rl_end_date.setOnClickListener {
            et_end_date.performClick()
        }


        if(isHasEnd.equals("false")){
            checkBox.isChecked=true
            ll_end_date_id.visibility=View.GONE
            rl_end_date.isClickable = false
            et_end_date.isEnabled = false
            et_end_date.isClickable = false
            et_end_date.setTextColor(
                ContextCompat.getColor(this, R.color.login_text_hint)
            )
            check_box = false
        }else{
            ll_end_date_id.visibility=View.VISIBLE
            rl_end_date.isClickable = true
            et_end_date.isEnabled = true
            et_end_date.isClickable = true
            et_end_date.setTextColor(
                ContextCompat.getColor(this, R.color.login_text)
            )

            check_box = true
        }


        checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                ll_end_date_id.visibility=View.GONE
                rl_end_date.isClickable = false
                et_end_date.isEnabled = false
                et_end_date.isClickable = false
                et_end_date.setTextColor(
                    ContextCompat.getColor(this, R.color.login_text_hint)
                )
                check_box = false
            } else {
                ll_end_date_id.visibility=View.VISIBLE
                rl_end_date.isClickable = true
                et_end_date.isEnabled = true
                et_end_date.isClickable = true
                et_end_date.setTextColor(
                    ContextCompat.getColor(this, R.color.login_text)
                )

                check_box = true
            }
        }

        full_dialog = Dialog(this, R.style.DialogTheme)
        full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        // full_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        full_dialog.setCancelable(false)

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        full_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);

        full_dialog.setContentView(R.layout.edit_stripe_pyament_dialog)
        stripe_card_id = full_dialog.findViewById(R.id.stripe_card_id)
        et_card_holder_id = full_dialog.findViewById(R.id.et_card_holder_id)
        btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        img_close_id = full_dialog.findViewById<ImageView>(R.id.img_close_id)
         tv_single_name_id = full_dialog.findViewById<TextView>(R.id.tv_name_id)
        tv_single_card_no_id = full_dialog.findViewById<TextView>(R.id.tv_card_no_id)
        ll_card_id = full_dialog.findViewById<LinearLayout>(R.id.ll_card_id)

        ll_card_id.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)

            // set message of alert dialog
            dialogBuilder.setMessage("Do you want to Save Recurring Donation Details ?")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Proceed") { dialog, id ->
                    try {
                        val inputSDF = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
                        val outputSDF = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
                        // val sDate = inputSDF.parse(et_start_date.text.toString())
                        if (et_end_date.text.toString().equals("")) {
                            endDate = ""
                        } else {
                            val eDate = inputSDF.parse(et_end_date.text.toString())
                            endDate = outputSDF.format(eDate!!)
                        }

///*{
//    "userId": "5f61aeb1f8b7140963b97381",
//    "donationId":"5f63137882778ee60131206d",
//    "amount": 800,
//    "paymentType": 2,
//    "cardId": "",
//    "bankAccountId":"ba_1HRu85KijxtVwMc3aAH9tj48",
//    "cardToken": "",
//    "reccurringType": "day",
//    "startDate": "09-17-2020",
//    "stripeFeePercentage": 3.2,
//    "stripeFeeAmount": 0.16,
//    "stripeExtraCharges": 30,
//    "piesaFeePercentage": 0,
//    "piesaFeeAmount": 0,
//    "isHasEnd": true,
//    "endDate": "09-21-2020"
//}*/
                        //hbhbh
                        endDate = et_end_date.text.toString()

                        startDate = et_start_date.text.toString()!!
                        if (screen_type.equals("project")) {
                            val obj = JSONObject()
                            obj.put("userId", userId)
                            obj.put("donationId", get_donationId)
                            obj.put("amount", et_amount.text.toString().toDouble() * 100)
                            obj.put("paymentType", 1)
                            obj.put("cardId", sourceId)/*sourceId*/
                            obj.put("bankAccountId", "")/*sourceId*/
                            obj.put("reccurringType", freq_mode_id)
                            obj.put("cardToken", "")
                            obj.put("startDate", startDate)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("isHasEnd", check_box)
                            if (check_box.equals(true)) {
                                obj.put("endDate", endDate)
                            } else {
                                obj.put("endDate", "")
                            }

                            if (freq_mode.equals("Weekly")) {
                                obj.put("weekMonthDay", week_freq_id)
                            }
                            val jParser = JsonParser()
                            val finalObj = jParser.parse(obj.toString()) as JsonObject

                            Log.e("service1", finalObj.toString())
                            StripePaymentApi(finalObj)
                        }

                        Log.e("Dates", startDate + "---" + endDate)
                    } catch (e: java.lang.Exception) {
                        Log.e("Dates", "----")
                        e.printStackTrace()
                    }

                }
                // negative button text and action
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Save Details!")
            // show alert dialog
            alert.show()
        }

        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        SavedCardsListApi(jObject)


        img_close_id.setOnClickListener {
            full_dialog.dismiss()
        }

        btn_pay_id.setOnClickListener {

            val cardToSave = stripe_card_id.card
            if (cardToSave == null) {
                Toast.makeText(
                    this@EditRecurringDonationActivity,
                    "Invalid Card Data",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (et_card_holder_id.text.toString().trim().equals("")) {
                toast("Enter Nick Name")
            } else {
                get_card = stripe_card_id.card!!
                loading_dialog.show()
                buy()
            }
        }

        TooltipCompat.setTooltipText(btn_pay_id, "Payment Button")

        tv_save.setOnClickListener {

            edit_amount = et_amount.text.toString().trim().toDouble()
            if (et_amount.text.toString().trim().equals("") || et_amount.text.toString()
                    .toDouble() <= 4.99
            ) {
                toast("You must enter donation amount min $5")
            } else if (check_box.equals(true) && et_end_date.text.toString().equals("")) {
                toast("You must enter End Date")
            } else {

                if (payType.equals(1)) {

                    full_dialog.show()

                    val strpextracharge = (stripeCardExtracharges.toDouble())
                    val decimal =
                        (edit_amount!! * (stripeCardPercentage / 100)) + (strpextracharge) + (piesaCardExtracharges.toDouble()) + (edit_amount!! * (piesaCardPercentage / 100))
                    println(decimal)

                    val tax = "%.2f".format(decimal)
                    Log.e("tax_ll", decimal.toString())
                    Log.e("final_all", tax)

                    global_stripe_percent = stripeCardPercentage
                    global_stripe_total_fee =
                        "%.2f".format((edit_amount!! * stripeCardPercentage / 100)).toDouble()
                    global_ex_amount = (stripeCardExtracharges * 100).toInt()
                    global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                    global_piesa_percent = piesaCardPercentage
                    global_piesa_total_fee =
                        "%.2f".format((edit_amount!! * piesaCardPercentage / 100)).toDouble()
                } else {
                    bank_full_dialog.show()

                    val strpextracharge = (stripeBankExtracharges.toDouble())
                    val decimal =
                        (edit_amount!! * (stripeBankPercentage / 100)) + (strpextracharge) + (piesaBankExtracharges.toDouble()) + (edit_amount!! * (piesaBankPercentage / 100))
                    println(decimal)
                    // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                    val tax = "%.2f".format(decimal)
                    Log.e("tax_ll", decimal.toString())
                    Log.e("final_all", tax)

                    global_stripe_percent = stripeBankPercentage
                    global_stripe_total_fee =
                        "%.2f".format((edit_amount!! * stripeBankPercentage / 100)).toDouble()
                    global_bank_ex_amount = (stripeBankExtracharges)
                    global_piesa_bank_ex_amount = (piesaBankExtracharges)
                    global_piesa_percent = piesaBankPercentage
                    global_piesa_total_fee =
                        "%.2f".format((edit_amount!! * piesaBankPercentage / 100)).toDouble()

                }
            }

        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        val user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        userName = user_details[SessionManager.FIRST_NAME_KEY]!!

        tv_save = findViewById(R.id.tv_save)
        checkBox = findViewById(R.id.checkBox)

        tv_org_name = findViewById(R.id.tv_org_name)
        et_to_recurring_id = findViewById(R.id.et_to_recurring_id)
        et_amount = findViewById(R.id.et_amount)
        et_amount.setFilters(arrayOf(DecimalDigitsInputFilter(5, 2)))

        rl_start_date = findViewById(R.id.rl_start_date)
        et_start_date = findViewById(R.id.et_start_date)
        rl_end_date = findViewById(R.id.rl_end_date)
        ll_end_date_id = findViewById(R.id.ll_end_date_id)
        et_end_date = findViewById(R.id.et_end_date)
        iv_back = findViewById(R.id.iv_back)
        sp_freq_type_id = findViewById(R.id.sp_freq_type_id)

        sp_weeks_id = findViewById(R.id.sp_weeks_id)
        ll_week_id = findViewById(R.id.ll_week_id)
        sp_donation_type_id = findViewById(R.id.sp_donation_type_id)

        datePicker = ShowDatePikerDialog()

        val adapter = ArrayAdapter<String>(
            this@EditRecurringDonationActivity,
            R.layout.support_simple_spinner_dropdown_item,
            freq_names
        )
        sp_freq_type_id.setAdapter(adapter)
        sp_freq_type_id.isEnabled = false
        sp_freq_type_id.isClickable = false
        sp_freq_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {

                freq_mode = freq_names.get(position).toString()
                freq_mode_id = freq_names_ids.get(position).toString()
                Log.e("freq_mode_stg", freq_mode)

                if (freq_mode.equals("Weekly")) {
                    ll_week_id.visibility = View.VISIBLE
                } else if (freq_mode.equals("Monthly")) {
                    ll_week_id.visibility = View.GONE
                } else {
                    ll_week_id.visibility = View.GONE
                }

            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })
        for (i in 0 until freq_names!!.size) {
            Log.e("get_reccurringType", get_reccurringType + "---" + freq_names)
            if (freq_names[i].equals(get_reccurringType)) {
                sp_freq_type_id.setSelection(i)
                Log.e("get_reccurringType", get_reccurringType)

                if (get_reccurringType.equals("Weekly")) {
                    ll_week_id.visibility = View.VISIBLE
                } else if (get_reccurringType.equals("Monthly")) {
                    ll_week_id.visibility = View.GONE
                } else {
                    ll_week_id.visibility = View.GONE
                }

            }
        }


        val weeks_adapter = ArrayAdapter<String>(
            this@EditRecurringDonationActivity,
            R.layout.support_simple_spinner_dropdown_item,
            week_array
        )
        sp_weeks_id.setAdapter(weeks_adapter)
        sp_weeks_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                week_freq_id = week_array.get(position).toLowerCase(Locale.getDefault())

                Log.e("week_freq_id", week_freq_id)
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }
        })


/*Payment Method*/
        val donation_type_adapter = ArrayAdapter<String>(
            this,
            R.layout.support_simple_spinner_dropdown_item,
            donation_types_array
        )
        sp_donation_type_id.setAdapter(donation_type_adapter)
        sp_donation_type_id.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                arg0: AdapterView<*>, arg1: View,
                position: Int, id: Long
            ) {
                payType = donation_types_array_ids.get(position)
                Log.e("type_name_stg", payType.toString())

                if (payType.equals(1)) {
                    if (edit_amount!!.toDouble() > 0.00) {
                        val strpextracharge = (stripeCardExtracharges.toDouble())
                        val decimal =
                            (edit_amount!! * (stripeCardPercentage / 100)) + (strpextracharge) + (piesaCardExtracharges.toDouble()) + (edit_amount!! * (piesaCardPercentage / 100))
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e("tax_ll", decimal.toString())
                        Log.e("final_all", tax)

                        global_stripe_percent = stripeCardPercentage
                        global_stripe_total_fee =
                            "%.2f".format((edit_amount!! * stripeCardPercentage / 100)).toDouble()
                        global_ex_amount = (stripeCardExtracharges * 100).toInt()
                        global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                        global_piesa_percent = piesaCardPercentage
                        global_piesa_total_fee =
                            "%.2f".format((edit_amount!! * piesaCardPercentage / 100)).toDouble()
                    }


                } else {
                    if (edit_amount!!.toDouble() > 0.00) {
                        val strpextracharge = (stripeBankExtracharges.toDouble())
                        val decimal =
                            (edit_amount!! * (stripeBankPercentage / 100)) + (strpextracharge) + (piesaBankExtracharges.toDouble()) + (edit_amount!! * (piesaBankPercentage / 100))
                        println(decimal)
                        // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                        val tax = "%.2f".format(decimal)
                        Log.e("tax_ll", decimal.toString())
                        //tv_tax_amount_id.setText("$" + tax.toString())
                        //tv_tax_amount_id_payment.setText("$" + tax.toString())
                        //tv_donate_amount_id.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        //tv_donate_amount_id_payment.setText("$" + (edit_amount!!.toDouble() - tax.toDouble()))
                        //tv_total_amount_id.setText((edit_amount!!.toDouble()).toString())
                        //tv_total_amount_id_payment.setText((edit_amount!!.toDouble()).toString())
                        Log.e("final_all", tax)

                        global_stripe_percent = stripeBankPercentage
                        global_stripe_total_fee =
                            "%.2f".format((edit_amount!! * stripeBankPercentage / 100)).toDouble()
                        global_bank_ex_amount = (stripeBankExtracharges)
                        global_piesa_bank_ex_amount = (piesaBankExtracharges)
                        global_piesa_percent = piesaBankPercentage
                        global_piesa_total_fee =
                            "%.2f".format((edit_amount!! * piesaBankPercentage / 100)).toDouble()
                    }
                }

            }

            override fun onNothingSelected(arg0: AdapterView<*>) {

            }

        })


    }

    override fun onBackPressed() {
        super.onBackPressed()
    }


    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun buy() {
        /*NEW*/
        val validation = get_card!!.validateCard()
        if (validation) {


            /*Card Token Generate with API call*/
            val stripe_apiInterface = StripeInterFace.create()
            val call = stripe_apiInterface.StripeCardToken(
                number = get_card.number!!,
                exp_month = get_card.expMonth!!,
                exp_year = get_card.expYear!!,
                cvc = get_card.cvc!!,
                name = et_card_holder_id.text.toString()
            )

            //Log.e("@response", call.toString())
            call.enqueue(object : Callback<CardTokenPojo> {
                override fun onFailure(call: Call<CardTokenPojo>, t: Throwable) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.e("responseStatus", "Exception  " + call + "  " + t)

                }

                override fun onResponse(
                    call: Call<CardTokenPojo>,
                    response: Response<CardTokenPojo>?
                ) {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    try {
                        try {
                            endDate = et_end_date.text.toString()

                            startDate = /*outputSDF.format(*/et_start_date.text.toString()!!/*)*/
                            Log.e("Dates", startDate + "---" + endDate)
                        } catch (e: java.lang.Exception) {
                            Log.e("Dates", "----")
                            e.printStackTrace()
                        }

                        if (screen_type.equals("project")) {
                            val obj = JSONObject()
                            obj.put("userId", userId)
                            obj.put("donationId", get_donationId)
                            obj.put("amount", et_amount.text.toString().toDouble() * 100)
                            obj.put("paymentType", 1)
                            obj.put("cardId", "")/*sourceId*/
                            obj.put("bankAccountId", "")/*sourceId*/
                            obj.put("reccurringType", freq_mode_id)
                            obj.put("cardToken", response!!.body()!!.id!!.toString())
                            obj.put("startDate", startDate)
                            obj.put("stripeFeePercentage", global_stripe_percent)
                            obj.put("stripeFeeAmount", global_stripe_total_fee)
                            obj.put("stripeExtraCharges", global_ex_amount)
                            obj.put("piesaFeePercentage", global_piesa_percent)
                            obj.put("piesaExtraCharges", global_piesa_card_ex_amount)
                            obj.put("piesaFeeAmount", global_piesa_total_fee)
                            obj.put("isHasEnd", check_box)
                            if (check_box.equals(true)) {
                                obj.put("endDate", endDate)
                            } else {
                                obj.put("endDate", "")
                            }

                            if (freq_mode.equals("Weekly")) {
                                obj.put("weekMonthDay", week_freq_id.toLowerCase(Locale.getDefault()))
                            }
                            val jParser = JsonParser()
                            val finalObj = jParser.parse(obj.toString()) as JsonObject

                            Log.e("service2", finalObj.toString())
                            StripePaymentApi(finalObj)
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })


            /*OLD*/


        } else if (!get_card!!.validateNumber()) {
            Log.d("Stripe", "The card number that you entered is invalid")
        } else if (!get_card!!.validateExpiryDate()) {
            Log.d("Stripe", "The expiration date that you entered is invalid")
        } else if (!get_card!!.validateCVC()) {
            Log.d("Stripe", "The CVC code that you entered is invalid")
        } else {
            Log.d("Stripe", "The card details that you entered are invalid")
        }
    }

    private fun StripePaymentApi(jsonObj: JsonObject) {
//Normal Recuring Edit
        loading_dialog.show()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.EditRecrringDonationsApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<StripePaymentPojo> {
            override fun onFailure(call: Call<StripePaymentPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("resp_fail", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<StripePaymentPojo>,
                response: Response<StripePaymentPojo>
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    if (response!!.body()!!.responseStatus == 1) {


                        val intent =
                            Intent(this@EditRecurringDonationActivity, ThanksActivity::class.java)
                        intent.putExtra("from_screen", "recurring")
                        intent.putExtra("from_type", screen_type)
                        intent.putExtra("special_cause_id", "")
                        intent.putExtra("display_name", church_name)
                        intent.putExtra("project_name", projectName)
                        intent.putExtra(
                            "paid_amount",
                            et_amount.text.toString()!!.toDouble().toString()
                        )

                        startActivity(intent)
                        finish()

                    } else {

                        Log.e("failed_0", response!!.body()!!.result!!.toString())
                        Toast.makeText(
                            this@EditRecurringDonationActivity,
                            response!!.body()!!.result!!.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    private fun Bank_full_dilag() {
        bank_full_dialog = Dialog(this, R.style.DialogTheme)
        bank_full_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        bank_full_dialog.setCancelable(false);

        // val width=ViewGroup.LayoutParams.MATCH_PARENT
        // full_dialog.window!!.setLayout(width,width)
        bank_full_dialog.getWindow()!!.setLayout(
            ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT
        )

        bank_full_dialog.getWindow()!!.setWindowAnimations(R.style.AppTheme_Slide);

        bank_full_dialog.setContentView(R.layout.bank_payment_dialog)
        //btn_pay_id = full_dialog.findViewById(R.id.btn_pay_id)
        img_bank_close_id = bank_full_dialog.findViewById<ImageView>(R.id.img_bank_close_id)
        btn_saved_bank_pay = bank_full_dialog.findViewById<Button>(R.id.btn_saved_bank_pay)
        id_empty_data_id = bank_full_dialog.findViewById<TextView>(R.id.id_empty_data_id)

        rv_account_details_id =
            bank_full_dialog.findViewById<RecyclerView>(R.id.rv_account_details_id)

        img_bank_close_id.setOnClickListener {
            bank_full_dialog.dismiss()
        }
        val obj = JSONObject()
        obj.put(
            "userId",
            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString()
        )
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject

        AccountsListApi(jObject)

        SavedCardsListApi(jObject)
        btn_saved_bank_pay.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)

            // set message of alert dialog
            dialogBuilder.setMessage("Do you want to Save Recurring Donation Details ?")
                // if the dialog is cancelable
                .setCancelable(false)
                // positive button text and action
                .setPositiveButton("Proceed") { dialog, id ->
                    endDate = et_end_date.text.toString()

                    startDate = et_start_date.text.toString()!!

                    //jnjnjn
                    if (screen_type.equals("project")) {
                        val obj = JSONObject()
                        obj.put("userId", userId)
                        obj.put("donationId", get_donationId)
                        obj.put("amount", et_amount.text.toString().toDouble() * 100)
                        obj.put("paymentType", 2)
                        obj.put("cardId", "")/*sourceId*/
                        obj.put("bankAccountId", bank_card_token)/*sourceId*/
                        obj.put("reccurringType", freq_mode_id)
                        obj.put("cardToken", "")
                        obj.put("startDate", startDate)
                        obj.put("stripeFeePercentage", global_stripe_percent)
                        obj.put("stripeFeeAmount", global_stripe_total_fee)
                        obj.put("stripeExtraCharges", global_ex_amount)
                        obj.put("piesaFeePercentage", global_piesa_percent)
                        obj.put("piesaExtraCharges", global_piesa_bank_ex_amount)
                        obj.put("piesaFeeAmount", global_piesa_total_fee)
                        obj.put("isHasEnd", check_box)
                        if (check_box.equals(true)) {
                            obj.put("endDate", endDate)
                        } else {
                            obj.put("endDate", "")
                        }

                        if (freq_mode.equals("Weekly")) {
                            obj.put("weekMonthDay", week_freq_id)
                        }
                        val jParser = JsonParser()
                        val finalObj = jParser.parse(obj.toString()) as JsonObject

                        Log.e("service3", finalObj.toString())
                        StripePaymentApi(finalObj)
                    }
                    Log.e("Dates", startDate + "---" + endDate)


                }
                // negative button text and action
                .setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, id ->
                    dialog.cancel()
                })

            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Save Details!")
            // show alert dialog
            alert.show()
        }


    }


    private fun AccountsListApi(jsonObj: JsonObject) {

        loading_dialog.show()
        array_account_list = ArrayList()
        array_account_list.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.BankAccountListApi(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<BankAccountListPojo> {
            override fun onFailure(call: Call<BankAccountListPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<BankAccountListPojo>,
                response: Response<BankAccountListPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    Log.e("STRIPERESPOMNSEDONATION", response!!.body().toString())
                    if (response!!.body()!!.responseStatus == 1) {
                        // toast( response!!.body()!!.result!!)
                        if (response.body()!!.bankAccountList!!.data!!.size.equals(0)) {
                            id_empty_data_id.visibility = View.VISIBLE
                        }
                        for (i in 0 until response.body()!!.bankAccountList!!.data!!.size) {
                            array_account_list.add(response.body()!!.bankAccountList!!.data!![i])
                        }
                        rv_account_details_id.visibility = View.VISIBLE

                        val adapter = BankListAdapter(
                            this@EditRecurringDonationActivity,
                            array_account_list,
                            sessionManager.getUserDetails().get(SessionManager.ID_KEY).toString(),
                            sourceId
                        )
                        rv_account_details_id.setHasFixedSize(true)
                        rv_account_details_id.adapter = adapter
                        adapter.notifyDataSetChanged()

                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    inner class BankListAdapter(
        context: Context,
        val account_list: ArrayList<BankAccountsArrayList>,
        val user_id: String,
        var bank_default_source_id: String
    ) :
        RecyclerView.Adapter<BankListAdapter.ViewHolder>() {

        private var mContext: Context? = null
        var selectedPosition = -1

        init {
            this.mContext = context
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_wallet_accounts, parent, false)
            return ViewHolder(view)

        }

        override fun getItemCount(): Int {
            return account_list.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_bank_name_id.text = account_list[position].bank_name
            holder.tv_ac_no_id.text = "*******" + account_list[position].last4
            holder.tv_ac_holder_name_id.text = account_list[position].account_holder_name
            holder.img_delete_id.visibility = View.GONE

            if (account_list[position].status.equals("new")) {
                //holder.img_verify_id.setImageDrawable(mContext!!.resources.getDrawable(R.drawable.ic_close))
                //holder.tv_verify_id.visibility=View.VISIBLE
                holder.cv_setupwallet.visibility = View.GONE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.tv_verify_id.visibility = View.GONE
                holder.img_verify_id.visibility = View.GONE
                holder.cv_setupwallet.visibility = View.VISIBLE
                holder.ll_verify_amount_id.visibility = View.GONE


            }
            holder.cv_setupwallet.setOnClickListener {
                bank_card_token = account_list[position].id!!
                btn_saved_bank_pay.visibility = View.VISIBLE
                selectedPosition = position
                holder.img_verify_id.visibility = View.GONE

                notifyDataSetChanged()
                bank_default_source_id = ""

                Log.e("bank_card_token",bank_card_token)
            }

            if (selectedPosition == position) {
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_list_bg));
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
            } else {
                (holder.img_verify_id.getDrawable() as Animatable).start()
                //holder.itemView.setBackground(ContextCompat.getDrawable(DefineNew.this,R.drawable.brand_normal_bg));
                holder.img_verify_id.visibility = View.INVISIBLE
            }

            if (bank_default_source_id.equals(account_list[position].id)) {
                //bank_card_token = array_savedcards_list[position].id!!.toString()
                (holder.img_verify_id.getDrawable() as Animatable).start()
                holder.img_verify_id.visibility = View.VISIBLE
                btn_saved_bank_pay.visibility = View.VISIBLE
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var cv_setupwallet: CardView = view.findViewById(R.id.cv_setupwallet)
            var img_verify_id: ImageView = view.findViewById(R.id.img_verify_id)
            var tv_bank_name_id: TextView = view.findViewById(R.id.tv_bank_name_id)
            var tv_ac_no_id: TextView = view.findViewById(R.id.tv_ac_no_id)
            var tv_ac_holder_name_id: TextView = view.findViewById(R.id.tv_ac_holder_name_id)
            var tv_verify_id: TextView = view.findViewById(R.id.tv_verify_id)
            var img_delete_id: ImageView = view.findViewById(R.id.img_delete_id)
            var ll_verify_amount_id: LinearLayout = view.findViewById(R.id.ll_verify_amount_id)
        }

    }


    private fun DonationFeeApi() {

        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetExtraFeeApi()
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationFeePojo> {
            override fun onFailure(call: Call<DonationFeePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<DonationFeePojo>,
                response: Response<DonationFeePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {

                        piesaCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaCardExtracharges!!
                        piesaCardPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaCardPercentage!!
                        piesagivingPercentage =
                            response.body()!!.stripeFeeDetails!!.piesagivingPercentage!!
                        piesaBankPercentage =
                            response.body()!!.stripeFeeDetails!!.piesaBankPercentage!!
                        piesaBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.piesaBankExtracharges!!
                        stripeBankExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeBankExtracharges!!
                        stripeBankPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeBankPercentage!!
                        stripeCardExtracharges =
                            response.body()!!.stripeFeeDetails!!.stripeCardExtracharges!!
                        stripeCardPercentage =
                            response.body()!!.stripeFeeDetails!!.stripeCardPercentage!!
                        Log.e(
                            "STRIPECHARGES",
                            "" + stripeBankExtracharges + "-------" + stripeBankPercentage
                        )

                        if (paymentType.equals("bank_account")) {
                            sp_donation_type_id.setSelection(0)
                            val edit_amount = subscriptionAmount.toDouble()

                            val strpextracharge = (stripeBankExtracharges.toDouble())
                            val decimal =
                                (edit_amount!! * (stripeBankPercentage / 100)) + (strpextracharge) + (piesaBankExtracharges.toDouble()) + (edit_amount!! * (piesaBankPercentage / 100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            global_stripe_percent = stripeBankPercentage
                            global_stripe_total_fee =
                                "%.2f".format((edit_amount!! * stripeBankPercentage / 100))
                                    .toDouble()
                            global_bank_ex_amount = (stripeBankExtracharges)
                            global_piesa_bank_ex_amount = (piesaBankExtracharges)
                            global_piesa_percent = piesaBankPercentage
                            global_piesa_total_fee =
                                "%.2f".format((edit_amount!! * piesaBankPercentage / 100))
                                    .toDouble()
                        } else {
                            sp_donation_type_id.setSelection(1)
                            val edit_amount = subscriptionAmount.toDouble()
                            val strpextracharge = (stripeCardExtracharges.toDouble())
                            val decimal =
                                (edit_amount!! * (stripeCardPercentage / 100)) + (strpextracharge) + (piesaCardExtracharges.toDouble()) + (edit_amount!! * (piesaCardPercentage / 100))
                            println(decimal)
                            // val tax = "%.2f".format((progress!! * 0.032) + 0.3)

                            val tax = "%.2f".format(decimal)
                            Log.e("tax_ll", decimal.toString())
                            Log.e("final_all", tax)

                            global_stripe_percent = stripeCardPercentage
                            global_stripe_total_fee =
                                "%.2f".format((edit_amount!! * stripeCardPercentage / 100))
                                    .toDouble()
                            global_ex_amount = (stripeCardExtracharges * 100).toInt()
                            global_piesa_card_ex_amount = (piesaCardExtracharges * 100).toInt()
                            global_piesa_percent = piesaCardPercentage
                            global_piesa_total_fee =
                                "%.2f".format((edit_amount!! * piesaCardPercentage / 100))
                                    .toDouble()
                        }


                    } else {
                        Log.e("failed_0", response!!.body()!!.result!!.toString())

                        Toast.makeText(
                            this@EditRecurringDonationActivity,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun SavedCardsListApi(jsonObj: JsonObject) {

        loading_dialog.show()


        val apiInterface = ApiInterface.create()
        val call = apiInterface.savedCardsList(jsonObj)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<SavedCardListResponse> {
            override fun onFailure(call: Call<SavedCardListResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<SavedCardListResponse>,
                response: Response<SavedCardListResponse>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    val resp = response!!.body()
                    Log.e("STRIPERESPOMNSEDONATION", resp.toString())
                    if (response!!.body()!!.responseStatus == "1") {
                        // toast( response!!.body()!!.result!!)
                        for (i in 0 until response.body()!!.savedCardsList!!.data!!.size) {
                            if (sourceId.equals(response.body()!!.savedCardsList!!.data!![i].id!!)){
                                ll_card_id.visibility=View.VISIBLE
                                tv_single_name_id.setText(response.body()!!.savedCardsList!!.data!![i].name!!)
                                tv_single_card_no_id.setText("**** " + response.body()!!.savedCardsList!!.data!![i].dynamic_last4!!)
                            }


                        }


                    } else {
                        toast(response!!.body()!!.result!!)
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })


    }

    override fun onDestroy() {
        if (full_dialog.isShowing) {
            full_dialog.dismiss()
        }
        if (bank_full_dialog.isShowing) {
            bank_full_dialog.dismiss()
        }
        super.onDestroy()
    }
}
