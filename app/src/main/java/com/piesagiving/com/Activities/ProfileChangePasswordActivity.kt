package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.SessionSplash
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.util.regex.Pattern

class ProfileChangePasswordActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var et_currentpassword: EditText
    lateinit var et_newpassword: EditText
    lateinit var et_confirmpassword: EditText
    lateinit var tv_forgot_password: TextView
    lateinit var tv_submit: TextView
    lateinit var loading_dialog: Dialog

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var userDetails: HashMap<String, String>
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var userId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_profile_change_password)
        initialize()

        userDetails = sessionManager.getUserDetails()
        userId = userDetails[SessionManager.ID_KEY]!!

        iv_back.setOnClickListener {
            onBackPressed()
        }

        // login to app
        tv_submit.setOnClickListener {
            if (et_currentpassword.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileChangePasswordActivity,
                    "Current Password cannot be empty!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (et_newpassword.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileChangePasswordActivity,
                    "NewPassword cannot be empty!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (et_confirmpassword.text.toString().isEmpty()) {
                Toast.makeText(
                    this@ProfileChangePasswordActivity,
                    "Confirm Password cannot be empty!",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (et_newpassword.text.toString() != et_confirmpassword.text.toString()) {
                Toast.makeText(
                    this@ProfileChangePasswordActivity,
                    "Passwords does not match.",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (!isValidPassword(et_newpassword.text.toString().trim())) {
                Toast.makeText(
                    this@ProfileChangePasswordActivity,
                    getString(R.string.password_requirement),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if (isNetworkAvailable) {
                    val dataObj = JSONObject()
                    dataObj.put("uid", userId)
                    dataObj.put("password", et_currentpassword.text.toString())
                    dataObj.put("newPassword", et_newpassword.text.toString())
                    dataObj.put("confirmNewPassword", et_confirmpassword.text.toString())
                    val jsonParser = JsonParser()
                    val finalJsonObj = jsonParser.parse(dataObj.toString()) as JsonObject
                    changePasswordApi(finalJsonObj)
                } else {
                    Toast.makeText(
                        this@ProfileChangePasswordActivity,
                        resources.getString(R.string.no_network_available),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }

        // navigate to forgot password activity
        tv_forgot_password.setOnClickListener {
            val intent = Intent(this, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        et_currentpassword = findViewById(R.id.et_currentpassword)
        et_newpassword = findViewById(R.id.et_newpassword)
        et_confirmpassword = findViewById(R.id.et_confirmpassword)
        tv_forgot_password = findViewById(R.id.tv_forgot_password)

        tv_submit = findViewById(R.id.tv_submit)


        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ProfileChangePasswordActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ProfileChangePasswordActivity)
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun isValidPassword(password: String): Boolean {

        /* password matcher condition
        (? = .*\d)         must contains one digit from 0-9
        (? = .*[a-z])      must contains one lowercase characters
        (? = .*[A-Z])      must contains one uppercase characters
        (? = .*[@#$%])     must contains one special symbols in the list "@#$%"
        .                  match anything with previous condition checking
        { 6, 20 }          length at least 6 characters and maximum of 20*/

        val passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=])(?=\\S+$).{8,}$"
        val pattern = Pattern.compile(passwordPattern)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun changePasswordApi(final_object: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.changePasswordApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ProfileChangePasswordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        "1" -> {
                            Toast.makeText(
                                this@ProfileChangePasswordActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                            sessionManager.logout()
                            val sessionSplash = SessionSplash(this@ProfileChangePasswordActivity)
                            sessionSplash.setSplashScreen("1")
                            val intent =
                                Intent(
                                    this@ProfileChangePasswordActivity,
                                    LoginActivity::class.java
                                )
                            intent.putExtra("from_intent", "reset")
                            startActivity(intent)
                            finish()
                        }
                        else -> {
                            Toast.makeText(
                                this@ProfileChangePasswordActivity,
                                resp.result,
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

}
