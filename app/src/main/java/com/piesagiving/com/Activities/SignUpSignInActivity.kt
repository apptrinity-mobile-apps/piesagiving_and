package com.piesagiving.com.Activities

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R


class SignUpSignInActivity : AppCompatActivity() {

    lateinit var tv_sign_in:TextView
    lateinit var tv_create_account:TextView
    lateinit var cd:ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sesstion:SessionManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_sign_up_sign_in)

        // initialize view
        initialize()



        // goto login page
        tv_sign_in.setOnClickListener {
            val intent = Intent(this@SignUpSignInActivity,LoginActivity::class.java)
            intent.putExtra("from_intent","")
            startActivity(intent)
        }

        // goto register page
        tv_create_account.setOnClickListener {
            val intent = Intent(this@SignUpSignInActivity,RegisterAccountActivity::class.java)
            startActivity(intent)
        }

    }

    private fun initialize(){
        cd = ConnectionDetector(this)
        tv_sign_in = findViewById(R.id.tv_sign_in)
        tv_create_account = findViewById(R.id.tv_create_account)

        isNetworkAvailable = cd.isConnectingToInternet
        sesstion= SessionManager(this)

        if (sesstion.isLoggedIn) {

            val home_intent = Intent(this@SignUpSignInActivity, DashBoardActivity::class.java)
            home_intent.putExtra("to_fragment","")
            home_intent.putExtra("cat_id","")
            home_intent.putExtra("show_lock","yes")
            startActivity(home_intent)
            finish()
        }

    }

    override fun onBackPressed() {
        finishAffinity()
    }


}
