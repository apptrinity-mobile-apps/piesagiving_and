package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Adapters.AnnouncementsAdapter
import com.piesagiving.com.ApiInterface.AnnouncementsResponse
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

@SuppressLint("UseSwitchCompatOrMaterialCode,SetTextI18n")
class AnnouncementsListActivity : AppCompatActivity() {

    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var orgId = ""
    private var orgName = ""

    private lateinit var iv_back: ImageView
    private lateinit var loading_dialog: Dialog
    private lateinit var mAnnouncements: ArrayList<AnnouncementsResponse>
    private lateinit var rv_announcements: RecyclerView
    private lateinit var sw_notifications: SwitchMaterial
    private lateinit var tv_no_announcements: TextView
    private lateinit var tv_header: TextView
    private lateinit var cv_toast: CardView
    private lateinit var tv_toast_header: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_announncements_list)

        initialize()
        orgId = intent.getStringExtra("orgId")!!
        orgName = intent.getStringExtra("orgName")!!
        tv_header.text = "${getString(R.string.announcements)} of $orgName"
        iv_back.setOnClickListener {
            onBackPressed()
        }
        if (isNetworkAvailable) {
            val jObj = JSONObject()
            jObj.put("userId", userId)
            jObj.put("orgId", orgId)
            val jsonParser = JsonParser()
            val finalObj = jsonParser.parse(jObj.toString()) as JsonObject
            getAnnouncementsApi(finalObj)
        } else {
            Toast.makeText(
                this@AnnouncementsListActivity,
                getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        cv_toast.visibility = View.GONE
        sw_notifications.setOnClickListener {
            if (isNetworkAvailable) {
                val jObj = JSONObject()
                jObj.put("userId", userId)
                jObj.put("orgId", orgId)
                val jsonParser = JsonParser()
                val finalObj = jsonParser.parse(jObj.toString()) as JsonObject
                blockAnnouncements(finalObj)
            } else {
                Toast.makeText(
                    this@AnnouncementsListActivity,
                    getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
        sw_notifications.setOnCheckedChangeListener { buttonView, isChecked ->
            cv_toast.visibility = View.VISIBLE
            if (isChecked) {
                tv_toast_header.text =
                    getString(R.string.you_will_receive_announcement_notifications)
            } else {
                tv_toast_header.text =
                    getString(R.string.you_won_t_receive_announcement_notifications)
            }
            Handler().postDelayed({ cv_toast.visibility = View.GONE }, 2000)
        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        loadingDialog()

        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        iv_back = findViewById(R.id.iv_back)

        cv_toast = findViewById(R.id.cv_toast)
        tv_toast_header = findViewById(R.id.tv_toast_header)
        tv_header = findViewById(R.id.tv_header)
        sw_notifications = findViewById(R.id.sw_notifications)
        tv_no_announcements = findViewById(R.id.tv_no_announcements)
        rv_announcements = findViewById(R.id.rv_announcements)
        val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        rv_announcements.layoutManager = layoutManager
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getAnnouncementsApi(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getAnnouncementsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Toast.makeText(
                    this@AnnouncementsListActivity,
                    getString(R.string.try_again),
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val list = resp.announcements
                        mAnnouncements = list!!
                        if (mAnnouncements.size > 0) {
                            rv_announcements.visibility = View.VISIBLE
                            tv_no_announcements.visibility = View.GONE
                        } else {
                            rv_announcements.visibility = View.GONE
                            tv_no_announcements.visibility = View.VISIBLE
                        }
                        val adapter =
                            AnnouncementsAdapter(this@AnnouncementsListActivity, mAnnouncements)
                        rv_announcements.adapter = adapter
                        adapter.notifyDataSetChanged()
                        sw_notifications.isChecked = true
                    } else if (resp.responseStatus == "2") {
                        sw_notifications.isChecked = false
                        rv_announcements.visibility = View.GONE
                        tv_no_announcements.visibility = View.VISIBLE
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun blockAnnouncements(final_object: JsonObject) {
        val api = ApiInterface.create()
        val call = api.blockAnnouncementsApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {}

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

}