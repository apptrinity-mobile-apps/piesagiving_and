package com.piesagiving.com.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatSeekBar
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.FollowPojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap

@Suppress("DEPRECATION")
@SuppressLint("SetTextI18n")
class TitheCalculatorActivity : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_hint_text: LinearLayout
    lateinit var et_tithe_amount: EditText
    lateinit var et_percentage: EditText
    lateinit var tv_goalreachdonate: TextView
    lateinit var seekbar_tithecalc: AppCompatSeekBar
    lateinit var seekbar_reachedgoal: AppCompatSeekBar
    lateinit var tv_thitheweekly: TextView
    lateinit var tv_submit: TextView
    lateinit var tv_clear_data: TextView
    lateinit var tv_thithemonthly: TextView
    lateinit var iv_percentage: ImageView
    lateinit var iv_amount: ImageView
    lateinit var tv_thitheyearly: TextView
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    private lateinit var weekly_string: String
    private lateinit var monthly_string: String
    private lateinit var yearly_string: String
    private var totalamount: Double = 0.0

    lateinit var loading_dialog: Dialog
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var getUserTitheAmount = ""
    private var getUserTithePercent = ""

    var lock_percent_default = true
    var lock_amount_default = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_tithecalculator)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!
        if (intent.getStringExtra("totalamount") != null) {
            totalamount = intent.getStringExtra("totalamount")!!.toDouble()
        }
        Log.e("TOTALAMOUNT", "" + totalamount)
        et_tithe_amount = findViewById(R.id.et_tithe_amount)
        tv_submit = findViewById(R.id.tv_submit)
        iv_amount = findViewById(R.id.iv_amount)
        iv_percentage = findViewById(R.id.iv_percentage)
        et_percentage = findViewById(R.id.et_percentage)
        tv_goalreachdonate = findViewById(R.id.tv_goalreachdonate)
        seekbar_tithecalc = findViewById(R.id.seekbar_tithecalc)
        seekbar_reachedgoal = findViewById(R.id.seekbar_reachedgoal)
        tv_thitheweekly = findViewById(R.id.tv_thitheweekly)
        tv_thithemonthly = findViewById(R.id.tv_thithemonthly)
        tv_thitheyearly = findViewById(R.id.tv_thitheyearly)
        isNetworkAvailable = cd.isConnectingToInternet
        ll_hint_text = findViewById(R.id.ll_hint_text)
        tv_clear_data = findViewById(R.id.tv_clear_data)
        seekbar_tithecalc.isEnabled = false
        et_tithe_amount.isFocusable = false
        et_tithe_amount.isFocusableInTouchMode =
            false // user touches widget on phone with touch screen
        et_tithe_amount.isClickable = false
        et_percentage.isFocusable = false
        et_percentage.isFocusableInTouchMode =
            false // user touches widget on phone with touch screen
        et_percentage.isClickable = false
        val followobj = JSONObject()
        followobj.put("userId", userId)
        val followjParser = JsonParser()
        val followjObject = followjParser.parse(followobj.toString()) as JsonObject
        Log.e("dash_bard_input", followjObject.toString())
        getGivingCalculatorApi(followjObject)
        et_tithe_amount.setText("0")
        et_percentage.setText("0")
        iv_amount.setOnClickListener {
            if (lock_amount_default) {
                lock_amount_default = false
                iv_amount.setImageResource(R.drawable.unlock)
                et_tithe_amount.isFocusable = true
                et_tithe_amount.isFocusableInTouchMode =
                    true // user touches widget on phone with touch screen
                et_tithe_amount.isClickable = true
            } else {
                lock_amount_default = true
                iv_amount.setImageResource(R.drawable.ic_lock)
                et_tithe_amount.isFocusable = false
                et_tithe_amount.isFocusableInTouchMode =
                    false // user touches widget on phone with touch screen
                et_tithe_amount.isClickable = false
            }
        }
        iv_percentage.setOnClickListener {
            if (lock_percent_default) {
                lock_percent_default = false
                iv_percentage.setImageResource(R.drawable.unlock)
                et_percentage.isFocusable = true
                et_percentage.isFocusableInTouchMode =
                    true // user touches widget on phone with touch screen
                et_percentage.isClickable = true
                seekbar_tithecalc.isEnabled = true
            } else {
                lock_percent_default = true
                iv_percentage.setImageResource(R.drawable.ic_lock)
                et_percentage.isFocusable = false
                et_percentage.isFocusableInTouchMode =
                    false // user touches widget on phone with touch screen
                et_percentage.isClickable = false
                seekbar_tithecalc.isEnabled = false
            }
        }

        seekbar_tithecalc.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) { // You can have your own calculation for progress
                et_percentage.setText(progress.toString())
                if (!et_tithe_amount.text.toString().equals("")) {
                    val titheamtweekly = et_tithe_amount.text.toString().toDouble() * progress
                    Log.e("percent1", titheamtweekly.toString())
                    val titheamtperweekly = titheamtweekly / 100
                    Log.e("percent2", titheamtperweekly.toString())
                    val titheamtpertotalweekly = titheamtperweekly / 52
                    Log.e("percent3", titheamtpertotalweekly.toString())
                    val titheamtmonthly = et_tithe_amount.text.toString().toDouble() / 100
                    val titheamtpermonthly = titheamtmonthly * progress
                    val titheamtpertotalmonthly = titheamtpermonthly / 12
                    val titheamtyearly = et_tithe_amount.text.toString().toDouble() / 100
                    val titheamtperyearly = titheamtyearly * progress
                    val titheamtpertotalyearly = titheamtperyearly / 1
                    weekly_string = titheamtpertotalweekly.toString()
                    monthly_string = titheamtpertotalmonthly.toString()
                    yearly_string = titheamtpertotalyearly.toString()
                    Log.e(
                        "finalpercent",
                        "" + progress + "---------" + progress + "---------" + weekly_string + "------" + monthly_string + "------" + yearly_string + "------" + et_tithe_amount.text.toString()
                    )
                    tv_thitheweekly.text = "$${Formatter().format("%,.2f", weekly_string.toDouble())}"
                    tv_thithemonthly.text = "$${Formatter().format("%,.2f", monthly_string.toDouble())}"
                    tv_thitheyearly.text = "$${Formatter().format("%,.2f", yearly_string.toDouble())}"
                    val totalbar = totalamount / yearly_string.toDouble()
                    Log.e("totalbar", totalbar.toString())
                    val finaltotalbar = totalbar * 100
                    Log.e("finaltotalbar", "" + finaltotalbar.toInt())
                    if (finaltotalbar >= 100) {
                        seekbar_reachedgoal.progress = 100
                    } else {
                        seekbar_reachedgoal.progress = finaltotalbar.toInt()
                    }
                    val goalpercent = String.format("%.2f", finaltotalbar)
                    tv_goalreachdonate.text = "You’ve reached $goalpercent% of your goal so far!"
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        et_tithe_amount.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    if (p0!!.isNotEmpty()) {
                        ll_hint_text.visibility = View.GONE
                    }
                    Log.e("ET_TEXT", et_tithe_amount.text.toString())
                    et_tithe_amount.setSelection(et_tithe_amount.length())
                    val per = et_percentage.text.toString()
                    val perr = per.toDouble()
                    if (!et_tithe_amount.text.toString().equals("")) {
                        val titheamtweekly = et_tithe_amount.text.toString().toDouble() * perr
                        Log.e("percent1", titheamtweekly.toString())
                        val titheamtperweekly = titheamtweekly / 100
                        Log.e("percent2", titheamtperweekly.toString())
                        val titheamtpertotalweekly = titheamtperweekly / 52
                        Log.e("percent3", titheamtpertotalweekly.toString())
                        val titheamtmonthly = et_tithe_amount.text.toString().toDouble() / 100
                        val titheamtpermonthly = titheamtmonthly * perr
                        val titheamtpertotalmonthly = titheamtpermonthly / 12
                        val titheamtyearly = et_tithe_amount.text.toString().toDouble() / 100
                        val titheamtperyearly = titheamtyearly * perr
                        val titheamtpertotalyearly = titheamtperyearly / 1
                        weekly_string = titheamtpertotalweekly.toString()
                        monthly_string = titheamtpertotalmonthly.toString()
                        yearly_string = titheamtpertotalyearly.toString()
                        Log.e(
                            "finalpercent",
                            "" + perr + "---------" + perr + "---------" + weekly_string + "------" + monthly_string + "------" + yearly_string + "------" + et_tithe_amount.text.toString()
                        )
                        tv_thitheweekly.text = "$${Formatter().format("%,.2f", weekly_string.toDouble())}"
                        tv_thithemonthly.text = "$${Formatter().format("%,.2f", monthly_string.toDouble())}"
                        tv_thitheyearly.text = "$${Formatter().format("%,.2f", yearly_string.toDouble())}"
                        val totalbar = totalamount / yearly_string.toDouble()
                        Log.e("totalbar", totalbar.toString())
                        val finaltotalbar = totalbar * 100
                        Log.e("finaltotalbar", "" + finaltotalbar.toInt())
                        if (finaltotalbar >= 100) {
                            seekbar_reachedgoal.progress = 100
                        } else {
                            seekbar_reachedgoal.progress = finaltotalbar.toInt()
                        }
                        val goalpercent = String.format("%.2f", finaltotalbar)
                        tv_goalreachdonate.text =
                            "You’ve reached $goalpercent% of your goal so far!"
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Log.e("exception", et_tithe_amount.text.toString())
                }
            }
        })

        et_percentage.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                try {
                    if (p0!!.isNotEmpty()) {
                        ll_hint_text.visibility = View.GONE
                    }
                    et_percentage.setSelection(et_percentage.length())
                    seekbar_tithecalc.progress = (et_percentage.text.toString()).toDouble().toInt()
                    val per = et_percentage.text.toString()
                    val perr = per.toInt()
                    Log.e(
                        "percent",
                        per + "---------" + perr + "--------" + et_tithe_amount.toString()
                    )
                    val titheamtweekly = et_tithe_amount.text.toString().toDouble() * perr
                    Log.e("percent1", titheamtweekly.toString())
                    val titheamtperweekly = titheamtweekly / 100
                    Log.e("percent2", titheamtperweekly.toString())
                    val titheamtpertotalweekly = titheamtperweekly / 52
                    Log.e("percent3", titheamtpertotalweekly.toString())
                    val titheamtmonthly = et_tithe_amount.text.toString().toDouble() / 100
                    val titheamtpermonthly = titheamtmonthly * perr
                    val titheamtpertotalmonthly = titheamtpermonthly / 12
                    val titheamtyearly = et_tithe_amount.text.toString().toDouble() / 100
                    val titheamtperyearly = titheamtyearly * perr
                    val titheamtpertotalyearly = titheamtperyearly / 1
                    weekly_string = titheamtpertotalweekly.toString()
                    monthly_string = titheamtpertotalmonthly.toString()
                    yearly_string = titheamtpertotalyearly.toString()
                    Log.e(
                        "finalpercent",
                        per + "---------" + perr + "---------" + weekly_string + "------" + monthly_string + "------" + yearly_string + "------" + et_tithe_amount.text.toString()
                    )
                    tv_thitheweekly.text = "$${Formatter().format("%,.2f", weekly_string.toDouble())}"
                    tv_thithemonthly.text = "$${Formatter().format("%,.2f", monthly_string.toDouble())}"
                    tv_thitheyearly.text = "$${Formatter().format("%,.2f", yearly_string.toDouble())}"
                    val totalbar = totalamount / yearly_string.toDouble()
                    Log.e("totalbar", totalbar.toString())
                    val finaltotalbar = totalbar * 100
                    Log.e("finaltotalbar", "" + finaltotalbar.toInt())
                    if (finaltotalbar >= 100) {
                        seekbar_reachedgoal.progress = 100
                    } else {
                        seekbar_reachedgoal.progress = finaltotalbar.toInt()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                    Log.e("exception", et_percentage.text.toString())
                }
            }
        })

        tv_submit.setOnClickListener {
            if (et_tithe_amount.text.toString().equals("")) {
                Toast.makeText(
                    this@TitheCalculatorActivity,
                    "Enter amount",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (et_percentage.text.toString().equals("")) {
                Toast.makeText(
                    this@TitheCalculatorActivity,
                    "Enter percentage",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val followobj = JSONObject()
                followobj.put("userId", userId)
                followobj.put("amount", et_tithe_amount.text.toString())
                followobj.put("percentage", et_percentage.text.toString())
                val followjParser = JsonParser()
                val followjObject = followjParser.parse(followobj.toString()) as JsonObject
                Log.e("dash_bard_input", followjObject.toString())
                givingCalculatorApiCall(followjObject)
            }
        }

        tv_clear_data.setOnClickListener {
            when {
                lock_amount_default -> {
                    Toast.makeText(
                        this@TitheCalculatorActivity,
                        "Please unlock and try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                lock_percent_default -> {
                    Toast.makeText(
                        this@TitheCalculatorActivity,
                        "Please unlock and try again!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    et_percentage.setText("0")
                    et_tithe_amount.setText("0")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        seekbar_tithecalc.setProgress(0, true)
                    } else {
                        seekbar_tithecalc.progress = 0
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        seekbar_reachedgoal.setProgress(0, true)
                    } else {
                        seekbar_reachedgoal.progress = 0
                    }
                    tv_goalreachdonate.text = "You’ve reached 0% of your goal so far!"
                    val jObj = JSONObject()
                    jObj.put("userId", userId)
                    jObj.put("amount", et_tithe_amount.text.toString())
                    jObj.put("percentage", et_percentage.text.toString())
                    val jParser = JsonParser()
                    val jObject = jParser.parse(jObj.toString()) as JsonObject
                    Log.e("_input", jObject.toString())
                    givingCalculatorApiCall(jObject)
                }
            }
        }
    }

    private fun givingCalculatorApiCall(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.givingCalculatorApi(json_obj)
        call.enqueue(object : Callback<FollowPojo> {
            override fun onFailure(call: Call<FollowPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<FollowPojo>,
                response: Response<FollowPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        Toast.makeText(
                            this@TitheCalculatorActivity,
                            response.body()!!.result.toString(),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        /*Toast.makeText(
                            this@TitheCalculatorActivity,
                            "Data Fetching Failed",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getGivingCalculatorApi(json_obj: JsonObject) {
        loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getUserGivingCalculatorApi(json_obj)
        call.enqueue(object : Callback<FollowPojo> {
            override fun onFailure(call: Call<FollowPojo>, t: Throwable) {
                try {
                    Log.d("TAG", "citiesApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@TitheCalculatorActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<FollowPojo>,
                response: Response<FollowPojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.get_user_giving_calculator
                            if (data!!.size > 0) {
                                getUserTitheAmount = data[0].amount.toString()
                                getUserTithePercent = data[0].percentage.toString()
                                et_tithe_amount.setText(getUserTitheAmount)
                                et_percentage.setText(getUserTithePercent)
                                if (getUserTitheAmount == "" && getUserTithePercent == "") {
                                    ll_hint_text.visibility = View.VISIBLE
                                } else {
                                    ll_hint_text.visibility = View.GONE
                                }
                            } else {
                                /*Toast.makeText(
                                    this@TitheCalculatorActivity,
                                    getString(R.string.no_record_available),
                                    Toast.LENGTH_SHORT
                                ).show()*/
                            }
                        }
                        else -> {
                            ll_hint_text.visibility = View.VISIBLE
                            /* Toast.makeText(
                                 this@TitheCalculatorActivity,
                                 getString(R.string.no_record_available),
                                 Toast.LENGTH_SHORT
                             ).show()*/
                        }
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}

