package com.piesagiving.com.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.piesagiving.com.Adapters.FaqAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.FaqArrayListResponse
import com.piesagiving.com.ApiInterface.FaqResponsePojo
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.HandSetModelInfo
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BrowseFaqs : AppCompatActivity() {

    lateinit var iv_back: ImageView
    lateinit var ll_qtn1_bg: LinearLayout
    lateinit var ll_qtn2_bg: LinearLayout
    lateinit var ll_qtn3_bg: LinearLayout
    lateinit var ll_qtn4_bg: LinearLayout
    lateinit var ll_qtn5_bg: LinearLayout
    lateinit var ll_qtn6_bg: LinearLayout
    lateinit var ll_qtn7_bg: LinearLayout
    lateinit var ll_qtn8_bg: LinearLayout
    lateinit var ll_qtn9_bg: LinearLayout
    lateinit var faq_image1: ImageView
    lateinit var faq_image2: ImageView
    lateinit var faq_image3: ImageView
    lateinit var faq_image4: ImageView
    lateinit var faq_image5: ImageView
    lateinit var faq_image6: ImageView
    lateinit var faq_image7: ImageView
    lateinit var faq_image8: ImageView
    lateinit var faq_image9: ImageView
    lateinit var faq_image1_answer: TextView
    lateinit var faq_image2_answer: TextView
    lateinit var faq_image3_answer: TextView
    lateinit var faq_image4_answer: TextView
    lateinit var faq_image5_answer: TextView
    lateinit var faq_image6_answer: TextView
    lateinit var faq_image7_answer: TextView
    lateinit var faq_image8_answer: TextView
    lateinit var faq_image9_answer: TextView
    lateinit var tv_faq_qtn1: TextView
    lateinit var tv_faq_qtn2: TextView
    lateinit var tv_faq_qtn3: TextView
    lateinit var tv_faq_qtn4: TextView
    lateinit var tv_faq_qtn5: TextView
    lateinit var tv_faq_qtn6: TextView
    lateinit var tv_faq_qtn7: TextView
    lateinit var tv_faq_qtn8: TextView
    lateinit var tv_faq_qtn9: TextView


    lateinit var rv_faq_id: RecyclerView
    lateinit var loading_dialog: Dialog
    lateinit var faqArraylist: ArrayList<FaqArrayListResponse>
    lateinit var cd: ConnectionDetector
    var isNetworkAvailable = false
    lateinit var sessionManager: SessionManager
    var from_intent = ""
    lateinit var handSetModelInfo: HandSetModelInfo
    var deviceName = ""
    var arrowdownup = true
    lateinit var fade_in_anim: Animation
    lateinit var fade_out_anim: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_faqs)
        initialize()
        // go to previous activity
        iv_back.setOnClickListener {
            onBackPressed()
        }


        faq_image1.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image1_answer.visibility = View.VISIBLE
                faq_image1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image1_answer.startAnimation(fade_in_anim)
                ll_qtn1_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image1_answer.visibility = View.GONE
                faq_image1.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn1_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image2.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image2_answer.visibility = View.VISIBLE
                faq_image2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image2_answer.startAnimation(fade_in_anim)
                ll_qtn2_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image2_answer.visibility = View.GONE
                faq_image2.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn2_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image3.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image3_answer.visibility = View.VISIBLE
                faq_image3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image1_answer.startAnimation(fade_in_anim)
                ll_qtn3_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image3_answer.visibility = View.GONE
                faq_image3.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn3_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image4.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image4_answer.visibility = View.VISIBLE
                faq_image4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image4_answer.startAnimation(fade_in_anim)
                ll_qtn4_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image4_answer.visibility = View.GONE
                faq_image4.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn4_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image5.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image5_answer.visibility = View.VISIBLE
                faq_image5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image5_answer.startAnimation(fade_in_anim)
                ll_qtn5_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image5_answer.visibility = View.GONE
                faq_image5.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn5_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image6.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image6_answer.visibility = View.VISIBLE
                faq_image6.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image6_answer.startAnimation(fade_in_anim)
                ll_qtn6_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image6_answer.visibility = View.GONE
                faq_image6.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn6_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image7.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image7_answer.visibility = View.VISIBLE
                faq_image7.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image7_answer.startAnimation(fade_in_anim)
                ll_qtn7_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image7_answer.visibility = View.GONE
                faq_image7.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn7_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image8.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image8_answer.visibility = View.VISIBLE
                faq_image8.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image8_answer.startAnimation(fade_in_anim)
                ll_qtn8_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image8_answer.visibility = View.GONE
                faq_image8.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn8_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        faq_image9.setOnClickListener {
            if (arrowdownup) {
                arrowdownup = false
                faq_image9_answer.visibility = View.VISIBLE
                faq_image9.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_collapse))
                faq_image9_answer.startAnimation(fade_in_anim)
                ll_qtn9_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.header))
            } else {
                arrowdownup = true
                faq_image9_answer.visibility = View.GONE
                faq_image9.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_expand))
                ll_qtn9_bg.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
        }
        FaqApi()

    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        handSetModelInfo = HandSetModelInfo()
        sessionManager = SessionManager(this)
        iv_back = findViewById(R.id.iv_back)
        rv_faq_id = findViewById(R.id.rv_faq_id)

        ll_qtn1_bg = findViewById(R.id.ll_qtn1_bg)
        ll_qtn2_bg = findViewById(R.id.ll_qtn2_bg)
        ll_qtn3_bg = findViewById(R.id.ll_qtn3_bg)
        ll_qtn4_bg = findViewById(R.id.ll_qtn4_bg)
        ll_qtn5_bg = findViewById(R.id.ll_qtn5_bg)
        ll_qtn6_bg = findViewById(R.id.ll_qtn6_bg)
        ll_qtn7_bg = findViewById(R.id.ll_qtn7_bg)
        ll_qtn8_bg = findViewById(R.id.ll_qtn8_bg)
        ll_qtn9_bg = findViewById(R.id.ll_qtn9_bg)
        faq_image1 = findViewById(R.id.faq_image1)
        faq_image2 = findViewById(R.id.faq_image2)
        faq_image3 = findViewById(R.id.faq_image3)
        faq_image4 = findViewById(R.id.faq_image4)
        faq_image5 = findViewById(R.id.faq_image5)
        faq_image6 = findViewById(R.id.faq_image6)
        faq_image7 = findViewById(R.id.faq_image7)
        faq_image8 = findViewById(R.id.faq_image8)
        faq_image9 = findViewById(R.id.faq_image9)
        faq_image1_answer = findViewById(R.id.faq_image1_answer)
        faq_image2_answer = findViewById(R.id.faq_image2_answer)
        faq_image3_answer = findViewById(R.id.faq_image3_answer)
        faq_image4_answer = findViewById(R.id.faq_image4_answer)
        faq_image5_answer = findViewById(R.id.faq_image5_answer)
        faq_image6_answer = findViewById(R.id.faq_image6_answer)
        faq_image7_answer = findViewById(R.id.faq_image7_answer)
        faq_image8_answer = findViewById(R.id.faq_image8_answer)
        faq_image9_answer = findViewById(R.id.faq_image9_answer)
        tv_faq_qtn1 = findViewById(R.id.tv_faq_qtn1)
        tv_faq_qtn2 = findViewById(R.id.tv_faq_qtn2)
        tv_faq_qtn3 = findViewById(R.id.tv_faq_qtn3)
        tv_faq_qtn4 = findViewById(R.id.tv_faq_qtn4)
        tv_faq_qtn5 = findViewById(R.id.tv_faq_qtn5)
        tv_faq_qtn6 = findViewById(R.id.tv_faq_qtn6)
        tv_faq_qtn7 = findViewById(R.id.tv_faq_qtn7)
        tv_faq_qtn8 = findViewById(R.id.tv_faq_qtn8)
        tv_faq_qtn9 = findViewById(R.id.tv_faq_qtn9)


        isNetworkAvailable = cd.isConnectingToInternet
        deviceName = handSetModelInfo.getDeviceName
        Log.d("handSetModelInfo", deviceName)
        fade_in_anim = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        fade_out_anim = AnimationUtils.loadAnimation(this, R.anim.fade_out)
    }

    private fun FaqApi() {
        loading_dialog.show()
        faqArraylist = ArrayList()
        faqArraylist.clear()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.getFAQsApi()
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<FaqResponsePojo> {
            override fun onFailure(call: Call<FaqResponsePojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<FaqResponsePojo>,
                response: Response<FaqResponsePojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()

                try {
                    if (response!!.body()!!.responseStatus!!.toString() == "1") {
                        for (i in 0 until response.body()!!.item_tagsList!!.size) {
                            faqArraylist.add(response.body()!!.item_tagsList!!.get(i))
                        }
                        val layoutManager =
                            LinearLayoutManager(this@BrowseFaqs, RecyclerView.VERTICAL, false)
                        rv_faq_id.layoutManager = layoutManager
                        rv_faq_id.setHasFixedSize(true)
                        val adapter = FaqAdapter(this@BrowseFaqs, faqArraylist)
                        rv_faq_id.adapter = adapter
                        adapter.notifyDataSetChanged()
                    } else {
                        Toast.makeText(
                            this@BrowseFaqs,
                            "Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

}

