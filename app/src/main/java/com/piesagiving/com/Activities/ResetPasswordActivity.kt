package com.piesagiving.com.Activities

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Pattern

class ResetPasswordActivity : AppCompatActivity() {

    lateinit var et_code: EditText
    lateinit var et_new_password: EditText
    lateinit var et_confirm_password: EditText
    lateinit var tv_submit: TextView
    lateinit var tv_login: TextView
    lateinit var ll_reset_success: LinearLayout
    lateinit var ll_reset_edit: LinearLayout
    lateinit var loading_dialog: Dialog

    lateinit var cd: ConnectionDetector
    lateinit var sessionManager: SessionManager
    var isNetworkAvailable = false
    var email = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_reset_password)

        initialize()
        email = intent.getStringExtra("email")!!

        ll_reset_edit.visibility = View.VISIBLE
        ll_reset_success.visibility = View.GONE

        // navigate to login after successful password change.
        tv_login.setOnClickListener {
            if (sessionManager.isLoggedIn) {
                sessionManager.logout()
            }
            val intent = Intent(this@ResetPasswordActivity, LoginActivity::class.java)
            intent.putExtra("from_intent", "reset")
            startActivity(intent)
            finish()
        }

        // submit new password
        tv_submit.setOnClickListener {
            if (isNetworkAvailable) {
                if (isValid()) {
                    val jObj = JSONObject()
                    jObj.put("email", email)
                    jObj.put("otp", et_code.text.toString())
                    jObj.put("newPassword", et_new_password.text.toString())
                    jObj.put("confirmNewPassword", et_confirm_password.text.toString())
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(jObj.toString()) as JsonObject
                    resetPassword(finalObject = finalObj)
                }
            } else {
                Toast.makeText(
                    this@ResetPasswordActivity,
                    getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun initialize() {
        loadingDialog()
        cd = ConnectionDetector(this)
        sessionManager = SessionManager(this)
        isNetworkAvailable = cd.isConnectingToInternet
        et_code = findViewById(R.id.et_code)
        et_new_password = findViewById(R.id.et_new_password)
        et_confirm_password = findViewById(R.id.et_confirm_password)
        tv_submit = findViewById(R.id.tv_submit)
        tv_login = findViewById(R.id.tv_login)
        ll_reset_success = findViewById(R.id.ll_reset_success)
        ll_reset_edit = findViewById(R.id.ll_reset_edit)
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(this@ResetPasswordActivity)
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(this@ResetPasswordActivity).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun isValidPassword(password: String): Boolean {

        /* password matcher condition
        (? = .*\d)         must contains one digit from 0-9
        (? = .*[a-z])      must contains one lowercase characters
        (? = .*[A-Z])      must contains one uppercase characters
        (? = .*[@#$%])     must contains one special symbols in the list "@#$%"
        .                  match anything with previous condition checking
        { 6, 20 }          length at least 6 characters and maximum of 20*/

        val passwordPattern = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#\$%&*()-+=^.])(?=\\S+$).{8,}$"
        val pattern = Pattern.compile(passwordPattern)
        val matcher = pattern.matcher(password)
        return matcher.matches()
    }

    private fun isValid(): Boolean {
        if (et_code.text.toString().isEmpty()) {
            Toast.makeText(
                this@ResetPasswordActivity,
                "Enter OTP sent to your mail",
                Toast.LENGTH_SHORT
            )
                .show()
        } else if (et_new_password.text.toString().isEmpty()) {
            Toast.makeText(
                this@ResetPasswordActivity,
                "Password must not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_confirm_password.text.toString().isEmpty()) {
            Toast.makeText(
                this@ResetPasswordActivity,
                "Password must not be empty",
                Toast.LENGTH_SHORT
            ).show()
        } else if (et_new_password.text.toString() != et_confirm_password.text.toString()) {
            Toast.makeText(
                this@ResetPasswordActivity,
                "Passwords must be same",
                Toast.LENGTH_SHORT
            ).show()
        } else if (!isValidPassword(et_new_password.text.toString().trim())) {
            Toast.makeText(
                this@ResetPasswordActivity,
                getString(R.string.password_requirement),
                Toast.LENGTH_SHORT
            ).show()
        } else {
            return true
        }
        return false
    }

    private fun resetPassword(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.passwordResetApi(body = finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Log.d("sendOtp", "resetPasswordApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        this@ResetPasswordActivity,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        ll_reset_edit.visibility = View.GONE
                        ll_reset_success.visibility = View.VISIBLE
                    } else {
                        // do nothing
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }
}
