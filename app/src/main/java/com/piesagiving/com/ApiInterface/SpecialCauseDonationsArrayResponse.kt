package com.piesagiving.com.ApiInterface

class SpecialCauseDonationsArrayResponse {

    val comments:ArrayList<CommentsResponse>?=null
    val donatedBy:String?=null
    val profilePic:String?=null
    val userId:String?=null
}