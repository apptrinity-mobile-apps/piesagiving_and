package com.piesagiving.com.ApiInterface

class FundraisersDataResponse {
    var amountNeeded: Double? = null
    var amountRaised: Double? = null
    var amountRaisedPercent: Double? = null
    var goalAmount: Double? = null
    var category: String? = null
    var description: String? = null
    var createdAgo: String? = null
    var donationsCount: Int? = null
    var id: String? = null
    var image: String? = null
    var name: String? = null
    var fundraiserLink: String? = null
    var organizationName: String? = null
    var orgId: String? = null
    var status: String? = null
    var streetAddress: String? = null
    var followStatus: Boolean? = null

    var firstDonationRecord: DonationsDataResponse? = null
    var latestDonationRecord: DonationsDataResponse? = null
    var topDonationRecord: DonationsDataResponse? = null
    var latestDonationsList: ArrayList<DonationsDataResponse>? = null
    var allDonationsList: ArrayList<DonationsDataResponse>? = null
    var topDonationsList: ArrayList<DonationsDataResponse>? = null
    var wordsOfSupport: ArrayList<DonationsDataResponse>? = null
}

class DonationsDataResponse {
    var createdOn: String? = null
    var donationAmount: Double? = null
    var donationMessage: String? = null
    var firstName: String? = null
    var lastName: String? = null
}