package com.piesagiving.com.ApiInterface

class HistoryResultsResponse {
    /*  {
            "date": "2020-06-15",
            "time": "18:07",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        }*/
    val date:String?=null
    val time:String?=null
    val type:String?=null
    val location:String?=null
    val status:Int?=null

}