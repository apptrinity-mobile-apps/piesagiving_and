package com.piesagiving.com.ApiInterface

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface StripeInterFace {

    @Headers("Authorization: Bearer $token")
    @FormUrlEncoded
    @POST("tokens")
    fun StripeCreatBankAccount(
        @Field("bank_account[country]") country: String,
        @Field("bank_account[currency]") currency: String,
        @Field("bank_account[account_holder_name]") user_name: String,
        @Field("bank_account[account_holder_type]") type: String,
        @Field("bank_account[routing_number]") route: String,
        @Field("bank_account[account_number]") acnumber: String
    ): Call<CreateBankAccountPojo>


    @Headers("Authorization: Bearer $token")
    @FormUrlEncoded
    @POST("tokens")
    fun StripeCardToken(
        @Field("card[number]") number: String,
        @Field("card[exp_month]") exp_month: Int,
        @Field("card[exp_year]") exp_year: Int,
        @Field("card[cvc]") cvc: String,
        @Field("card[name]") name: String
    ): Call<CardTokenPojo>

    companion object Factory {

        private const val BASE_URL = "https://api.stripe.com/v1/"

        // live token
//        private const val token = "pk_live_w4Epwz1onHynjfjaZFF2E7Dr00itQEIs9e"
        // testing token
        private const val token = "pk_test_8AhIcIToh3eZFUBaX7WyqF3000Cge8sBFL"

        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)

        fun create(): StripeInterFace {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(StripeInterFace::class.java)
        }
    }

}


