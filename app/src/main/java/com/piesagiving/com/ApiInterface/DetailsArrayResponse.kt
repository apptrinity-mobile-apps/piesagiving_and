package com.piesagiving.com.ApiInterface

class DetailsArrayResponse {

    val categoryId: String? = null
    val categoryName: String? = null
    val ceoEmail: String? = null
    val ceoMobileNumber: String? = null
    val ceoName: String? = null
    val cfoEmail: String? = null
    val ceoType: String? = null
    val cfoMobileNumber: String? = null
    val cfoName: String? = null
    val cfoType: String? = null
    val displayName: String? = null
    val id: String? = null
    val organizationType: String? = null
    val pastorEmail: String? = null
    val pastorMobileNumber: String? = null
    val pastorName: String? = null

    //    val pastorType:String?=null
    val phoneNumber: String? = null
    val status: Int? = null
    val streetAddress: String? = null
    val treasurerEmail: String? = null
    val treasurerMobileNumber: String? = null
    val treasurerName: String? = null
    val treasurerType: String? = null
    val websiteLink: String? = null
    val followStatus: Int? = null
    val profilePic: String? = null
    val bio: String? = null
    val ceoImage: String? = null
    val cfoImage: String? = null
    val pastorImage: String? = null
    val treasurerImage: String? = null

}