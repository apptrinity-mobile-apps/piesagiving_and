package com.piesagiving.com.ApiInterface

class LocationPojo {
    /*{
    "status": "success",
    "country": "India",
    "countryCode": "IN",
    "region": "TG",
    "regionName": "Telangana",
    "city": "Hyderabad",
    "zip": "500009",
    "lat": 17.3846,
    "lon": 78.4574,
    "timezone": "Asia/Kolkata",
    "isp": "City Online",
    "org": "",
    "as": "AS17483 CityOnline Services Ltd",
    "query": "49.156.151.123"
}*/

    val country:String?=null
    val countryCode:String?=null
    val regionName:String?=null
    val city:String?=null

}