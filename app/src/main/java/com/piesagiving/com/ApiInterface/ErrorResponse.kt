package com.piesagiving.com.ApiInterface

class ErrorResponse {
    /*  "code": "routing_number_invalid",
    "doc_url": "https://stripe.com/docs/error-codes/routing-number-invalid",
    "message": "Routing number must have 9 digits",
    "param": "bank_account[routing_number]",
    "type": "invalid_request_error"*/
    val code:String?=null
    val message:String?=null
    val param:String?=null
    val type:String?=null

}