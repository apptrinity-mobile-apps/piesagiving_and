package com.piesagiving.com.ApiInterface

class SavedCardListResponse {

    var responseStatus: String? = null
    var result: String? = null
    var savedCardsList: SavedCardsListDataResponse? = null

    val customerDetails:CustomerDetailsList?=null
}
