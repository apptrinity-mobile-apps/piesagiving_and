package com.piesagiving.com.ApiInterface

class CategoriesDataResponse {

    var categoryImageActive: String? = null
    var categoryImageInActive: String? = null
    var categoryName: String? = null
    var id: String? = null
    var status: Int? = null

}
