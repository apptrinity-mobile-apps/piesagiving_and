package com.piesagiving.com.ApiInterface

class RecurringDonationsListResponse {
    /*{
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1448,
            "cardType": "PNC BANK, NA",
            "endDate": null,
            "id": "5f52bdea1c77cfe43fa905c3",
            "isHasEnd": false,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "general_fund",
            "reccurringType": "Yearly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "10-31-2020",
            "status": 1,
            "subscriptionId": "sub_HxijnWjrd33SJn"
        }*/

    val amount: Double? = null
    val cardHolderName: String? = null
    val cardLast4Digits: Int? = null
    val cardType: String? = null
    val endDate: String? = null
    val id: String? = null
    val name: String? = null
    val isHasEnd: Boolean? = null
    val isReccurring: Boolean? = null
    val orgCityId: String? = null
    val orgId: String? = null
    val orgName: String? = null
    val orgStateId: String? = null
    val orgStreetAddress: String? = null
    val projectId: String? = null
    val causeId: String? = null
    val reccurringType: String? = null
    val projectName: String? = null
    val paymentType: String? = null
    val startDate: String? = null
    val transactionId: String? = null
    val status: Int? = null
    val userId: String? = null
    val sourceId: String? = null
    val subscriptionId: String? = null
    val subscriptionAmount: String? = null
    val totalAmount: String? = null
    val schedules: ArrayList<ScheduleArrayList>? = null

}
