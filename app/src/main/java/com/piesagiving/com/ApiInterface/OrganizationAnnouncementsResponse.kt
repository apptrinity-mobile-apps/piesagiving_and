package com.piesagiving.com.ApiInterface

class OrganizationAnnouncementsResponse {
    val orgId: String? = null
    val orgName: String? = null
    val profilePic: String? = null
}
