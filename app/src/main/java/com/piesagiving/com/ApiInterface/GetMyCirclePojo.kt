package com.piesagiving.com.ApiInterface

class GetMyCirclePojo {

    val charityList: ArrayList<CharityListDataResponse>? = null
    val churchList: ArrayList<CharityListDataResponse>? = null
    val SpecialCauses: ArrayList<CharityListDataResponse>? = null
    val user_followers: ArrayList<CharityListDataResponse>? = null
    val fundraiserList: ArrayList<FundRaiserFavouriteListDataResponse>? = null
    val responseStatus: Int? = null
    val result: String? = null

}
