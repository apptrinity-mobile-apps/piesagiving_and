package com.piesagiving.com.ApiInterface

class WalletDonationsPojo{
    /*{
    "responseStatus": 1,
    "result": "Wallet donations fetched successfully",
    "userWalletData": {
        "charityDonations": 7406,
        "churchDonations": 180,
        "specialCauseDonations": 750
    }
}*/

    val responseStatus:Int?=null
    val result:String?=null
    val userWalletData:WalletDonationsObjResponse?=null

}