package com.piesagiving.com.ApiInterface

class FundraisersResponse {
    var responseStatus: Int? = null
    var result: String? = null
    var fundraiserData: FundraisersDataResponse? = null
}
