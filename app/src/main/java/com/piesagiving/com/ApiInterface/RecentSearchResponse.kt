package com.piesagiving.com.ApiInterface

class RecentSearchResponse {

    var responseStatus: String? = null
    var result: String? = null
    var get_searches_list: ArrayList<RecentSearchDataResponse>? = null

}
