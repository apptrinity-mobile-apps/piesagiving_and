package com.piesagiving.com.ApiInterface

class CardTokenPojo {
    /*{
  "id": "tok_1H4g44KijxtVwMc3L5NODfr9",
  "object": "token",
  "card": {
    "id": "card_1H4g43KijxtVwMc3HkkdaHw2",
    "object": "card",
    "address_city": null,
    "address_country": null,
    "address_line1": null,
    "address_line1_check": null,
    "address_line2": null,
    "address_state": null,
    "address_zip": null,
    "address_zip_check": null,
    "brand": "Visa",
    "country": "US",
    "cvc_check": "unchecked",
    "dynamic_last4": null,
    "exp_month": 8,
    "exp_year": 2021,
    "funding": "credit",
    "last4": "4242",
    "metadata": {},
    "name": "anvesh",
    "tokenization_method": null
  },
  "client_ip": "49.156.151.148",
  "created": 1594702104,
  "livemode": false,
  "type": "card",
  "used": false
}*/

    val id:String?=null
    val client_ip:String?=null
    val card:CardTokenResponse?=null
}