package com.piesagiving.com.ApiInterface

class RecurringDonationsResponse {


    val result: String? = null
    val responseStatus: String? = null
    val church_rec_donations: ArrayList<RecurringDonationsListResponse>? = null
    val charity_rec_donations: ArrayList<RecurringDonationsListResponse>? = null
    val charity_general_fund: ArrayList<RecurringDonationsListResponse>? = null
    val charity_project_donations: ArrayList<RecurringDonationsListResponse>? = null
    val church_general_fund: ArrayList<RecurringDonationsListResponse>? = null
    val church_project_donations: ArrayList<RecurringDonationsListResponse>? = null
    val inactive_charity_general_fund: ArrayList<RecurringDonationsListResponse>? = null
    val inactive_charity_project_donations: ArrayList<RecurringDonationsListResponse>? = null
    val inactive_church_general_fund: ArrayList<RecurringDonationsListResponse>? = null
    val inactive_church_project_donations: ArrayList<RecurringDonationsListResponse>? = null

    /*{
    "charity_general_fund": [],
    "charity_project_donations": [],
    "church_general_fund": [
        {
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1448,
            "cardType": "PNC BANK, NA",
            "endDate": null,
            "id": "5f52bdea1c77cfe43fa905c3",
            "isHasEnd": false,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "general_fund",
            "reccurringType": "Yearly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "10-31-2020",
            "status": 1,
            "subscriptionId": "sub_HxijnWjrd33SJn"
        }
    ],
    "church_project_donations": [
        {
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1448,
            "cardType": "PNC BANK, NA",
            "endDate": "09-18-2020",
            "id": "5f523dd14e57cecd587bbb24",
            "isHasEnd": true,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "IPC Himachal ",
            "reccurringType": "Weekly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "09-04-2020",
            "status": 1,
            "subscriptionId": "sub_HxZuG39q18Uw4Z"
        },
        {
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1448,
            "cardType": "PNC BANK, NA",
            "endDate": null,
            "id": "5f52413b580f78a8f5f6e4af",
            "isHasEnd": false,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "Building Fund ",
            "reccurringType": "Quarter Yearly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "09-04-2020",
            "status": 1,
            "subscriptionId": "sub_Hxa9Cgyy0aPvI0"
        },
        {
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1744,
            "cardType": "HANMI BANK",
            "endDate": null,
            "id": "5f52417129e0e1e5cbc6c817",
            "isHasEnd": false,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "IPC Himachal ",
            "reccurringType": "Half Yearly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "09-04-2020",
            "status": 1,
            "subscriptionId": "sub_HxaApp1Dl4SBmT"
        },
        {
            "amount": "5.00",
            "cardHolderName": "Titus Eapen",
            "cardLast4Digits": 1744,
            "cardType": "HANMI BANK",
            "endDate": null,
            "id": "5f5241bea3126e579a873bc5",
            "isHasEnd": false,
            "name": "IPC Chicago",
            "orgCityId": "Chicago",
            "orgId": "5f2aeead8b85b0e44d44f7cc",
            "orgStateId": "Illinois",
            "orgStreetAddress": "52 E Northwest Hwy",
            "projectName": "IPC Himachal ",
            "reccurringType": "Yearly",
            "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],
            "startDate": "09-04-2020",
            "status": 1,
            "subscriptionId": "sub_HxaBM6kmqqrYvE"
        }
    ],
    "responseStatus": 1,
    "result": "Recurring donations list fetched successfully"
}*/

}
