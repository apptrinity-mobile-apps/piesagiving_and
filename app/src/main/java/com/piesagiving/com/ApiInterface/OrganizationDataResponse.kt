package com.piesagiving.com.ApiInterface

import com.google.gson.JsonArray

class OrganizationDataResponse {

    var cityId: String? = null
    var cityName: String? = null
    // "id" is special cause id when "special cause" is selected, else it is organization Id
    var id: String? = null
    var organizationName: String? = null
    var profilePic: String? = null
    var streetAddress: String? = null
    // "organizationId" is organization Id when "special cause" is selected
    var organizationId: String? = null
    var categoryTypes: JsonArray? = null
}
