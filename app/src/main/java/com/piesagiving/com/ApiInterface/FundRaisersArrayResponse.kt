package com.piesagiving.com.ApiInterface

class FundRaisersArrayResponse {
    val category: String? = null
    val createdOn: String? = null
    var description: String? = null
    var endDate: String? = null
    var goalAmount: Double? = null
    var id: String? = null
    var image: String? = null
    var name: String? = null
    var startDate: String? = null

}
