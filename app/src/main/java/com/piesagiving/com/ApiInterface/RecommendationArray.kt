package com.piesagiving.com.ApiInterface

class RecommendationArray {

    /* {
            "coverPhoto": null,
            "displayName": "churuch",
            "id": "5ed61d4af190ba2c06a8bad5",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ed61d4af190ba2c06a8bad5.jpg"
        },*/

    val coverPhoto:String?=null
    val displayName:String?=null
    val id:String?=null
    val profilePic:String?=null
    val location:String?=null
    val organizationType:String?=null
    val name:String?=null
    val type:String?=null

}