package com.piesagiving.com.ApiInterface

class SearchOrganizationCategoriesResponse {

    var responseStatus: String? = null
    var result: String? = null
    var categoryTypeList:ArrayList<CategoryTypeDataResponse>? = null
    var organizationList:ArrayList<OrganizationDataResponse>? = null

}
