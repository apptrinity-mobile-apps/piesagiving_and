package com.piesagiving.com.ApiInterface

class OrganizationDetailsPojo {
    val organization: ArrayList<DetailsArrayResponse>? = null
    val churchProject: ArrayList<ChurchProjectArrayResponse>? = null
    val specialCauses: ArrayList<ChurchProjectArrayResponse>? = null
    val organisation_follwers: ArrayList<OrganisationFollowersArrayResponse>? = null
    val fundraisers_list: ArrayList<FundRaisersArrayResponse>? = null
    val responseStatus: Int? = null
    val organisation_follwers_count: Int? = null
    val result: String? = null
}