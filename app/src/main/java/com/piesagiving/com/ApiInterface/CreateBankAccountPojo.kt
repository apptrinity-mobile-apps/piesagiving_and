package com.piesagiving.com.ApiInterface

class CreateBankAccountPojo {
    /*{
  "id": "btok_1Gxr4h2eZvKYlo2CwrZGu6Bp",
  "object": "token",
  "bank_account": {
    "id": "ba_1Gxr4h2eZvKYlo2CyAGNDMdy",
    "object": "bank_account",
    "account_holder_name": "Jenny Rosen",
    "account_holder_type": "individual",
    "bank_name": "STRIPE TEST BANK",
    "country": "US",
    "currency": "usd",
    "fingerprint": "1JWtPxqbdX5Gamtc",
    "last4": "6789",
    "routing_number": "110000000",
    "status": "new"
  },
  "client_ip": "49.156.151.124",
  "created": 1593076131,
  "livemode": false,
  "type": "bank_account",
  "used": false
}*/

    val id:String?=null
    val client_ip:String?=null
    val created:Int?=null
    val livemode:Boolean?=null
    val type:String?=null
    val used:Boolean?=null
    val bank_account:BankAccountResponse?=null

    /* "error": {
    "code": "routing_number_invalid",
    "doc_url": "https://stripe.com/docs/error-codes/routing-number-invalid",
    "message": "Routing number must have 9 digits",
    "param": "bank_account[routing_number]",
    "type": "invalid_request_error"
  }*/

    val error:ErrorResponse?=null

}