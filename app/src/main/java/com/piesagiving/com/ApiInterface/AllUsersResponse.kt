package com.piesagiving.com.ApiInterface

class AllUsersResponse {

    var responseStatus: String? = null
    var result: String? = null
    var usersList:ArrayList<UsersDataResponse>?=null
    var requested_users:ArrayList<UsersDataResponse>?=null
    var follwers_list:ArrayList<UsersDataResponse>?=null
}
