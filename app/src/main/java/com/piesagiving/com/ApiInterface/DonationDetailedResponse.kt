package com.piesagiving.com.ApiInterface

class DonationDetailedResponse {
    val amount: Double? = null
    val cityName: String? = null
    val createdOn: String? = null
    val date: String? = null
    val id: String? = null
    val orgName: String? = null
    val orgType: String? = null
    val paymentType: String? = null
    val profilePic: String? = null
    val projectName: String? = null
    val status: String? = null
    val transactionId: String? = null
}
