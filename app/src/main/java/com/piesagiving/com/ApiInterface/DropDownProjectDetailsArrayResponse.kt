package com.piesagiving.com.ApiInterface

class DropDownProjectDetailsArrayResponse {
    /* {
            "createdOn": "Wed, 03 Jun 2020 13:28:18 GMT",
            "description": "project testing new1",
            "projectId": "5ed7a572afc54f5d3c57b520",
            "projectName": "project testing",
            "projectPicture": "http://3.133.43.47/media/images/project/5ed7a572afc54f5d3c57b520.png",
            "status": 1
        },*/

    val createdOn:String?=null
    val description:String?=null
    val projectId:String?=null
    val projectName:String?=null
    val projectPicture:String?=null
    val status:Int?=null

}