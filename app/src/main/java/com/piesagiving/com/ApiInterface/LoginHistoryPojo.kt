package com.piesagiving.com.ApiInterface

class LoginHistoryPojo {
/*{
    "responseStatus": 1,
    "result": [
        {
            "date": "2020-06-17",
            "time": "14:45",
            "type": "mobile",
            "location": "",
            "status": 1
        },
        {
            "date": "2020-06-16",
            "time": "19:21",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-16",
            "time": "18:07",
            "type": "mobile",
            "location": "Not Working",
            "status": 1
        },
        {
            "date": "2020-06-16",
            "time": "11:08",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:51",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:40",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:38",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:13",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:10",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:08",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        },
        {
            "date": "2020-06-15",
            "time": "18:07",
            "type": "mobile",
            "location": "Hyderabad",
            "status": 1
        }
    ]
}*/
    val responseStatus:Int?=null
    val result:ArrayList<HistoryResultsResponse>?=null
}