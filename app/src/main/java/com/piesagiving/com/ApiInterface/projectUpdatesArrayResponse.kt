package com.piesagiving.com.ApiInterface

class projectUpdatesArrayResponse {
    /* {
            "createdOn": "Wed, 03 Jun 2020 13:31:02 GMT",
            "description": "project testing new1",
            "id": "5ed7a6167a30268dd54f2994",
            "updatePicture": "5ed7a6167a30268dd54f2994",
            "updatedOn": "Wed, 03 Jun 2020 13:31:02 GMT"
        }*/

    val createdOn:String?=null
    val description:String?=null
    val id:String?=null
    val updatePicture:String?=null
    val updatedOn:String?=null

}