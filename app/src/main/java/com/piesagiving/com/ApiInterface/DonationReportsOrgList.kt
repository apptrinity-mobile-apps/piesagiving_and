package com.piesagiving.com.ApiInterface

class DonationReportsOrgList {

    val cityId: String? = null
    val cityName: String? = null
    val id: String? = null
    val organizationName: String? = null
    val profilePic: String? = null
    val streetAddress: String? = null
    val date: String? = null
    val paymentType: String? = null
    val proCauseName: String? = null
    val status: String? = null
    val donationSum: Float? = null
    val totalSum: Float? = null

}
