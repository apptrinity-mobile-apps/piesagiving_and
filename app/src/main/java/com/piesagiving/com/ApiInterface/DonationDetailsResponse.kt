package com.piesagiving.com.ApiInterface

class DonationDetailsResponse {
    val donationsByLength: Int? = null
    val totalAmount: Float? = null

}
