package com.piesagiving.com.ApiInterface

class TicketsListResponse {
    val date: String? = null
    val orgId: String? = null
    val orgName: String? = null
    val orgType: String? = null
    val subject: String? = null
    val ticketNo: String? = null
    val ticketStatus: String? = null
    val ticketId: String? = null
}
