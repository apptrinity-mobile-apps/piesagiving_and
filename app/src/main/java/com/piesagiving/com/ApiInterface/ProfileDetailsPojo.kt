package com.piesagiving.com.ApiInterface

class ProfileDetailsPojo {

    val project: ArrayList<ProjectArrayResponse>? = null
    val projectDonations: ArrayList<SpecialCauseDonationsArrayResponse>? = null
    val projectUpdates: ArrayList<projectUpdatesArrayResponse>? = null
    val responseStatus: Int? = null
    val result: String? = null

    val specialCause: ArrayList<SpecialCauseArrayResponse>? = null
    val donationDetails: DonationDetailsResponse? = null
    val specialCauseDonations: ArrayList<SpecialCauseDonationsArrayResponse>? = null
    val specialCauseUpdates: ArrayList<SpecialCauseUpdatesArrayResponse>? = null


}