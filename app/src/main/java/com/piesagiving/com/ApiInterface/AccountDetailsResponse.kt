package com.piesagiving.com.ApiInterface

class AccountDetailsResponse {
    /* "accountDetails": {
        "account_holder_name": "Jenny Rosen",
        "account_holder_type": "individual",
        "bank_name": "STRIPE TEST BANK",
        "country": "US",
        "currency": "usd",
        "customer": "cus_HYoamS3yHECVWQ",
        "fingerprint": "bKfIFLB7l7sxrLet",
        "id": "ba_1GzxlXKijxtVwMc3wrtTN53N",
        "last4": "2227",
        "metadata": {},
        "object": "bank_account",
        "routing_number": "110000000",
        "status": "new"
    },*/

    val account_holder_name:String?=null
    val account_holder_type:String?=null
    val bank_name:String?=null
    val country:String?=null
    val currency:String?=null
    val customer:String?=null
    val fingerprint:String?=null
    val id:String?=null
    val last4:String?=null
    val routing_number:String?=null
    val status:String?=null
}