package com.piesagiving.com.ApiInterface

class DonationFeePojo {
/*{
    "responseStatus": 1,
    "result": "Stripe Fee details fetched successfully!",
    "stripeFeeDetails": {
        "piesagivingPercentage": 5.87,
        "stripeBankExtracharges": 3,
        "stripeBankPercentage": 2.0,
        "stripeCardExtracharges": 1,
        "stripeCardPercentage": 5.6
    }
}*/

    val responseStatus:Int?=null
    val result:String?=null
    val stripeFeeDetails:StripeFeeDetailsResponse?=null
}
