package com.piesagiving.com.ApiInterface

class TicketMessagesResponse {
    val date: String? = null
    val message: String? = null
    val type: String? = null
}
