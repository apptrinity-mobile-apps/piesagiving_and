package com.piesagiving.com.ApiInterface

import java.io.Serializable

class ScheduleArrayList: Serializable {
    /* "schedules": [
                {
                    "amount": "5.00",
                    "date": "09-04-2020",
                    "term": 1
                }
            ],*/

    val amount:Double?=null
    val date:String?=null
    val term:String?=null

}
