package com.piesagiving.com.ApiInterface

class RecentSearchDataResponse {

    var coverPhoto: String? = null
    var displayName: String? = null
    var id: String? = null
    var location: String? = null
    var organizationType: String? = null
    var profilePic: String? = null

}
