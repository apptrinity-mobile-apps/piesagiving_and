package com.piesagiving.com.ApiInterface

class SpecialCauseArrayResponse {

    val description:String?=null
    val id:String?=null
    val name:String?=null
    val endDate:String?=null
    val projectPicture:String?=null
    val goalAmount:Int?=null
    val orgId:String?=null
    val orgName:String?=null
    val startDate:String?=null
    val status:String?=null
    val followStatus:String?=null
}