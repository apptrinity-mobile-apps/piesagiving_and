package com.piesagiving.com.ApiInterface

class BankAccountResponse {
    /* "bank_account": {
    "id": "ba_1Gxr4h2eZvKYlo2CyAGNDMdy",
    "object": "bank_account",
    "account_holder_name": "Jenny Rosen",
    "account_holder_type": "individual",
    "bank_name": "STRIPE TEST BANK",
    "country": "US",
    "currency": "usd",
    "fingerprint": "1JWtPxqbdX5Gamtc",
    "last4": "6789",
    "routing_number": "110000000",
    "status": "new"
  },*/

    val id:String?=null
    val account_holder_name:String?=null
    val account_holder_type:String?=null
    val bank_name:String?=null
    val country:String?=null
    val currency:String?=null
    val fingerprint:String?=null
    val last4:String?=null
    val routing_number:String?=null
    val status:String?=null
}