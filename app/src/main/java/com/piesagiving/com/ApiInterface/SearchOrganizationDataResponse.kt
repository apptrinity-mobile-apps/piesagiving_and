package com.piesagiving.com.ApiInterface

class SearchOrganizationDataResponse {

    val cityId:String?=null
    val cityName:String?=null
    val id:String?=null
    val organizationName:String?=null
    val profilePic:String?=null
    val streetAddress:String?=null
    val name:String?=null
    val profile:String?=null
    val state:String?=null

}
