package com.piesagiving.com.ApiInterface

import com.google.gson.JsonObject
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface ApiInterface {

    @Headers("Content-type: application/json")
    @POST("create_user")
    fun registerUserApi(@Body user_details: JsonObject): Call<RegisterUserResponse>

    @GET("get_all_countries")
    fun getCountriesApi(): Call<CountriesResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_states")
    fun getStatesApi(@Body countryId: JsonObject): Call<StatesResponse>

    @Headers("Content-type: application/json")
    @POST("get_all_cities")
    fun getCitiesApi(@Body stateId: JsonObject): Call<CitiesResponse>

    @Headers("Content-type: application/json")
    @POST("change_password")
    fun changePasswordApi(@Body passwords: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("user_login")
    fun loginApi(@Body body: JsonObject): Call<LoginPojo>

    @Headers("Content-type: application/json")
    @POST("sign_in_update")
    fun SigninUpdateApi(@Body body: JsonObject): Call<StripePaymentPojo>

    @Headers("Content-type: application/json")
    @POST("sign_in_history")
    fun SigninHistoryApi(@Body body: JsonObject): Call<LoginHistoryPojo>

    @Headers("Content-type: application/json")
    @POST("get_dashboard")
    fun DashboardApi(@Body body: JsonObject): Call<DashboardPojo>

    @Headers("Content-type: application/json")
    @POST("view_organization")
    fun OrganizationDetailsApi(@Body body: JsonObject): Call<OrganizationDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("org_follwing")
    fun FollowingApi(@Body body: JsonObject): Call<FollowPojo>

    @Headers("Content-type: application/json")
    @POST("project_detail_view")
    fun ProjectDetailsApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("special_causes_detail_view")
    fun ProjectDetailsSpecialCauseApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("location_service")
    fun LocationServiceApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("user_privacy")
    fun UserPrivacyApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("user_security")
    fun UserSecurityApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("my_notifications")
    fun NotificationsApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("categories")
    fun getCategoriesApi(@Body userId: JsonObject): Call<CategoriesResponse>

    @Headers("Content-type: application/json")
    @POST("search_organization_category")
    fun getOrganizationByCategoriesApi(@Body body: JsonObject): Call<SearchOrganizationCategoriesResponse>

    @Headers("Content-type: application/json")
    @POST("search_organization")
    fun searchOrganizationApi(@Body body: JsonObject): Call<SearchOrganizationResponse>

    @Headers("Content-type: application/json")
    @POST("contact_us")
    fun ContactUsApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("user_profile_update")
    fun ProfileApi(@Body body: JsonObject): Call<ProfileDetailsPojo>

    @Headers("Content-type: application/json")
    @POST("get_mycircles")
    fun GetMyCirclesApi(@Body body: JsonObject): Call<GetMyCirclePojo>

    @Headers("Content-type: application/json")
    @POST("view_special_causes_follwers")
    fun ViewSpecialCauseDonationApi(@Body body: JsonObject): Call<GetMyCirclePojo>

    @Headers("Content-type: application/json")
    @POST("get_user_profile")
    fun GetUserProfileApi(@Body body: JsonObject): Call<UserProfileResponse>

    @Headers("Content-type: application/json")
    @POST("add_recurring_donate_now")
    fun RecrringDonationsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("special_cause_recurring_donations")
    fun SpecialCauseRecrringDonationsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("edit_special_cause_recurring_donations")
    fun EditSpecialCauseRecrringDonationsApi(@Body body: JsonObject): Call<StripePaymentPojo>


    @Headers("Content-type: application/json")
    @POST("edit_recurring_donations")
    fun EditRecrringDonationsApi(@Body body: JsonObject): Call<StripePaymentPojo>


    @Headers("Content-type: application/json")
    @POST("recurring_donations_list")
    fun DonationHistoryApi(@Body body: JsonObject): Call<DonationHistoryPojo>

    @Headers("Content-type: application/json")
    @POST("wallet_donations_get")
    fun WalletTotalDonationsApi(@Body body: JsonObject): Call<WalletDonationsPojo>

    @Headers("Content-type: application/json")
    @POST("view_user_followers")
    fun GetMyFriendsCircle(@Body body: JsonObject): Call<GetMyCirclePojo>

    @GET("view_projects_special_causes/{org_id}")
    fun ProjectViewSpecialCausesApi(@Path("org_id") id: String): Call<ProjectTypePojo>

    @POST("update_cause_comment")
    fun UpdateSpecialCauseComment(@Body body: JsonObject): Call<ProjectTypePojo>

    @POST("wallet_donations_get")
    fun TotalDonationHistory(@Body body: JsonObject): Call<DonationHistoryPojoResponse>

    @Headers("Content-type: application/json")
    @POST("user_profile_pic_update")
    fun updateProfilePicApi(@Body body: JsonObject): Call<ProfilePicResponse>

    @Headers("Content-type: application/json")
    @POST("recurring_donations_list")
    fun getRecurringListApi(@Body body: JsonObject): Call<RecurringDonationsResponse>

    @Headers("Content-type: application/json")
    @POST("special_cause_recurring_donations_list")
    fun getSpecialCauseRecurringListApi(@Body body: JsonObject): Call<RecurringDonationsResponse>

    @Headers("Content-type: application/json")
    @POST("donations_reports")
    fun getDonationsReportsApi(@Body body: JsonObject): Call<DonationReportsResponse>

    @Headers("Content-type: application/json")
    @POST("recurring_donations")
    fun StripePaymentDonationApi(@Body body: JsonObject): Call<StripePaymentPojo>

    @Headers("Content-type: application/json")
    @POST("bank_account")
    fun AddBankAccountApi(@Body body: JsonObject): Call<NewBankAccountPojo>


    @Headers("Content-type: application/json")
    @POST("bank_account_list")
    fun BankAccountListApi(@Body body: JsonObject): Call<BankAccountListPojo>


    @Headers("Content-type: application/json")
    @POST("bank_account_verify")
    fun BankAccountVerifyApi(@Body body: JsonObject): Call<BankAccountVerificationPojo>

    @GET("get_extra_fees")
    fun GetExtraFeeApi(): Call<DonationFeePojo>

    @Headers("Content-type: application/json")
    @POST("get_all_recent_searches")
    fun getRecentSearchesApi(@Body body: JsonObject): Call<RecentSearchResponse>

    @Headers("Content-type: application/json")
    @POST("save_recent_searches")
    fun saveRecentSearchesApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("clear_recent_searches")
    fun clearRecentSearchesApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("add_reports")
    fun reportApi(@Body body: JsonObject): Call<FollowPojo>

    @Headers("Content-type: application/json")
    @POST("add_user_giving_calculator")
    fun givingCalculatorApi(@Body body: JsonObject): Call<FollowPojo>

    @Headers("Content-type: application/json")
    @POST("get_user_giving_calculator")
    fun getUserGivingCalculatorApi(@Body body: JsonObject): Call<FollowPojo>

    @Headers("Content-type: application/json")
    @POST("get_all_users")
    fun getAllUsersApi(@Body body: JsonObject): Call<AllUsersResponse>

    @GET("get_all_faqs")
    fun getFAQsApi(): Call<FaqResponsePojo>

    @Headers("Content-type: application/json")
    @POST("total_donations_history")
    fun getTotalDonationsHistoryApi(@Body body: JsonObject): Call<TotalDonationsResponse>

    @Headers("Content-type: application/json")
    @POST("get_user_privacy")
    fun getUserPrivacy(@Body body: JsonObject): Call<GetUserPrivacyResponse>

    @Headers("Content-type: application/json")
    @POST("user_follwing")
    fun userFollowingApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("get_user_notification_settings")
    fun getUserNotificationsSettings(@Body body: JsonObject): Call<GetNotificationSettingsResponse>

    @Headers("Content-type: application/json")
    @POST("user_detail_view")
    fun getUserDetailViewApi(@Body body: JsonObject): Call<UserDetailResponse>

    @Headers("Content-type: application/json")
    @POST("friend_request")
    fun sendFriendRequestApi(@Body body: JsonObject): Call<UserDetailResponse>

    @Headers("Content-type: application/json")
    @POST("requests_list")
    fun getFriendRequestsListApi(@Body body: JsonObject): Call<AllUsersResponse>

    @Headers("Content-type: application/json")
    @POST("accept_or_reject")
    fun acceptRejectApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("user_donated_organisations")
    fun getUserDonatedListApi(@Body body: JsonObject): Call<SearchOrganizationResponse>

    @Headers("Content-type: application/json")
    @POST("follwing_users_list")
    fun getFollowingUsersListApi(@Body body: JsonObject): Call<AllUsersResponse>

    @Headers("Content-type: application/json")
    @POST("cause_follwing")
    fun causeFollowingApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("saved_cards_list")
    fun savedCardsList(@Body body: JsonObject): Call<SavedCardListResponse>

    @Headers("Content-type: application/json")
    @POST("change_email")
    fun updateEmailApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("card")
    fun SaveCardApi(@Body body: JsonObject): Call<NewBankAccountPojo>

    @Headers("Content-type: application/json")
    @POST("remove_source")
    fun RemoveCardBankApi(@Body body: JsonObject): Call<NewBankAccountPojo>

    @Headers("Content-type: application/json")
    @POST("forgot_password")
    fun forgotPasswordApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("password_reset")
    fun passwordResetApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("total_donations")
    fun totalDonationsApi(@Body body: JsonObject): Call<DonationHistoryPojoResponse>

    @Headers("Content-type: application/json")
    @POST("update_device_token")
    fun updateDeviceTokenApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("my_recurring_donations")
    fun getMyRecurringDonationsApi(@Body body: JsonObject): Call<RecurringDonationsResponse>

    @Headers("Content-type: application/json")
    @POST("delete_reccurring")
    fun deleteRecurringDonationApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("delete_user")
    fun deleteAccountApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("email_verify")
    fun verifyEmailApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("get_notifications")
    fun getNotificationsApi(@Body body: JsonObject): Call<GetNotificationsResponse>

    @Headers("Content-type: application/json")
    @POST("modify_notification_status")
    fun readNotificationsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("get_notifications_count")
    fun getNotificationCountApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("org_following_dropdown")
    fun getOrganizationFollowingApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("add_ticket")
    fun addTicketApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("tickets")
    fun getAllTicketsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("send_message")
    fun sendMessageInTicketApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("messages")
    fun getMessagesInTicketApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("anouncements")
    fun getAnnouncementsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("block_anouncements")
    fun blockAnnouncementsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("forgot_passcode")
    fun forgotPasscodeApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("update_device_pin")
    fun updateAppPinApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("reopen_ticket")
    fun reOpenTicketApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("following_org_announcements_list")
    fun getFollowingOrganizationsAnnouncementsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("get_donation_details_list")
    fun getDonationDetailsApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("single_fundraiser_view")
    fun getFundraiserDetailsApi(@Body body: JsonObject): Call<FundraisersResponse>

    @Headers("Content-type: application/json")
    @POST("add_fundraiser_donation")
    fun donateFundraiserApi(@Body body: JsonObject): Call<RegisterUserResponse>

    @Headers("Content-type: application/json")
    @POST("user_follow_fundraiser")
    fun fundraiserFollowingApi(@Body body: JsonObject): Call<RegisterUserResponse>

    companion object Factory {

        const val BASE = "http://3.21.72.239/"
//        const val BASE = "https://piesagiving.com/"

        private const val BASE_URL = "${BASE}api/user/"

        private val client = OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)

        fun create(): ApiInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
}
