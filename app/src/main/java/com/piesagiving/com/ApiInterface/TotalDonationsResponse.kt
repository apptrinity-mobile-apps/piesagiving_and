package com.piesagiving.com.ApiInterface

class TotalDonationsResponse {

    var responseStatus: String? = null
    var result: String? = null
    var history:ArrayList<TotalDonationsDataResponse>?=null

}
