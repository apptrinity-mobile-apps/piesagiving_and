package com.piesagiving.com.ApiInterface

class DonationHistoryPojoResponse {

    val responseStatus: Int? = null
    val result: String? = null
    val totalCharitySum: Double? = null
    val totalChurchSum: Double? = null
    val totalDonationSum: Double? = null
    val totalSpecialCauseSum: Double? = null
    val totalFundraiserSum: Double? = null
    val unqYears: ArrayList<String>? = null

}
