package com.piesagiving.com.ApiInterface

class GetResultNotificationDataresponse {
    var donationProcessingEmail:String? =null
    var donationProcessingPhone:String? =null
    var DonationProcessedEmail: String? = null
    var donationProcessedPhone: String? = null
    var hapticss: String? = null
    var newPiesaRecommendation: String? = null
    var newProjectCharity: String? = null
    var newProjectChurch: String? = null
    var newSpecialCausesCharity: String? = null
    var newSpecialCausesChurch: String? = null
    var newTrendingNow: String? = null
    var recurringDonationReminder: String? = null
    var someoneFollowedYou: String? = null

}
