package com.piesagiving.com.ApiInterface

class DashboardPojo {
    /*{
    "ourRecommendations": [
        {
            "coverPhoto": null,
            "displayName": "churuch",
            "id": "5ed61d4af190ba2c06a8bad5",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ed61d4af190ba2c06a8bad5.jpg"
        },
        {
            "coverPhoto": null,
            "displayName": "Bethel churuch",
            "id": "5ed601a5eb5eeef535de701e",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ed601a5eb5eeef535de701e.jpg"
        },
        {
            "coverPhoto": "http://3.133.43.47/media/images/organizations/coverFile/5ed1ee3e149b16734bed6d8d.png",
            "displayName": "church123",
            "id": "5ed1ee3e149b16734bed6d8d",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ed1ee3e149b16734bed6d8d.png"
        }
    ],
    "responseStatus": 1,
    "result": " dashboard data fetched Successfully",
    "trendingNow": [
        {
            "coverPhoto": null,
            "displayName": "Bethel churuch",
            "id": "5ed601a5eb5eeef535de701e",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ed601a5eb5eeef535de701e.jpg"
        },
        {
            "coverPhoto": null,
            "displayName": "Charity",
            "id": "5ecf6efe1781e4a039de7880",
            "profilePic": "http://3.133.43.47/media/images/organizations/proFile/5ecf6efe1781e4a039de7880.png"
        }
    ],
    "yourCirle": []
}*/

    val ourRecommendations:ArrayList<RecommendationArray>?=null
    val trendingNow:ArrayList<RecommendationArray>?=null
    val yourCirle:ArrayList<RecommendationArray>?=null
    val responseStatus:Int?=null
    val result:String?=null
}