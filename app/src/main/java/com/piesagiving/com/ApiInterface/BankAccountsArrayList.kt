package com.piesagiving.com.ApiInterface

class BankAccountsArrayList {
    /*{
                "account_holder_name": "Jenny Rosen",
                "account_holder_type": "individual",
                "bank_name": "STRIPE TEST BANK",
                "country": "US",
                "currency": "usd",
                "customer": "cus_HYoamS3yHECVWQ",
                "fingerprint": "A13k2gSmpLRaeF1W",
                "id": "ba_1GzhX6KijxtVwMc3FcIIHy2m",
                "last4": "6789",
                "metadata": {},
                "object": "bank_account",
                "routing_number": "110000000",
                "status": "verified"
            },*/

    val  account_holder_name:String?=null
    val  account_holder_type:String?=null
    val  bank_name:String?=null
    val  country:String?=null
    val  currency:String?=null
    val  customer:String?=null
    val  fingerprint:String?=null
    val  id:String?=null
    val  last4:String?=null
    val  routing_number:String?=null
    val  status:String?=null
}