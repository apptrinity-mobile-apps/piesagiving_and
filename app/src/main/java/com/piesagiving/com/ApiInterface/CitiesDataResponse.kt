package com.piesagiving.com.ApiInterface

class CitiesDataResponse {

    var _id: CountryId? = null
    var cityId: Int? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var latitude: String? = null
    var longitude: String? = null
    var name: String? = null
    var stateId: Int? = null
    var stateCode: String? = null

}
