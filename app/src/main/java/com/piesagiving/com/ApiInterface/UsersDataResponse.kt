package com.piesagiving.com.ApiInterface

class UsersDataResponse {

    var firstName: String? = null
    var id: String? = null
    var lastName: String? = null
    var privacy: String? = null
    var profilePic: String? = null
    var name: String? = null
    var userName: String? = null
    var lasttName: String? = null
    var notifcationId: String? = null

}
