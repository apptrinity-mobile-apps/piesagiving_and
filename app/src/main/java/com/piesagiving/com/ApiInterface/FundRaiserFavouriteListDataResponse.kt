package com.piesagiving.com.ApiInterface

class FundRaiserFavouriteListDataResponse {
    var category: String? = null
    var createdOn: String? = null
    var description: String? = null
    var endDate: String? = null
    var goalAmount: Double? = null
    var id: String? = null
    var image: String? = null
    var name: String? = null
    var startDate: String? = null
    var displayHeader: String? = null
    var isSection: Boolean = false
}
