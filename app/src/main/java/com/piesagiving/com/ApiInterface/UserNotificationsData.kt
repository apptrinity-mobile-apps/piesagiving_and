package com.piesagiving.com.ApiInterface

class UserNotificationsData {
    var date: String? = null
    val donation: ArrayList<NotificationsData>? = null
    val follow_request: ArrayList<NotificationsData>? = null
    val friend_request: ArrayList<NotificationsData>? = null
    val organisation: ArrayList<NotificationsData>? = null
}
