package com.piesagiving.com.ApiInterface

class NotificationsResponse {

    val DonationProcessedEmail: Int? = null
    val donationProcessedPhone: Int? = null
    val hapticss: Int? = null
    val newPiesaRecommendation: Int? = null
    val newProjectCharity: Int? = null
    val newProjectChurch: Int? = null
    val newSpecialCausesCharity: Int? = null
    val newSpecialCausesChurch: Int? = null
    val newTrendingNow: Int? = null
    val recurringDonationReminder: Int? = null
    val someoneFollowedYou: Int? = null
    var donationProcessingEmail:String? =null
    var donationProcessingPhone:String? =null

}


