package com.piesagiving.com.ApiInterface

class ProjectArrayResponse {
    /*"description": "project testing new1",
            "id": "5ed7a572afc54f5d3c57b520",
            "name": "project testing",
            "organizationId": "5ecf6efe1781e4a039de7880",
            "projectPicture": "http://3.133.43.47/media/images/project/5ed7a572afc54f5d3c57b520.png",
            "status": 1*/

    val description:String?=null
    val id:String?=null
    val name:String?=null
    val organizationId:String?=null
    val projectPicture:String?=null
}