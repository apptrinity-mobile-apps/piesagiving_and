package com.piesagiving.com.ApiInterface

import java.io.Serializable

class NotificationsData :Serializable{

    val createdOn: String? = null
    val description: String? = null
    val notificationId: String? = null
    val notificationType: String? = null
    val orgId: String? = null
    val projectId: String? = null
    val causeId: String? = null
    val projectType: String? = null
    val causeName: String? = null
    val projectName: String? = null
    val read_status: Int? = null
    val status: Int? = null
    val userId: String? = null
    val friendUserId: String? = null
    val friendLastName: String? = null
    val friendFirstName: String? = null
    val friendState: String? = null
    val friendUserName: String? = null
    val profilePic: String? = null

}
