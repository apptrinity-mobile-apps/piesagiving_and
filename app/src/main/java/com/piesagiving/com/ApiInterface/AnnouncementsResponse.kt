package com.piesagiving.com.ApiInterface

class AnnouncementsResponse {
    val date: String? = null
    val message: String? = null
    val orgId: String? = null
}
