package com.piesagiving.com.ApiInterface

class BankAccountListPojo {
    /*{
    "bankAccountList": {
        "data": [
            {
                "account_holder_name": "Jenny Rosen",
                "account_holder_type": "individual",
                "bank_name": "STRIPE TEST BANK",
                "country": "US",
                "currency": "usd",
                "customer": "cus_HYoamS3yHECVWQ",
                "fingerprint": "A13k2gSmpLRaeF1W",
                "id": "ba_1GzhX6KijxtVwMc3FcIIHy2m",
                "last4": "6789",
                "metadata": {},
                "object": "bank_account",
                "routing_number": "110000000",
                "status": "verified"
            },
            {
                "account_holder_name": "Jenny Rosen",
                "account_holder_type": "individual",
                "bank_name": "STRIPE TEST BANK",
                "country": "US",
                "currency": "usd",
                "customer": "cus_HYoamS3yHECVWQ",
                "fingerprint": "bKfIFLB7l7sxrLet",
                "id": "ba_1GzxlXKijxtVwMc3wrtTN53N",
                "last4": "2227",
                "metadata": {},
                "object": "bank_account",
                "routing_number": "110000000",
                "status": "new"
            },
            {
                "account_holder_name": "Jenny Rosen",
                "account_holder_type": "individual",
                "bank_name": "STRIPE TEST BANK",
                "country": "US",
                "currency": "usd",
                "customer": "cus_HYoamS3yHECVWQ",
                "fingerprint": "L1Fb0oE6MBMlRzvD",
                "id": "ba_1GziPZKijxtVwMc3cHRwUuJ4",
                "last4": "3335",
                "metadata": {},
                "object": "bank_account",
                "routing_number": "110000000",
                "status": "verified"
            }
        ],
        "has_more": false,
        "object": "list",
        "url": "/v1/customers/cus_HYoamS3yHECVWQ/sources"
    },
    "responseStatus": 1,
    "result": "Bank Account List fetched Successfully!!!"
}*/
    val responseStatus:Int?=null
    val result:String?=null
    val bankAccountList:BankAccountListResponse?=null

    val customerDetails:CustomerDetailsList?=null

}