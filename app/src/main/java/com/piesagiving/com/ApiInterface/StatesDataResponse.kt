package com.piesagiving.com.ApiInterface

class StatesDataResponse {

    var _id: CountryId? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var name: String? = null
    var stateCode: String? = null
    var stateId: Int? = null
    var status: String? = null

}
