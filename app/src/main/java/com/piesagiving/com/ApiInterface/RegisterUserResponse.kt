package com.piesagiving.com.ApiInterface

class RegisterUserResponse {

    var responseStatus: String? = null
    var result: String? = null
    var specialCauseId: String? = null
    var notifications_count: Int? = null
    var organisations: ArrayList<OrganizationsFollowingListResponse>? = null
    var tickets: ArrayList<TicketsListResponse>? = null
    var messages: ArrayList<TicketMessagesResponse>? = null
    var announcements: ArrayList<AnnouncementsResponse>? = null
    var org_list: ArrayList<OrganizationAnnouncementsResponse>? = null
    var donationsList: ArrayList<DonationDetailedResponse>? = null

}
