package com.piesagiving.com.ApiInterface

class BankAccountVerificationPojo {
    /*{
    "accountDetails": {
        "account_holder_name": "Jenny Rosen",
        "account_holder_type": "individual",
        "bank_name": "STRIPE TEST BANK",
        "country": "US",
        "currency": "usd",
        "customer": "cus_HYoamS3yHECVWQ",
        "fingerprint": "bKfIFLB7l7sxrLet",
        "id": "ba_1GzxlXKijxtVwMc3wrtTN53N",
        "last4": "2227",
        "metadata": {},
        "object": "bank_account",
        "routing_number": "110000000",
        "status": "verified"
    },
    "responseStatus": 1,
    "result": "Bank Account Verified Successfully!!"
}*/

    val responseStatus:Int?=null
    val result:String?=null
    val accountDetails:AccountDetailsResponse?=null

}
