package com.piesagiving.com.ApiInterface

class DropDownSpecialCauseDetailsArrayResponse {

    /*{
            "createdOn": "Sat, 30 May 2020 04:52:25 GMT",
            "description": "test",
            "": "Sat, 20 Jun 2020 00:00:00 GMT",
            "goalAmount": 1000,
            "projectPicture": "http://3.133.43.47/media/images/specialcause/5ed1e689d6501b9b4a111d8b.png",
            "specialCauseId": "5ed1e689d6501b9b4a111d8b",
            "specialCauseName": "testing123",
            "startDate": "Sun, 05 Apr 2020 00:00:00 GMT",
            "status": 1
        },*/

    val createdOn:String?=null
    val description:String?=null
    val endDate:String?=null
    val goalAmount:Int?=null
    val projectPicture:String?=null
    val specialCauseId:String?=null
    val specialCauseName:String?=null
    val startDate:String?=null
    val status:Int?=null
}