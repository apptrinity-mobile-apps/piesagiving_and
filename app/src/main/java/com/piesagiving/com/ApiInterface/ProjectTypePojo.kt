package com.piesagiving.com.ApiInterface

class ProjectTypePojo {
    /*{
    "projectDetails": [
        {
            "createdOn": "Wed, 03 Jun 2020 13:28:18 GMT",
            "description": "project testing new1",
            "projectId": "5ed7a572afc54f5d3c57b520",
            "projectName": "project testing",
            "projectPicture": "http://3.133.43.47/media/images/project/5ed7a572afc54f5d3c57b520.png",
            "status": 1
        },
        {
            "createdOn": "Wed, 03 Jun 2020 13:28:47 GMT",
            "description": "project testing2 description",
            "projectId": "5ed7a58fafc54f5d3c57b521",
            "projectName": "project testing2",
            "projectPicture": "http://3.133.43.47/media/images/project/5ed7a58fafc54f5d3c57b521.png",
            "status": 1
        }
    ],
    "responseStatus": 1,
    "result": "Data fetched Successfully",
    "specialCauseDetails": [
        {
            "createdOn": "Sat, 30 May 2020 04:52:25 GMT",
            "description": "test",
            "endDate": "Sat, 20 Jun 2020 00:00:00 GMT",
            "goalAmount": 1000,
            "projectPicture": "http://3.133.43.47/media/images/specialcause/5ed1e689d6501b9b4a111d8b.png",
            "specialCauseId": "5ed1e689d6501b9b4a111d8b",
            "specialCauseName": "testing123",
            "startDate": "Sun, 05 Apr 2020 00:00:00 GMT",
            "status": 1
        },
        {
            "createdOn": "Mon, 01 Jun 2020 07:22:55 GMT",
            "description": "desc",
            "endDate": "Mon, 01 Jun 2020 00:00:00 GMT",
            "goalAmount": 200,
            "projectPicture": null,
            "specialCauseId": "5ed4accf5fc3341fa16f9aa7",
            "specialCauseName": "testing",
            "startDate": "Mon, 01 Jun 2020 00:00:00 GMT",
            "status": 1
        },
        {
            "createdOn": "Fri, 05 Jun 2020 09:37:43 GMT",
            "description": "corona ",
            "endDate": "Wed, 10 Jun 2020 00:00:00 GMT",
            "goalAmount": 25000,
            "projectPicture": "http://3.133.43.47/media/images/specialcause/5eda126754ccf3ef41901822.png",
            "specialCauseId": "5eda126754ccf3ef41901822",
            "specialCauseName": "Corona",
            "startDate": "Fri, 05 Jun 2020 00:00:00 GMT",
            "status": 1
        }
    ]
}*/

    val projectDetails:ArrayList<DropDownProjectDetailsArrayResponse>?=null
    val specialCauseDetails:ArrayList<DropDownSpecialCauseDetailsArrayResponse>?=null
    val responseStatus:Int?=null
    val result:String?=null

}