package com.piesagiving.com.ApiInterface

class SpecialCauseUpdatesArrayResponse {

    val description:String?=null
    val id:String?=null
    val causeId:String?=null
    val updatePicture:String?=null
    val updatedOn:String?=null
    val status:String?=null
}