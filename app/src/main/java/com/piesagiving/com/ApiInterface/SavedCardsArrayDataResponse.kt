package com.piesagiving.com.ApiInterface

class SavedCardsArrayDataResponse {


    var address_city: String? = null
    var address_country: String? = null
    var address_line1: String? = null
    var address_line1_check: String? = null
    var address_line2: String? = null
    var address_state: String? = null
    var address_zip: String? = null
    var address_zip_check: String? = null
    var brand: String? = null
    var country: String? = null
    var customer: String? = null
    var cvc_check: String? = null
    var dynamic_last4: String? = null
    var exp_month: String? = null
    var exp_year: String? = null
    var fingerprint: String? = null
    var funding: String? = null
    var id: String? = null
    var last4: String? = null
    //TODO add metadata obj later
//    var metadata: String? = null
    var name: String? = null
    //var object: String? = null
    var tokenization_method: String? = null


}
