package com.piesagiving.com.ApiInterface

class CountriesDataResponse {

    var _id: CountryId? = null
    var capital: String? = null
    var countryCode: String? = null
    var countryId: Int? = null
    var currency: String? = null
    var iso3: String? = null
    var name: String? = null
    var phoneCode: String? = null
    var status: String? = null

}
