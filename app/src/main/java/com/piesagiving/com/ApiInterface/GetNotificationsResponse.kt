package com.piesagiving.com.ApiInterface

class GetNotificationsResponse {

    val responseStatus: String? = null
    val result: String? = null
    var friend_requests_count: Int? = null
    val user_notifications: ArrayList<UserNotificationsData>? = null

}
