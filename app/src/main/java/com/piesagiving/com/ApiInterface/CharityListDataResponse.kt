package com.piesagiving.com.ApiInterface

class CharityListDataResponse {

    var city: String? = null
    var country: String? = null
    var coverPhoto: String? = null
    var displayName: String? = null
    var displayHeader: String? = null
    var organizationId: String? = null
    var phoneNumber: String? = null
    var profilePic: String? = null
    var state: String? = null
    var streetAddress: String? = null
    var websiteLink: String? = null
    var isSection: Boolean = false
    var otherUserId: String? = null
    var zipcode: String? = null
    var firstName: String? = null
    var lastName: String? = null
    var privacy: String? = null
    var userName: String? = null
    var causeId: String? = null

}
