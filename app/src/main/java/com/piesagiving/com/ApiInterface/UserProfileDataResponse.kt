package com.piesagiving.com.ApiInterface

class UserProfileDataResponse {

    var city: String? = null
    var country: String? = null
    var createdOn: String? = null
    var email: String? = null
    var firstName: String? = null
    var geotagging: Int? = null
    var faceId: Int? = null
    var id: String? = null
    var language: String? = null
    var lastName: String? = null
    var privacy: String? = null
    var profilePic: String? = null
    var state: String? = null
    var streetAddress: String? = null
    var terms: String? = null
    var zipcode: String? = null
    var status: String? = null
    var birthday: String? = null
    var cityId: String? = null
    var countryId: String? = null
    var stateId: String? = null
    var userName: String? = null

    var acceptStatus:String?=null
    var follwStatus:String?=null
    var location:String?=null
    var name:String?=null
    var church_list:ArrayList<RecommendationArray>?=null
    var charity_list:ArrayList<RecommendationArray>?=null
    var special_cause_list:ArrayList<RecommendationArray>?=null
}
