package com.piesagiving.com.ApiInterface

class WalletDonationsObjResponse {
    /*"userWalletData": {
        "charityDonations": 7406,
        "churchDonations": 180,
        "specialCauseDonations": 750
    }*/

    val charityDonations:Double?=null
    val churchDonations:Double?=null
    val specialCauseDonations:Double?=null
}