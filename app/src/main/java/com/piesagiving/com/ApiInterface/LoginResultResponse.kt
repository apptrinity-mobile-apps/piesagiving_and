package com.piesagiving.com.ApiInterface

class LoginResultResponse {

    val city: String? = null
    val country: String? = null
    val createdOn: String? = null
    val email: String? = null
    val firstName: String? = null
    val id: String? = null
    val lastName: String? = null
    val state: String? = null
    val status: String? = null
    val streetAddress: String? = null
    val zipcode: String? = null
    val privacy: String? = null
    val profilePic: String? = null
    val terms: String? = null
    val birthday: String? = null
    val faceId: Int? = null
    val geotagging: Int? = null
    val language: String? = null
    val userName: String? = null
    val devicePinCode: String? = null
    val notifications: NotificationsResponse? = null

}