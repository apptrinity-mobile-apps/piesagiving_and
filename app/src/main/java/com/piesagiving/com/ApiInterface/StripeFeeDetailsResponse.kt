package com.piesagiving.com.ApiInterface

class StripeFeeDetailsResponse {
/*     " "stripeFeeDetails": {
        "piesaBankExtracharges": 0.2,
        "piesaBankPercentage": 0.2,
        "piesaCardExtracharges": 0.0,
        "piesaCardPercentage": 0.3,
        "piesagivingPercentage": 0.0,
        "stripeBankExtracharges": 0.0,
        "stripeBankPercentage": 0.8,
        "stripeCardExtracharges": 0.3,
        "stripeCardPercentage": 2.9
    }*/

    val piesaBankExtracharges:Double?=null
    val piesaBankPercentage:Double?=null
    val piesaCardExtracharges:Double?=null
    val piesaCardPercentage:Double?=null
    val piesagivingPercentage:Double?=null
    val stripeBankExtracharges:Double?=null
    val stripeBankPercentage:Double?=null
    val stripeCardExtracharges:Double?=null
    val stripeCardPercentage:Double?=null
}
