package com.piesagiving.com.Utils

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import com.piesagiving.com.Helpers.SessionManager

@Suppress("DEPRECATION")
class Haptics {

    fun vibrate(context: Context) {
        val haptics = SessionManager(context).getUserNotifications[SessionManager.NOTIFY_HAPTICS_KEY]!!
        if (haptics == "1") {
            val v: Vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(45, 2))
            } else {
                v.vibrate(45)
            }
        }
    }
}