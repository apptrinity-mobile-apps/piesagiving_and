package com.piesagiving.com.Utils

import java.text.NumberFormat
import java.util.*

class StringManipulations {
    fun capitalizeFirstLetterOfEachWord(string: String): String {
        var output = ""
        val words = string.split(" ").toMutableList()
        for (word in words) {
            output += word.replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() } + " "
        }
        output = output.trim()
        return output
    }

    fun amountFormatter(amount: String): String {
        return NumberFormat.getNumberInstance(Locale.ROOT).format(amount)
    }
}