package com.piesagiving.com.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.tabs.TabLayout
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Activities.RecurringDonationDetailedViewActivity
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.RecurringDonationsListResponse
import com.piesagiving.com.ApiInterface.RecurringDonationsResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ChurchesFragment : Fragment() {

    private lateinit var rootview: View
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""

    lateinit var rv_active: RecyclerView
    lateinit var rv_in_active: RecyclerView
    lateinit var loading_dialog: Dialog
    lateinit var mActiveList: ArrayList<RecurringDonationsListResponse>
    lateinit var mInActiveList: ArrayList<RecurringDonationsListResponse>
    lateinit var tv_active_recurring_empty: TextView
    lateinit var tv_in_active_recurring_empty: TextView
    lateinit var tabLayout: TabLayout
    lateinit var tabOne: TextView
    lateinit var tabTwo: TextView
    lateinit var ll_active: LinearLayout
    lateinit var ll_in_active: LinearLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // creating view for fragment
        try {
            rootview = inflater.inflate(R.layout.church_fragment, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)

        if (isInternetConnected) {
            getMyRecurringDonationsApi()
        } else {
            Toast.makeText(
                requireActivity(),
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }
        ll_active.visibility = View.VISIBLE
        ll_in_active.visibility = View.GONE
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab!!.position == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(
                            resources.getColor(R.color.login_button_background, activity!!.theme)
                        )
                        tabTwo.setTextColor(resources.getColor(R.color.black, activity!!.theme))
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.login_button_background))
                        tabTwo.setTextColor(resources.getColor(R.color.black))
                    }
                    ll_active.visibility = View.VISIBLE
                    ll_in_active.visibility = View.GONE
                } else if (tab.position == 1) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tabOne.setTextColor(resources.getColor(R.color.black, activity!!.theme))
                        tabTwo.setTextColor(
                            resources.getColor(R.color.login_button_background, activity!!.theme)
                        )
                    } else {
                        tabOne.setTextColor(resources.getColor(R.color.black))
                        tabTwo.setTextColor(resources.getColor(R.color.login_button_background))
                    }
                    ll_active.visibility = View.GONE
                    ll_in_active.visibility = View.VISIBLE
                }
            }
        })
        return rootview
    }

    private fun initialize(view: View?) {
        loadingDialog()
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!

        if (view != null) {
            rv_active = view.findViewById(R.id.rv_active)
            tv_active_recurring_empty = view.findViewById(R.id.tv_active_recurring_empty)
            rv_in_active = view.findViewById(R.id.rv_in_active)
            tv_in_active_recurring_empty = view.findViewById(R.id.tv_in_active_recurring_empty)
            ll_in_active = view.findViewById(R.id.ll_in_active)
            ll_active = view.findViewById(R.id.ll_active)
            tabLayout = view.findViewById(R.id.tabLayout)
        }

        setupTabIcons()
        val rv_LayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        val rv_LayoutManager_inactive =
            LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        rv_active.layoutManager = rv_LayoutManager
        rv_in_active.layoutManager = rv_LayoutManager_inactive
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(requireActivity())
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(requireActivity())
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun setupTabIcons() {
        val view = LayoutInflater.from(requireActivity()).inflate(R.layout.tab_header, null)
        val view1 = LayoutInflater.from(requireActivity()).inflate(R.layout.tab_header, null)
        tabOne = view.findViewById(R.id.tv_tab)
        tabTwo = view1.findViewById(R.id.tv_tab)
        tabOne.text = getString(R.string.active)
        tabTwo.text = getString(R.string.inactive)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tabOne.setTextColor(
                resources.getColor(R.color.login_button_background, requireActivity().theme)
            )
        } else {
            tabOne.setTextColor(resources.getColor(R.color.login_button_background))
        }
        tabLayout.addTab(tabLayout.newTab().setCustomView(view), true)
        tabLayout.addTab(tabLayout.newTab().setCustomView(view1))
    }

    private fun getMyRecurringDonationsApi() {
        mActiveList = ArrayList()
        mInActiveList = ArrayList()
        loading_dialog.show()
        val obj = JSONObject()
        obj.put("userId", userId)
        val jParser = JsonParser()
        val finalObj = jParser.parse(obj.toString()) as JsonObject

        val api = ApiInterface.create()
        val call = api.getMyRecurringDonationsApi(finalObj)
        call.enqueue(object : Callback<RecurringDonationsResponse> {
            override fun onFailure(call: Call<RecurringDonationsResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        activity!!, getString(R.string.try_again), Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RecurringDonationsResponse>,
                response: Response<RecurringDonationsResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val church_project_data = resp.church_project_donations!!
                        val church_general_data = resp.church_general_fund!!
                        val inactive_church_project_data = resp.inactive_church_project_donations!!
                        val inactive_church_general_data = resp.inactive_church_general_fund!!
                        mActiveList.addAll(church_general_data)
                        mActiveList.addAll(church_project_data)
                        mInActiveList.addAll(inactive_church_general_data)
                        mInActiveList.addAll(inactive_church_project_data)
                        val inactive_adapter =
                            RecurringChurchesAdapterNew(activity!!, mInActiveList, "project", "inactive")
                        rv_in_active.adapter = inactive_adapter
                        inactive_adapter.notifyDataSetChanged()
                        val active_adapter =
                            RecurringChurchesAdapterNew(activity!!, mActiveList, "project", "active")
                        rv_active.adapter = active_adapter
                        active_adapter.notifyDataSetChanged()
                        if (mInActiveList.size > 0) {
                            rv_in_active.visibility = View.VISIBLE
                            tv_in_active_recurring_empty.visibility = View.GONE
                        } else {
                            rv_in_active.visibility = View.GONE
                            tv_in_active_recurring_empty.visibility = View.VISIBLE
                        }
                        if (mActiveList.size > 0) {
                            rv_active.visibility = View.VISIBLE
                            tv_active_recurring_empty.visibility = View.GONE
                        } else {
                            rv_active.visibility = View.GONE
                            tv_active_recurring_empty.visibility = View.VISIBLE
                        }
                    } else {
                        Toast.makeText(activity!!, resp.result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun deleteRecurringDonation(finalObject: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.deleteRecurringDonationApi(finalObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Log.d("delete_recurring", "error $t")
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        getMyRecurringDonationsApi()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    @SuppressLint("SetTextI18n")
    inner class RecurringChurchesAdapterNew(
        context: Context,
        list: ArrayList<RecurringDonationsListResponse>,
        private val screen_type: String,
        private val activeStatus: String
    ) :
        RecyclerView.Adapter<RecurringChurchesAdapterNew.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<RecurringDonationsListResponse>? = null

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.new_recur_donation_item, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList!!.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.tv_recurring_amount.text =
                Formatter().format("%,.2f", mList!![position].amount!!).toString()
            holder.tv_recurring_freq.text =
                mList!![position].reccurringType + " " + mContext!!.getString(R.string.recurring)
            holder.tv_name_id.text = mList!![position].name
            holder.tv_start_date_id.text = mList!![position].startDate
            if (mList!![position].endDate == null) {
                holder.tv_end_date_id.text = "Not Scheduled"
            } else {
                holder.tv_end_date_id.text = mList!![position].endDate

            }
            if (mList!![position].projectName.equals("general_fund")) {
                holder.tv_org_name.text = "General Fund"
            } else {
                holder.tv_org_name.text =
                    mList!![position].projectName
            }
            holder.tv_bottom_title_location.text =
                " (" + mList!![position].orgStreetAddress + "," + mList!![position].orgStateId + ")"
            holder.cv_delete.setOnClickListener {
                if (isInternetConnected) {
                    val obj = JSONObject()
                    obj.put("userId", userId)
                    obj.put("donationId", mList!![position].id.toString())
                    val jParser = JsonParser()
                    val finalObj = jParser.parse(obj.toString()) as JsonObject
                    deleteRecurringDonation(finalObj)
                } else {
                    Toast.makeText(
                        mContext!!,
                        getString(R.string.no_network_available),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
            holder.rl_main.setOnClickListener {
                val intent = Intent(mContext!!, RecurringDonationDetailedViewActivity::class.java)
                if (mList!![position].reccurringType == null) {
                    intent.putExtra("reccurringType", "")
                } else {
                    intent.putExtra("reccurringType", mList!![position].reccurringType)
                }
                if (mList!![position].paymentType == null) {
                    intent.putExtra("paymentType", "")
                } else {
                    intent.putExtra("paymentType", mList!![position].paymentType)
                }
                if (mList!![position].cardHolderName == null) {
                    intent.putExtra("cardHolderName", "")
                } else {
                    intent.putExtra("cardHolderName", mList!![position].cardHolderName)
                }
                if (mList!![position].name == null) {
                    intent.putExtra("orgName", "")
                } else {
                    intent.putExtra("orgName", mList!![position].name)
                }
                if (mList!![position].orgStreetAddress == null) {
                    intent.putExtra("orgStreetAddress", "")
                } else {
                    intent.putExtra("orgStreetAddress", mList!![position].orgStreetAddress)
                }
                if (mList!![position].orgStateId == null) {
                    intent.putExtra("orgStateId", "")
                } else {
                    intent.putExtra("orgStateId", mList!![position].orgStateId)
                }
                if (mList!![position].cardLast4Digits == null) {
                    intent.putExtra("cardLast4Digits", "")
                } else {
                    intent.putExtra("cardLast4Digits", mList!![position].cardLast4Digits.toString())
                }
                if (mList!![position].amount == null) {
                    intent.putExtra("amount", "")
                } else {
                    intent.putExtra("amount", mList!![position].amount!!.toString())
                }
                if (mList!![position].id == null) {
                    intent.putExtra("donationId", "")
                } else {
                    intent.putExtra("donationId", mList!![position].id)
                }
                if (mList!![position].startDate == null) {
                    intent.putExtra("start_date", "")
                } else {
                    intent.putExtra("start_date", mList!![position].startDate)
                }
                if (mList!![position].endDate == null) {
                    intent.putExtra("end_date", "")
                } else {
                    intent.putExtra("end_date", mList!![position].endDate)
                }
                if (mList!![position].totalAmount == null) {
                    intent.putExtra("totalAmount", "")
                } else {
                    intent.putExtra("totalAmount", mList!![position].totalAmount)
                }
                intent.putExtra("screen_type", screen_type)
                intent.putExtra("schedule_array", mList!![position].schedules)
                intent.putExtra("sourceId", mList!![position].sourceId)
                intent.putExtra("projectName", mList!![position].projectName)
                intent.putExtra("subscriptionAmount", mList!![position].subscriptionAmount)
                intent.putExtra("isHasEnd", mList!![position].isHasEnd.toString())
                intent.putExtra("activeStatus", activeStatus)

                mContext!!.startActivity(intent)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var tv_recurring_amount = view.findViewById(R.id.tv_recurring_amount) as TextView
            var tv_recurring_freq = view.findViewById(R.id.tv_recurring_freq) as TextView
            var tv_org_name = view.findViewById(R.id.tv_org_name) as TextView
            var tv_bottom_title_location =
                view.findViewById(R.id.tv_bottom_title_location) as TextView
            var rl_main = view.findViewById(R.id.rl_main) as RelativeLayout
            var cv_delete = view.findViewById(R.id.cv_delete) as CardView
            var tv_name_id = view.findViewById(R.id.tv_name_id) as TextView
            var tv_start_date_id = view.findViewById(R.id.tv_start_date_id) as TextView
            var tv_end_date_id = view.findViewById(R.id.tv_end_date_id) as TextView
        }

    }

}