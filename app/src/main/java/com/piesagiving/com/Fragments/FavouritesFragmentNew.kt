package com.piesagiving.com.Fragments

import android.os.Bundle
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R

class FavouritesFragmentNew : Fragment() {

    private lateinit var rootview: View/*? =null*/
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private var isChurchClicked = false
    private var isCharityClicked = false
    private var isCausedClicked = false
    private var isCirclesClicked = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""

    private lateinit var mFragmentManager: FragmentManager
    lateinit var ll_church: LinearLayout
    lateinit var tv_church: TextView
    lateinit var iv_church: ImageView
    lateinit var ll_friends: LinearLayout
    lateinit var tv_friends: TextView
    lateinit var iv_friends: ImageView
    lateinit var ll_charities: LinearLayout
    lateinit var tv_charities: TextView
    lateinit var iv_charities: ImageView
    lateinit var ll_special_causes: LinearLayout
    lateinit var tv_special_causes: TextView
    lateinit var iv_special_causes: ImageView
    lateinit var tv_header: TextView
    lateinit var iv_back: ImageView
    lateinit var contentView: FrameLayout

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for fragment
        try {
            rootview = inflater.inflate(R.layout.fragment_favourites_new, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)

        try {
            val tx = requireActivity().supportFragmentManager.beginTransaction()
            val fragment = MyChurchesFragmentNew()
            val bundle = Bundle()
            bundle.putString("clicked", "charity")
            fragment.arguments = bundle
            tx.replace(R.id.contentView, fragment)
            tx.commit()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        // default setting charity as activated
        isCharityClicked = true
        tv_header.text = getString(R.string.my_circle)
        tv_charities.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_button_background))
        iv_charities.setImageResource(R.drawable.ic_selected_charity)
        iv_church.setImageResource(R.drawable.ic_unselected_church)
        iv_special_causes.setImageResource(R.drawable.ic_unselected_splcause)
        iv_friends.setImageResource(R.drawable.ic_unselected_friends)
        iv_back.visibility = View.INVISIBLE

        ll_church.setOnClickListener {
            isChurchClicked = true
            isCharityClicked = false
            isCausedClicked = false
            isCirclesClicked = false
            iv_back.visibility = View.VISIBLE
//            tv_header.text = getString(R.string.my_church)
            tv_church.setTextColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.login_button_background
                )
            )
            tv_charities.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_special_causes.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_friends.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            iv_church.setImageResource(R.drawable.ic_selected_church)
            iv_charities.setImageResource(R.drawable.ic_unselected_charity)
            iv_special_causes.setImageResource(R.drawable.ic_unselected_splcause)
            iv_friends.setImageResource(R.drawable.ic_unselected_friends)
            changeInnerFragment(MyChurchesFragmentNew(), "church",true)
        }
        ll_charities.setOnClickListener {
            isChurchClicked = false
            isCharityClicked = true
            isCausedClicked = false
            isCirclesClicked = false
            iv_back.visibility = View.INVISIBLE
//            tv_header.text = getString(R.string.charities)
            tv_church.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_charities.setTextColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.login_button_background
                )
            )
            tv_special_causes.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_friends.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            iv_church.setImageResource(R.drawable.ic_unselected_church)
            iv_charities.setImageResource(R.drawable.ic_selected_charity)
            iv_special_causes.setImageResource(R.drawable.ic_unselected_splcause)
            iv_friends.setImageResource(R.drawable.ic_unselected_friends)
            changeInnerFragment(MyChurchesFragmentNew(), "charity",true)}
        ll_special_causes.setOnClickListener {
            isChurchClicked = false
            isCharityClicked = false
            isCausedClicked = true
            isCirclesClicked = false
            iv_back.visibility = View.VISIBLE
//            tv_header.text = getString(R.string.special_causes)
            tv_church.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_charities.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_special_causes.setTextColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.login_button_background
                )
            )
            tv_friends.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            iv_church.setImageResource(R.drawable.ic_unselected_church)
            iv_charities.setImageResource(R.drawable.ic_unselected_charity)
            iv_special_causes.setImageResource(R.drawable.ic_selected_splcause)
            iv_friends.setImageResource(R.drawable.ic_unselected_friends)
//            changeInnerFragment(MyChurchesFragmentNew(), "special_cause",true)
            changeInnerFragment(MyChurchesFragmentNew(), "fund_raiser",true)
        }
        ll_friends.setOnClickListener {
            isChurchClicked = false
            isCharityClicked = false
            isCausedClicked = false
            isCirclesClicked = true
            iv_back.visibility = View.VISIBLE
//            tv_header.text = getString(R.string.friends)
            tv_church.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_charities.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_special_causes.setTextColor(ContextCompat.getColor(requireActivity(), R.color.login_text))
            tv_friends.setTextColor(
                ContextCompat.getColor(
                    requireActivity(),
                    R.color.login_button_background
                )
            )
            iv_church.setImageResource(R.drawable.ic_unselected_church)
            iv_charities.setImageResource(R.drawable.ic_unselected_charity)
            iv_special_causes.setImageResource(R.drawable.ic_unselected_splcause)
            iv_friends.setImageResource(R.drawable.ic_selected_friends)
            changeInnerFragment(MyChurchesFragmentNew(), "friends",true)
        }
        iv_back.setOnClickListener {
            ll_charities.performClick()
        }
        return rootview
    }

    private fun initialize(view: View?) {
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet

        if (view != null) {
            ll_church = view.findViewById(R.id.ll_church)
            tv_church = view.findViewById(R.id.tv_church)
            iv_church = view.findViewById(R.id.iv_church)
            ll_charities = view.findViewById(R.id.ll_charities)
            tv_charities = view.findViewById(R.id.tv_charities)
            iv_charities = view.findViewById(R.id.iv_charities)
            ll_special_causes = view.findViewById(R.id.ll_special_causes)
            tv_special_causes = view.findViewById(R.id.tv_special_causes)
            iv_special_causes = view.findViewById(R.id.iv_special_causes)
            ll_friends = view.findViewById(R.id.ll_friends)
            tv_friends = view.findViewById(R.id.tv_friends)
            iv_friends = view.findViewById(R.id.iv_friends)
            tv_header = view.findViewById(R.id.tv_header)
            iv_back = view.findViewById(R.id.iv_back)
            contentView = view.findViewById(R.id.contentView)
        }

        mFragmentManager = requireActivity().supportFragmentManager
    }

    private fun changeInnerFragment(fragment: Fragment, clicked: String, addToBackStack: Boolean) {
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        val bundle = Bundle()
        bundle.putString("clicked", clicked)
        fragment.arguments = bundle
        mFragmentTransaction.replace(R.id.contentView, fragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        if (addToBackStack)
            mFragmentTransaction.addToBackStack(null)

        mFragmentTransaction.commit()
    }


}