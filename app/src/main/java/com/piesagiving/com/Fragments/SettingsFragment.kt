package com.piesagiving.com.Fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.piesagiving.com.Activities.*
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.SessionSplash
import com.piesagiving.com.R


class SettingsFragment : Fragment() {

    private var rootview: View? = null
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false

    lateinit var ll_account_settings: LinearLayout
    lateinit var ll_location_settings: LinearLayout
    lateinit var ll_privacy_settings: LinearLayout
    lateinit var ll_security_settings: LinearLayout
    lateinit var ll_notification_settings: LinearLayout
    lateinit var ll_help_settings: LinearLayout
    lateinit var ll_language_settings: LinearLayout
    lateinit var ll_about_settings: LinearLayout
    lateinit var ll_logout: LinearLayout
    lateinit var ll_tickets: LinearLayout
    lateinit var tv_invitefriends: TextView
    lateinit var tv_rateus: TextView
    var package_name = "com.piesagiving.com"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for fragment
        if (rootview != null) {
            val parent = rootview!!.parent as ViewGroup
            parent.removeView(rootview)
        }
        try {
            rootview = inflater.inflate(R.layout.fragment_settings, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)

        return rootview
    }

    private fun initialize(view: View?) {
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet
        if (view != null) {
            ll_account_settings = view.findViewById(R.id.ll_account_settings)
            ll_location_settings = view.findViewById(R.id.ll_location_settings)
            ll_privacy_settings = view.findViewById(R.id.ll_privacy_settings)
            ll_security_settings = view.findViewById(R.id.ll_security_settings)
            ll_notification_settings = view.findViewById(R.id.ll_notification_settings)
            ll_help_settings = view.findViewById(R.id.ll_help_settings)
            ll_language_settings = view.findViewById(R.id.ll_language_settings)
            ll_about_settings = view.findViewById(R.id.ll_about_settings)
            ll_tickets = view.findViewById(R.id.ll_tickets)
            tv_invitefriends = view.findViewById(R.id.tv_invitefriends)
            ll_logout = view.findViewById(R.id.ll_logout)
            tv_rateus = view.findViewById(R.id.tv_rateus)
        }

        ll_account_settings.setOnClickListener {
            val intent = Intent(context, AccountActivity::class.java)
            startActivity(intent)
        }
        ll_location_settings.setOnClickListener {
            val intent = Intent(context, LocationServicesActivity::class.java)
            startActivity(intent)
        }
        ll_privacy_settings.setOnClickListener {
            val intent = Intent(context, PrivacyActivity::class.java)
            startActivity(intent)
        }
        ll_security_settings.setOnClickListener {
            val intent = Intent(context, SecurityActivity::class.java)
            startActivity(intent)
        }
        ll_notification_settings.setOnClickListener {
            val intent = Intent(context, NotificationsActivity::class.java)
            startActivity(intent)
        }
        ll_help_settings.setOnClickListener {
            val intent = Intent(context, HelpActivity::class.java)
            startActivity(intent)
        }
        ll_language_settings.setOnClickListener {
            val intent = Intent(context, LanguagesActivity::class.java)
            startActivity(intent)
        }
        ll_about_settings.setOnClickListener {
            val intent = Intent(context, AboutUsActivity::class.java)
            startActivity(intent)
        }
        ll_tickets.setOnClickListener {
            val intent = Intent(context, TicketsListActivity::class.java)
            startActivity(intent)
        }
        tv_invitefriends.setOnClickListener {
//            val text = "https://play.google.com/store/apps/details?id=$package_name&hl=en"
//            whatsApp_sendMsg(text)
            share()
        }
        tv_rateus.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$package_name")))
        }
        ll_logout.setOnClickListener {
            AlertDialog.Builder(requireActivity())
                .setTitle("Logout")
                .setMessage("Confirm Logging out?")
                .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                    sessionManager.logout()
                    val sessionSplash = SessionSplash(requireActivity())
                    sessionSplash.setSplashScreen("1")
                    val intent = Intent(requireActivity(), SignUpSignInActivity::class.java)
                    startActivity(intent)
                    requireActivity().finish()
                    dialog!!.dismiss()
                }
                .setNegativeButton(resources.getString(R.string.cancel)
                ) { dialog, which -> dialog!!.dismiss() }
                .create()
                .show()
        }
    }

    private fun whatsApp_sendMsg(text: String) {
        val pm = requireActivity().packageManager
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, text)
            sendIntent.type = "text/plain"
            Intent.createChooser(sendIntent, "Share via")
            startActivity(sendIntent)
        } catch (e: Exception) {

        }
    }

    private fun share() {
        try {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
            val shareMessage = "https://play.google.com/store/apps/details?id=$package_name&hl=en"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "Share via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}



