package com.piesagiving.com.Fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Activities.ChurchProfileActivity
import com.piesagiving.com.Adapters.ChurchesAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.DashboardPojo
import com.piesagiving.com.ApiInterface.RecommendationArray
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.Helpers.SnapHelperOneByOne
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


@SuppressLint("SetTextI18n")
class HomeFragment : Fragment() {

    private lateinit var rootview: View
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    private var user_name = ""
    private var userId = ""
    private var display_name = ""

    lateinit var tv_user_name: TextView
    lateinit var tv_empty_recommendations: TextView
    lateinit var tv_empty_trending: TextView
    lateinit var tv_empty_circles: TextView
    lateinit var rv_recommendations: RecyclerView
    lateinit var rv_circles: RecyclerView
    lateinit var rv_trending: RecyclerView
    lateinit var loading_dialog: Dialog
    lateinit var recommendations_rv_LayoutManager: LinearLayoutManager
    lateinit var trending_rv_LayoutManager: LinearLayoutManager
    lateinit var circles_rv_LayoutManager: LinearLayoutManager

    lateinit var ourRecommendationsArray: ArrayList<RecommendationArray>
    lateinit var trendingNowArray: ArrayList<RecommendationArray>
    lateinit var yourCirleArray: ArrayList<RecommendationArray>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for fragment
        try {
            rootview = inflater.inflate(R.layout.fragment_home, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)

        val s1: String = user_name.substring(0, 1).toUpperCase(Locale.getDefault())
        val nameCapitalized = s1 + user_name.substring(1)

        tv_user_name.text = resources.getString(R.string.welcome) + " " + nameCapitalized + "!"
        val obj = JSONObject()
        obj.put("userId", userId)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("dash_bard_input", jObject.toString())



        HomeResponse(jObject)

        rv_recommendations.addOnItemTouchListener(
            RecyclerItemClickListener(requireActivity(),
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(activity!!, ChurchProfileActivity::class.java)
                        intent.putExtra("from_screen", "profile")
                        intent.putExtra("display_name", ourRecommendationsArray[position].displayName.toString())
                        intent.putExtra("reco_organizationId", ourRecommendationsArray[position].id)
                        startActivity(intent)
                    }
                })
        )

        rv_trending.addOnItemTouchListener(
            RecyclerItemClickListener(requireActivity(),
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(activity!!, ChurchProfileActivity::class.java)
                        intent.putExtra("from_screen", "profile")
                        intent.putExtra("display_name", trendingNowArray[position].displayName.toString())
                        intent.putExtra("reco_organizationId", trendingNowArray[position].id)
                        startActivity(intent)
                    }
                })
        )

        rv_circles.addOnItemTouchListener(
            RecyclerItemClickListener(requireActivity(),
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        val intent = Intent(activity!!, ChurchProfileActivity::class.java)
                        intent.putExtra("from_screen", "profile")
                        intent.putExtra("display_name", yourCirleArray[position].displayName.toString())
                        intent.putExtra("reco_organizationId", yourCirleArray[position].id)
                        startActivity(intent)
                    }
                })
        )

        return rootview
    }

    private fun initialize(view: View?) {
        loadingDialog()
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        user_name = user_details[SessionManager.FIRST_NAME_KEY]!!
        userId = user_details[SessionManager.ID_KEY]!!

        if (view != null) {
            tv_user_name = view.findViewById(R.id.tv_user_name)
            tv_empty_recommendations = view.findViewById(R.id.tv_empty_recommendations)
            tv_empty_trending = view.findViewById(R.id.tv_empty_trending)
            tv_empty_circles = view.findViewById(R.id.tv_empty_circles)

            rv_recommendations = view.findViewById(R.id.rv_recommendations)
            rv_trending = view.findViewById(R.id.rv_trending)
            rv_circles = view.findViewById(R.id.rv_circles)
        }

        recommendations_rv_LayoutManager =
            LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        trending_rv_LayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        circles_rv_LayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)

        // initializing layout manager to recyclerView

    }

    private fun HomeResponse(json_obj: JsonObject) {
        loading_dialog.show()
        ourRecommendationsArray = ArrayList()
        ourRecommendationsArray.clear()

        trendingNowArray = ArrayList()
        trendingNowArray.clear()

        yourCirleArray = ArrayList()
        yourCirleArray.clear()

        val apiInterface = ApiInterface.create()
        val call = apiInterface.DashboardApi(json_obj)
        call.enqueue(object : Callback<DashboardPojo> {
            override fun onFailure(call: Call<DashboardPojo>, t: Throwable) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                Log.e("responseStatus", "Exception  " + call + "  " + t)

            }

            override fun onResponse(
                call: Call<DashboardPojo>,
                response: Response<DashboardPojo>?
            ) {
                if (loading_dialog.isShowing)
                    loading_dialog.dismiss()
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {

                        if (!response.body()!!.ourRecommendations!!.size.equals(null)) {
                            for (i in 0 until response.body()!!.ourRecommendations!!.size) {
                                ourRecommendationsArray.add(response.body()!!.ourRecommendations!!.get(i))
                                display_name = ourRecommendationsArray[i].displayName.toString()
                            }
                        }

                        if (!response.body()!!.trendingNow!!.size.equals(null)) {
                            for (i in 0 until response.body()!!.trendingNow!!.size) {
                                trendingNowArray.add(response.body()!!.trendingNow!!.get(i))
                                display_name = trendingNowArray[i].displayName.toString()

                            }
                        }

                        if (!response.body()!!.yourCirle!!.size.equals(null)) {
                            for (i in 0 until response.body()!!.yourCirle!!.size) {
                                yourCirleArray.add(response.body()!!.yourCirle!!.get(i))
                                display_name = yourCirleArray[i].displayName.toString()

                            }
                            tv_empty_circles.visibility = View.GONE
                        } else {
                            tv_empty_circles.visibility = View.VISIBLE
                        }



                        rv_recommendations.layoutManager = recommendations_rv_LayoutManager
                        rv_trending.layoutManager = trending_rv_LayoutManager
                        rv_circles.layoutManager = circles_rv_LayoutManager
                        rv_recommendations.setHasFixedSize(true)
                        rv_trending.setHasFixedSize(true)
                        rv_circles.setHasFixedSize(true)

                        val adapter = ChurchesAdapter(activity!!, ourRecommendationsArray)
                        val adapter1 = ChurchesAdapter(activity!!, trendingNowArray)
                        val adapter2 = ChurchesAdapter(activity!!, yourCirleArray)
                        rv_recommendations.adapter = adapter
                        rv_trending.adapter = adapter1
                        rv_circles.adapter = adapter2
                        adapter.notifyDataSetChanged()
                        adapter1.notifyDataSetChanged()
                        adapter2.notifyDataSetChanged()

                        val linear = SnapHelperOneByOne()
                        linear.attachToRecyclerView(rv_recommendations)
                        val linear_trending = SnapHelperOneByOne()
                        linear_trending.attachToRecyclerView(rv_trending)
                        val linear_circles = SnapHelperOneByOne()
                        linear_circles.attachToRecyclerView(rv_circles)


                    } else {
                        /*Toast.makeText(
                            activity,
                            "Data Fetching Failed",
                            Toast.LENGTH_SHORT
                        ).show()*/
                    }
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(requireActivity())
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(requireActivity()).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }


}