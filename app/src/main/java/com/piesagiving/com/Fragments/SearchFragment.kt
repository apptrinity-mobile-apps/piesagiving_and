package com.piesagiving.com.Fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Activities.SearchPeopleActivity
import com.piesagiving.com.Adapters.OrganizationsAdapter
import com.piesagiving.com.Adapters.RecentSearchChurchesAdapter
import com.piesagiving.com.ApiInterface.*
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.RecyclerItemClickListener
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchFragment : Fragment() {

    private lateinit var rootview: View
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private lateinit var user_details: HashMap<String, String>
    private var isFilterVisible = false
    private var isSpecialCauseSelected = false
    private var userId = ""
    private var categoryId = ""

    lateinit var rv_categories: RecyclerView
    lateinit var rv_recent_searches: RecyclerView

    lateinit var rv_filter: RecyclerView
    lateinit var search_rv_LayoutManager: LinearLayoutManager
    lateinit var recent_search_rv_LayoutManager: LinearLayoutManager
    lateinit var searched_church_rv_LayoutManager: LinearLayoutManager
    lateinit var filter_rv_LayoutManager: LinearLayoutManager
    lateinit var ll_recent_search: LinearLayout
    lateinit var ll_church_search: LinearLayout
    lateinit var ll_org_by_type: LinearLayout
    lateinit var ll_filter: LinearLayout
    lateinit var ll_filter_button: LinearLayout
    lateinit var ll_recyclerView: LinearLayout
    lateinit var tv_selected_type: TextView
    lateinit var tv_special_causes: TextView
    lateinit var tv_filter_header: TextView
    lateinit var tv_apply_filters: TextView
    lateinit var tv_clear_filters: TextView
    lateinit var tv_empty_searched_churches: TextView
    lateinit var iv_filter_icon: ImageView
    lateinit var loading_dialog: Dialog
    lateinit var et_search: EditText
    lateinit var ll_categories: LinearLayout
    lateinit var tv_clear_all: TextView
    lateinit var contentView: LinearLayout
    lateinit var ll_search_bar: LinearLayout

    lateinit var searchChurchesAdapter: SearchChurchesAdapter
    lateinit var selectedFilterItems: JSONArray
    lateinit var categoriesList: ArrayList<CategoriesDataResponse>
    lateinit var categoryTypeList: ArrayList<CategoryTypeDataResponse>
    lateinit var organizationsList: ArrayList<OrganizationDataResponse>
    lateinit var recentSearchChurchesAdapter: RecentSearchChurchesAdapter
    lateinit var recentSearchesList: ArrayList<RecentSearchDataResponse>

    var row_index = -1
    var isFilterDataAvailable = false
    var selectedPosition = -1
    var cat_id = ""
    var isFilterSelected = false
    var reLoadFragment = false

    // to convert int to dp
    private val Int.dp: Int
        get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for fragment
        try {
            rootview = inflater.inflate(R.layout.fragment_search, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)
        val arg = arguments
        if (arg != null) {
            cat_id = arg.getString("cat_id")!!
        }

        ll_recent_search.visibility = View.VISIBLE
        tv_selected_type.text = ""

        if (isInternetConnected) {
            val obj = JSONObject()
            obj.put("userId", userId)
            val jParser = JsonParser()
            val finalObject = jParser.parse(obj.toString()) as JsonObject
            getRecentSearches(finalObject, true)
        } else {
            Toast.makeText(
                requireActivity(),
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        if (isInternetConnected) {
            val obj = JSONObject()
            obj.put("userId", userId)
            val jsonParser = JsonParser()
            val finalObj = jsonParser.parse(obj.toString()) as JsonObject
            getCategoriesApi(finalObj)
        } else {
            Toast.makeText(
                requireActivity(),
                resources.getString(R.string.no_network_available),
                Toast.LENGTH_SHORT
            ).show()
        }

        // to show filter
        ll_filter_button.setOnClickListener {
//            showFilterDialog()
            if (isFilterVisible) {
                isFilterVisible = false
                ll_org_by_type.visibility = View.VISIBLE
                ll_filter.visibility = View.GONE
                tv_filter_header.setTextColor(
                    getColor(requireActivity(), R.color.login_button_background)
                )
                iv_filter_icon.colorFilter = null
                ll_filter_button.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.button_background_white)
            } else {
                isFilterVisible = true
                ll_org_by_type.visibility = View.GONE
                ll_filter.visibility = View.VISIBLE
                tv_filter_header.setTextColor(getColor(requireActivity(), R.color.white))
                iv_filter_icon.setColorFilter(R.color.white)
                ll_filter_button.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.button_background_blue)
            }
        }

        tv_special_causes.setOnClickListener {
            if (isSpecialCauseSelected) {
                isSpecialCauseSelected = false
                tv_special_causes.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.button_background_white)
                tv_special_causes.setTextColor(
                    getColor(
                        requireActivity(),
                        R.color.login_button_background
                    )
                )
            } else {
                isSpecialCauseSelected = true
                tv_special_causes.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.button_background_blue)
                tv_special_causes.setTextColor(getColor(requireActivity(), R.color.white))
            }
            getOrganizationByCategories()
        }

        tv_clear_filters.setOnClickListener {
            selectedFilterItems = JSONArray()
            if (categoryTypeList.size > 0) {
                val filterAdapter = FilterAdapter(requireActivity(), categoryTypeList)
                rv_filter.adapter = filterAdapter
                filterAdapter.notifyDataSetChanged()
            }
            getOrganizationByCategories()
        }

        rv_categories.addOnItemTouchListener(
            RecyclerItemClickListener(requireActivity(),
                object : RecyclerItemClickListener.OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {
                        selectedFilterItems = JSONArray()
                        isFilterDataAvailable = false
                        // special causes visibility gone
                        if (isSpecialCauseSelected) {
                            isSpecialCauseSelected = false
                            tv_special_causes.background =
                                ContextCompat.getDrawable(
                                    activity!!,
                                    R.drawable.button_background_white
                                )
                            tv_special_causes.setTextColor(
                                getColor(
                                    activity!!,
                                    R.color.login_button_background
                                )
                            )
                        }
                        // filter visibility gone
                        if (isFilterVisible) {
                            isFilterVisible = false
                            ll_org_by_type.visibility = View.VISIBLE
                            ll_filter.visibility = View.GONE
                            tv_filter_header.setTextColor(
                                getColor(activity!!, R.color.login_button_background)
                            )
                            iv_filter_icon.colorFilter = null
                            ll_filter_button.background =
                                ContextCompat.getDrawable(
                                    activity!!,
                                    R.drawable.button_background_white
                                )
                        }
                        categoryId = categoriesList[position].id!!
                        Log.d("searchFragment", "categoryId===$categoryId")
                        if (selectedPosition == -1 || selectedPosition != position) {
                            selectedPosition = position
                            getOrganizationByCategories()
                            Log.d("position", "position if $selectedPosition")
                        } else if (position == selectedPosition) {
                            Log.d("position", "position elsif $selectedPosition")
                            getOrganizationByCategories()
                        }
                    }

                })
        )

        tv_apply_filters.setOnClickListener {
            if (isFilterVisible) {
                isFilterVisible = false
                ll_org_by_type.visibility = View.VISIBLE
                ll_filter.visibility = View.GONE
                tv_filter_header.setTextColor(
                    getColor(
                        requireActivity(),
                        R.color.login_button_background
                    )
                )
                iv_filter_icon.colorFilter = null
                ll_filter_button.background =
                    ContextCompat.getDrawable(requireActivity(), R.drawable.button_background_white)
            }
            getOrganizationByCategories()
        }

        tv_clear_all.setOnClickListener {

            val obj = JSONObject()
            obj.put("userId", userId)
            val jParser = JsonParser()
            val finalObj = jParser.parse(obj.toString()) as JsonObject
            clearSearches(finalObj)
        }

        et_search.setOnClickListener {
            ll_search_bar.performClick()
        }

        ll_search_bar.setOnClickListener {
            val i = Intent(requireActivity(), SearchPeopleActivity::class.java)
            i.putExtra("from_screen", "search")
            startActivity(i)
        }

        return rootview
    }

    private fun initialize(view: View?) {
        loadingDialog()
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!

        if (view != null) {
            ll_recent_search = view.findViewById(R.id.ll_recent_search)
            ll_church_search = view.findViewById(R.id.ll_church_search)
            ll_org_by_type = view.findViewById(R.id.ll_org_by_type)
            ll_filter = view.findViewById(R.id.ll_filter)
            ll_filter_button = view.findViewById(R.id.ll_filter_button)
            tv_selected_type = view.findViewById(R.id.tv_selected_type)
            iv_filter_icon = view.findViewById(R.id.iv_filter_icon)
            tv_filter_header = view.findViewById(R.id.tv_filter_header)
            tv_special_causes = view.findViewById(R.id.tv_special_causes)
            tv_empty_searched_churches = view.findViewById(R.id.tv_empty_searched_churches)
            ll_recyclerView = view.findViewById(R.id.ll_recyclerView)
            tv_apply_filters = view.findViewById(R.id.tv_apply_filters)
            tv_clear_filters = view.findViewById(R.id.tv_clear_filters)
            et_search = view.findViewById(R.id.et_search)
            ll_categories = view.findViewById(R.id.ll_categories)
            tv_clear_all = view.findViewById(R.id.tv_clear_all)
            contentView = view.findViewById(R.id.contentView)
            ll_search_bar = view.findViewById(R.id.ll_search_bar)
            rv_categories = view.findViewById(R.id.rv_categories)
            rv_recent_searches = view.findViewById(R.id.rv_recent_searches)
            rv_filter = view.findViewById(R.id.rv_filter)
        }

        search_rv_LayoutManager =
            LinearLayoutManager(requireActivity(), RecyclerView.HORIZONTAL, false)
        searched_church_rv_LayoutManager =
            LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        filter_rv_LayoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        recent_search_rv_LayoutManager =
            LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        rv_categories.layoutManager = search_rv_LayoutManager
        rv_recent_searches.layoutManager = recent_search_rv_LayoutManager
        rv_filter.layoutManager = filter_rv_LayoutManager
        rv_categories.hasFixedSize()
        rv_recent_searches.hasFixedSize()
        rv_filter.hasFixedSize()

        selectedFilterItems = JSONArray()

    }

    fun createRecyclerView(list: ArrayList<OrganizationDataResponse>) {
        ll_recyclerView.removeAllViews()
        if (selectedFilterItems.length() > 0) {
            isFilterSelected = true
        } else {
            isFilterSelected = false
        }
        if (list.size > 0) {
            val recyclerView = RecyclerView(requireActivity())
            val layoutManager =
                LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
            recyclerView.layoutManager = layoutManager
            val params = RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(10.dp, 7.dp, 10.dp, 7.dp)
            recyclerView.layoutParams = params
            recyclerView.setHasFixedSize(true)
            val organizationsAdapter =
                OrganizationsAdapter(
                    requireActivity(),
                    list,
                    userId,
                    isSpecialCauseSelected,
                    isFilterSelected
                )
            recyclerView.adapter = organizationsAdapter
            ll_recyclerView.addView(recyclerView)
        } else {
            val textView = TextView(requireActivity())
            val params = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            params.gravity = Gravity.CENTER
            textView.layoutParams = params
            textView.gravity = Gravity.CENTER
            textView.text = resources.getString(R.string.no_organizations_yet)
            ll_recyclerView.addView(textView)
        }
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(requireActivity())
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(requireActivity())
                .inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getCategoriesApi(final_object: JsonObject) {
        categoriesList = ArrayList()
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.getCategoriesApi(final_object)
        call.enqueue(object : Callback<CategoriesResponse> {
            override fun onFailure(call: Call<CategoriesResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        activity!!, getString(R.string.try_again), Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<CategoriesResponse>,
                response: Response<CategoriesResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.categoryList!!
                        categoriesList = data
                        searchChurchesAdapter =
                            SearchChurchesAdapter(activity!!, categoriesList)
                        rv_categories.adapter = searchChurchesAdapter
                        if (cat_id != "") {
                            for (i in 0 until categoriesList.size) {
                                if (cat_id == categoriesList[i].id) {
                                    categoryId = categoriesList[i].id.toString()
                                    selectedPosition = i
                                    row_index = i
                                    getOrganizationByCategories()
                                    ll_recent_search.visibility = View.GONE
                                    ll_church_search.visibility = View.VISIBLE
                                    tv_selected_type.text = categoriesList[i].categoryName
                                }
                            }
                        }
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getOrganizationByCategories() {

        organizationsList = ArrayList()
        if (!isFilterDataAvailable) {
            categoryTypeList = ArrayList()
        }
        loading_dialog.show()

        val dataObj = JSONObject()
        dataObj.put("userId", userId)
        dataObj.put("categoryId", categoryId)
        dataObj.put("specialCause", isSpecialCauseSelected)
        dataObj.put("categoryTypeList", selectedFilterItems)
        val jsonParser = JsonParser()
        val finalObj = jsonParser.parse(dataObj.toString()) as JsonObject
        Log.d("SearchFragment", "finalObj====$finalObj")

        val api = ApiInterface.create()
        val call = api.getOrganizationByCategoriesApi(finalObj)
        call.enqueue(object : Callback<SearchOrganizationCategoriesResponse> {
            override fun onFailure(
                call: Call<SearchOrganizationCategoriesResponse>,
                t: Throwable
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        activity!!,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<SearchOrganizationCategoriesResponse>,
                response: Response<SearchOrganizationCategoriesResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val categoryTypeData = resp.categoryTypeList!!
                        val organizationData = resp.organizationList!!
                        if (categoryTypeData.size > 0) {
                            if (!isFilterDataAvailable) {
                                isFilterDataAvailable = true
                                categoryTypeList = categoryTypeData
                                val filterAdapter = FilterAdapter(activity!!, categoryTypeList)
                                rv_filter.adapter = filterAdapter
                                filterAdapter.notifyDataSetChanged()
                            }
                        }
                        if (organizationData.size > 0) {
                            organizationsList = organizationData
                        }
                        createRecyclerView(organizationsList)
                    } else {
                        createRecyclerView(organizationsList)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getRecentSearches(final_object: JsonObject, showDialog: Boolean) {
        if (showDialog) {
            loading_dialog.show()
        }
        recentSearchesList = ArrayList()
        val api = ApiInterface.create()
        val call = api.getRecentSearchesApi(final_object)
        call.enqueue(object : Callback<RecentSearchResponse> {
            override fun onFailure(call: Call<RecentSearchResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        activity!!,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RecentSearchResponse>,
                response: Response<RecentSearchResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        val data = resp.get_searches_list!!
                        recentSearchesList = data
                        recentSearchChurchesAdapter =
                            RecentSearchChurchesAdapter(activity!!, recentSearchesList, userId)
                        rv_recent_searches.adapter = recentSearchChurchesAdapter
                        recentSearchChurchesAdapter.notifyDataSetChanged()
                        if (recentSearchesList.size > 0) {
                            tv_empty_searched_churches.visibility = View.GONE
                            rv_recent_searches.visibility = View.VISIBLE
                            tv_clear_all.visibility = View.VISIBLE
                        } else {
                            tv_empty_searched_churches.visibility = View.VISIBLE
                            rv_recent_searches.visibility = View.GONE
                            tv_clear_all.visibility = View.GONE
                        }
                    } else {
                        tv_empty_searched_churches.visibility = View.VISIBLE
                        rv_recent_searches.visibility = View.GONE
                        //Toast.makeText(activity!!, resp.result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun clearSearches(final_object: JsonObject) {
        loading_dialog.show()
        val api = ApiInterface.create()
        val call = api.clearRecentSearchesApi(final_object)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(
                        activity!!,
                        getString(R.string.try_again),
                        Toast.LENGTH_SHORT
                    ).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    if (resp!!.responseStatus == "1") {
                        tv_empty_searched_churches.visibility = View.VISIBLE
                        rv_recent_searches.visibility = View.GONE
                        tv_clear_all.visibility = View.GONE
//                        Toast.makeText(activity!!, resp.result, Toast.LENGTH_SHORT).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    inner class SearchChurchesAdapter(
        context: Context, list: ArrayList<CategoriesDataResponse>
    ) :
        RecyclerView.Adapter<SearchChurchesAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<CategoriesDataResponse> = ArrayList()

        init {
            this.mContext = context
            this.mList = list
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_search, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {

            holder.tv_list_name.text = mList[position].categoryName
            Picasso.with(activity!!).load(mList[position].categoryImageInActive.toString())
                .error(R.drawable.ic_profile)
                .into(holder.iv_list_icon)

            holder.ll_item_header.setOnClickListener {
                if (row_index == -1 || row_index != position) {
                    row_index = position
                    ll_recent_search.visibility = View.GONE
                    ll_church_search.visibility = View.VISIBLE
                    tv_selected_type.text = mList[position].categoryName
                } else if (position == row_index) {
                    row_index = -1
                    ll_recent_search.visibility = View.VISIBLE
                    ll_church_search.visibility = View.GONE
                    tv_selected_type.text = ""
                }
                notifyDataSetChanged()
            }
            if (row_index == position) {
                holder.tv_list_name.setTextColor(
                    getColor(mContext!!, R.color.login_button_background)
                )
                Picasso.with(activity!!).load(mList[position].categoryImageActive.toString())
                    .error(R.drawable.ic_profile)
                    .into(holder.iv_list_icon)

            } else {
                holder.tv_list_name.setTextColor(getColor(mContext!!, R.color.login_text))
                Picasso.with(activity!!).load(mList[position].categoryImageInActive.toString())
                    .error(R.drawable.ic_profile)
                    .into(holder.iv_list_icon)
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var ll_item_header: LinearLayout = view.findViewById(R.id.ll_item_header)
            var tv_list_name: TextView = view.findViewById(R.id.tv_list_name)
            var iv_list_icon: ImageView = view.findViewById(R.id.iv_list_icon)
        }
    }

    inner class FilterAdapter(
        context: Context,
        filter_options: ArrayList<CategoryTypeDataResponse>
    ) :
        RecyclerView.Adapter<FilterAdapter.ViewHolder>() {

        private var mContext: Context? = null
        private var mList: ArrayList<CategoryTypeDataResponse> = ArrayList()

        init {
            this.mContext = context
            this.mList = filter_options
        }

        override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_item_filter, parent, false)
            return ViewHolder(view)
        }

        override fun getItemCount(): Int {
            return mList.size
        }

        override fun onBindViewHolder(
            holder: ViewHolder,
            @SuppressLint("RecyclerView") position: Int
        ) {
            holder.checkBox.text = mList[position].typeName
            holder.checkBox.isChecked = false
            holder.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->
                Log.d("SearchFragment", "selectedFilterItems===$isChecked")
                if (isChecked) {
                    selectedFilterItems.put(mList[position].id)
                } else {
                    if (selectedFilterItems.length() > 0) {
                        for (i in 0 until selectedFilterItems.length()) {
                            if (selectedFilterItems[i] == mList[position].id) {
                                selectedFilterItems.remove(i)
                                break
                            }
                        }
                    }
                }
                Log.d(
                    "SearchFragment",
                    "selectedFilterItems===${selectedFilterItems.length()}"
                )
            }
        }

        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var checkBox: CheckBox = view.findViewById(R.id.checkBox)
        }

    }

    override fun onResume() {
        super.onResume()
        if (reLoadFragment) {
            if (isInternetConnected) {
                val obj = JSONObject()
                obj.put("userId", userId)
                val jParser = JsonParser()
                val finalObject = jParser.parse(obj.toString()) as JsonObject
                getRecentSearches(finalObject, false)
            } else {
                Toast.makeText(
                    requireActivity(),
                    resources.getString(R.string.no_network_available),
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        reLoadFragment = true
    }
}