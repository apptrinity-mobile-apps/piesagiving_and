package com.piesagiving.com.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Activities.ChurchProfileActivity
import com.piesagiving.com.Activities.FundRaisersActivity
import com.piesagiving.com.Activities.ProjectDetailedViewActivity
import com.piesagiving.com.Activities.YourCircleProfileActivity
import com.piesagiving.com.Adapters.ChurchSectionWiseAdapter2
import com.piesagiving.com.Adapters.FundRaiserSectionWiseAdapter
import com.piesagiving.com.Adapters.SectionWiseFriendsAdapter
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.CharityListDataResponse
import com.piesagiving.com.ApiInterface.FundRaiserFavouriteListDataResponse
import com.piesagiving.com.ApiInterface.GetMyCirclePojo
import com.piesagiving.com.Helpers.*
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class MyChurchesFragmentNew : Fragment() {

    private lateinit var rootview: View
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    private var clicked = ""
    private var reloadFragment = false

    lateinit var recyclerView: RecyclerView
    lateinit var alphabetik: Alphabetik
    lateinit var mFriendsList: ArrayList<CharityListDataResponse>
    lateinit var mChurchesList: ArrayList<CharityListDataResponse>
    lateinit var mChurchesListData: ArrayList<CharityListDataResponse>
    lateinit var mFundRaisersList: ArrayList<FundRaiserFavouriteListDataResponse>
    lateinit var mFundRaisersListData: ArrayList<FundRaiserFavouriteListDataResponse>
    lateinit var layoutManager: LinearLayoutManager
    var lastSelectedPos = 0
    lateinit var smoothScroller: SmoothScroller
    lateinit var tv_empty: TextView
    lateinit var loading_dialog: Dialog

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for fragment
        try {
            rootview = inflater.inflate(R.layout.fragment_my_churches_new, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        clicked = requireArguments().getString("clicked")!!
        Log.d("clicked", clicked)
        initialize(rootview)
        tv_empty.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
        when (clicked) {
            "church" -> {
                tv_empty.text = getString(R.string.not_following_churches)
            }
            "charity" -> {
                tv_empty.text = getString(R.string.not_following_charities)
            }
            "special_cause" -> {
                tv_empty.text = getString(R.string.not_following_spl_causes)
            }
            "friends" -> {
                tv_empty.text = getString(R.string.not_following_friends)
            }
            "fund_raiser" -> {
                tv_empty.text = getString(R.string.not_following_fund_raisers)
            }
        }

        alphabetik.onSectionIndexClickListener(object : Alphabetik.SectionIndexClickListener {
            override fun onItemClick(view: View?, position: Int, character: String?) {
                alphabetik.setLetterToBold(character!!)
                val pos = getPositionFromData(character)
                smoothScroller.targetPosition = pos
                layoutManager.startSmoothScroll(smoothScroller)
            }
        })

        return rootview
    }

    private fun initialize(view: View?) {
        loadingDialog()
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet

        if (view != null) {
            recyclerView = view.findViewById(R.id.recyclerView)
            alphabetik = view.findViewById(R.id.alphaSectionIndex)
            tv_empty = view.findViewById(R.id.tv_empty)
        }

        mChurchesList = ArrayList()
        mFriendsList = ArrayList()
        when {
            clicked == "church" -> {
                getMyCircleListApi(clicked)
            }
            clicked == "charity" -> {
                getMyCircleListApi(clicked)
            }
            clicked == "special_cause" -> {
                ViewSpecialCauseDonation(clicked)
            }
            clicked === "friends" -> {
                getMyCircleListFriendsApi(clicked)
            }
            clicked === "fund_raiser" -> {
                getMyCircleListApi(clicked)
            }
        }

        recyclerView.overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        layoutManager = LinearLayoutManager(requireActivity(), RecyclerView.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_START
            }
        }
    }

    private fun loadingDialog() {
        loading_dialog = Dialog(requireActivity())
        loading_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val view =
            LayoutInflater.from(requireActivity()).inflate(R.layout.loading_dialog, null)
        loading_dialog.setContentView(view)
        loading_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        loading_dialog.window!!.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        loading_dialog.setCanceledOnTouchOutside(false)
    }

    private fun getPositionFromData(character: String): Int {
        var position = 0
        if (clicked == "friends") {
            if (mFriendsList.isNotEmpty()) {
                for (i in 0 until mFriendsList.size) {
                    val name = mFriendsList[i].firstName
                    val letter = "" + name!![0]
                    if (letter == "" + character) {
                        lastSelectedPos = position
                        return position
                    } else if (position == (mFriendsList.size - 1)) {
                        return lastSelectedPos
                    }
                    position++
                }
            } else {
                position = 0
            }
        } else if (clicked == "fund_raiser") {
            if (mFundRaisersList.isNotEmpty()) {
                for (i in 0 until mFundRaisersList.size) {
                    val name = mFundRaisersList[i].name
                    val letter = "" + name!![0]
                    if (letter == "" + character) {
                        lastSelectedPos = position
                        return position
                    } else if (position == (mFundRaisersList.size - 1)) {
                        return lastSelectedPos
                    }
                    position++
                }
            } else {
                position = 0
            }
        } else {
            if (mChurchesList.isNotEmpty()) {
                for (i in 0 until mChurchesList.size) {
                    val name = mChurchesList[i].displayName
                    val letter = "" + name!![0]
                    if (letter == "" + character) {
                        lastSelectedPos = position
                        return position
                    } else if (position == (mChurchesList.size - 1)) {
                        return lastSelectedPos
                    }
                    position++
                }
            } else {
                position = 0
            }
        }
        return position
    }

    private fun generateHeadersForFriends(list: ArrayList<CharityListDataResponse>) {
        mFriendsList = ArrayList()
        Collections.sort(list, object : Comparator<CharityListDataResponse> {
            override fun compare(
                friendsModel1: CharityListDataResponse?,
                friendsModel2: CharityListDataResponse?
            ): Int {
                return (friendsModel1!!.firstName!![0]).uppercaseChar()
                    .compareTo((friendsModel2!!.firstName!![0]).uppercaseChar())
            }
        })
        var lastHeader = ""
        for (i in 0 until list.size) {
            val model = list[i]
            val header = model.firstName!![0].uppercaseChar().toString()
            if (!TextUtils.equals(lastHeader, header)) {
                lastHeader = header
                val charityListDataResponse = CharityListDataResponse()
                charityListDataResponse.displayName = model.displayName
                charityListDataResponse.city = model.city
                charityListDataResponse.country = model.country
                charityListDataResponse.coverPhoto = model.coverPhoto
                charityListDataResponse.organizationId = model.organizationId
                charityListDataResponse.isSection = true
                charityListDataResponse.displayHeader = model.firstName!![0].toString()
                charityListDataResponse.phoneNumber = model.phoneNumber
                charityListDataResponse.profilePic = model.profilePic
                charityListDataResponse.state = model.state
                charityListDataResponse.streetAddress = model.streetAddress
                charityListDataResponse.websiteLink = model.websiteLink
                charityListDataResponse.firstName = model.firstName
                charityListDataResponse.lastName = model.lastName
                charityListDataResponse.privacy = model.privacy
                charityListDataResponse.userName = model.userName
                mFriendsList.add(charityListDataResponse)
            }
            mFriendsList.add(model)
        }
        val adapter =
            SectionWiseFriendsAdapter(requireActivity(), mFriendsList)
        recyclerView.adapter = adapter
        Log.d("mFriendsList", "mFriendsList=====${mFriendsList.size}")
    }

    private fun generateHeadersForChurches(list: ArrayList<CharityListDataResponse>, tab_name:String) {
        mChurchesList = ArrayList()
        Collections.sort(list, object : Comparator<CharityListDataResponse> {
            override fun compare(
                churchModel1: CharityListDataResponse?,
                churchModel2: CharityListDataResponse?
            ): Int {
                return (churchModel1!!.displayName!![0]).toUpperCase()
                    .compareTo((churchModel2!!.displayName!![0]).toUpperCase())
            }
        })
        var lastHeader = ""
        for (i in 0 until list.size) {
            val model = list[i]
            val header = model.displayName!![0].toUpperCase().toString()
            if (!TextUtils.equals(lastHeader, header)) {
                lastHeader = header
                // modify data response
                val charityListDataResponse = CharityListDataResponse()
                charityListDataResponse.displayName = model.displayName
                charityListDataResponse.city = model.city
                charityListDataResponse.country = model.country
                charityListDataResponse.coverPhoto = model.coverPhoto
                charityListDataResponse.organizationId = model.organizationId
                charityListDataResponse.isSection = true
                charityListDataResponse.displayHeader = model.displayName!![0].toString()
                charityListDataResponse.phoneNumber = model.phoneNumber
                charityListDataResponse.profilePic = model.profilePic
                charityListDataResponse.state = model.state
                charityListDataResponse.streetAddress = model.streetAddress
                charityListDataResponse.websiteLink = model.websiteLink
                mChurchesList.add(charityListDataResponse)
            }
            mChurchesList.add(model)
        }
        val adapter =
            ChurchSectionWiseAdapter2(requireActivity(), tab_name, mChurchesList)
        recyclerView.adapter = adapter
        Log.d("mChurchesList", "mChurchesList====${mChurchesList.size}")
    }

    private fun generateHeadersForFundRaisers(list: ArrayList<FundRaiserFavouriteListDataResponse>) {
        mFundRaisersList = ArrayList()
        list.sortWith { model1, model2 ->
            (model1!!.name!![0]).uppercaseChar().compareTo((model2!!.name!![0]).uppercaseChar())
        }
        var lastHeader = ""
        for (i in 0 until list.size) {
            val model = list[i]
            val header = model.name!![0].uppercaseChar().toString()
            if (!TextUtils.equals(lastHeader, header)) {
                lastHeader = header
                // modify data response
                val dataResponse = FundRaiserFavouriteListDataResponse()
                dataResponse.category = model.category
                dataResponse.createdOn = model.createdOn
                dataResponse.description = model.description
                dataResponse.endDate = model.endDate
                dataResponse.goalAmount = model.goalAmount
                dataResponse.id = model.id
                dataResponse.image = model.image
                dataResponse.name = model.name
                dataResponse.startDate = model.startDate
                dataResponse.displayHeader = model.name!![0].toString()
                dataResponse.isSection = true
                mFundRaisersList.add(dataResponse)
            }
            mFundRaisersList.add(model)
        }
        val adapter = FundRaiserSectionWiseAdapter(requireActivity(), mFundRaisersList)
        recyclerView.adapter = adapter
        Log.d("mFundRaisersList", "mFundRaisersList====${mFundRaisersList.size}")
    }

    private fun getMyCircleListApi(tab_name: String) {

        val dataObj = JSONObject()
        dataObj.put("userId", sessionManager.getUserDetails().get(SessionManager.ID_KEY.toString()))
        Log.d("TAG", "dataObj===$dataObj")
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject

        loading_dialog.show()
        mChurchesListData = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetMyCirclesApi(finalJsonObject)
        call.enqueue(object : Callback<GetMyCirclePojo> {
            override fun onFailure(call: Call<GetMyCirclePojo>, t: Throwable) {
                try {
                    Log.d("TAG", "GetMyCirclesApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(requireActivity(), getString(R.string.try_again), Toast.LENGTH_SHORT)
                        .show()
                    tv_empty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMyCirclePojo>,
                response: Response<GetMyCirclePojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.charityList
                            val data2 = resp.churchList
                            val data3 = resp.fundraiserList
                            if (tab_name == "charity") {
                                mChurchesListData = data!!
                                mChurchesListData.reverse()
                                generateHeadersForChurches(mChurchesListData, tab_name)
                                if (mChurchesListData.size > 0) {
                                    tv_empty.visibility = View.GONE
                                    recyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_empty.visibility = View.VISIBLE
                                    recyclerView.visibility = View.GONE
                                }
                            } else if (tab_name == "church") {
                                mChurchesListData = data2!!
                                mChurchesListData.reverse()
                                generateHeadersForChurches(mChurchesListData, tab_name)
                                if (mChurchesListData.size > 0) {
                                    tv_empty.visibility = View.GONE
                                    recyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_empty.visibility = View.VISIBLE
                                    recyclerView.visibility = View.GONE
                                }
                            } else if (tab_name == "fund_raiser") {
                                mFundRaisersListData = data3!!
                                mFundRaisersListData.reverse()
                                generateHeadersForFundRaisers(mFundRaisersListData)
                                if (mFundRaisersListData.size > 0) {
                                    tv_empty.visibility = View.GONE
                                    recyclerView.visibility = View.VISIBLE
                                } else {
                                    tv_empty.visibility = View.VISIBLE
                                    recyclerView.visibility = View.GONE
                                }
                            }
                        }
                        else -> {
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getMyCircleListFriendsApi(tab_name: String) {

        val dataObj = JSONObject()
        dataObj.put("userId", sessionManager.getUserDetails().get(SessionManager.ID_KEY.toString()))
        Log.d("TAG", "dataObj===$dataObj")
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject

        loading_dialog.show()
        mFriendsList = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.GetMyFriendsCircle(finalJsonObject)
        call.enqueue(object : Callback<GetMyCirclePojo> {
            override fun onFailure(call: Call<GetMyCirclePojo>, t: Throwable) {
                try {
                    Log.d("TAG", "citiesApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(requireActivity(), getString(R.string.try_again), Toast.LENGTH_SHORT)
                        .show()
                    tv_empty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMyCirclePojo>,
                response: Response<GetMyCirclePojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.user_followers
                            if (tab_name == "friends") {
                                if (data!!.size > 0) {
                                    mFriendsList = data
                                    mFriendsList.reverse()
                                    generateHeadersForFriends(mFriendsList)
                                    if (mFriendsList.size > 0) {
                                        recyclerView.visibility = View.VISIBLE
                                        tv_empty.visibility = View.GONE
                                    } else {
                                        recyclerView.visibility = View.GONE
                                        tv_empty.visibility = View.VISIBLE
                                    }
                                } else {
                                    /*Toast.makeText(
                                        requireActivity(),
                                        getString(R.string.no_record_available),
                                        Toast.LENGTH_SHORT
                                    ).show()*/
                                }
                            }
                        }
                        else -> {
                            /*Toast.makeText(requireActivity(),
                                getString(R.string.no_record_available), Toast.LENGTH_SHORT).show()*/
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    private fun ViewSpecialCauseDonation(tab_name: String) {

        val dataObj = JSONObject()
        dataObj.put("userId", sessionManager.getUserDetails().get(SessionManager.ID_KEY.toString()))
        Log.d("TAG", "dataObj===$dataObj")
        val jsonParser = JsonParser()
        val finalJsonObject = jsonParser.parse(dataObj.toString()) as JsonObject

        loading_dialog.show()
        mChurchesListData = ArrayList()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.ViewSpecialCauseDonationApi(finalJsonObject)
        call.enqueue(object : Callback<GetMyCirclePojo> {
            override fun onFailure(call: Call<GetMyCirclePojo>, t: Throwable) {
                try {
                    Log.d("TAG", "citiesApi error $t")
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    Toast.makeText(requireActivity(), getString(R.string.try_again), Toast.LENGTH_SHORT)
                        .show()
                    tv_empty.visibility = View.VISIBLE
                    recyclerView.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<GetMyCirclePojo>,
                response: Response<GetMyCirclePojo>
            ) {
                try {
                    if (loading_dialog.isShowing)
                        loading_dialog.dismiss()
                    val resp = response.body()
                    when (resp!!.responseStatus) {
                        1 -> {
                            val data = resp.SpecialCauses
                            if (tab_name == "special_cause") {
                                if (data!!.size > 0) {
                                    mChurchesListData = data
                                    mChurchesListData.reverse()
                                    generateHeadersForChurches(mChurchesListData, tab_name)
                                    if (mChurchesListData.size > 0) {
                                        tv_empty.visibility = View.GONE
                                        recyclerView.visibility = View.VISIBLE
                                    } else {
                                        tv_empty.visibility = View.VISIBLE
                                        recyclerView.visibility = View.GONE
                                    }
                                } else {
                                    tv_empty.visibility = View.VISIBLE
                                    recyclerView.visibility = View.GONE
                                    /*Toast.makeText(
                                        requireActivity(),
                                        getString(R.string.no_record_available),
                                        Toast.LENGTH_SHORT
                                    ).show()*/
                                }
                            }

                        }
                        else -> {
                            /*Toast.makeText(requireActivity(),
                                getString(R.string.no_record_available), Toast.LENGTH_SHORT).show()*/
                            tv_empty.visibility = View.VISIBLE
                            recyclerView.visibility = View.GONE
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
        if (reloadFragment) {
            when {
                clicked == "church" -> {
                    getMyCircleListApi(clicked)
                }
                clicked == "charity" -> {
                    getMyCircleListApi(clicked)
                }
                clicked == "special_cause" -> {
                    ViewSpecialCauseDonation(clicked)
                }
                clicked === "friends" -> {
                    getMyCircleListFriendsApi(clicked)
                }
                clicked === "fund_raiser" -> {
                    getMyCircleListApi(clicked)
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        reloadFragment = true
    }
}