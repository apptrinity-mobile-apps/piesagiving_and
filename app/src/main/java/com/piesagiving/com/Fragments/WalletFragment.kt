package com.piesagiving.com.Fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.InflateException
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.piesagiving.com.Activities.*
import com.piesagiving.com.ApiInterface.ApiInterface
import com.piesagiving.com.ApiInterface.DonationHistoryPojoResponse
import com.piesagiving.com.ApiInterface.RegisterUserResponse
import com.piesagiving.com.Helpers.ConnectionDetector
import com.piesagiving.com.Helpers.SessionManager
import com.piesagiving.com.R
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

@SuppressLint("SetTextI18n")
class WalletFragment : Fragment() {

    private lateinit var rootview: View/*? =null*/
    lateinit var sessionManager: SessionManager
    lateinit var cd: ConnectionDetector
    private var isInternetConnected = false
    lateinit var cv_profile: CardView
    lateinit var cv_wallet: CardView
    lateinit var cv_tithecalculator: CardView
    lateinit var cv_recurrindonations: CardView
    lateinit var cv_donationhistory: CardView
    lateinit var cv_notifications: CardView
    lateinit var tv_church_donations_id: TextView
    lateinit var tv_charity_donations_id: TextView
    lateinit var tv_special_donations_id: TextView
    lateinit var tv_total_donations_id: TextView
    lateinit var tv_notifications_title: TextView
    lateinit var tv_year_total: TextView
    private lateinit var user_details: HashMap<String, String>
    private var userId = ""
    private var totalDonation: Double = 0.0
    private var reload = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {

        // creating view for manibabu fragment
        try {
            rootview = inflater.inflate(R.layout.fragment_wallet, container, false)
        } catch (e: InflateException) {
            e.printStackTrace()
        }
        initialize(rootview)

        return rootview
    }

    private fun initialize(view: View?) {
        cd = ConnectionDetector(requireActivity())
        sessionManager = SessionManager(requireActivity())
        isInternetConnected = cd.isConnectingToInternet
        user_details = sessionManager.getUserDetails()
        userId = user_details[SessionManager.ID_KEY]!!

        if (view != null) {
            cv_profile = view.findViewById(R.id.cv_profile)
            cv_wallet = view.findViewById(R.id.cv_wallet)
            cv_tithecalculator = view.findViewById(R.id.cv_tithecalculator)
            cv_recurrindonations = view.findViewById(R.id.cv_recurrindonations)
            cv_donationhistory = view.findViewById(R.id.cv_donationhistory)
            cv_notifications = view.findViewById(R.id.cv_notifications)

            tv_church_donations_id = view.findViewById(R.id.tv_church_donations_id)
            tv_charity_donations_id = view.findViewById(R.id.tv_charity_donations_id)
            tv_special_donations_id = view.findViewById(R.id.tv_special_donations_id)
            tv_total_donations_id = view.findViewById(R.id.tv_total_donations_id)
            tv_year_total = view.findViewById(R.id.tv_year_total)
            tv_notifications_title = view.findViewById(R.id.tv_notifications_title)
        }

        val c: Calendar = Calendar.getInstance()

        // in java month starts from 0 not from 1 so for december 11+1 = 12
        val year = c.get(Calendar.YEAR)
        Log.e("YEAR", year.toString())

        tv_year_total.text = "$year Total"
        TotalDonationHistory(year.toString())

        val obj = JSONObject()
        obj.put("userId", userId)
        val parser = JsonParser()
        val finalObj = parser.parse(obj.toString()) as JsonObject
        getNotificationsCount(finalObj)


        cv_profile.setOnClickListener {
            val intent = Intent(context, GivingProfileActivity::class.java)
            startActivity(intent)
//            activity!!.finish()

        }
        cv_wallet.setOnClickListener {
            val intent = Intent(context, WalletActivity::class.java)
            startActivity(intent)
//            activity!!.finish()
        }
        cv_tithecalculator.setOnClickListener {
            Log.e("TOTALDONATION", totalDonation.toString())
            val intent = Intent(context, TitheCalculatorActivity::class.java)
            intent.putExtra("totalamount", totalDonation.toString())
            startActivity(intent)
            Log.e("TOTALDONATION", totalDonation.toString())
//            activity!!.finish()
        }
        cv_recurrindonations.setOnClickListener {
            val intent = Intent(context, RecurringDonationsActivity::class.java)
            startActivity(intent)
//            activity!!.finish()
        }
        cv_donationhistory.setOnClickListener {
            val intent = Intent(context, DonationHistoryActivity::class.java)
            startActivity(intent)
//            activity!!.finish()
        }

        cv_notifications.setOnClickListener {
            val intent = Intent(context, NotificationsListActivity::class.java)
            startActivity(intent)
        }


        /*  val obj = JSONObject()
          obj.put("userId", sessionManager.getUserDetails().get(SessionManager.ID_KEY.toString()))
          val jParser = JsonParser()
          val jObject = jParser.parse(obj.toString()) as JsonObject
          Log.e("dash_bard_input", jObject.toString())

          WalletDonationsApi(jObject)*/

    }

    /*  private fun WalletDonationsApi(org_id: JsonObject) {

          //  loading_dialog.show()


          val apiInterface = ApiInterface.create()
          val call = apiInterface.WalletTotalDonationsApi(org_id)
          Log.e("@response", call.toString())
          call.enqueue(object : Callback<WalletDonationsPojo> {
              override fun onFailure(call: Call<WalletDonationsPojo>, t: Throwable) {
                  Log.e("responseStatus", "Exception  " + call + "  " + t)

              }

              override fun onResponse(
                  call: Call<WalletDonationsPojo>,
                  response: Response<WalletDonationsPojo>?
              ) {

                  try {
                      if (response!!.body()!!.responseStatus!! == 1) {


                          Log.e("Wallet_Donations"," Chrurch - "+response!!.body()!!.userWalletData!!.churchDonations+" Charity - "+ response!!.body()!!.userWalletData!!.charityDonations +" Special - " + response!!.body()!!.userWalletData!!.specialCauseDonations)

                          val totalcharityDecimal =  String.format("%.2f", response!!.body()!!.userWalletData!!.charityDonations)
                          val totalChurchDecimal =  String.format("%.2f", response!!.body()!!.userWalletData!!.churchDonations)
                          val totalSpecialCauseDecimal =  String.format("%.2f", response!!.body()!!.userWalletData!!.specialCauseDonations)

                          tv_church_donations_id.setText("$"+totalChurchDecimal)
                          tv_charity_donations_id.setText("$"+totalcharityDecimal)
                          tv_special_donations_id.setText("$"+totalSpecialCauseDecimal)

                          Log.e("Wallet_DonationsDecimal"," Chrurch - "+totalChurchDecimal+" Charity - "+ totalcharityDecimal +" Special - " + totalSpecialCauseDecimal)



                          try{
                              val total= totalChurchDecimal.toDouble() + totalcharityDecimal.toDouble() + totalSpecialCauseDecimal.toDouble()
                              Log.e("Total",total.toString())
                              val totalDonationDecimal =  String.format("%.2f", total)
                              Log.e("TotalDecimal",totalDonationDecimal)
                              tv_total_donations_id.setText("$"+totalDonationDecimal)
                          }catch (e:Exception){

                          }



                      } else {
                          *//*Toast.makeText(
                            context,
                            "Donation Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()*//*
                    }
*//*if (loading_dialog.isShowing)
                        loading_dialog.dismiss()*//*


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })



    }*/

    private fun TotalDonationHistory(yearstg: String) {

        val obj = JSONObject()
        obj.put("userId", sessionManager.getUserDetails().get(SessionManager.ID_KEY.toString()))
        obj.put("year", yearstg)
        val jParser = JsonParser()
        val jObject = jParser.parse(obj.toString()) as JsonObject
        Log.e("dash_bard_input", jObject.toString())
        //loading_dialog.show()
        val apiInterface = ApiInterface.create()
        val call = apiInterface.TotalDonationHistory(jObject)
        Log.e("@response", call.toString())
        call.enqueue(object : Callback<DonationHistoryPojoResponse> {
            override fun onFailure(call: Call<DonationHistoryPojoResponse>, t: Throwable) {
                /*if (loading_dialog.isShowing)
                    loading_dialog.dismiss()*/
                Log.e("responseStatus", "Exception  " + call + "  " + t)
            }

            override fun onResponse(
                call: Call<DonationHistoryPojoResponse>,
                response: Response<DonationHistoryPojoResponse>?
            ) {

                /*if (loading_dialog.isShowing)
                    loading_dialog.dismiss()*/
                try {
                    if (response!!.body()!!.responseStatus!! == 1) {
                        val totalCharitySum = response.body()!!.totalCharitySum
                        val totalChurchSum = response.body()!!.totalChurchSum
                        val totalDonationSum = response.body()!!.totalDonationSum
                        val totalSpecialCauseSum = response.body()!!.totalSpecialCauseSum
                        val totalFundraiserSum = response.body()!!.totalFundraiserSum
                        Log.e(
                            "DonationHistiryValues",
                            "Charity - " + totalCharitySum + "Church - " + totalChurchSum + "Total - " + totalDonationSum + "Speciacl - " + totalSpecialCauseSum
                        )

                        val totalcharityDecimal = String.format("%.2f", totalCharitySum)
                        val totalChurchDecimal = String.format("%.2f", totalChurchSum)
                        val totalDonationDecimal = String.format("%.2f", totalDonationSum)
                        val totalSpecialCauseDecimal = String.format("%.2f", totalSpecialCauseSum)
                        val totalFundraiserSumDecimal = String.format("%.2f", totalFundraiserSum)

                        tv_church_donations_id.text = "$${Formatter().format("%,.2f",totalChurchSum)}"
                        tv_charity_donations_id.text = "$${Formatter().format("%,.2f",totalCharitySum)}"
//                        tv_special_donations_id.text = "$${Formatter().format("%,.2f",$$totalSpecialCauseDecimal)}"
                        tv_special_donations_id.text = "$${Formatter().format("%,.2f",totalFundraiserSum)}"

                        try {
                            totalDonation =
                                totalChurchDecimal.toDouble() + totalcharityDecimal.toDouble() + totalFundraiserSumDecimal.toDouble()/*+ totalSpecialCauseDecimal.toDouble()*/
                            Log.e("Total", totalDonation.toString())
                            val totalDonationDecimal = String.format("%.2f", totalDonation)
                            Log.e("TotalDecimal", totalDonationDecimal)
                            tv_total_donations_id.text = "$${Formatter().format("%,.2f", totalDonation)}"
                        } catch (e: Exception) {

                        }

                    } else {
                        Toast.makeText(
                            activity!!,
                            "Project Type Fetching Failed!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    /*if (loading_dialog.isShowing)
                        loading_dialog.dismiss()*/


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })

    }

    private fun getNotificationsCount(jsonObject: JsonObject) {
        val api = ApiInterface.create()
        val call = api.getNotificationCountApi(jsonObject)
        call.enqueue(object : Callback<RegisterUserResponse> {
            override fun onFailure(call: Call<RegisterUserResponse>, t: Throwable) {
                try {
                    Log.d("notification count", "error", t)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onResponse(
                call: Call<RegisterUserResponse>,
                response: Response<RegisterUserResponse>
            ) {
                try {
                    val resp = response.body()!!
                    if (resp.responseStatus == "1") {
                        val count = resp.notifications_count
                        if (count == 0) {
                            tv_notifications_title.text = getString(R.string.notifications)
                        } else {
                            if (count!! > 99) {
                                tv_notifications_title.text =
                                    getString(R.string.notifications) + " (99+)"
                            } else {
                                tv_notifications_title.text =
                                    getString(R.string.notifications) + " ($count)"
                            }
                        }
                    } else if (resp.responseStatus == "0") {
                        tv_notifications_title.text = getString(R.string.notifications)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        Log.d("status", "onResume")
        if (reload){
            val obj = JSONObject()
            obj.put("userId", userId)
            val parser = JsonParser()
            val finalObj = parser.parse(obj.toString()) as JsonObject
            getNotificationsCount(finalObj)
        }
    }

    override fun onStop() {
        super.onStop()
        Log.d("status", "onStop")
        reload = true
    }
}