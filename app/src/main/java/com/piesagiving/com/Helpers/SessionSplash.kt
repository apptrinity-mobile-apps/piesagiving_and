package com.piesagiving.com.Helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class SessionSplash(context: Context) {

    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor
    private var PRIVATE_MODE = 0

    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    fun getSplashScreen(): HashMap<String, String> {
        val account = HashMap<String, String>()
        account[BANK_AC_SKIP] = pref.getString(BANK_AC_SKIP, "")!!
        return account
    }

    fun setSplashScreen(status: String) {
        editor.putString(BANK_AC_SKIP, status)
        editor.commit()
    }



    companion object {
        const val PREF_NAME = "PiesaGiving"
        const val BANK_AC_SKIP = "skip"


    }

}