package com.piesagiving.com.Helpers

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.piesagiving.com.Activities.*
import com.piesagiving.com.R
import org.json.JSONObject


class MyFirebaseMessagingService : FirebaseMessagingService() {
    var dataStorage: DataStorage = DataStorage()

    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Log.e("NEW_TOKEN", s)
        dataStorage.saveFcmToken(applicationContext, s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // ...
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d("TAG", "From: ${remoteMessage.from}")

        // Check if message contains a data payload.
        if (remoteMessage.data.isNotEmpty()) {
            Log.d("TAG", "Message data payload: ${remoteMessage.data}")

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                //scheduleJob()
                sendNotification(remoteMessage, "", "")
            } else {
                // Handle message within 10 seconds
                //handleNow()
                val map = remoteMessage.data
                handleDataMessage(remoteMessage.toString())
            }
        }

        // Check if message contains a notification payload.
        remoteMessage.notification?.let {
            Log.d("TAG", "Message Notification Body: ${it.body}")
            Log.d("TAG", "Message Notification Body: ${it.title}")
            sendNotification(remoteMessage, it.title!!, it.body!!)
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }


    /*override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        if (remoteMessage.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                Log.e("JSON_OBJECT", params.toString())
                val object1 = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", object1.toString())
                //  bitmap = getBitmapFromUrl(object1.getString("image"))
                //val type = object1.getString("type")
                sendNotification(remoteMessage *//*bitmap!!,message*//*)

                try {
                    val map = remoteMessage.data
                    handleDataMessage(map)
                } catch (e: Exception) {
                    Log.e("TAG", "Exception: " + e.message)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }*/


    private fun sendNotification(remoteMessage: RemoteMessage?, title: String, message: String) {
        Log.e("remoteMessage", remoteMessage.toString())
        Log.e("remoteMessage_rrrr", remoteMessage.toString())
        var type = ""
        var id = ""
        var name = ""
        if (remoteMessage!!.data.isNotEmpty()) {
            try {
                val params = remoteMessage.data
                val object1 = JSONObject(params as Map<*, *>)
                Log.e("JSON_OBJECT", object1.toString())
                Log.e("JSON_else", object1.toString())
                type = object1.getString("type")
                if (type != "donation") {
                    id = object1.getString("id")
                    name = object1.getString("name")
                }
                Log.e("TYPEANDID", type + "-----" + id)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationId = 1
        val channelId = getString(R.string.default_notification_channel_id)
        val channelName = getString(R.string.default_notification_channel_name)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(channelId, channelName, importance)
            notificationManager.createNotificationChannel(mChannel)
        }

        val sound: Uri =
            Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + applicationContext.packageName + "/" + RingtoneManager.TYPE_NOTIFICATION)
        val mBuilder = NotificationCompat.Builder(this, channelId)
            .setContentTitle(title)
            .setContentText(message)
            .setSound(sound)
            .setAutoCancel(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.ic_stat_group_1588);
            //mBuilder.setColor(getResources().getColor(R.color.white));
        } else {
            mBuilder.setSmallIcon(R.drawable.app_icon);
        }

        when (type) {
            "project" -> {
                val resultIntent = Intent(this, ProjectDetailedViewActivity::class.java)
                resultIntent.putExtra("project_id", id)
                resultIntent.putExtra("project_name", type)
                resultIntent.putExtra("from_screen", "project")
                resultIntent.putExtra("organiz_id", "")
                resultIntent.putExtra("display_name", "")
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
            "special_cause" -> {
                val resultIntent = Intent(this, ProjectDetailedViewActivity::class.java)
                resultIntent.putExtra("project_id", id)
                resultIntent.putExtra("project_name", type)
                resultIntent.putExtra("from_screen", "special_cause")
                resultIntent.putExtra("organiz_id", "")
                resultIntent.putExtra("display_name", "")
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
            "follow" -> {
                val resultIntent = Intent(this, DashBoardActivity::class.java)
                // resultIntent.putExtra("id",id)
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
            "friend_request" -> {
                val resultIntent = Intent(this, SearchPeopleActivity::class.java)
                /* resultIntent.putExtra("from_screen","request")
                 resultIntent.putExtra("privacy", "")
                 resultIntent.putExtra("firstName", "")
                 resultIntent.putExtra("lastName", "")
                 resultIntent.putExtra("profilePic", "")
                 resultIntent.putExtra("id", "")
                 resultIntent.putExtra("userName", "")
                 resultIntent.putExtra("notificationId", "")*/
                resultIntent.putExtra("from_screen", "request")
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
            "donation" -> {
                val resultIntent = Intent(this, DonationHistoryActivity::class.java)
                // resultIntent.putExtra("id",id)
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
            "announcement" -> {
                val resultIntent = Intent(this, ChurchProfileActivity::class.java)
                resultIntent.putExtra("reco_organizationId", id)
                resultIntent.putExtra("from_screen", "announcement")
                resultIntent.putExtra("display_name", name)
                resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                val stackBuilder = TaskStackBuilder.create(this)
                stackBuilder.addNextIntent(resultIntent)
                val resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
                mBuilder.setContentIntent(resultPendingIntent)
                notificationManager.notify(notificationId, mBuilder.build())
            }
        }
    }

    private fun handleDataMessage(map: String) {
        Log.e("TAG", "push json: " + map.toString())
        try {
            //val type = map["type"]
            // val alert = map["alert"]
        } catch (e: Exception) {
            Log.e("TAG", "Exception: " + e.message)
        }


    }
}