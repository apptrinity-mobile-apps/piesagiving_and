package com.piesagiving.com.Helpers

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

@SuppressLint("CommitPrefEdits")
class SessionManager(context: Context) {

    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor
    private var PRIVATE_MODE = 0

    init {
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGGED_IN, false)

    fun loginSession(
        firstName: String,
        lastName: String,
        email: String,
        id: String,
        state: String,
        country: String,
        city: String,
        street: String,
        zip: String,
        privacy: String,
        terms: String,
        birthday: String,
        faceid: String,
        geotag: String,
        language: String,
        profilePic: String,
        userName: String,
        appPin: String
    ) {


        editor.putBoolean(IS_LOGGED_IN, true)
        editor.putString(FIRST_NAME_KEY, firstName)
        editor.putString(LAST_NAME_KEY, lastName)
        editor.putString(EMAIL_KEY, email)
        editor.putString(ID_KEY, id)
        editor.putString(STATE_KEY, state)
        editor.putString(COUNTRY_KEY, country)
        editor.putString(CITY_KEY, city)
        editor.putString(STREET_ADDRESS_KEY, street)
        editor.putString(ZIP_KEY, zip)
        editor.putString(PRIVACY_KEY, privacy)
        editor.putString(TERMS_KEY, terms)
        editor.putString(BIRTHDAY_KEY, birthday)
        editor.putString(FACEID_KEY, faceid)
        editor.putString(GEOTAG_KEY, geotag)
        editor.putString(LANGUAGE_KEY, language)
        editor.putString(PROFILEPIC_KEY, profilePic)
        editor.putString(USERNAME_KEY, userName)
        editor.putString(PASSCODE_KEY, appPin)

        editor.commit()
    }

    fun notificationSession(
        recurringdonation: String,
        newpiesarecommend: String,
        trendingnow: String,
        someonefollowed: String,
        haptics: String,
        newProjectChurch: String,
        newProjectCharity: String,
        newSpecialCausesChurch: String,
        newSpecialCausesCharity: String,
        DonationProcessedEmail: String,
        DonationProcessedPhone: String,
        DonationProcessingEmail: String,
        DonationProcessingPhone: String

    ) {


        editor.putString(NOTIFY_RECURRINDONATION_KEY, recurringdonation)
        editor.putString(NOTIFY_NEWPIESARECOMMEND_KEY, newpiesarecommend)
        editor.putString(NOTIFY_TRENDINGNOW_KEY, trendingnow)
        editor.putString(NOTIFY_SOMEONEFOLLOWED_KEY, someonefollowed)
        editor.putString(NOTIFY_HAPTICS_KEY, haptics)
        editor.putString(NOTIFY_NEWPROJECTCHURCH_KEY, newProjectChurch)
        editor.putString(NOTIFY_NEWPROJECTCHARITY_KEY, newProjectCharity)
        editor.putString(NOTIFY_NEWSPECIALCAUSECHURCH_KEY, newSpecialCausesChurch)
        editor.putString(NOTIFY_NEWSPECIALCAUSECHARITY_KEY, newSpecialCausesCharity)
        editor.putString(NOTIFY_DONATIONPROCESSEDEMAIL_KEY, DonationProcessedEmail)
        editor.putString(NOTIFY_DONATIONPROCESSEDPHONE_KEY, DonationProcessedPhone)
        editor.putString(NOTIFY_DONATIONPROCESSINGEMAIL_KEY, DonationProcessingEmail)
        editor.putString(NOTIFY_DONATIONPROCESSINGPHONE_KEY, DonationProcessingPhone)
        editor.commit()

    }

    val getUserNotifications: HashMap<String, String>
        get() {
            val usernotify = HashMap<String, String>()

            usernotify[NOTIFY_RECURRINDONATION_KEY] =
                pref.getString(NOTIFY_RECURRINDONATION_KEY, "0")!!
            usernotify[NOTIFY_NEWPIESARECOMMEND_KEY] =
                pref.getString(NOTIFY_NEWPIESARECOMMEND_KEY, "0")!!
            usernotify[NOTIFY_TRENDINGNOW_KEY] = pref.getString(NOTIFY_TRENDINGNOW_KEY, "0")!!
            usernotify[NOTIFY_SOMEONEFOLLOWED_KEY] =
                pref.getString(NOTIFY_SOMEONEFOLLOWED_KEY, "0")!!
            usernotify[NOTIFY_HAPTICS_KEY] = pref.getString(NOTIFY_HAPTICS_KEY, "0")!!
            usernotify[NOTIFY_NEWPROJECTCHURCH_KEY] =
                pref.getString(NOTIFY_NEWPROJECTCHURCH_KEY, "0")!!
            usernotify[NOTIFY_NEWPROJECTCHARITY_KEY] =
                pref.getString(NOTIFY_NEWPROJECTCHARITY_KEY, "0")!!
            usernotify[NOTIFY_NEWSPECIALCAUSECHURCH_KEY] =
                pref.getString(NOTIFY_NEWSPECIALCAUSECHURCH_KEY, "0")!!
            usernotify[NOTIFY_NEWSPECIALCAUSECHARITY_KEY] =
                pref.getString(NOTIFY_NEWSPECIALCAUSECHARITY_KEY, "0")!!
            usernotify[NOTIFY_DONATIONCONFIRMATIONEMAIL_KEY] =
                pref.getString(NOTIFY_DONATIONCONFIRMATIONEMAIL_KEY, "0")!!
            usernotify[NOTIFY_UPCOMINGDONATIONEMAIL_KEY] =
                pref.getString(NOTIFY_UPCOMINGDONATIONEMAIL_KEY, "0")!!
            usernotify[NOTIFY_DONATIONPROCESSEDEMAIL_KEY] =
                pref.getString(NOTIFY_DONATIONPROCESSEDEMAIL_KEY, "0")!!
            usernotify[NOTIFY_DONATIONPROCESSEDPHONE_KEY] =
                pref.getString(NOTIFY_DONATIONPROCESSEDPHONE_KEY, "0")!!
            usernotify[NOTIFY_DONATIONPROCESSINGEMAIL_KEY] =
                pref.getString(NOTIFY_DONATIONPROCESSINGEMAIL_KEY, "0")!!
            usernotify[NOTIFY_DONATIONPROCESSINGPHONE_KEY] =
                pref.getString(NOTIFY_DONATIONPROCESSINGPHONE_KEY, "0")!!
            usernotify[NOTIFY_UPCOMINGDONATIONPHONE_KEY] =
                pref.getString(NOTIFY_UPCOMINGDONATIONPHONE_KEY, "0")!!
            usernotify[NOTIFY_DONATIONCONFIRMATIONPHONE_KEY] =
                pref.getString(NOTIFY_DONATIONCONFIRMATIONPHONE_KEY, "0")!!

            return usernotify

        }

    fun getUserDetails(): HashMap<String, String> {
        val user = HashMap<String, String>()

        user[FIRST_NAME_KEY] = pref.getString(FIRST_NAME_KEY, "")!!
        user[LAST_NAME_KEY] = pref.getString(LAST_NAME_KEY, "")!!
        user[EMAIL_KEY] = pref.getString(EMAIL_KEY, "")!!
        user[ID_KEY] = pref.getString(ID_KEY, "")!!
        user[STATE_KEY] = pref.getString(STATE_KEY, "")!!
        user[COUNTRY_KEY] = pref.getString(COUNTRY_KEY, "")!!
        user[CITY_KEY] = pref.getString(CITY_KEY, "")!!
        user[STREET_ADDRESS_KEY] = pref.getString(STREET_ADDRESS_KEY, "")!!
        user[ZIP_KEY] = pref.getString(ZIP_KEY, "")!!
        user[PRIVACY_KEY] = pref.getString(PRIVACY_KEY, "")!!
        user[TERMS_KEY] = pref.getString(TERMS_KEY, "")!!
        user[BIRTHDAY_KEY] = pref.getString(BIRTHDAY_KEY, "")!!
        user[FACEID_KEY] = pref.getString(FACEID_KEY, "")!!
        user[GEOTAG_KEY] = pref.getString(GEOTAG_KEY, "")!!
        user[LANGUAGE_KEY] = pref.getString(LANGUAGE_KEY, "")!!
        user[PROFILEPIC_KEY] = pref.getString(PROFILEPIC_KEY, "")!!
        user[USERNAME_KEY] = pref.getString(USERNAME_KEY, "")!!
        user[PASSCODE_KEY] = pref.getString(PASSCODE_KEY, "")!!

        return user
    }

    fun getBankAccountSkip(): HashMap<String, String> {
        val account = HashMap<String, String>()
        account[BANK_AC_SKIP] = pref.getString(BANK_AC_SKIP, "")!!
        return account
    }

    fun profileUpdate(status: String) {
        editor.putString(BANK_AC_SKIP, status)
        editor.commit()
    }

    fun updateProfilePic(image: String) {
        editor.putString(PROFILEPIC_KEY, image)
        editor.commit()
    }

    fun getProfilePic(): HashMap<String, String> {
        val user_pic = HashMap<String, String>()
        user_pic[PROFILEPIC_KEY] = pref.getString(PROFILEPIC_KEY, "")!!


        return user_pic
    }


    fun logout() {
        editor.clear()
        editor.commit()
    }

    fun updateBiometrics(biometricStatus: Int) {
        editor.putString(FACEID_KEY, biometricStatus.toString())
        editor.commit()
    }

    fun updatePrivacy(privacy: String) {
        editor.putString(PRIVACY_KEY, privacy)
        editor.commit()
    }

    fun updateEmail(email: String) {
        editor.putString(EMAIL_KEY, email)
        editor.commit()
    }

    fun updatePasscode(passcode: String) {
        editor.putString(PASSCODE_KEY, passcode)
        editor.commit()
    }

    fun userProfileUpdate(
        firstName: String,
        lastName: String,
        email: String,
        id: String,
        state: String,
        country: String,
        city: String,
        street: String,
        zip: String,
        terms: String,
        birthday: String,
        faceid: String,
        geotag: String,
        language: String,
        cityId: String,
        countryId: String,
        stateId: String,
        profilePic: String,
        userName: String
    ) {
        editor.putString(FIRST_NAME_KEY, firstName)
        editor.putString(LAST_NAME_KEY, lastName)
        editor.putString(EMAIL_KEY, email)
        editor.putString(ID_KEY, id)
        editor.putString(STATE_KEY, state)
        editor.putString(COUNTRY_KEY, country)
        editor.putString(CITY_KEY, city)
        editor.putString(STREET_ADDRESS_KEY, street)
        editor.putString(ZIP_KEY, zip)
        editor.putString(TERMS_KEY, terms)
        editor.putString(BIRTHDAY_KEY, birthday)
        editor.putString(FACEID_KEY, faceid)
        editor.putString(GEOTAG_KEY, geotag)
        editor.putString(LANGUAGE_KEY, language)
        editor.putString(CITY_ID_KEY, cityId)
        editor.putString(STATE_ID_KEY, stateId)
        editor.putString(COUNTRY_ID_KEY, countryId)
        editor.putString(PROFILEPIC_KEY, profilePic)
        editor.putString(USERNAME_KEY, userName)

        editor.commit()
    }

    companion object {
        const val PREF_NAME = "PiesaGiving"
        const val IS_LOGGED_IN = "isLoggedIn"
        const val KEY_EMAIL = "email"
        const val KEY_USER_NAME = "user_name"
        const val KEY_DEVICE_NAME = "device_name"

        const val FIRST_NAME_KEY = "first_name"
        const val LAST_NAME_KEY = "last_name"
        const val EMAIL_KEY = "email"
        const val ID_KEY = "user_id"
        const val STATE_KEY = "state"
        const val COUNTRY_KEY = "country"
        const val CITY_KEY = "city"
        const val STATE_ID_KEY = "state_id"
        const val COUNTRY_ID_KEY = "country_id"
        const val CITY_ID_KEY = "city_id"
        const val STREET_ADDRESS_KEY = "street"
        const val ZIP_KEY = "zip"
        const val PRIVACY_KEY = "privacy"
        const val TERMS_KEY = "terms"
        const val BIRTHDAY_KEY = "birthday"
        const val FACEID_KEY = "faceId"
        const val GEOTAG_KEY = "geotagging"
        const val LANGUAGE_KEY = "language"
        const val PROFILEPIC_KEY = "profilePic"
        const val USERNAME_KEY = "userName"
        const val PASSCODE_KEY = "passcode"


        const val NOTIFY_RECURRINDONATION_KEY = "notifyrecurring"
        const val NOTIFY_NEWPIESARECOMMEND_KEY = "notifypiesarecommend"
        const val NOTIFY_TRENDINGNOW_KEY = "notifytrendingnow"
        const val NOTIFY_SOMEONEFOLLOWED_KEY = "notifysomeonefollowed"
        const val NOTIFY_HAPTICS_KEY = "notifyhaptics"
        const val NOTIFY_NEWPROJECTCHURCH_KEY = "notifynewprojectchurch"
        const val NOTIFY_NEWPROJECTCHARITY_KEY = "notifynewprojectcharity"
        const val NOTIFY_NEWSPECIALCAUSECHURCH_KEY = "notifynewsplchurch"
        const val NOTIFY_NEWSPECIALCAUSECHARITY_KEY = "notifynewsplcharity"
        const val NOTIFY_DONATIONCONFIRMATIONEMAIL_KEY = "notifydonationconfemail"
        const val NOTIFY_UPCOMINGDONATIONEMAIL_KEY = "notifyupocomingdonation"
        const val NOTIFY_DONATIONPROCESSEDEMAIL_KEY = "notifydonationprocessemail"
        const val NOTIFY_DONATIONPROCESSEDPHONE_KEY = "notifydonationprocessphone"
        const val NOTIFY_DONATIONPROCESSINGEMAIL_KEY = "notifydonationprocessingemail"
        const val NOTIFY_DONATIONPROCESSINGPHONE_KEY = "notifydonationprocessingphone"
        const val NOTIFY_UPCOMINGDONATIONPHONE_KEY = "notifyupocomingdonationphone"
        const val NOTIFY_DONATIONCONFIRMATIONPHONE_KEY = "notifydonationconfphone"

        const val BANK_AC_SKIP = "skip_dialog"


    }

}