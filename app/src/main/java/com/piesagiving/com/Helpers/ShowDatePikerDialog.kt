package com.piesagiving.com.Helpers

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.widget.EditText
import java.text.SimpleDateFormat
import java.util.*

class ShowDatePikerDialog {
    var mDay = 0
    var mMonth = 0
    var mYear = 0
    lateinit var mDate: Calendar

    fun showPiker(
        context: Context, /*isStartFromToday: Boolean,*/
        date: String,
        editText: EditText
    ) {
        val calendar = Calendar.getInstance()
        if (date != "") {
            val sdf = SimpleDateFormat("MM-dd-yyyy", Locale.getDefault())
            val newDate = sdf.parse(date)!!
            calendar.time = newDate
        }
        mDay = calendar[Calendar.DAY_OF_MONTH]
        mMonth = calendar[Calendar.MONTH]
        mYear = calendar[Calendar.YEAR]
        mDate = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(
            context,
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                mDate.set(year, monthOfYear, dayOfMonth)
                val mFormat = "MM-dd-yyyy"
                val sdf = SimpleDateFormat(mFormat, Locale.getDefault())
                val date = sdf.format(mDate.time)
                editText.setText(date)
            },
            mYear, mMonth, mDay
        )
        /*if (isStartFromToday) {
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000
        }*/
        datePickerDialog.show()
    }

}