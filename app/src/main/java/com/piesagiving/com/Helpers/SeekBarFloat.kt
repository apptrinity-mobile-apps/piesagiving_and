package com.piesagiving.com.Helpers

import android.annotation.TargetApi
import android.content.Context
import android.content.res.TypedArray
import android.os.Build
import android.util.AttributeSet
import android.widget.SeekBar
import androidx.appcompat.widget.AppCompatSeekBar
import com.piesagiving.com.R


class SeekBarFloat : AppCompatSeekBar {
    private val mAccuracy = 10000f
    private var mMin = 0f
    private var mMax = 100f

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initValue(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initValue(context, attrs)
    }

    private fun initValue(context: Context, attrs: AttributeSet) {
        val tArray: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.seekBarFloat)
        val min = tArray.getFloat(R.styleable.seekBarFloat_seekBarFloat_min, 0f)
        val max = tArray.getFloat(R.styleable.seekBarFloat_seekBarFloat_max, 100f)
        var default_value =
            tArray.getFloat(R.styleable.seekBarFloat_seekBarFloat_default_value, 0f)
        tArray.recycle()
        setRangeF(min, max)
        if (default_value < mMin || default_value > mMax) {
            default_value = mMin
        }
        setProgressF(default_value)
    }

    private var mOnSeekBarChangeListenerEx: OnSeekBarChangeListener? = null
    var mOnSeekBarChangeListener: SeekBar.OnSeekBarChangeListener? = null

    /**
     * don't use it, use seekBarFloat#setSecondaryProgressF instead
     * Sets the seekBar's secondary progress value
     */
    @Synchronized
    override fun setSecondaryProgress(secondaryProgress: Int) {
        super.setSecondaryProgress(secondaryProgress)
    }

    /**
     * Sets the seekBar's secondary progress value
     * @param secondaryProgress the new secondary progress, between 0 and [.getMax]
     * can be negative number and float number
     */
    @Synchronized
    fun setSecondaryProgressF(secondaryProgress: Float) {
        val coverProgress = Math.abs(((secondaryProgress - mMin) * mAccuracy).toInt())
        super.setSecondaryProgress(coverProgress)
    }

    /**
     * get the seekBar's secondary progress value
     * @return the secondary progress, between [.getMinF] ()} and [.getMaxF]
     * can be negative number and float number
     */
    @Synchronized
    fun getSecondaryProgressF(): Float {
        val progress = super.getSecondaryProgress()
        return (if (mMax - mMin == 0f || super.getMax() == 0) 0 else progress.toFloat() / super.getMax().toFloat() * (mMax - mMin) + mMin) as Float
    }

    /**
     * Don't use it, use [SeekBarFloat.getMaxF] instead
     * you cann't get right value if you call it
     */
    @Synchronized
    override fun getMax(): Int {
        return super.getMax()
    }

    /**
     * Don't use it, use [SeekBarFloat.getSecondaryProgressF] instead
     * get the seekBar's secondary progress value
     */
    @Synchronized
    override fun getSecondaryProgress(): Int {
        return super.getSecondaryProgress()
    }

    /**
     * Don't use it, use [SeekBarFloat.setProgressF] instead
     * Sets the current progress to the specified value, optionally animating
     * the visual position between the current and target values.
     *
     *
     * @param progress the new progress value, between [.getMinF] and [.getMaxF]
     */
    @Synchronized
    override fun setProgress(progress: Int) {
        super.setProgress(progress)
    }

    /**
     * see [SeekBarFloat.setProgressF]
     * Sets the current progress to the specified value, optionally animating
     * the visual position between the current and target values.
     *
     *
     * @param progress the new progress value, between [.getMinF] and [.getMaxF]
     * can be negative number and float number
     */
    @Synchronized
    fun setProgressF(progress: Float) {
        val coverProgress = Math.abs(((progress - mMin) * mAccuracy).toInt())
        super.setProgress(coverProgress)
    }

    /**
     * Don't use it, use [SeekBarFloat.setProgressF] instead
     * Sets the current progress to the specified value, optionally animating
     * the visual position between the current and target values.
     *
     *
     * @param progress the new progress value, between [.getMinF] and [.getMaxF]
     * can be negative number and float number
     * @param animate `true` to animate between the current and target
     * values or `false` to not animate
     */
    override fun setProgress(progress: Int, animate: Boolean) {
        super.setProgress(progress, animate)
    }

    /**
     * Sets the current progress to the specified value, optionally animating
     * the visual position between the current and target values.
     *
     *
     * Animation does not affect the result of [.getProgress], which
     * will return the target value immediately after this method is called.
     *
     * @param progress the new progress value, between [.getMinF] and [.getMaxF]
     * can be negative number and float number
     * @param animate `true` to animate between the current and target
     * values or `false` to not animate
     */
    @TargetApi(Build.VERSION_CODES.N)
    fun setProgressF(progress: Float, animate: Boolean) {
        val coverProgress = Math.abs(((progress - mMin) * mAccuracy).toInt())
        super.setProgress(coverProgress, animate)
    }

    /**
     * Don't use it, use [SeekBarFloat.getProgressF] instead
     */
    @Synchronized
    override fun getProgress(): Int {
        return super.getProgress()
    }

    /**
     *
     * Get the progress bar's current level of progress
     *
     * @return the current progress, between [.getMinF] ()} and [.getMaxF]
     * can be negative number and float number
     */
    @Synchronized
    fun getProgressF(): Float {
        val progress = super.getProgress()
        return (if (mMax - mMin == 0f || super.getMax() == 0) 0 else progress.toFloat() / super.getMax().toFloat() * (mMax - mMin) + mMin) as Float
    }

    /**
     * get the seekBar's max value
     * @return can be negative number and float number
     */
    @Synchronized
    fun getMaxF(): Float {
        return mMax
    }

    /**
     * get the seekBar's min value
     * @return can be negative number and float number
     */
    @Synchronized
    fun getMinF(): Float {
        return mMin
    }

    /**
     * Don't use it, use [SeekBarFloat.setMaxF] instead
     * Sets the seekBar's max value
     * @param max
     * will auto correct max & min if max < min
     */
    @Synchronized
    override fun setMax(max: Int) {
        super.setMax(max)
    }

    /**
     * Sets the seekBar's max value
     * @param max max can be negative number and float number
     * will auto correct max & min if max < min
     */
    @Synchronized
    fun setMaxF(max: Float) {
        setRangeF(mMin, max)
    }

    /**
     * Sets the seekBar's min value
     * @param min min can be negative number and float number
     * will auto correct max & min if max < min
     */
    @Synchronized
    fun setMinF(min: Float) {
        setRangeF(min, mMax)
    }

    /**
     * Sets the seekBar's min value and max value
     * @param min min can be negative number and float number
     * @param max min can be negative number and float number
     * will auto correct max & min if max < min
     */
    @Synchronized
    fun setRangeF(min: Float, max: Float) {
        mMin = Math.min(min, max)
        mMax = Math.max(min, max)
        val range = Math.abs(((mMax - mMin) * mAccuracy).toInt())
        super.setMax(range)
    }

    /**
     * Sets a listener to receive notifications of changes to the SeekBar's progress level. Also
     * provides notifications of when the user starts and stops a touch gesture within the SeekBar.
     *
     * @param l The seek bar notification listener
     *
     * @see SeekBarFloat.OnSeekBarChangeListener
     */
    fun setOnSeekBarChangeListener(l: OnSeekBarChangeListener?) {
        mOnSeekBarChangeListenerEx = l
        mOnSeekBarChangeListener = object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(
                seekBar: SeekBar,
                progress: Int,
                fromUser: Boolean
            ) {
                if (mOnSeekBarChangeListenerEx != null) {
                    mOnSeekBarChangeListenerEx!!.onProgressChanged(
                        this@SeekBarFloat,
                        getProgressF(),
                        fromUser
                    )
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                if (mOnSeekBarChangeListenerEx != null) {
                    mOnSeekBarChangeListenerEx!!.onStartTrackingTouch(this@SeekBarFloat)
                }
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                if (mOnSeekBarChangeListenerEx != null) {
                    mOnSeekBarChangeListenerEx!!.onStopTrackingTouch(this@SeekBarFloat)
                }
            }
        }
        setOnSeekBarChangeListener(mOnSeekBarChangeListener)
    }

    interface OnSeekBarChangeListener {
        /**
         * Notification that the progress level has changed. Clients can use the fromUser parameter
         * to distinguish user-initiated changes from those that occurred programmatically.
         *
         * @param seekBarFloat The seekBarFloat whose progress has changed
         * @param progress The current progress value, can be float or negative.
         * @param fromUser True if the progress change was initiated by the user.
         */
        fun onProgressChanged(
            seekBarFloat: SeekBarFloat?,
            progress: Float,
            fromUser: Boolean
        )

        /**
         * Notification that the user has started a touch gesture. Clients may want to use this
         * to disable advancing the seekBarFloat.
         * @param seekBarFloat The SeekBar in which the touch gesture began
         */
        fun onStartTrackingTouch(seekBarFloat: SeekBarFloat?)

        /**
         * Notification that the user has finished a touch gesture. Clients may want to use this
         * to re-enable advancing the seekbar.
         * @param seekBarFloat The seekBarFloat in which the touch gesture began
         */
        fun onStopTrackingTouch(seekBarFloat: SeekBarFloat?)
    }
}